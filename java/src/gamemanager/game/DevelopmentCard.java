package gamemanager.game;

import shared.definitions.DevCardType;
import cs340.model.player.Player;

public class DevelopmentCard {

	/**
	 * The Player that has the card.
	 */
	private Player owner;
	/**
	 * Each DevelopmentCard has a type.  
	 * SOLDIER, YEAR_OF_PLENTY, MONOPOLY, ROAD_BUILD, MONUMENT
	 */
	private DevCardType type;
	/**
	 * Is the card playable or not.
	 */
	private boolean isPlayable;
	
	/**
	 * Creates a new DevelopmentCard. Owner is set to null and isPlayabel is set to false.
	 * @param type The type of card that is to be created.
	 */
	public DevelopmentCard(DevCardType type){
		
	}

	public Player getOwner() {
		return owner;
	}

	public void setOwner(Player owner) {
		this.owner = owner;
	}

	public DevCardType getType() {
		return type;
	}

	public void setType(DevCardType type) {
		this.type = type;
	}

	public boolean isPlayable() {
		return isPlayable;
	}

	public void setPlayable(boolean isPlayable) {
		this.isPlayable = isPlayable;
	}
}
