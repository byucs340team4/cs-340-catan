package gamemanager.game;

import com.google.gson.JsonObject;

import shared.definitions.DevCardType;

public class DevCardList {

	private int monopoly;
	private int monument;
	private int roadBuilding;
	private int soldier;
	private int yearOfPlenty;
	
	public int getMonopoly() {
		return monopoly;
	}
	public void setMonopoly(int monopoly) {
		this.monopoly = monopoly;
	}
	public int getMonument() {
		return monument;
	}
	public void setMonument(int monument) {
		this.monument = monument;
	}
	public int getRoadBuilding() {
		return roadBuilding;
	}
	public void setRoadBuilding(int roadBuilding) {
		this.roadBuilding = roadBuilding;
	}
	public int getSoldier() {
		return soldier;
	}
	public void setSoldier(int soldier) {
		this.soldier = soldier;
	}
	public int getYearOfPlenty() {
		return yearOfPlenty;
	}
	public void setYearOfPlenty(int yearOfPlenty) {
		this.yearOfPlenty = yearOfPlenty;
	}
	public int getDevCard(DevCardType type){
		//SOLDIER, YEAR_OF_PLENTY, MONOPOLY, ROAD_BUILD, MONUMENT
		switch(type){
		case SOLDIER: return this.soldier;
		case YEAR_OF_PLENTY: return this.yearOfPlenty;
		case MONOPOLY: return this.monopoly;
		case ROAD_BUILD: return this.roadBuilding;
		case MONUMENT: return this.monument;
		default: return 0;
		}
	}
	public void setDevCard(DevCardType type, int amount){
		switch(type){
		case SOLDIER: this.soldier = amount;
		case YEAR_OF_PLENTY: this.yearOfPlenty = amount;
		case MONOPOLY: this.monopoly = amount;
		case ROAD_BUILD: this.roadBuilding = amount;
		case MONUMENT: this.monument = amount;
		default: return;
		}
	}
	public void add(DevCardList newDevCards) {
		// TODO Auto-generated method stub
		monopoly+=newDevCards.monopoly;
		monument+=newDevCards.monument;
		soldier+=newDevCards.soldier;
		roadBuilding+=newDevCards.roadBuilding;
		yearOfPlenty+=newDevCards.yearOfPlenty;
	}
	public void empty() {
		// TODO Auto-generated method stub
		monopoly = 0;
		monument = 0;
		soldier = 0;
		roadBuilding = 0;
		yearOfPlenty = 0;
	}
	public void playMonopoly() {
		// TODO Auto-generated method stub
		monopoly--;
	}
	public JsonObject toJson(){
		JsonObject result = new JsonObject();
//		for(DevCardType type: DevCardType.values()){
//			result.addProperty(type.getValue(), this.getDevCard(type));
//		}
		return result;
	}
	public int total() {
		// TODO Auto-generated method stub
		return monopoly+monument+soldier+roadBuilding+yearOfPlenty;
	}
}
