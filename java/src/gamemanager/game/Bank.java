package gamemanager.game;

import com.google.gson.JsonObject;

import shared.definitions.ResourceType;
import gamemanager.player.ResourceList;

public class Bank extends ResourceList {

	public Bank(){
		this.brick = ResourceList.MaxResource;
		this.ore = ResourceList.MaxResource;
		this.sheep = ResourceList.MaxResource;
		this.wheat = ResourceList.MaxResource;
		this.wood = ResourceList.MaxResource;
	}
	
	public Bank(ResourceList bank) {
		// TODO Auto-generated constructor stub
		this.brick = bank.getBrick();
		this.ore = bank.getOre();
		this.sheep = bank.getSheep();
		this.wheat = bank.getWheat();
		this.wood = bank.getWood();
	}

	public void buyDevCard(){
		ore++;
		wheat++;
		sheep++;
	}
	
	public void buyRoad(boolean free){
		if(!free){
			brick++;
			wood++;
		}
	}
	
	public void buySettlement(boolean free){
		if(!free){
			brick++;
			wood++;
			wheat++;
			sheep++;
		}
	}
	
	public void buyCity(){
		wheat+=2;
		ore+=3;
	}
	
	public void YoP(ResourceType type1, ResourceType type2){
		this.gainResource(type1, -1);
		this.gainResource(type2, -1);
	}
	
	public void discard(ResourceList list){
		ore+=list.getOre();
		brick+=list.getBrick();
		wheat+=list.getWheat();
		wood+=list.getWood();
		sheep+=list.getSheep();
	}

	public void distribute(ResourceList list) {
		// TODO Auto-generated method stub
		ore-=list.getOre();
		brick-=list.getBrick();
		wheat-=list.getWheat();
		wood-=list.getWood();
		sheep-=list.getSheep();
	}
	
	public JsonObject toJson(){
		JsonObject result = new JsonObject();
		for(ResourceType type: ResourceType.values()){
			result.addProperty(type.getValue(), this.getResource(type));
		}
		return result;
	}
}
