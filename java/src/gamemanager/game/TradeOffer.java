package gamemanager.game;

import shared.proxy.request.OfferTrade;
import gamemanager.player.ResourceList;

public class TradeOffer {
	int sender;
	int receiver;
	ResourceList trade;
	
	public TradeOffer(int sender, int receiver, ResourceList trade) {
		this.sender = sender;
		this.receiver = receiver;
		this. trade = trade;
	}
	
	public TradeOffer(OfferTrade trade){
		this.sender = trade.getPlayerIndex();
		this.receiver = trade.getReceiver();
		this.trade = trade.getOffer();
	}
	
	public TradeOffer(){
		trade = new ResourceList();
	}

	public int getSender() {
		return sender;
	}

	public void setSender(int sender) {
		this.sender = sender;
	}

	public int getReceiver() {
		return receiver;
	}

	public void setReceiver(int receiver) {
		this.receiver = receiver;
	}

	public ResourceList getOffer() {
		return trade;
	}

	public void setOffer(ResourceList trade) {
		this.trade = trade;
	}
	
	
}
