package gamemanager.game;

import shared.definitions.ResourceType;
import gamemanager.player.Player;
import gamemanager.player.ResourceList;

public class CardDistributor {
		
	public static void distributeCards(ResourceList totalList, ResourceList[] toDistribute, ResourceList bank, Player[] players){
		distributeBrick(totalList.getBrick(),toDistribute,bank.getBrick(), players,bank);
		distributeOre(totalList.getOre(),toDistribute,bank.getOre(),players,bank);
		distributeWheat(totalList.getWheat(),toDistribute,bank.getWheat(),players,bank);
		distributeWood(totalList.getWood(),toDistribute,bank.getWood(),players,bank);
		distributeSheep(totalList.getSheep(),toDistribute,bank.getSheep(),players,bank);
	}

	private static void distributeOre(int ore, ResourceList[] toDistribute,
			int inBank, Player[] players, ResourceList bank) {
		// TODO Auto-generated method stub
		if(ore<=inBank){
			bank.setOre(inBank-ore);
			for(int i = 0;i<toDistribute.length;i++){
				Player currPlayer = players[i];
				int quantity = toDistribute[i].getOre();
				currPlayer.gainResource(ResourceType.ORE, quantity);
			}
		}	
	}

	private static void distributeBrick(int brick, ResourceList[] toDistribute,
			int inBank, Player[] players, ResourceList bank) {
		// TODO Auto-generated method stub
		if(brick<=inBank){
			bank.setBrick(inBank-brick);
			for(int i = 0;i<toDistribute.length;i++){
				Player currPlayer = players[i];
				int quantity = toDistribute[i].getBrick();
				currPlayer.gainResource(ResourceType.BRICK, quantity);
			}
		}	
	}
	
	private static void distributeSheep(int sheep, ResourceList[] toDistribute,
			int inBank, Player[] players, ResourceList bank) {
		// TODO Auto-generated method stub
		if(sheep<=inBank){
			bank.setSheep(inBank-sheep);
			for(int i = 0;i<toDistribute.length;i++){
				Player currPlayer = players[i];
				int quantity = toDistribute[i].getSheep();
				currPlayer.gainResource(ResourceType.SHEEP, quantity);
			}
		}	
	}
	
	private static void distributeWheat(int wheat, ResourceList[] toDistribute,
			int inBank, Player[] players, ResourceList bank) {
		// TODO Auto-generated method stub
		if(wheat<=inBank){
			bank.setWheat(inBank-wheat);
			for(int i = 0;i<toDistribute.length;i++){
				Player currPlayer = players[i];
				int quantity = toDistribute[i].getWheat();
				currPlayer.gainResource(ResourceType.WHEAT, quantity);
			}
		}	
	}
	
	private static void distributeWood(int wood, ResourceList[] toDistribute,
			int inBank, Player[] players, ResourceList bank) {
		// TODO Auto-generated method stub
		if(wood<=inBank){
			bank.setWood(inBank-wood);
			for(int i = 0;i<toDistribute.length;i++){
				Player currPlayer = players[i];
				int quantity = toDistribute[i].getWood();
				currPlayer.gainResource(ResourceType.WOOD, quantity);
			}
		}	
	}

}
