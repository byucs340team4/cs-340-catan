package gamemanager.game;

import gamemanager.player.Player;

public class UpdateStatus {
	
	public static void update(Game game,boolean endTurn){
		String status = game.getTurnTracker().getStatus();
		
		int version = game.getVersion() + 1;
		game.setVersion(version);
		//for rolling 7's just change to discarding status in the rob method in game
		if(endTurn)
			endTurn(game);
		else if(status.equals("Discarding"))
			discard(game);
		else if(status.equals("Robbing"))
			rob(game);
		else if(status.equals("Rolling"))
			roll(game);
		
		updateLargestArmy(game);
		updateLongestRoad(game);
		updateGameWinner(game);
	}

	private static void updateGameWinner(Game game) {
		// TODO Auto-generated method stub
		Player[] players = game.getPlayers();
		for(int i = 0;i<players.length;i++){
			Player curr = players[i];
			if(curr.getVictoryPoints()>=10){
				game.setWinner(i);
				return;
			}
		}
	}

	private static void updateLongestRoad(Game game) {
		// TODO Auto-generated method stub
		boolean changed = false;
		Player[] players = game.getPlayers();
		int longestRoad = game.getTurnTracker().getLongestRoad();
		for(int i=0;i<players.length;i++){
			Player curr = players[i];
			int numRoads = 15-curr.getRoads();
			if(numRoads>=5){
				if(longestRoad!=-1){
					int currLongest = 15 - players[longestRoad].getRoads();
					if(numRoads>currLongest){
						changed = true;
						int oldLongest = players[longestRoad].getVictoryPoints();
						players[longestRoad].setVictoryPoints(oldLongest - 2); 
						longestRoad = i;
					}
				}
				else{
					changed = true;
					longestRoad = i;
				}
			}
		}
		if(changed) {
			game.getTurnTracker().setLongestRoad(longestRoad);
			int victoryPoints = players[longestRoad].getVictoryPoints();
			players[longestRoad].setVictoryPoints(victoryPoints+2);
		}
	}

	private static void updateLargestArmy(Game game) {
		// TODO Auto-generated method stub
		boolean changed = false;
		Player[] players = game.getPlayers();
		int largestArmy = game.getTurnTracker().getLargestArmy();
		for(int i=0;i<players.length;i++){
			Player curr = players[i];
			int numSoldiers = curr.getSoldiers();
			if(numSoldiers>=3){
				if(largestArmy!=-1){
					int currLongest = players[largestArmy].getSoldiers();
					if(numSoldiers>currLongest){
						changed = true;
						int oldLargest = players[largestArmy].getVictoryPoints();
						players[largestArmy].setVictoryPoints(oldLargest-2); 
						largestArmy = i;
					}
				}
				else{
					changed = true;
					largestArmy = i;
				}
			}
		}
		if(changed) {
			game.getTurnTracker().setLargestArmy(largestArmy);
			int victoryPoints = players[largestArmy].getVictoryPoints();
			players[largestArmy].setVictoryPoints(victoryPoints+2);
		}
		
	}

	private static void roll(Game game) {
		// TODO Auto-generated method stub
		game.getTurnTracker().setStatus("Playing");
	}

	private static void rob(Game game) {
		// TODO Auto-generated method stub
		game.getTurnTracker().setStatus("Playing");
	}

	private static void discard(Game game) {
		// TODO Auto-generated method stub
		Player[]players = game.getPlayers();
		game.getTurnTracker().setStatus("Robbing");
		for(Player player:players){
			if(player.getDiscarded()==false && player.getResources().total()>7)
				game.getTurnTracker().setStatus("Discarding");
		}
		if(game.getTurnTracker().getStatus().equals("Robbing")){
			for(Player player:players){
				player.setDiscarded(false);
			}
		}
	}

	private static void endTurn(Game game) {
		// TODO Auto-generated method stub
		String status = game.getTurnTracker().getStatus();
		int currTurn = game.getTurnTracker().getCurrentTurn();

		if(status.equals("FirstRound")){
			if(currTurn == 3){
				game.getTurnTracker().setStatus("SecondRound");
				//don't update whose turn it is
			}
			else
				currTurn++;
		}
		else if(status.equals("SecondRound")){
			if(currTurn==0){
				game.getTurnTracker().setStatus("Rolling");
			}
			else
				currTurn--;
		}		
		else if(status.equals("Playing")){
			System.out.println("Transfer all new dev cards to old devcards");
			game.getTurnTracker().setStatus("Rolling");
			currTurn++;
		}

		game.getTurnTracker().setCurrentTurn(currTurn%4);
	}
	
}
