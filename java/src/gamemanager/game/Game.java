package gamemanager.game;


import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import client.data.PlayerInfo;
import gamemanager.communication.ChatLog;
import gamemanager.communication.GameLog;
import gamemanager.communication.Message;
import gamemanager.communication.TurnTracker;
import gamemanager.map.Road;
import gamemanager.player.Player;
import gamemanager.player.ResourceList;
import cs340.model.player.ResourceHand;
import cs340.model.player.TradeOffer;
import shared.definitions.DevCardType;
import shared.definitions.ResourceType;
import shared.locations.EdgeLocation;
import shared.locations.HexLocation;
import shared.locations.VertexLocation;
import shared.locations.VertexObject;
import shared.proxy.MoveType;
import shared.proxy.request.AcceptTrade;
import shared.proxy.request.BuildCity;
import shared.proxy.request.BuildRoad;
import shared.proxy.request.BuildSettlement;
import shared.proxy.request.BuyDevCardRequest;
import shared.proxy.request.DiscardCards;
import shared.proxy.request.FinishTurnRequest;
import shared.proxy.request.MaritimeTrade;
import shared.proxy.request.Monopoly;
import shared.proxy.request.Monument;
import shared.proxy.request.OfferTrade;
import shared.proxy.request.RoadBuildingRequest;
import shared.proxy.request.RobPlayerRequest;
import shared.proxy.request.RollNumberRequest;
import shared.proxy.request.SendChatRequest;
import shared.proxy.request.Soldier;
import shared.proxy.request.YearOfPlentyRequest;

/**
 * Object to hold all the game elements required
 * @author Michael and Bentz
 *
 */
public class Game {

	/**
	 * The title of the game
	 */
	private String title;
	
	/**
	 * Deck that the player with draw DevCards from.
	 */
	private Deck deck;
	
	/**
	 * game map
	 */
	private gamemanager.map.Map map;
	
	/**
	 * Contains information about the current players in this game.
	 */
	private Player[] players;
	
	/**
	 * All the log messages
	 */
	private GameLog log;
	
	/**
	 * All the chat messages.
	 */
	private ChatLog chat;
	/**
	 * The cards that are available to be distributed to the players.
	 */
	private Bank bank;

	/**
	 * Tracks who's turn it is and what action's being done.
	 */
	private TurnTracker turnTracker;
	
	/**
	 * Id of the winner.  -1 if no one has won yet.
	 */
	private int winner;

	/**
	 * The version of the game, which is changed whenever a move is changed.
	 */
	private int version;
	

	
	/**
	 * Optional.  Contains information about a current trade offer(if there is one)
	 */
	private TradeOffer tradeOffer;
	
	
	
	/**
	 * Gets the title of the game
	 * @return String - game title
	 */
	public String getTitle(){
		return title;
	}
	
	/**
	 * Sets the title of the game
	 * @param title - String to set the title to.
	 */
	public void setTitle(String title){
		this.title = title;
	}
	
	
	
	/**
	 * method to return the map of the game
	 * @return the map
	 */
	public gamemanager.map.Map getMap() {
		return map;
	}

	/**
	 * method to set the game map
	 * @param map - map to be set
	 */
	public void setMap(gamemanager.map.Map map) {
		this.map = map;
	}
		
	/**
	 * default constructor that takes no parameters
	 */
	public Game(){
		
	}
	
	public void setPlayersGame(){
		for(int i = 0;i<players.length;i++){
			if(players[i]!=null) {
				players[i].setGame(this);
			}
		}
	}
	
	public Game(ResourceList bank, GameLog log, Player[] players,
			ChatLog chat, gamemanager.map.Map map,
			gamemanager.communication.TurnTracker turnTracker, Deck deck, int version, int winner, TradeOffer tradeOffer) {
		this.bank = new Bank(bank);
		this.log = log;
		this.players = players;
		this.chat = chat;
		this.map = map;
		this.turnTracker = turnTracker;
		this.deck = deck;
		this.version = version;
		this.winner = winner;
		this.tradeOffer = tradeOffer;
	}

	/**
	 * method to initialize the game board
	 */
	public void initializer(){
		
	}

	/**
	 * method to determine whether or not a road can be built at a given edge
	 */
	public boolean canBuildRoad(BuildRoad buildRoad){
		Player player = players[buildRoad.getPlayerIndex()];
		String status = turnTracker.getStatus();
		if(status.equals("FirstRound") || status.equals("SecondRound")){
			return map.canBuildRoadServer(buildRoad.getLocation(),buildRoad.getPlayerIndex(), true,status);
		}else if(player.canBuildRoad()){
			return map.canBuildRoadServer(buildRoad.getLocation(),buildRoad.getPlayerIndex(), false,status);
		}
		return false;
	}
	
	/**
	 * method to determine whether or not a city can be built at a given vertex
	 */
	public boolean canBuildCity(BuildCity buildCity){
		VertexLocation location = buildCity.getLocation();
		int playerIndex = buildCity.getPlayerIndex();
		Player player = players[playerIndex];
		if(turnTracker.getStatus().equals("Playing") && playerIndex == turnTracker.getCurrentTurn() && player.canBuildCity()) {
			return map.canBuildCity(location, playerIndex);
		}
		return false;
	}
	
	/**
	 * method to determine whether or not a settlement can be built at a given vertex
	 */
	public boolean canBuildSettlement(BuildSettlement buildSettlement){
		VertexLocation location = buildSettlement.getLocation();
		int playerIndex = buildSettlement.getPlayerIndex();
		Player player = players[playerIndex];
		boolean playerCanBuild= player.canBuildSettlement();
		boolean initialization = buildSettlement.getFree();
		String status = turnTracker.getStatus();
		int currTurn = turnTracker.getCurrentTurn();
		
		if(status.equals("Playing") && playerIndex == currTurn && playerCanBuild) {
			return map.canBuildSettlement(location, playerIndex, initialization);
		}
		return false;
	}
		
	public Deck getDeck() {
		return deck;
	}
	
	/**
	 * Adds a player to the playerManager.
	 * @param player Contains information about the player.
	 */
	public void addPlayer(Player player) {
		Player[]temp = new Player[players.length+1];
		
		for(int i=0;i<temp.length;i++){
			if(i >= players.length){
				temp[i] = player;
				player.setOrderNumber(i);
			}
			else
				temp[i] = players[i];
		}
		players = temp;
	}
	
	public DevCardType drawDevCard() {
		return null;
	}

	public Bank getBank() {
		return bank;
	}

	public void setBank(ResourceList bank) {
		this.bank = (Bank) bank;
	}

	public ChatLog getChat() {
		return chat;
	}

	public void setChat(ChatLog chat) {
		this.chat = chat;
	}

	public GameLog getLog() {
		return log;
	}

	public void setLog(GameLog log) {
		this.log = log;
	}

	public TradeOffer getTradeOffer() {
		return tradeOffer;
	}

	public void setTradeOffer(TradeOffer tradeOffer) {
		this.tradeOffer = tradeOffer;
	}

	public TurnTracker getTurnTracker() {
		return turnTracker;
	}

	public void setTurnTracker(TurnTracker turnTracker) {
		this.turnTracker = turnTracker;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public int getWinner() {
		return winner;
	}

	public void setWinner(int winner) {
		this.winner = winner;
	}

	public void setDeck(Deck deck) {
		this.deck = deck;
	}
	
	public void resetPlayers(){
		for(Player player: players){	
			player.setCities(4);
			player.setNewDevCards(new DevCardList());
			player.setOldDevCards(new DevCardList());
			player.setResources(new ResourceList());
			player.setRoads(15);
			player.setSettlements(5);
			player.setSoldiers(0);
			player.setVictoryPoints(0);

		
		}
	}
	
	public PlayerInfo[] getPlayersInfo(){
		PlayerInfo[] playersInfo = new PlayerInfo[4];
		List<PlayerInfo> p = new ArrayList<PlayerInfo>();
		int i=0;
		for(Player player: players){
			if(player == null)continue;
			playersInfo[i] = new PlayerInfo();
			playersInfo[i].setId(player.getPlayerID());
			playersInfo[i].setName(player.getName());
			playersInfo[i].setPlayerIndex(player.getOrderNumber());
			playersInfo[i].setColor(player.getColor());
			playersInfo[i].setStringColor(player.getColor().toString().toLowerCase());
			p.add(playersInfo[i]);
			i++;
		}
		playersInfo = new PlayerInfo[p.size()];
		i = 0;
		for(PlayerInfo player: p){
			playersInfo[i] = player;
			i++;
		}
		return playersInfo;
	}

	public boolean canFinishTurn(FinishTurnRequest request) {
		String status = turnTracker.getStatus();
		int playerIndex = request.getPlayerIndex();
		int currTurn = turnTracker.getCurrentTurn();
		Player player = players[playerIndex];
		if(request.getPlayerIndex() == turnTracker.getCurrentTurn() && turnTracker.getStatus().equals("Playing")) {
			return true;
		}
		else if(status.equals("FirstRound") && player.getSettlements()==4 && player.getRoads()==14 && currTurn==playerIndex)
			return true;
		else if(status.equals("SecondRound") && player.getSettlements()==3 && player.getRoads()==13 && currTurn==playerIndex)
			return true;
		
		return false;
	}

	public boolean canBuyDevCard(BuyDevCardRequest request) {
		int playerIndex = request.getPlayerIndex();
		int currTurn = turnTracker.getCurrentTurn();
		String status = turnTracker.getStatus();
		if(currTurn == playerIndex && status.equals("Playing")) {
			Player player = players[playerIndex];
			if(deck.total()>0){
				return player.canBuyDevCardServer();
			}
		}
		return false;
	}

	public boolean canPlaySoldier(Soldier soldier) {
		if(soldier.getPlayerIndex() == turnTracker.getCurrentTurn() && turnTracker.getStatus().equals("Playing")) {
			if (players[soldier.getPlayerIndex()].canPlaySoldier()) {
				int victimIndex = soldier.getVictimIndex();
				int playerIndex = soldier.getPlayerIndex();
				HexLocation hLoc = soldier.getLocation();
				if(victimIndex == -1)
					return true;
				if(soldier.getVictimIndex() != -1) {
					if (players[soldier.getVictimIndex()].getResources().total() > 0) {
						Set<Integer>hexOwners = map.getOtherHexOwners(hLoc, playerIndex);
						if(hexOwners.contains(victimIndex))
							return true;
					}
				}	
			}
		}
		return false;
	}

	public boolean canRoll(RollNumberRequest request) {
		if(request.getPlayerIndex() == turnTracker.getCurrentTurn() && turnTracker.getStatus().equals("Rolling")) {
			int number = request.getNumber();
			if(number<2 || number>12)
				return false;
			else
				return true;
		}
		return false;
	}
	
	/**
	 * method for server to rob player
	 * @param rob - the RobPlayerRequest for the server
	 * 
	 */
		public void robPlayer(RobPlayerRequest request,boolean soldierCard){
			if(canRob(request) || soldierCard){
				int playerIndex = request.getPlayerIndex();
				int victimIndex = request.getVictimIndex();
				Player robber = players[playerIndex];
				HexLocation hLoc = request.getLocation();
				map.setRobber(hLoc);
				if(victimIndex==-1){
					log.addMessage(new Message(robber.getName(),robber.getName() + " moved the robber."));
				}
				else if(victimIndex != -1){
					Player victim = players[victimIndex];
					int total = victim.getResources().total();
					if(total>0){
						ResourceType resource = victim.getResources().removeRandom();
						robber.gainResource(resource, 1);
						log.addMessage(new Message(robber.getName(),robber.getName() + " moved the robber and robbed "+victim.getName()+"."));
					}				
				}
			
				UpdateStatus.update(this, false);
			}
	}
		

	public boolean canRob(RobPlayerRequest request) {
		int currTurn = turnTracker.getCurrentTurn();
		int playerIndex = request.getPlayerIndex();
		String status = turnTracker.getStatus();
		int victimIndex = request.getVictimIndex();
		if(playerIndex == currTurn && status.equals("Robbing")) {
			HexLocation hLoc = request.getLocation();
				if(victimIndex == -1)
					return true;
				if(request.getVictimIndex() != -1) {
					if (players[request.getVictimIndex()].getResources().total() > 0) {
						Set<Integer>hexOwners = map.getOtherHexOwners(hLoc, playerIndex);
						if(hexOwners.contains(victimIndex))
							return true;
					}
				}		
			
		}
		return false;
	}

	public boolean canYoP(YearOfPlentyRequest request) {
		if(request.getPlayerIndex() == turnTracker.getCurrentTurn() && turnTracker.getStatus().equals("Playing")) {
			if(players[request.getPlayerIndex()].canPlayYoP()) {
				if(bank.has(ResourceType.valueOf(request.getResource1().toUpperCase())) && bank.has(ResourceType.valueOf(request.getResource2().toUpperCase())) ) {
					return true;
				}
			}
		}
		return false;
	}

	public boolean canRoadBuiding(RoadBuildingRequest request) {
		if(request.getPlayerIndex() == turnTracker.getCurrentTurn() && turnTracker.getStatus().equals("Playing")) {
			Player player = players[request.getPlayerIndex()];
			int playerIndex = request.getPlayerIndex();
			String status = turnTracker.getStatus();
			if(player.canPlayRoadBuilding()) {
//				boolean initialization = true;
				if(map.canBuildRoadServer(request.getSpot1(),player.getOrderNumber(),false,status)) {
					Road road = new Road(request.getSpot1(),playerIndex);
					map.addRoad(road);
					if(map.canBuildRoadServer(request.getSpot2(), player.getOrderNumber(), false,status)) {
						Road road2 = new Road(request.getSpot2(),player.getOrderNumber());
						map.addRoad(road2);
						return true;
					}
					else
						map.removeRoad(request.getSpot1());
				}
				else if(map.canBuildRoadServer(request.getSpot2(), player.getOrderNumber(), false,status)) {
					Road road = new Road(request.getSpot2(),playerIndex);
					map.addRoad(road);
					if(map.canBuildRoadServer(request.getSpot1(), player.getOrderNumber(), false,status)) {
						Road road2 = new Road(request.getSpot1(),playerIndex);
						map.addRoad(road2);
						return true;
					}
					else
						map.removeRoad(request.getSpot2());
				}
			}
		}
		return false;
	}

	public boolean canMonopoly(Monopoly monopoly) {
		if(turnTracker.getCurrentTurn() == monopoly.getPlayerIndex() && turnTracker.getStatus().equals("Playing")) {
			if(players[monopoly.getPlayerIndex()].canPlayMonopoly()) {
				return true;
			}
		}
		return false;
	}

	public boolean canMonument(Monument monument) {
		if(turnTracker.getCurrentTurn() == monument.getPlayerIndex() && turnTracker.getStatus().equals("Playing")) {
			return players[monument.getPlayerIndex()].canPlayMonument();
		}
		return false;
	}

	public Player[] getPlayers() {
		return players;
	}

	public void setPlayers(Player[] players) {
		this.players = players;
	}
	
	public boolean canOfferTrade(){
		if(turnTracker.getCurrentTurn()!=tradeOffer.getSender())
			return false;
		if(!turnTracker.getStatus().equals("Playing"))
			return false;
		if(!(players[tradeOffer.getSender()].canOfferTrade(tradeOffer)))
			return false;
		return true;
	}
	
	public boolean canOfferTrade(TradeOffer trade){
		if(turnTracker.getCurrentTurn()!=trade.getSender())
			return false;
		if(!turnTracker.getStatus().equals("Playing"))
			return false;
		if(!(players[trade.getSender()].canOfferTrade(trade)))
			return false;
		return true;
	}

	public boolean canAcceptTrade(AcceptTrade accept) {
		boolean willAccept = accept.getWillAccept();
		if(tradeOffer != null && accept.getPlayerIndex() < 4) {
			if(players[accept.getPlayerIndex()] != null) {
				if(players[tradeOffer.getReceiver()].canAcceptTrade(tradeOffer) || !willAccept){
					return true;
				}
			}
		}
		return false;
	}

	public boolean canMaritimeTrade(MaritimeTrade trade) {
		@SuppressWarnings("unused")
		ResourceType type;
		if(trade.getPlayerIndex() == turnTracker.getCurrentTurn() && turnTracker.getStatus().equals("Playing")) {
			String output = trade.getOutputResources();
			if(output == "brick") {
				if (bank.getBrick() == 0) {
					return false;
				}
			}
			if(output == "ore") {
				if (bank.getOre() == 0) {
					return false;
				}
			}
			if(output == "sheep") {
				if (bank.getSheep() == 0) {
					return false;
				}
			}
			if(output == "wheat") {
				if(bank.getSheep() == 0) {
					return false;
				}
			}
			if(output == "wood") {
				if (bank.getSheep() == 0) {
					return false;
				}
			}
			String input = trade.getInputResources();
			if(input.equals("brick")) {
				type = ResourceType.BRICK;
				if(players[trade.getPlayerIndex()].getResources().getBrick() < trade.getRatio()) {
					return false;
				}
			}
			else if(input.equals("ore")) {
				type = ResourceType.ORE;
				if(players[trade.getPlayerIndex()].getResources().getOre() < trade.getRatio()) {
					return false;
				}
			}
			else if(input.equals("sheep")) {
				type = ResourceType.SHEEP;
				if(players[trade.getPlayerIndex()].getResources().getSheep() < trade.getRatio()) {
					return false;
				}
			}
			else if(input.equals("wheat")) {
				type = ResourceType.WHEAT;
				if(players[trade.getPlayerIndex()].getResources().getWheat() < trade.getRatio()) {
					return false;
				}
			}
			else if (input.equals("wood")) {
				type = ResourceType.WOOD;
				if(players[trade.getPlayerIndex()].getResources().getWood() < trade.getRatio()) {
					return false;
				}
 			}
			else {
				type = null;
			}
			if(trade.getRatio() < 4) {
			//if(!map.canMaritimeTrade(type.toString().toLowerCase(),trade.getPlayerIndex())) {
			//	return false;
			//}
			}
			return true;
		}
		return false;
	}
	
	public boolean canDiscardCards(DiscardCards request) {
		if(turnTracker.getStatus().equals("Discarding")) {
			if(players[request.getPlayerIndex()].canDiscard(request.getDiscarded())) {
				return true;
			}
		}
		return false;
	}
	
	public boolean canPlayMonopoly(Monopoly monopoly) {
		if(monopoly.getPlayerIndex() < 4 && turnTracker.getStatus().equals("Playing")) {
			if(players[monopoly.getPlayerIndex()] != null) {
				if (turnTracker.getCurrentTurn() == monopoly.getPlayerIndex()) {
					return players[monopoly.getPlayerIndex()].canPlayMonopoly();
				}
			}
		}
		return false;
	}
	public Player getPlayer(int index){
//		for(int i = 0;i<players.length;i++){
//			if(players[i].getPlayerID()==index)
//				return players[i];
//		}
//		return null;
		return players[index];
	}
	
	public Player getPlayerByID(int id){
		for(int i = 0;i<players.length;i++){
			if(players[i].getPlayerID()==id)
				return players[i];
		}
		return null;

	}
	
	public Player getPlayer(String name){
		for(int i=0;i<players.length;i++){
			if(players[i].getName().equals(name))
				return players[i];
		}
		return null;	
	}
	
	public int getNumberOfPlayers() {
		int index = 0;
		for(Player player : players) {
			if(player != null) {
				index++;
			}
		}
		return index;
	}
	
	/**
	 * method for server to send chat message, will just call the add chat in the chat tracker
	 * @param chat - the SendChatRequest used in the message creation
	 *  
	 */
	public void sendChat(SendChatRequest request){
		int playerIndex = request.getPlayerIndex();
		String source = getPlayer(playerIndex).getName();
		Message message = new Message(source,request.getContent());
		chat.addMessage(message);
		version++;
	}
	
	/**
	 * method for server to divy up the resources as needed based on the dice roll
	 * @param roll -- the RollNumberRequest for the server
	 *  
	 */
	public void rollNumber(RollNumberRequest request){
		if(canRoll(request)){
			int playerIndex = request.getPlayerIndex();
			String name = players[playerIndex].getName();
			int rollNumber = request.getNumber();	
			map.rollNumber(players,rollNumber,bank);
			Message message = new Message(name,name+" rolled a " + rollNumber + ".");
			log.addMessage(message);
			if(rollNumber == 7) turnTracker.setStatus("Discarding");
			
			UpdateStatus.update(this, false);
			System.out.println("Roll number doesn't take into account how many cards are left yet");
			return;
		}
	}
	

	/**
	 * method for server to end the turn for current player
	 * @param endTurn - FinishTurnRequets for the server
	 * 
	 */
	public void finishTurn(FinishTurnRequest endTurn){
		if(canFinishTurn(endTurn)){	
			int playerIndex= endTurn.getPlayerIndex();
			Player player = players[playerIndex];
			player.newDevCardsToOld();
			UpdateStatus.update(this, true);
			log.addMessage(new Message(player.getName(),player.getName() + "'s turn just ended"));
		}
	}
	
	/**
	 * method for server to buy a dev card
	 * @param buyCard - BuyDevCardRequest for the server
	 * 
	 */
	public void buyDevCard(BuyDevCardRequest request){
		if(canBuyDevCard(request)){
			int playerIndex = request.getPlayerIndex();
			Player player = players[playerIndex];
			player.buyDevCard(this);
			bank.buyDevCard();
			UpdateStatus.update(this, false);
			log.addMessage(new Message(player.getName(),player.getName() + " bought a Development Card."));
		}
	}
	
	/**
	 * method for server to play year of plenty card
	 * @param yop - YearOfPlentyRequest for the server
	 * 
	 */
	public void playYearOfPlenty(YearOfPlentyRequest request){
		int playerIndex = request.getPlayerIndex();
		Player player = players[playerIndex];
		bank.YoP(ResourceType.valueOf(request.getResource1().toUpperCase()), ResourceType.valueOf(request.getResource2().toUpperCase()));
		player.playYearOfPlenty(request);
		UpdateStatus.update(this, false);
		log.addMessage(new Message(player.getName(),player.getName() + " used Year of Plenty and got a "+ request.getResource1() + " and a "+request.getResource2()));
	}
	
	/**
	 * method for server to play road builder card
	 * @param roadBuilder - RoadBuildingRequest for the server
	 * 
	 */
	public void playRoadBuilder(RoadBuildingRequest request){
		int playerIndex = request.getPlayerIndex();
		//EdgeLocation spot1 = request.getSpot1();
		//EdgeLocation spot2 = request.getSpot2();
		//Road road1 = new Road(spot1,playerIndex);
		//Road road2 = new Road(spot2,playerIndex);
		Player player = players[playerIndex];
		player.playRoadBuilding();
		//map.addRoad(road1);
		//map.addRoad(road2);
		UpdateStatus.update(this, false);
		log.addMessage(new Message(player.getName(),player.getName() + " played road builder"));
	}
	
	/**
	 * method for server to play soldier card
	 * @param soldier - Solder (request) for the server
	 * 
	 */
	public void playSoldier(Soldier request){
		int robberIndex = request.getPlayerIndex();
		int victimIndex = request.getVictimIndex();
		HexLocation hLoc = request.getLocation();
		RobPlayerRequest robRequest = new RobPlayerRequest(MoveType.ROB_PLAYER,robberIndex,victimIndex,hLoc);
		robPlayer(robRequest,true);
		Player robber = players[robberIndex];
		robber.playSoldier();
		map.setRobber(hLoc);
		UpdateStatus.update(this, false);
		log.addMessage(new Message(robber.getName(), robber.getName() + " used a soldier"));
	}
	
	/**
	 * method for the server to play monopoly card
	 * @param monopoly - Solder (request) for the server
	 * 
	 */
	public void playMonopoly(Monopoly request){
		int playerIndex = request.getPlayerIndex();
		ResourceType resource = ResourceType.valueOf(request.getResource().toUpperCase());
		Player player = players[playerIndex];
		player.playMonopoly();
		int total = 0;
		for(int i=0;i<players.length;i++){
			if(i!=playerIndex){
				Player victim = players[i];
				int amount = victim.getResourceAmount(resource);
				victim.setResourceAmount(resource,0);
				total+=amount;
			}
		}
		player.gainResource(resource, total);
		UpdateStatus.update(this, false);
		log.addMessage(new Message(player.getName(), player.getName() + " stole everyones " + resource.toString()));
	}
	
	/**
	 * method for the server to play monument card
	 * @param monument - Monument (request) for the server
	 * 
	 */
	public void playMonument(Monument monument){
		int playerIndex = monument.getPlayerIndex();
		Player player = players[playerIndex];
		player.playMonument();
		UpdateStatus.update(this, false);
		log.addMessage(new Message(player.getName(),player.getName() + " built a monument and gained a victory point"));
		};
	
	/**
	 * method for the server to build a road
	 * @param buildRoad - BuildRoad (request) for the server
	 * 
	 */
	public boolean buildRoad(BuildRoad request){
		Road road = new Road(request.getLocation(),request.getPlayerIndex());
		int playerIndex = request.getPlayerIndex();
		boolean free = request.getFree();
		Player player = players[playerIndex];
		String status = turnTracker.getStatus();
		
		boolean canBuild = false;
		if(free && (status.equals("FirstRound")||status.equals("SecondRound"))){
			canBuild = canBuildInitialRoad(request); //no need to check player can build in init 
		}
		else{
			canBuild = canBuildRoad(request);
		}
		if(canBuild){
			map.addRoad(road);
			bank.buyRoad(free);
			player.buildRoad(free);
			UpdateStatus.update(this, false);
			log.addMessage(new Message(player.getName(),player.getName() + " built a road."));
			return true;
		}
		
		return false;
	}
	
	@SuppressWarnings("unused")
	private boolean canBuildInitialRoad(BuildRoad request) {
		int playerIndex = request.getPlayerIndex();
		Player player = players[playerIndex];
		String status = turnTracker.getStatus();
		int currTurn = turnTracker.getCurrentTurn();
		int numSettlements = player.getSettlements();

		if(currTurn==playerIndex && status.equals("FirstRound") && numSettlements==4){
			return map.canBuildRoadServer(request.getLocation(), playerIndex, true,status);
		}
		if(currTurn==playerIndex && status.equals("SecondRound") && numSettlements==3){
			return map.canBuildRoadServer(request.getLocation(), playerIndex, true,status);
		}
		return false;
	}

	/**
	 * method for the server to build a settlement
	 * @param buildSettlement - BuildSettlement (request) for the server
	 * 
	 */
	public boolean buildSettlement(BuildSettlement buildSettlement){
		VertexObject settle = new VertexObject(buildSettlement.getPlayerIndex(),buildSettlement.getLocation());
		int playerIndex = buildSettlement.getPlayerIndex();
		boolean free = buildSettlement.getFree();
		
		boolean canBuild = false;
		if(free){
			canBuild = canBuildInitialSettlement(buildSettlement); //no need to check player can build in init 
		}
		else{
			canBuild = canBuildSettlement(buildSettlement);
		}
		if(canBuild){
			Player player = players[playerIndex];
			if(player.getSettlements()==4 && free){
				ResourceList initial = map.addSettlement(settle,true);
				player.setResources(initial);
				bank.distribute(initial);
			}
			else
				map.addSettlement(settle,false);
			
			bank.buySettlement(free);
			player.buildSettlement(free);
			UpdateStatus.update(this, false);
			log.addMessage(new Message(player.getName(),player.getName() + " built a settlement"));
			return true;
		}
		return false;
	}
	
	private boolean canBuildInitialSettlement(BuildSettlement request) {
		int playerIndex = request.getPlayerIndex();
		Player player = players[playerIndex];
		String status = turnTracker.getStatus();
		int currTurn = turnTracker.getCurrentTurn();
		int numSettlements = player.getSettlements();
		int numRoads = player.getRoads();
		if(currTurn==playerIndex && status.equals("FirstRound") && numSettlements==5){
			return map.canBuildSettlement(request.getLocation(), playerIndex, true);
		}
		if(currTurn==playerIndex && status.equals("SecondRound") && numRoads==14){
			return map.canBuildSettlement(request.getLocation(), playerIndex, true);
		}
		return false;
	}

	/**
	 * method for the server to build a city
	 * @param buildCity - BuildCity (request) for the server
	 * 
	 */
	public void buildCity(BuildCity request){
		if(canBuildCity(request)){
			int playerIndex = request.getPlayerIndex();
			Player player = players[playerIndex];
			bank.buyCity();
			player.buildCity();
			VertexLocation loc = request.getLocation();
			VertexObject city = new VertexObject(playerIndex, loc);
			map.addCity(city);
			UpdateStatus.update(this,false);
			log.addMessage(new Message(player.getName(),player.getName() + " upgraded to a city"));
		}
	}
	
	/**
	 * method for the server to offer a trade
	 * @param offer - OfferTrade (request) for the server
	 * 
	 */
	public void offerTrade(OfferTrade request){
		int receiver = request.getReceiver();
		int sender = request.getPlayerIndex();
		ResourceList offer = request.getOffer();
		this.tradeOffer = new TradeOffer();
		ResourceHand hand = new ResourceHand();
		hand.setBrick(offer.getBrick());
		hand.setOre(offer.getOre());
		hand.setSheep(offer.getSheep());
		hand.setWheat(offer.getWheat());
		hand.setWood(offer.getWood());
		tradeOffer.setOffer(hand);
		tradeOffer.setReciever(receiver);
		tradeOffer.setSender(sender);
		UpdateStatus.update(this, false);

	}
	
	/**
	 * method for the server to accept or reject a trade
	 * @param accept - AcceptTrade (request) for the server
	 * 
	 */
	public void acceptTrade(AcceptTrade request){
		int playerIndex = request.getPlayerIndex();
		Player receiver = players[playerIndex];
		boolean accepted = request.getWillAccept();
		if(accepted){
			int senderIndex = tradeOffer.getSender();
			Player sender = players[senderIndex];
			sender.performTrade(tradeOffer.getOffer(),-1);
			receiver.performTrade(tradeOffer.getOffer(),1);			
		}
		tradeOffer = null;
		UpdateStatus.update(this, false);
		if(accepted){
			log.addMessage(new Message(receiver.getName(),receiver.getName() + " accepted a trade"));
		}else{
			log.addMessage(new Message(receiver.getName(),receiver.getName() + " rejected a trade"));
		}
	}
	
	/**
	 * method for the server to perform a maritime trade
	 * @param trade - MaritimeTrade (request) for the server
	 * 
	 */
	public void maritimeTrade(MaritimeTrade request){
		int playerIndex = request.getPlayerIndex();
		Player player = players[playerIndex];
		player.maritimeTrade(request);
		UpdateStatus.update(this, false);
	}
	
	/**
	 * method for the server to discard cards
	 * @param discard - DiscardCards (request) for the server
	 * 
	 */
	public void discardCards(DiscardCards request){
		int playerIndex = request.getPlayerIndex();
		Player player = players[playerIndex];
		ResourceList toDiscard = request.getDiscarded();
		player.discard(toDiscard);
		bank.discard(request.getDiscarded());
		UpdateStatus.update(this, false);
	}
	
	public JsonObject toJson(){
		JsonObject result = new JsonObject();
		result.add("deck", this.deck.toJson());
		result.add("map", this.map.toJson());
		JsonArray players = new JsonArray();
		for(Player player: this.players){
			players.add(player.toJson());
		}
		result.add("players", players);
		result.add("log", this.log.toJson());
		result.add("chat", this.chat.toJson());
		result.add("turnTracker", this.turnTracker.toJson());
		result.addProperty("winner", this.winner);
		result.addProperty("version", this.version);
		if(this.tradeOffer!=null)
			result.add("tradeOffer", this.tradeOfferJson());
		return result;
	}
	
	private JsonObject tradeOfferJson(){
		JsonObject result = new JsonObject();
		result.addProperty("sender", this.tradeOffer.getSender());
		result.addProperty("receiver", this.tradeOffer.getReceiver());
		result.add("offer", this.resourceHandJson());
		return result;
	}
	
	private JsonObject resourceHandJson(){
		JsonObject result = new JsonObject();
		result.addProperty("brick", this.tradeOffer.getOffer().getBrick());
		result.addProperty("ore", this.tradeOffer.getOffer().getOre());
		result.addProperty("sheep", this.tradeOffer.getOffer().getSheep());
		result.addProperty("wheat", this.tradeOffer.getOffer().getWheat());
		result.addProperty("wood", this.tradeOffer.getOffer().getWood());
		return result;
	}
	
	/**
	 * method to check who has the longest road
	 */
	@SuppressWarnings("unused")
	private void checkLongestRoad(){};
	
	/**
	 * method to check who has the largest army
	 */
	@SuppressWarnings("unused")
	private void checkLargestArmy(){};
	
}
