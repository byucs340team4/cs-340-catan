package gamemanager.game;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Stack;

import shared.definitions.DevCardType;

import com.google.gson.JsonObject;




public class Deck {

	private static final int TOTAL_SOLDIERS = 14;
	private static final int TOTAL_MONUMENTS = 5;
	private static final int TOTAL_YOP = 2;
	private static final int TOTAL_ROAD_BUILDING = 2;
	private static final int TOTAL_MONOPOLY = 2;
	/**
	 * Stack of DevCardType objects.
	 */
	private Stack<DevCardType> deck;
	/**
	 * Map of all the DevCardTypes.
	 */
	private Map<DevCardType, Integer> devCardList;
	
	/**
	 * Default constructor.  Calls createDeck().
	 * Creates a new deck
	 */
	public Deck(){
		devCardList = new HashMap<DevCardType, Integer>();
		deck = createDeck(TOTAL_SOLDIERS,TOTAL_MONUMENTS,TOTAL_YOP,TOTAL_ROAD_BUILDING,TOTAL_MONOPOLY);
	}
	
	public Deck(DevCardList devCards) {
		devCardList = new HashMap<DevCardType, Integer>();
		deck = createDeck(devCards.getSoldier(),devCards.getMonument(),devCards.getYearOfPlenty(),
				devCards.getRoadBuilding(),devCards.getMonopoly());
	}
	
	/**
	 * Returns the top card from the deck.
	 * @return Top DevCardType.
	 */
	public DevCardType draw(){
		return deck.pop();
	}
	
	public int total() {
		int total = devCardList.get(DevCardType.MONOPOLY);
		total += devCardList.get(DevCardType.MONUMENT);
		total += devCardList.get(DevCardType.ROAD_BUILD);
		total += devCardList.get(DevCardType.SOLDIER);
		total += devCardList.get(DevCardType.YEAR_OF_PLENTY);
		return total;
	}
	
	/**
	 * Creates all instances of DevCardTypes needed.
	 * Calls shuffle and returns the stack that is created.
	 * @return Stack of DevCardType.
	 */
	private Stack<DevCardType> createDeck(int soldiers, int monuments, int YoP, int roadBuilding,int monopoly){
		List<DevCardType> cards = new ArrayList<DevCardType>();
		for(int i=0;i<soldiers;i++){
			DevCardType card = DevCardType.SOLDIER;
			cards.add(card);
		}
		devCardList.put(DevCardType.SOLDIER, soldiers);
		
		for(int i=0;i<monuments;i++){
			DevCardType card = DevCardType.MONUMENT;
			cards.add(card);
		}
		devCardList.put(DevCardType.MONUMENT, monuments);
		
		for(int i=0;i<monopoly;i++){
			DevCardType card = DevCardType.YEAR_OF_PLENTY;
			DevCardType card1 = DevCardType.ROAD_BUILD;
			DevCardType card2 = DevCardType.MONOPOLY;
			cards.add(card);
			cards.add(card1);
			cards.add(card2);
		}
		devCardList.put(DevCardType.YEAR_OF_PLENTY, YoP);
		devCardList.put(DevCardType.ROAD_BUILD, roadBuilding);
		devCardList.put(DevCardType.MONOPOLY, monopoly);
		
		return shuffle(cards);
	}
	
	/**
	 * Shuffles cards into random order and pushes them onto a stack.
	 * @param cards List of cards from createDeck().
	 * @return Stack that will be used for deck.
	 */
	private Stack<DevCardType> shuffle(List<DevCardType> cards){
		int deckSize = cards.size();
		Random randomGenerator = new Random();
		Stack<DevCardType> newDeck = new Stack<DevCardType>();
		for(int i=0;i<deckSize;i++){
			int number = randomGenerator.nextInt(cards.size());
			newDeck.push(cards.get(number));
			cards.remove(number);
		}
		return newDeck;
	}

	public Stack<DevCardType> getDeck() {
		return deck;
	}

	public void setDeck(Stack<DevCardType> deck) {
		this.deck = deck;
	}

	public Map<DevCardType, Integer> getDevCardList() {
		return devCardList;
	}

	public void setSize(Map<DevCardType, Integer> devCardList) {
		this.devCardList = devCardList;
	}
	
	public JsonObject toJson(){
		JsonObject result = new JsonObject();
		Map<DevCardType, Integer> remainingCards = new HashMap<DevCardType, Integer>();
		for(DevCardType type: deck){
			remainingCards.put(type, remainingCards.get(type)+1);
		}
//		for(DevCardType type: DevCardType.values()){
//			result.addProperty(type.getValue(), remainingCards.get(type));
//		}
		return result;
	}
}
