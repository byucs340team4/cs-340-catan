package gamemanager;

import java.util.Observable;

import client.data.GameInfo;
import client.data.PlayerInfo;
import client.data.RobPlayerInfo;
import shared.definitions.PieceType;
import shared.locations.EdgeLocation;
import shared.locations.HexLocation;
import shared.locations.VertexLocation;
import shared.proxy.Proxy;
import gamemanager.game.Game;
import gamemanager.gamestate.GamePlayState;
import gamemanager.gamestate.GameState;
import gamemanager.gamestate.JoinState;
import gamemanager.gamestate.PlayerWaitingState;
import gamemanager.gamestate.SetupState;

/**
 * 
 * @author Wesley
 *
 */

public class GameManager extends Observable{
	private Game game;
	private Poller poller;
	private Proxy proxy;
	private GameState gameState;
	private static GameManager instance;
	private PlayerInfo localPlayer;
	private GameInfo localGame;
	
	public static GameManager getInstance(){
		if(instance == null){
			instance = new GameManager();
			return instance;
		}else{
			return instance;
		}
	}
	
	/**
	 * 
	 */
	public void createGame() {

	}
	
	public boolean canPlaceRoad(EdgeLocation edgeLoc){
		//will return gameState.canPlaceRoad 
			return gameState.canPlaceRoad(edgeLoc,localPlayer.getPlayerIndex()); //should only do something in the playing and setup states
	}
	
	public boolean canPlaceSettlement(VertexLocation vertLoc){
		
			return gameState.canPlaceSettlement(vertLoc, localPlayer.getPlayerIndex());
	}
	
	public boolean canPlaceCity(VertexLocation vertLoc){
		
			return gameState.canPlaceCity(vertLoc, localPlayer.getPlayerIndex());
	}
	
	public boolean canPlaceRobber(HexLocation hexLoc){
		return gameState.canPlaceRobber(hexLoc);
	}
	
	//should call proxy.placeRoad, then replace the game model to the returned data from server then 
	//notify the observer classes
	public boolean placeRoad(EdgeLocation edgeLoc){
			
		return gameState.placeRoad(edgeLoc);	
	
	}
	
	public boolean placeSettlement(VertexLocation vertLoc){
		
			return gameState.placeSettlement(vertLoc);
	
	}
	
	public boolean placeCity(VertexLocation vertLoc){
			return gameState.placeCity(vertLoc);
	}
	
	public boolean placeSoldier(HexLocation hexLoc){
			return gameState.placeRobber(hexLoc);
	}
	
	public void robSoldier(RobPlayerInfo victim){
		//gameState.robPlayer(victim);
	}
	
	//for placeCity and placeSettlement do the same as placeRoad.
	
	//have can do and do methods for every posssible move type
	
	public boolean startMove(PieceType pieceType,boolean isFree){
		//will return gameState.startMove
		if(!isTurn())
			return false;
		else
			return gameState.startMove(pieceType,localPlayer.getPlayerIndex(),isFree);
	}
	
	public boolean isTurn(){
		return (localPlayer.getPlayerIndex()==game.getTurnTracker().getCurrentTurn());
	}

	public Game getGame() {
		return game;
	}
	
	public PlayerInfo getLocalPlayer(){
		return localPlayer;
	}
	
	public void setLocalPlayer(PlayerInfo playerInfo){
		localPlayer = playerInfo;
	}

	public void setGame(Game game) {
		// call notifyObservers in here
		synchronized (this) {
			if (game != null && (this.game == null || this.game.getVersion() != game.getVersion())) {
				this.game = game;
				
				if(game.getTurnTracker().getStatus().equals("Playing") || game.getTurnTracker().getStatus().equals("Rolling")|| game.getTurnTracker().getStatus().equals("Robbing")){
					this.setGameState(new GamePlayState(null));
				}
				// have some kind of method here that switches the game state
				// based
				// on the turn tracker
				// only need it to switch back to GamePlayState, should switch
				// on
				// its own from endTurn to playerWaiting

				setChanged();
				System.out.println("observers = " + this.countObservers());
				notifyObservers();
			}
			if (gameState instanceof PlayerWaitingState && game.getNumberOfPlayers() == 4) {
				this.game = game;
				this.setGameState(new SetupState(game));
				setChanged();
				notifyObservers();
			}
			if (gameState instanceof JoinState) {
				if(this.game != null && game.getNumberOfPlayers()==4)
				{
					setGameState(new SetupState(null));
				}
				setChanged();
				notifyObservers();
			}
		}
		
		if(gameState instanceof PlayerWaitingState){
			this.game = game;
			setChanged();
			notifyObservers();
		}
	}

	public Poller getPoller() {
		return poller;
	}

	public void setPoller(Poller poller) {
		this.poller = poller;
	}

	public Proxy getProxy() {
		return proxy;
	}

	public void setProxy(Proxy proxy) {
		this.proxy = proxy;
	}

	public GameState getGameState() {
		return gameState;
	}

	public void setGameState(GameState gameState) {
		this.gameState = gameState;
	}

	public GameInfo getLocalGame() {
		return localGame;
	}

	public void setLocalGame(GameInfo localGame) {
		this.localGame = localGame;
	}
	
}
