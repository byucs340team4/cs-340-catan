package gamemanager;

import java.util.Timer;
import java.util.TimerTask;

import gamemanager.game.Game;
import shared.proxy.IProxy;
import shared.proxy.Proxy;

/**
 * 
 * @author Wesley
 *
 */
public class Poller {
	
	IProxy proxy;
	Game game;
	public Poller(){
		
	}
	
	public void setProxy(IProxy proxy){
		this.proxy = proxy;
	}
	
	/**
	 * @return 
	 * 
	 */
	public void poll() {
		Timer timer = new Timer(true);
		timer.scheduleAtFixedRate(new TaskExampleRepeating(this), 500, 1000);
	}

	public Game getGame() {
		return game;
	}

	public void setGame(Game game) {
		this.game = game;
	} 

	public IProxy getProxy() {
		return proxy;
	}
	
}
class TaskExampleRepeating extends TimerTask{
	Poller poller;
    public TaskExampleRepeating(Poller poller) {
		this.poller = poller;
		run();
	}

	//This task will repeat every four seconds
    public void run(){
        poller.game = poller.proxy.gameModel();
        //System.out.println("Poller polling");
        GameManager.getInstance().setGame(poller.game);
        //cancel();
    }
}