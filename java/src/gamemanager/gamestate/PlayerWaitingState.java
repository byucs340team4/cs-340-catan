package gamemanager.gamestate;

import gamemanager.GameManager;
import gamemanager.game.Game;
import shared.definitions.PieceType;
import shared.locations.EdgeLocation;
import shared.locations.HexLocation;
import shared.locations.VertexLocation;
import shared.proxy.request.AcceptTrade;
import shared.proxy.request.BuyDevCardRequest;
import shared.proxy.request.CreateGameRequests;
import shared.proxy.request.Credentials;
import shared.proxy.request.JoinGameRequest;
import shared.proxy.request.MaritimeTrade;
import shared.proxy.request.OfferTrade;
import client.data.RobPlayerInfo;

public class PlayerWaitingState extends GameState{

	public PlayerWaitingState(Game game) {
		super(game);
		// TODO Auto-generated constructor stub
	}

	@Override
	public boolean canPlaceSettlement(VertexLocation vertLoc, int playerIndex) {
		return false;
	}

	@Override
	public boolean canPlaceCity(VertexLocation vertLoc, int playerIndex) {
		return false;
	}

	@Override
	public boolean startMove(PieceType pieceType, int playerIndex,
			boolean isFree) {
		return false;
	}

	@Override
	public boolean playRoadBuildingCard(EdgeLocation edgeLoc) {
		return false;
	}

	@Override
	public boolean createGame(CreateGameRequests request) {
		return false;
	}

	@Override
	public boolean joinGame(JoinGameRequest request) {
		return false;
	}

	@Override
	public boolean buyDevCard(BuyDevCardRequest request) {
		return false;
	}

	@Override
	public boolean login(Credentials credentials) {
		return false;
	}

	@Override
	public boolean register(Credentials cred) {
		return false;
	}

/*	@Override
	public GameState nextState() {
		if(game.getTurnTracker().getStatus().equals("FirstRound") || game.getTurnTracker().getStatus().equals("SecondRound")) {
			return new SetupState(game);
		}
		else {
			return new GamePlayState(game);
		}
	}*/

	@Override
	public boolean canPlaceRoad(EdgeLocation edgeLoc, int playerIndex) {
		return false;
	}

	@Override
	public boolean canPlaceRobber(HexLocation hexLoc) {
		return false;
	}

	@Override
	public boolean placeRoad(EdgeLocation edgeLoc) {
		return false;
	}

	@Override
	public boolean placeSettlement(VertexLocation vertLoc) {
		return false;
	}

	@Override
	public boolean placeCity(VertexLocation vertLoc) {
		return false;
	}

	@Override
	public boolean placeRobber(HexLocation hexLoc) {
		return false;
	}

	@Override
	public boolean playSoldierCard() {
		return false;
	}


	@Override
	public boolean robPlayer(RobPlayerInfo victim, HexLocation hexLoc) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean maritimeTrade(MaritimeTrade trade) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public int getRolledValue() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void domesticTrade(OfferTrade offer) {
		return;
	}

	@Override
	public void acceptTrade(AcceptTrade accept) {
		return;
	}

}
