package gamemanager.gamestate;

import gamemanager.GameManager;
import gamemanager.game.Game;
import gamemanager.map.Hex;
import gamemanager.map.Road;
import shared.definitions.PieceType;
import shared.locations.EdgeLocation;
import shared.locations.HexLocation;
import shared.locations.VertexLocation;
import shared.proxy.MoveType;
import shared.proxy.Proxy;
import shared.proxy.request.AcceptTrade;
import shared.proxy.request.BuildCity;
import shared.proxy.request.BuildRoad;
import shared.proxy.request.BuildSettlement;
import shared.proxy.request.BuyDevCardRequest;
import shared.proxy.request.CreateGameRequests;
import shared.proxy.request.Credentials;
import shared.proxy.request.JoinGameRequest;
import shared.proxy.request.MaritimeTrade;
import shared.proxy.request.OfferTrade;
import shared.proxy.request.RoadBuildingRequest;
import shared.proxy.request.RobPlayerRequest;
import shared.proxy.request.Soldier;
import client.data.RobPlayerInfo;

public class GamePlayState extends GameState{
	
	private BuildRoad roadRequest;
	private RoadBuildingRequest roadBuilderRequest;
	private BuildSettlement settlementRequest;
	private BuildCity cityRequest;
	private Soldier soldierRequest;
	private int rollValue = -1;
	
	public GamePlayState(Game game) {
		super(game);
	}

	@Override
	public boolean canPlaceRoad(EdgeLocation edgeLoc,int playerIndex) {
		System.out.println(GameManager.getInstance().getGame().getMap().canBuildRoad(edgeLoc, playerIndex, false));
		return GameManager.getInstance().getGame().getMap().canBuildRoad(edgeLoc, playerIndex, false);//not in setup
	}

	@Override
	public boolean canPlaceSettlement(VertexLocation vertLoc, int playerIndex) {
		return GameManager.getInstance().getGame().getMap().canBuildSettlement(vertLoc, playerIndex, false);
	}

	@Override
	public boolean canPlaceCity(VertexLocation vertLoc, int playerIndex) {
		return GameManager.getInstance().getGame().getMap().canBuildCity(vertLoc, playerIndex);
	}

	@Override
	public boolean canPlaceRobber(HexLocation hexLoc) {
		Hex currHex = GameManager.getInstance().getGame().getMap().getLocation(hexLoc);
		return(!GameManager.getInstance().getGame().getMap().getRobber().equals(hexLoc) && currHex!=null);
	}

	@Override
	public boolean placeRoad(EdgeLocation edgeLoc) {
		if(roadBuilderRequest != null){
			return buildRoadBuildingCard(edgeLoc);
		}	
		
		roadRequest.setLocation(edgeLoc);
		Game newModel = GameManager.getInstance().getProxy().buildRoad(roadRequest);
		System.out.println(newModel);
		if(newModel == null)
			return false;
		//if(newModel.getVersion() == GameManager.getInstance().getGame().getVersion())
			//return false;
		
		//GameManager.getInstance().setGame(newModel);
		return true;
		
	}
	//have to be logged in and joined to a game for the proxy commands to work
	@Override
	public boolean placeSettlement(VertexLocation vertLoc) {
		settlementRequest.setLocation(vertLoc);
		Game newModel = GameManager.getInstance().getProxy().buildSettlement(settlementRequest);
		if(newModel == null)
			return false;
		//if(newModel.getVersion() == GameManager.getInstance().getGame().getVersion())
			//return false;
		
		//GameManager.getInstance().setGame(newModel);
		return true;
		
	}

	@Override
	public boolean placeCity(VertexLocation vertLoc) {
		cityRequest.setLocation(vertLoc);
		Game newModel = GameManager.getInstance().getProxy().buildCity(cityRequest);
		System.out.println("Calls proxy");
		if(newModel == null)
			return false;
		//if(newModel.getVersion() == GameManager.getInstance().getGame().getVersion())
			//return false;
		
		//GameManager.getInstance().setGame(newModel);
		return true;
	}

	@Override
	public boolean placeRobber(HexLocation hexLoc) {
		soldierRequest.setLocation(hexLoc);
		
		return true;
	}

	@Override
	public boolean startMove(PieceType pieceType,int playerIndex, boolean isFree) {
		if(!isFree){//not sure why this if is here exactly...you can probly remove it
			if(pieceType == PieceType.ROAD){
				roadRequest = new BuildRoad(playerIndex, null, isFree);
				return GameManager.getInstance().getGame().getPlayer(playerIndex).canBuildRoad();
			}
			else if(pieceType == PieceType.SETTLEMENT){
				settlementRequest = new BuildSettlement(playerIndex, null, isFree);
				return GameManager.getInstance().getGame().getPlayer(playerIndex).canBuildSettlement();
			}
			else if(pieceType == PieceType.CITY){
				cityRequest = new BuildCity(playerIndex, null);
				return GameManager.getInstance().getGame().getPlayer(playerIndex).canBuildCity();
			}
			else if(pieceType == PieceType.ROBBER){
				soldierRequest = new Soldier(playerIndex, -1, null);
				return GameManager.getInstance().getGame().getPlayer(playerIndex).canPlaySoldier();
			}
		}
		else if(isFree){
			//check if they have a road building card
			if(pieceType == PieceType.ROBBER)
				return true;
			if(GameManager.getInstance().getGame().getPlayer(playerIndex).canPlayRoadBuilding() && pieceType==PieceType.ROAD){
				if(roadBuilderRequest == null)
					roadBuilderRequest = new RoadBuildingRequest(MoveType.ROAD_BUILDING, playerIndex, null, null);
				
				return true;
			}
			else 
				return false;
		}
		return true;
	}

	@Override
	public boolean playSoldierCard() {
		return false;
		//does nothing in this state
	}
	
	@Override 
	public boolean playRoadBuildingCard(EdgeLocation edgeLoc){
		Game newModel = GameManager.getInstance().getProxy().roadBuilding(roadBuilderRequest);
		roadBuilderRequest = null;
		System.out.println("Calls proxy");
		if(newModel == null){
			//remove the two road you built
			GameManager.getInstance().getGame().getMap().removeLastTwoRoads();
			GameManager.getInstance().setGame(GameManager.getInstance().getGame());
			return false;
		}
		else{//(newModel.getVersion() == GameManager.getInstance().getGame().getVersion()){
			//remove the two roads you built
			//GameManager.getInstance().getGame().getMap().removeLastTwoRoads();
			//GameManager.getInstance().setGame(GameManager.getInstance().getGame());
			//GameManager.getInstance().setGame(newModel);
			return true;
		}
		
		
	}
	

	public boolean buildRoadBuildingCard(EdgeLocation edgeLoc) {
		if(roadBuilderRequest.getSpot1()==null){
			roadBuilderRequest.setSpot1(edgeLoc);
			GameManager.getInstance().getGame().getMap().addRoad(new Road(edgeLoc,GameManager.getInstance().getLocalPlayer().getPlayerIndex()));
			if(GameManager.getInstance().getGame().getPlayer(GameManager.getInstance().getLocalPlayer().getPlayerIndex()).getRoads()==1){
				edgeLoc = GameManager.getInstance().getGame().getMap().getRoadLoc(GameManager.getInstance().getLocalPlayer().getPlayerIndex());
				roadBuilderRequest.setSpot2(edgeLoc);
				return playRoadBuildingCard(edgeLoc);
				//proxy
			}
			return true;
		}
		else{
			roadBuilderRequest.setSpot2(edgeLoc);
			GameManager.getInstance().getGame().getMap().addRoad(new Road(edgeLoc,GameManager.getInstance().getLocalPlayer().getPlayerIndex()));
			return playRoadBuildingCard(edgeLoc);			
		}
	}

	public boolean robWithSoldier(RobPlayerInfo victim,HexLocation hexLoc){
		
		soldierRequest.setVictimIndex(victim.getPlayerIndex());
		soldierRequest.setLocation(hexLoc);
	
		Game newModel = GameManager.getInstance().getProxy().soldier(soldierRequest);
		if(newModel == null){
			GameManager.getInstance().setGame(GameManager.getInstance().getGame());
			return false;
		}
		else{//else if(newModel.getVersion() != GameManager.getInstance().getGame().getVersion()){
			//GameManager.getInstance().setGame(newModel);
			soldierRequest = null;
			return true;
		}
		
	}
	
	@Override
	public boolean robPlayer(RobPlayerInfo victim, HexLocation hexLoc) {
		System.out.println("Robbing " + victim.getName());
		if(soldierRequest!=null){
			return robWithSoldier(victim,hexLoc);
		}
		
		RobPlayerRequest robRequest = new RobPlayerRequest(MoveType.ROB_PLAYER, GameManager.getInstance().getLocalPlayer().getPlayerIndex(), victim.getPlayerIndex(), hexLoc);
		
		Game newModel = GameManager.getInstance().getProxy().robPlayer(robRequest);
		
		if(newModel == null){
			GameManager.getInstance().setGame(GameManager.getInstance().getGame());
			return false;
		}
		else{//else if(newModel.getVersion() != GameManager.getInstance().getGame().getVersion()){
			Game currModel = GameManager.getInstance().getGame();
			currModel.getTurnTracker().setStatus("Playing");
			synchronized(GameManager.getInstance()){
				GameManager.getInstance().setGame(newModel);
			}
			return true;
		}
		
	}

	@Override
	public boolean createGame(CreateGameRequests request) {
		return false;
	}

	@Override
	public boolean joinGame(JoinGameRequest request) {
		return false;
	}

	@Override
	public boolean buyDevCard(BuyDevCardRequest request) {
		if(GameManager.getInstance().getGame().getPlayer(request.getPlayerIndex()).canBuyDevCard()){
			Game newModel = GameManager.getInstance().getProxy().buyDevCard(request);
			System.out.println("Theoritically bought card.");
			System.out.println(GameManager.getInstance().getGame().getPlayer(GameManager.getInstance().getLocalPlayer().getPlayerIndex()));
	
			if(newModel == null){
				//GameManager.getInstance().setGame(GameManager.getInstance().getGame());
				return false;
			}
			else{//else if(newModel.getVersion() != GameManager.getInstance().getGame().getVersion()){
				//GameManager.getInstance().setGame(newModel);
				return true;
			}
			
		}
		
		return false;
	}

	@Override
	public boolean login(Credentials credentials) {
		return false;
	}

	@Override
	public boolean register(Credentials cred) {
		return false;
	}
	
	public boolean maritimeTrade(MaritimeTrade trade){
		Game game = GameManager.getInstance().getProxy().maritimeTrade(trade);
		//GameManager.getInstance().setGame(game);
		return true;
	}
	
	public void domesticTrade(OfferTrade offer){
		Proxy proxy = GameManager.getInstance().getProxy();
		Game game = proxy.offerTrade(offer);
		//GameManager.getInstance().setGame(game);
	}

	//@Override
	//public GameState nextState() {
	//	return null;
//	}

	@Override
	public int getRolledValue() {
		return rollValue;
	}
	
	public void setRolledValue(int rollValue){
		this.rollValue = rollValue;
	}

	@Override
	public void acceptTrade(AcceptTrade accept) {
		Proxy proxy = GameManager.getInstance().getProxy();
		Game game = proxy.acceptTrade(accept);
		//GameManager.getInstance().setGame(game);
	}

}
