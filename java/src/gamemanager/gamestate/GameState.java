package gamemanager.gamestate;

import gamemanager.GameManager;
import gamemanager.game.Game;
import shared.definitions.PieceType;
import shared.locations.EdgeLocation;
import shared.locations.HexLocation;
import shared.locations.VertexLocation;
import shared.proxy.request.BuyDevCardRequest;
import shared.proxy.MoveType;
import shared.proxy.request.AcceptTrade;
import shared.proxy.request.CreateGameRequests;
import shared.proxy.request.Credentials;
import shared.proxy.request.FinishTurnRequest;
import shared.proxy.request.JoinGameRequest;
import shared.proxy.request.MaritimeTrade;
import shared.proxy.request.OfferTrade;
import shared.proxy.request.SendChatRequest;
import client.data.RobPlayerInfo;

public abstract class GameState {
	
	protected Game game;

	public GameState(Game game) {
		this.game = game;
	}
	/**
	 * This method is called whenever the user is trying to place a road on the
	 * map. It is called by the view for each "mouse move" event. The returned
	 * value tells the view whether or not to allow the road to be placed at the
	 * specified location.
	 * 
	 * @param edgeLoc
	 *            The proposed road location
	 * @return true if the road can be placed at edgeLoc, false otherwise
	 */
	//based on the state it is in, this will return map.canBuildRoad
	public abstract boolean canPlaceRoad(EdgeLocation edgeLoc,int playerIndex);
	
	/**
	 * This method is called whenever the user is trying to place a settlement
	 * on the map. It is called by the view for each "mouse move" event. The
	 * returned value tells the view whether or not to allow the settlement to
	 * be placed at the specified location.
	 * 
	 * @param vertLoc
	 *            The proposed settlement location
	 * @return true if the settlement can be placed at vertLoc, false otherwise
	 */
	public abstract boolean canPlaceSettlement(VertexLocation vertLoc,int playerIndex);
	
	/**
	 * This method is called whenever the user is trying to place a city on the
	 * map. It is called by the view for each "mouse move" event. The returned
	 * value tells the view whether or not to allow the city to be placed at the
	 * specified location.
	 * 
	 * @param vertLoc
	 *            The proposed city location
	 * @return true if the city can be placed at vertLoc, false otherwise
	 */
	public abstract boolean canPlaceCity(VertexLocation vertLoc, int playerIndex);
	
	/**
	 * This method is called whenever the user is trying to place the robber on
	 * the map. It is called by the view for each "mouse move" event. The
	 * returned value tells the view whether or not to allow the robber to be
	 * placed at the specified location.
	 * 
	 * @param hexLoc
	 *            The proposed robber location
	 * @return true if the robber can be placed at hexLoc, false otherwise
	 */
	public abstract boolean canPlaceRobber(HexLocation hexLoc);
	
	/**
	 * This method is called when the user clicks the mouse to place a road.
	 * 
	 * @param edgeLoc
	 *            The road location
	 */
	public abstract boolean placeRoad(EdgeLocation edgeLoc);
	
	/**
	 * This method is called when the user clicks the mouse to place a
	 * settlement.
	 * 
	 * @param vertLoc
	 *            The settlement location
	 */
	public abstract boolean placeSettlement(VertexLocation vertLoc);
	
	/**
	 * This method is called when the user clicks the mouse to place a city.
	 * 
	 * @param vertLoc
	 *            The city location
	 */
	public abstract boolean placeCity(VertexLocation vertLoc);
	
	/**
	 * This method is called when the user clicks the mouse to place the robber.
	 * 
	 * @param hexLoc
	 *            The robber location
	 */
	public abstract boolean placeRobber(HexLocation hexLoc);
	
	/**
	 * This method is called when the user requests to place a piece on the map
	 * (road, city, or settlement)
	 * 
	 * @param pieceType
	 *            The type of piece to be placed
	 * @param isFree
	 *            true if the piece should not cost the player resources, false
	 *            otherwise. Set to true during initial setup and when a road
	 *            building card is played.
	 * @param allowDisconnected
	 *            true if the piece can be disconnected, false otherwise. Set to
	 *            true only during initial setup.
	 */
	//will call player.canPlace Road,City,Settlement,etc. with is free depending on the state
	//and return if it can or not
	public abstract boolean startMove(PieceType pieceType, int playerIndex, boolean isFree);

	
	/**
	 * This method is called when the user plays a "soldier" development card.
	 * It should initiate robber placement.
	 */
	public abstract boolean playSoldierCard();
	
	/**
	 * This method is called when the user plays a "road building" progress
	 * development card. It should initiate the process of allowing the player
	 * to place two roads.
	 */
	public abstract boolean playRoadBuildingCard(EdgeLocation edgeLoc);
	
	/**
	 * This method is called by the Rob View when a player to rob is selected
	 * via a button click.
	 * 
	 * @param victim
	 *            The player to be robbed
	 */
	public abstract boolean robPlayer(RobPlayerInfo victim, HexLocation hexLoc);
	
	public abstract boolean createGame(CreateGameRequests request);
	
	public abstract boolean joinGame(JoinGameRequest request);
	
	public abstract boolean buyDevCard(BuyDevCardRequest request);
	
	public abstract boolean login(Credentials credentials);
	
	public abstract boolean register(Credentials cred);
	
	public abstract boolean maritimeTrade(MaritimeTrade trade);
	
	public abstract void domesticTrade(OfferTrade offer);
	
	public abstract void acceptTrade(AcceptTrade accept);
	
	//public abstract GameState nextState();
	
	public void sendChat(String message) {
		int id = GameManager.getInstance().getLocalPlayer().getPlayerIndex();
		SendChatRequest request = new  SendChatRequest(id, message);
		Game newModel = GameManager.getInstance().getProxy().sendChat(request);
		if (newModel == null){
			return;
		}
		else{
			if(GameManager.getInstance().getGame().getVersion() != newModel.getVersion()){
				GameManager.getInstance().setGame(newModel);
			}
		}
	}
	
	public void endTurn(){
		int playerIndex = GameManager.getInstance().getLocalPlayer().getPlayerIndex();
		FinishTurnRequest request = new FinishTurnRequest(MoveType.FINISH_TURN,playerIndex);
		
		Game newModel = GameManager.getInstance().getProxy().finishTurn(request);
		if (newModel == null){
			return;
		}
		else{
			if(GameManager.getInstance().getGame().getVersion() != newModel.getVersion()){
				GameManager.getInstance().getLocalPlayer().setRolledNumber(-1);
				GameManager.getInstance().setGame(newModel);
			}
		}
	}
	
	public abstract int getRolledValue();
		
}
