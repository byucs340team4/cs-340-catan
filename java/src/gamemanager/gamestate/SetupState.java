package gamemanager.gamestate;

import gamemanager.GameManager;
import gamemanager.game.Game;
import shared.definitions.PieceType;
import shared.locations.EdgeLocation;
import shared.locations.HexLocation;
import shared.locations.VertexLocation;
import shared.proxy.request.AcceptTrade;
import shared.proxy.request.BuildCity;
import shared.proxy.request.BuildRoad;
import shared.proxy.request.BuildSettlement;
import shared.proxy.request.BuyDevCardRequest;
import shared.proxy.request.CreateGameRequests;
import shared.proxy.request.Credentials;
import shared.proxy.request.JoinGameRequest;
import shared.proxy.request.MaritimeTrade;
import shared.proxy.request.OfferTrade;
import shared.proxy.request.Soldier;
import client.data.RobPlayerInfo;

public class SetupState extends GameState{
	
	private BuildRoad roadRequest;
	private BuildSettlement settlementRequest;
	private int numSettlements;
	private int numRoads;
	
	public SetupState(Game game) {
		super(game);
		// TODO Auto-generated constructor stub
		numSettlements = 5 - GameManager.getInstance().getGame().getPlayer(GameManager.getInstance().getLocalPlayer().getPlayerIndex()).getSettlements();
		numRoads = 15 - GameManager.getInstance().getGame().getPlayer(GameManager.getInstance().getLocalPlayer().getPlayerIndex()).getRoads();
		if(numRoads>=2)
			GameManager.getInstance().setGameState(new GamePlayState(null));
	}

	@Override
	public boolean canPlaceRoad(EdgeLocation edgeLoc,int playerIndex) {
		// TODO Auto-generated method stub
		return GameManager.getInstance().getGame().getMap().canBuildRoad(edgeLoc, playerIndex, true);//in setup
	}
	
	public int getNumRoads(){
		return numRoads;
	}
	
	public int getNumSettlements(){
		return numSettlements;
	}

	@Override
	public boolean canPlaceSettlement(VertexLocation vertLoc, int playerIndex) {
		// TODO Auto-generated method stub
		return GameManager.getInstance().getGame().getMap().canBuildSettlement(vertLoc, playerIndex, true);//in setup
	}

	@Override
	public boolean canPlaceCity(VertexLocation vertLoc, int playerIndex) {
		// TODO Auto-generated method stub
		//cannot build city in setup state
		return false;
	}

	@Override
	public boolean canPlaceRobber(HexLocation hexLoc) {
		// TODO Auto-generated method stub
		//cannot move robber in setup state
		return false;
	}

	@Override
	public boolean placeRoad(EdgeLocation edgeLoc) {
		// TODO Auto-generated method stub
		roadRequest.setLocation(edgeLoc);
		Game newModel = GameManager.getInstance().getProxy().buildRoad(roadRequest);
		if(newModel == null)
			return false;
		
		else{//if(newModel.getVersion() != GameManager.getInstance().getGame().getVersion()){
			numRoads++;
			Game currGame = GameManager.getInstance().getGame();
		    currGame.getTurnTracker().setCurrentTurn(-1);
			this.endTurn();
			
			
			
			if(numRoads==2){ 
				GameManager.getInstance().setGameState(new GamePlayState(null));
				
			}
				
			
			System.out.println("End turn in setupstate");
			return true;
		}
		
		//return false;
	}
	//have to be logged in and joined to a game for the proxy commands to work
	@Override
	public boolean placeSettlement(VertexLocation vertLoc) {
		settlementRequest.setLocation(vertLoc);
		Game newModel = GameManager.getInstance().getProxy().buildSettlement(settlementRequest);
		if(newModel==null)
			return false;
		
		else//if(newModel.getVersion() != GameManager.getInstance().getGame().getVersion())
		{
			numSettlements++;
			//GameManager.getInstance().setGame(newModel);
			return true;
		}
		
		//return false;
		// TODO Auto-generated method stub	
	}

	@Override
	public boolean placeCity(VertexLocation vertLoc) {
		// TODO Auto-generated method stub
		//cannot build city in setup state
		return false;
	}

	@Override
	public boolean placeRobber(HexLocation hexLoc) {
		// TODO Auto-generated method stub
		//cannot move robber in setup state
		return false;
	}

	@Override
	public boolean startMove(PieceType pieceType,int playerIndex, boolean isFree) {
		// TODO Auto-generated method stub
		if(pieceType == PieceType.ROAD && numRoads<numSettlements){
			roadRequest = new BuildRoad(playerIndex, null, true);
			return true;
		}
		else if(pieceType == PieceType.SETTLEMENT && numRoads==numSettlements){
			settlementRequest = new BuildSettlement(playerIndex, null, true);
			return true;	
		}
		
		return false;
	}

	@Override
	public boolean playSoldierCard() {
		// TODO Auto-generated method stub
		//Do nothing. Cannot play dev cards in setup state
		return false;
	}

	@Override
	public boolean playRoadBuildingCard(EdgeLocation edgeLoc) {
		// TODO Auto-generated method stub
		//Do nothing. Cannot play dev cards in setup state
		return false;
	}


	@Override
	public boolean robPlayer(RobPlayerInfo victim, HexLocation hexLoc) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean createGame(CreateGameRequests request) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean joinGame(JoinGameRequest request) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean buyDevCard(BuyDevCardRequest request) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean login(Credentials credentials) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean register(Credentials cred) {
		// TODO Auto-generated method stub
		return false;
	}

/*	@Override
	public GameState nextState() {
		// TODO Auto-generated method stub
		return null;
	}*/

	@Override
	public boolean maritimeTrade(MaritimeTrade trade) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public int getRolledValue() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void domesticTrade(OfferTrade offer) {
		return;
	}

	@Override
	public void acceptTrade(AcceptTrade accept) {
		return;
	}

}
