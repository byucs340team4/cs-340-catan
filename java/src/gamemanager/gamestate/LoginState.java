package gamemanager.gamestate;

import gamemanager.GameManager;
import gamemanager.game.Game;
import shared.definitions.PieceType;
import shared.locations.EdgeLocation;
import shared.locations.HexLocation;
import shared.locations.VertexLocation;
import shared.proxy.request.AcceptTrade;
import shared.proxy.request.BuyDevCardRequest;
import shared.proxy.request.CreateGameRequests;
import shared.proxy.request.Credentials;
import shared.proxy.request.JoinGameRequest;
import shared.proxy.request.MaritimeTrade;
import shared.proxy.request.OfferTrade;
import shared.proxy.response.LoginResponse;
import client.data.PlayerInfo;
import client.data.RobPlayerInfo;

public class LoginState extends GameState{

	public LoginState(Game game) {
		super(game);
		// TODO Auto-generated constructor stub
	}

	@Override
	public
	boolean canPlaceRobber(HexLocation hexLoc) {
		return false;
	}

	@Override
	public boolean canPlaceRoad(EdgeLocation edgeLoc, int playerIndex) {
		return false;
	}

	@Override
	public boolean canPlaceSettlement(VertexLocation vertLoc, int playerIndex) {
		return false;
	}

	@Override
	public boolean canPlaceCity(VertexLocation vertLoc, int playerIndex) {
		return false;
	}

	@Override
	public boolean startMove(PieceType pieceType, int playerIndex,
			boolean isFree) {
		return false;
	}

	@Override
	public boolean playRoadBuildingCard(EdgeLocation edgeLoc) {
		return false;
	}

	@Override
	public boolean createGame(CreateGameRequests request) {
		return false;
	}

	@Override
	public boolean placeRoad(EdgeLocation edgeLoc) {
		return false;
	}

	@Override
	public boolean placeSettlement(VertexLocation vertLoc) {
		return false;
	}

	@Override
	public boolean placeCity(VertexLocation vertLoc) {
		return false;
	}

	@Override
	public boolean placeRobber(HexLocation hexLoc) {
		return false;
	}

	@Override
	public boolean playSoldierCard() {
		return false;
	}


	@Override
	public boolean robPlayer(RobPlayerInfo victim, HexLocation hexLoc) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean joinGame(JoinGameRequest request) {
		return false;
	}

	@Override
	public boolean buyDevCard(BuyDevCardRequest request) {
		return false;
	}

	@Override
	public boolean login(Credentials cred) {
		LoginResponse response = GameManager.getInstance().getProxy().userLogin(cred);
		if(response.isResponse()) {
			String user = response.getUsername();
			int id = response.getPlayerID();
			PlayerInfo playerInfo = new PlayerInfo();
			playerInfo.setName(user);
			playerInfo.setId(id);
			playerInfo.setPlayerIndex(-1);
			GameManager.getInstance().setLocalPlayer(playerInfo);
		}
		return response.isResponse();
	}

	@Override
	public boolean register(Credentials cred) {
		LoginResponse response = GameManager.getInstance().getProxy().userRegister(cred);
		return response.isResponse();
	}

/*	@Override
	public GameState nextState() {
		return new JoinState(game);
	}*/

	@Override
	public boolean maritimeTrade(MaritimeTrade trade) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public int getRolledValue() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void domesticTrade(OfferTrade offer) {
		return;		
	}

	@Override
	public void acceptTrade(AcceptTrade accept) {
		return;
	}

}
