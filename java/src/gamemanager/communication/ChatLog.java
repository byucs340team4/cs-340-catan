package gamemanager.communication;

import java.util.ArrayList;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

public class ChatLog extends Log{
	
	ArrayList<Message> log;

	
	public ChatLog() {
		super();
		log = new ArrayList<Message>();
	}
	/**
	 * 
	 * @param msg
	 */
	public void addMessage(Message msg)
	{
		lines.add(msg);
	}
	public void getNewMessage()
	{
		lines.get(lines.size()-1);
	}
	
	public String getPlayerNameOf(Message msg)
	{
		return msg.source;
	}
	
	public String getPlayerMessageOf(Message msg)
	{
		return msg.message;
	}
	
	public ArrayList<Message> readMessage()
	{
		return lines;
	}
	
	public JsonObject toJson(){
		JsonObject result = new JsonObject();
		JsonArray array = new JsonArray();
		for(Message message: lines){
			array.add(message.toJson());
		}
		return result;
	}
}
