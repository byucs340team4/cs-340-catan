package gamemanager.communication;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;


public class GameLog extends Log {
	
	public GameLog(){
		super();
	}
	/**
	 * 
	 * @param msg
	 */
	public void addMessage(Message msg){
		lines.add(msg);
	}
	
	public JsonObject toJson(){
		JsonObject result = new JsonObject();
		JsonArray array = new JsonArray();
		for(Message message: lines){
			array.add(message.toJson());
		}
		return result;
	}
}
