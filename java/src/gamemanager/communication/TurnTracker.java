package gamemanager.communication;

import java.util.Observable;

import com.google.gson.JsonObject;

public class TurnTracker extends Observable{
	private int currentTurn;
	private String status;
	private int longestRoad;
	private int largestArmy;
	public TurnTracker(){
		this.currentTurn = 0;
		this.status="FirstRound";
		this.longestRoad = -1;
		this.largestArmy = -1;
	}
	/**
	 * 
	 * @return 
	 */
	public int getCurrentTurn() {
		return currentTurn;
	}
	/**
	 * 
	 * @param index
	 */
	public void setCurrentTurn(int index) {
		this.currentTurn = index;
	}
	/**
	 * 
	 * @return 
	 */
	public String getStatus() {
		return status;
	}
	/**
	 * 
	 * @param status
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	/**
	 * 
	 * @return 
	 */
	public int getLongestRoad() {
		return longestRoad;
	}
	/**
	 * 
	 * @param index
	 */
	public void setLongestRoad(int index) {
		this.longestRoad = index;
	}
	/**
	 * 
	 * @return 
	 */
	public int getLargestArmy() {
		return largestArmy;
	}
	/**
	 * 
	 * @param index
	 */
	public void setLargestArmy(int index) {
		this.largestArmy = index;
	}
	
	public JsonObject toJson(){
		JsonObject result = new JsonObject();
		result.addProperty("currentTurn", this.currentTurn);
		result.addProperty("status", this.status);
		result.addProperty("longestRoad", this.longestRoad);
		result.addProperty("largestArmy", this.largestArmy);
		return result;
	}
}
