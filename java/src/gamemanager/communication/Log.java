package gamemanager.communication;

import java.util.ArrayList;
import java.util.Observable;

public class Log extends Observable{
	ArrayList<Message> lines;
	
	public Log(){
		lines = new ArrayList<Message>();
	}
	
	public ArrayList<Message> getLog(){
		return lines;
	}
}
