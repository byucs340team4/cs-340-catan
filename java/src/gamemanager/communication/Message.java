package gamemanager.communication;

import java.util.Observable;

import com.google.gson.JsonObject;

public class Message extends Observable{
	String source;
	String message;

	public Message() {
		
	}
	public Message(String aSource, String aMessage) {
		source = aSource;
		message = aMessage;
	}
	
	public void setSource(String aSource){
		source = aSource;
	}
	
	public String getSource(){
		return source;
	}
	
	public void setMessage(String aMessage){
		message = aMessage;
	}
	
	public String getMessage(){
		return message;
	}
	
	public JsonObject toJson(){
		JsonObject result = new JsonObject();
		result.addProperty("message", this.message);
		result.addProperty("source", this.source);
		return result;
	}
}
