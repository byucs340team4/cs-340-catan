package gamemanager.player;

import gamemanager.game.Game;

import java.util.HashMap;
import java.util.Map;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import shared.definitions.DevCardType;
import shared.definitions.ResourceType;

public class Hand {

	/**
	 * Contains information on how many of each resource the hand has.
	 */
	private Map<ResourceType, Integer> resources;
	/**
	 * Contains information on how many of each type of DevelopmentCard the hand has.
	 */
	private Map<DevCardType, Integer> newDevCards;

	private Map<DevCardType, Integer> oldDevCards;
	
	private Game game;
	
	/**
	 * Default constructor.  Initializes resources and devCards.
	 */
	public Hand(Game game){
		resources = new HashMap<>();
		resources.put(ResourceType.BRICK, 0);
		resources.put(ResourceType.ORE, 0);
		resources.put(ResourceType.SHEEP, 0);
		resources.put(ResourceType.WHEAT, 0);
		resources.put(ResourceType.WOOD, 0);
		newDevCards = new HashMap<>();
		newDevCards.put(DevCardType.MONOPOLY, 0);
		newDevCards.put(DevCardType.MONUMENT, 0);
		newDevCards.put(DevCardType.ROAD_BUILD, 0);
		newDevCards.put(DevCardType.SOLDIER, 0);
		newDevCards.put(DevCardType.YEAR_OF_PLENTY, 0);
		oldDevCards = new HashMap<>();
		oldDevCards.put(DevCardType.MONOPOLY, 0);
		oldDevCards.put(DevCardType.MONUMENT, 0);
		oldDevCards.put(DevCardType.ROAD_BUILD, 0);
		oldDevCards.put(DevCardType.SOLDIER, 0);
		oldDevCards.put(DevCardType.YEAR_OF_PLENTY, 0);
		this.game = game;
	}
	
	public Hand(JsonObject oldDevCards, JsonObject newDevCards,
			JsonObject resources) {
		Gson oldGson = new Gson();
		Gson newGson = new Gson();
		Gson resGson = new Gson();
		
		this.resources = new HashMap<>();
		this.resources.put(ResourceType.BRICK, oldGson.fromJson("brick",int.class));
		this.resources.put(ResourceType.ORE, resGson.fromJson("ore", int.class));
		this.resources.put(ResourceType.SHEEP, resGson.fromJson("sheep", int.class));
		this.resources.put(ResourceType.WHEAT, resGson.fromJson("wheat", int.class));
		this.resources.put(ResourceType.WOOD, resGson.fromJson("wood", int.class));
		this.newDevCards = new HashMap<>();
		this.newDevCards.put(DevCardType.MONOPOLY, newGson.fromJson("monopoly", int.class));
		this.newDevCards.put(DevCardType.MONUMENT, newGson.fromJson("monument", int.class));
		this.newDevCards.put(DevCardType.ROAD_BUILD, newGson.fromJson("roadBuilding", int.class));
		this.newDevCards.put(DevCardType.SOLDIER, newGson.fromJson("soldier", int.class));
		this.newDevCards.put(DevCardType.YEAR_OF_PLENTY, newGson.fromJson("yearOfPlenty", int.class));
		this.oldDevCards = new HashMap<>();
		this.oldDevCards.put(DevCardType.MONOPOLY, oldGson.fromJson("monopoly", int.class));
		this.oldDevCards.put(DevCardType.MONUMENT, oldGson.fromJson("monument", int.class));
		this.oldDevCards.put(DevCardType.ROAD_BUILD, oldGson.fromJson("roadBuilding", int.class));
		this.oldDevCards.put(DevCardType.SOLDIER, oldGson.fromJson("soldier", int.class));
		this.oldDevCards.put(DevCardType.YEAR_OF_PLENTY, oldGson.fromJson("yearOfPlenty", int.class));
	}

	/**
	 * 
	 * @param type
	 * @param amount
	 */
	public void addResources(ResourceType type, int amount) {
		int temp = resources.get(type);
		resources.put(type, temp+amount);
	}
	
	/**
	 * 
	 * @param type
	 */
	public void addDevCard(DevCardType type) {
		int temp = newDevCards.get(type);
		newDevCards.put(type, temp++);
	}
	/**
	 * Determines if resources has what is needed to build a road.
	 * @return
	 */
	public boolean canBuildRoad(){
		int brick = resources.get(ResourceType.BRICK);
		int wood = resources.get(ResourceType.WOOD);
		if(wood>0 && brick>0) {
			return true;
		}
		else {
			return false;
		}
	}
	
	/**
	 * Determines if resources has what is needed to build a settlement.
	 * @return
	 */
	public boolean canBuildSettlement(){
		int brick = resources.get(ResourceType.BRICK);
		int wood = resources.get(ResourceType.WOOD);
		int wheat = resources.get(ResourceType.WHEAT);
		int ore = resources.get(ResourceType.ORE);
		if (brick>0 && wood>0 && wheat>0 && ore>0) {
			return true;
		}
		else {
			return false;
		}
	}
	
	/**
	 * Determines if resources has what is needed to build a settlement.
	 * @return
	 */
	public boolean canBuildCity(){
		int ore = resources.get(ResourceType.ORE);
		int wheat = resources.get(ResourceType.WHEAT);
		if(ore>=3 && wheat >=2) {
			return true;
		}
		else {
			return false;
		}
	}

	public Map<ResourceType, Integer> getResources() {
		return resources;
	}

	public void setResources(Map<ResourceType, Integer> resources) {
		this.resources = resources;
	}

	public Map<DevCardType, Integer> getNewDevCards() {
		return newDevCards;
	}

	public void setNewDevCards(Map<DevCardType, Integer> devCards) {
		this.newDevCards = devCards;
	}
}
