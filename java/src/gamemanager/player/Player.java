package gamemanager.player;

import com.google.gson.JsonObject;

import gamemanager.GameManager;
import gamemanager.game.DevCardList;
import gamemanager.game.Game;
import gamemanager.gamestate.GamePlayState;
import cs340.model.player.ResourceHand;
import cs340.model.player.TradeOffer;
import shared.definitions.CatanColor;
import shared.definitions.DevCardType;
import shared.definitions.ResourceType;
import shared.proxy.request.MaritimeTrade;
import shared.proxy.request.YearOfPlentyRequest;

/**
 * 
 * @author Wesley
 *
 */
 
public class Player {
	private int playerID;
	private int playerIndex;
	private String name;
	private String color;
	private int soldiers;
	private int victoryPoints;
	private Boolean playedDevCard;
	private Boolean discarded;
	private int cities;
	private int settlements;
	private int roads;
	//private Hand hand;
	private ResourceList resources;
	private DevCardList oldDevCards;
	private DevCardList newDevCards;
	@SuppressWarnings("unused")
	private int monuments;
	//private Game game;
	
	/**
	 * 
	 * @param name
	 * @param color
	 * 
	 * @pre color == RED ORANGE YELLOW BLUE GREEN PURPLE PUCE WHITE or BROWN
	 */
	
	public Player(int playerID, int orderNumber, String name, String color, Game game) {
		this.name = name;
		this.color = color;
		this.playerID = playerID;
		this.playerIndex = orderNumber;
		soldiers = 0;
		victoryPoints = 0;
		playedDevCard = false;
		discarded = false;
		cities = 4;
		settlements = 5;
		roads = 15;
		//hand = new Hand(game);
	}
	
	public Player(int playerID, String name, String color, Game game){
		this.playerID = playerID;
		this.name = name;
		this.color = color;
		this.resources = new ResourceList();
		this.oldDevCards = new DevCardList();
		this.newDevCards = new DevCardList();
		this.roads = 15;
		this.cities = 4;
		this.settlements = 5;
		this.victoryPoints = 0;
		this.monuments = 0;
		this.playedDevCard = false;
		this.discarded = false;
		
		//this.game = game;
	}
	
	/**
	 * 
	 * @param playerID
	 * @param orderNumber
	 * @param name
	 * @param soldiers
	 * @param victoryPoints
	 * @param playedDev
	 * @param discarded
	 * @param buildings
	 * @param roads
	 * @param hand
	 */
	
	public Player(int playerID, int orderNumber, String name, int soldiers, int victoryPoints, Boolean playedDev, Boolean discarded,
			Boolean boughtDevCard, int cities, int settlements, int roads, ResourceList resoures) {
		this.playerID = playerID;
		this.playerIndex = orderNumber;
		this.name = name;
		this.soldiers = soldiers;
		this.victoryPoints = victoryPoints;
		this.playedDevCard = playedDev;
		this.discarded = discarded;
		this.cities = cities;
		this.settlements = settlements;
		this.roads = roads;
		//this.hand = hand;
	}

	public Player() {}

	/**
	 * @return the playerID
	 */
	public int getPlayerID() {
		return playerID;
	}

	/**
	 * @param playerID the playerID to set
	 */
	public void setPlayerID(int playerID) {
		this.playerID = playerID;
	}

	/**
	 * @return the orderNumber
	 */
	public int getOrderNumber() {
		return playerIndex;
	}

	/**
	 * @param orderNumber the orderNumber to set
	 */
	public void setOrderNumber(int orderNumber) {
		this.playerIndex = orderNumber;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the color
	 */
	public CatanColor getColor() {
		return CatanColor.valueOf(color.toUpperCase());
	}

	/**
	 * @param color the color to set
	 */
	public void setColor(String color) {
		this.color = color;
	}

	/**
	 * @return the soldiers
	 */
	public int getSoldiers() {
		return soldiers;
	}

	/**
	 * @param soldiers the soldiers to set
	 */
	public void setSoldiers(int soldiers) {
		this.soldiers = soldiers;
	}

	/**
	 * @return the victoryPoints
	 */
	public int getVictoryPoints() {
		return victoryPoints;
	}

	/**
	 * @param victoryPoints the victoryPoints to set
	 */
	public void setVictoryPoints(int victoryPoints) {
		this.victoryPoints = victoryPoints;
	}

	/**
	 * @return the playedDev
	 */
	public Boolean getPlayedDev() {
		return playedDevCard;
	}

	/**
	 * @param playedDev the playedDev to set
	 */
	public void setPlayedDev(Boolean playedDev) {
		this.playedDevCard = playedDev;
	}

	/**
	 * @return the discarded
	 */
	public Boolean getDiscarded() {
		return discarded;
	}

	/**
	 * @param discarded the discarded to set
	 */
	public void setDiscarded(Boolean discarded) {
		this.discarded = discarded;
	}

	/**
	 * @return the hand
	 */
//	public Hand getHand() {
//		return hand;
//	}
//
//	/**
//	 * @param hand the hand to set
//	 */
//	public void setHand(Hand hand) {
//		this.hand = hand;
//	}

	public void gainResource(ResourceType resource, int quantity) {
		if(resource == ResourceType.BRICK) {
			int temp = resources.getBrick();
			resources.setBrick(temp+quantity);
		}
		if(resource == ResourceType.ORE) {
			int temp = resources.getOre();
			resources.setOre(temp+quantity);
		}
		if(resource == ResourceType.SHEEP) {
			int temp = resources.getSheep();
			resources.setSheep(temp+quantity);
		}
		if(resource == ResourceType.WHEAT) {
			int temp = resources.getWheat();
			resources.setWheat(temp+quantity);
		}
		if(resource == ResourceType.WOOD) {
			int temp = resources.getWood();
			resources.setWood(temp+quantity);
		}
	}
	
	/**
	 * @pre canBuyDevCard()
	 */
	public void buyDevCard(Game game) {
		//if (canBuyDevCard()) {
		if(true){
			resources.setSheep(resources.getSheep()-1);
			resources.setOre(resources.getOre()-1);
			resources.setWheat(resources.getWheat()-1);
			DevCardType type = game.getDeck().draw();
			if (type == DevCardType.MONOPOLY) {
				newDevCards.setMonopoly(newDevCards.getMonopoly()+1);
			}
			if (type == DevCardType.MONUMENT) {
				newDevCards.setMonument(newDevCards.getMonument()+1);
			}
			if (type == DevCardType.ROAD_BUILD) {
				newDevCards.setRoadBuilding(newDevCards.getRoadBuilding() + 1);
			}
			if (type == DevCardType.SOLDIER) {
				newDevCards.setSoldier(newDevCards.getSoldier()+1);
			}
			if (type == DevCardType.YEAR_OF_PLENTY) {
				newDevCards.setYearOfPlenty(newDevCards.getYearOfPlenty()+1);
			}
		}
	}
	
	/**
	 * 
	 * @return
	 */
	public Boolean canBuyDevCard() {
		if(GameManager.getInstance().getGameState() instanceof GamePlayState){
			if(resources.getOre() > 0 && resources.getSheep() > 0 && resources.getWheat() > 0){// && game.getDeck().total() > 0) {
				return true;
			}else
				return false;
		}
		else {
			return false;
		}
	}
	
	public boolean canBuyDevCardServer(){
		if(resources.getOre() > 0 && resources.getSheep() > 0 && resources.getWheat() > 0){// && game.getDeck().total() > 0) {
			return true;
		}else
			return false;
	}
	
	public void buildRoad(boolean free) {
		if(free){
			roads--;
			return;
		}
		
		else if(canBuildRoad()) {
			resources.setBrick(resources.getBrick()-1);
			resources.setWood(resources.getWood()-1);
			roads--;
		}
	}
	
	/**
	 * 
	 * @return
	 */
	public Boolean canBuildRoad() {
		if (resources.getBrick() > 0 && resources.getWood() > 0 && roads > 0) {
			return true;
		}
		else return false;
	}
	
	public void buildSettlement(boolean free) {
		if(canBuildSettlement()) {
			resources.setBrick(resources.getBrick()-1);
			resources.setWheat(resources.getWheat()-1);
			resources.setSheep(resources.getSheep()-1);
			resources.setWood(resources.getWood()-1);
			settlements--;
			victoryPoints++;
		}
		else if(free){
			settlements--;
			victoryPoints++;
		}
	}
	
	/**
	 * 
	 * @return
	 */
	 
	public Boolean canBuildSettlement() {
		if(resources.getBrick() > 0 && resources.getWood() > 0 && resources.getWheat() > 0 && resources.getSheep() > 0 && settlements > 0) {
			return true;
		}
		else return false;
	}
	
	public void buildCity() {
		if(canBuildCity()) {
			resources.setWheat(resources.getWheat()-2);
			resources.setOre(resources.getOre()-3);
			cities--;
			victoryPoints+=1;
			settlements++;
		}
	}
	
	public Boolean canBuildCity() {
		if (resources.getWheat() >= 2 && resources.getOre() >= 3 && cities > 0 && settlements < 15) {
			return true;
		}
		else return false;
	}
	
	public Boolean canAcceptTrade(TradeOffer tradeOffer) {
		if(resources.getBrick() + tradeOffer.getOffer().getBrick() >= 0 && resources.getOre() + tradeOffer.getOffer().getOre() >= 0
				&& resources.getSheep() + tradeOffer.getOffer().getSheep() >= 0 && resources.getWheat() + tradeOffer.getOffer().getWheat() >= 0
				&& resources.getWood() + tradeOffer.getOffer().getWood() >= 0) {
			if(tradeOffer.getOffer().getBrick() > 0 || tradeOffer.getOffer().getOre() > 0 || tradeOffer.getOffer().getSheep() > 0 
					|| tradeOffer.getOffer().getWheat() > 0 || tradeOffer.getOffer().getWood() > 0) {
				return true;
			}
		}
		return false;
	}
	
	public boolean canOfferTrade(TradeOffer tradeOffer){
		if(resources.getBrick()<tradeOffer.getOffer().getBrick())
			return false;
		if(resources.getOre()< tradeOffer.getOffer().getOre())
			return false;
		if(resources.getSheep()< tradeOffer.getOffer().getSheep())
			return false;
		if(resources.getWheat()<tradeOffer.getOffer().getWheat())
			return false;
		if(resources.getWood()<tradeOffer.getOffer().getWood())
			return false;
		return true;
	}

	
	@Override
	public boolean equals(Object o){
		if(o == null){
			return false;
		}else if (o.getClass() != this.getClass()){
			return false;
		}
		Player p = (Player)o;
		if (p.playerID != this.playerID){
			return false;
		}else{
			return true;
		}
	}

	public int getCities() {
		return cities;
	}

	public void setCities(int cities) {
		this.cities = cities;
	}

	public int getSettlements() {
		return settlements;
	}

	public void setSettlements(int settlements) {
		this.settlements = settlements;
	}

	public int getRoads() {
		return roads;
	}

	public void setRoads(int roads) {
		this.roads = roads;
	}

	public ResourceList getResources() {
		return resources;
	}

	public void setResources(ResourceList resources) {
		this.resources = resources;
	}

	public Game getGame() {
		//return game;
		return null;
	}

	public void setGame(Game game) {
		//this.game = game;
	}

	public DevCardList getOldDevCards() {
		return oldDevCards;
	}

	public void setOldDevCards(DevCardList oldDevCards) {
		this.oldDevCards = oldDevCards;
	}

	public DevCardList getNewDevCards() {
		return newDevCards;
	}

	public void setNewDevCards(DevCardList newDevCards) {
		this.newDevCards = newDevCards;
	}

	public boolean canPlaySoldier() {
		if(oldDevCards.getSoldier() > 0) {
			if(!playedDevCard) {
				return true;
			}
		}
		return false;
	}

	public boolean canPlayYoP() {
		if(oldDevCards.getYearOfPlenty() > 0) {
			if(!playedDevCard) {
				return true;
			}
		}
		return false;
	}

	public boolean canPlayRoadBuilding() {
		if(oldDevCards.getRoadBuilding() > 0 && getRoads()>=1) {
			if(!playedDevCard) {
				return true;
			}
		}
		return false;
	}
	
	public void playRoadBuilding(){
		playedDevCard = true;
		roads-=2;
		if(roads < 0) {
			roads = 0;
		}
		int roadBuilding = oldDevCards.getRoadBuilding() - 1;
		oldDevCards.setRoadBuilding(roadBuilding);
	}

	public boolean canPlayMonopoly() {
		if(oldDevCards.getMonopoly() > 0) {
			if(!playedDevCard) {
				return true;
			}
		}
		return false;
	}

	public boolean canDiscard(ResourceList discarded2) {
		if(resources.total() > 7) {
			if(resources.hasResources(discarded2)) {
				return true;
			}
		}
		return false;
	}

	public boolean canPlayMonument() {
		if(oldDevCards.getMonument() + victoryPoints >= 10 && oldDevCards.getMonument() > 0) {
			return true;
		}
		return false;
	}

	public void discard(ResourceList toDiscard) {
		resources.discard(toDiscard);
		discarded = true;
	}

	public void playSoldier() {
		playedDevCard = true;
		soldiers++;
		int soldierCards = oldDevCards.getSoldier() - 1;
		oldDevCards.setSoldier(soldierCards);
	}

	public void performTrade(ResourceHand offer, int indicator) {
		resources.performTrade(offer,indicator);
		
	}

	public void newDevCardsToOld() {
		playedDevCard = false;
		oldDevCards.add(newDevCards);
		newDevCards.empty();
	}

	public void maritimeTrade(MaritimeTrade request) {
		String sending = request.getInputResources().toUpperCase();
		String receiving = request.getOutputResources().toUpperCase();
		int ratio = request.getRatio();
		this.gainResource(ResourceType.valueOf(receiving), 1);
		this.gainResource(ResourceType.valueOf(sending),-ratio);
	}

	public int getResourceAmount(ResourceType resource) {
		return resources.getResource(resource);
		
	}

	public void setResourceAmount(ResourceType resource, int i) {
		resources.setResource(resource, i);
	}

	public void playMonopoly() {
		playedDevCard = true;
		int monopolyCards = oldDevCards.getMonopoly() - 1;
		oldDevCards.setMonopoly(monopolyCards);
	}

	public void playYearOfPlenty(YearOfPlentyRequest request) {
		playedDevCard = true;
		ResourceType resource1 = ResourceType.valueOf(request.getResource1().toUpperCase());
		ResourceType resource2 = ResourceType.valueOf(request.getResource2().toUpperCase());
		this.gainResource(resource1, 1);
		this.gainResource(resource2, 1);
		int yearOfPlentyCards = oldDevCards.getYearOfPlenty() - 1;
		oldDevCards.setYearOfPlenty(yearOfPlentyCards);
		
		
	}
	
	public void addResources(ResourceList newResources){
		resources.addResources(newResources);
	}
	
	public JsonObject toJson(){
		JsonObject result = new JsonObject();
		result.addProperty("playerID", this.playerID);
		result.addProperty("playerIndex", this.playerIndex);
		result.addProperty("name", this.name);
		result.addProperty("color", this.color);
		result.addProperty("soldiers", this.soldiers);
		result.addProperty("victoryPoints", this.victoryPoints);
		result.addProperty("playedDevCard", this.playedDevCard);
		result.addProperty("discarded", this.discarded);
		result.addProperty("cities", this.cities);
		result.addProperty("settlements", this.settlements);
		result.addProperty("roads", this.roads);
		result.addProperty("monuments", this.monuments);
		result.add("resources", this.resources.toJson());
		result.add("oldDevCards", this.oldDevCards.toJson());
		result.add("newDevCards", this.newDevCards.toJson());
		return result;
	}

	public void playMonument() {
		// TODO Auto-generated method stub
		victoryPoints+=oldDevCards.getMonument();
		oldDevCards.setMonument(0);
	}
	
}
