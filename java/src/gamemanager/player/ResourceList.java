package gamemanager.player;

import java.util.Random;

import shared.definitions.ResourceType;

import com.google.gson.JsonObject;

import cs340.model.player.ResourceHand;

public class ResourceList {
	
	public static final int MaxResource = 19;
	
	protected int brick;
	protected int ore;
	protected int sheep;
	protected int wheat;
	protected int wood;
	
	public ResourceList() {
		brick = 0;
		ore = 0;
		sheep = 0;
		wheat = 0;
		wood = 0;
	}
	
	public ResourceList(int brick, int ore, int sheep, int wheat, int wood) {
		super();
		this.brick = brick;
		this.ore = ore;
		this.sheep = sheep;
		this.wheat = wheat;
		this.wood = wood;
	}


	public JsonObject toJson(){
		JsonObject result = new JsonObject();
//		for(ResourceType type: ResourceType.values()){
//			result.addProperty(type.getValue(), this.getResource(type));
//		}
		return result;
	}
	
	public int total() {
		return brick + ore + sheep + wheat + wood;
	}
	
	public int getBrick() {
		return brick;
	}
	public void setBrick(int brick) {
		this.brick = brick;
	}
	public int getOre() {
		return ore;
	}
	public void setOre(int ore) {
		this.ore = ore;
	}
	public int getSheep() {
		return sheep;
	}
	public void setSheep(int sheep) {
		this.sheep = sheep;
	}
	public int getWheat() {
		return wheat;
	}
	public void setWheat(int wheat) {
		this.wheat = wheat;
	}
	public int getWood() {
		return wood;
	}
	public void setWood(int wood) {
		this.wood = wood;
	}
	
	public void setResource(ResourceType type, int amount){
		switch(type){
		case BRICK: this.setBrick(amount);
			break;
		case ORE: this.setOre(amount);
			break;
		case SHEEP: this.setSheep(amount);
			break;
		case WHEAT: this.setWheat(amount);
			break;
		case WOOD: this.setWood(amount);
			break;
		default: return;
		}
	}
	
	public int getResource(ResourceType type){
		switch(type){
		case BRICK: return this.getBrick();
		case ORE: return this.getOre();
		case SHEEP: return this.getSheep();
		case WHEAT: return this.getWheat();
		case WOOD: return this.getWood();
		default: return 0;
		}
	}

	public boolean has(ResourceType resource) {
		if(resource == ResourceType.BRICK && brick > 0) {
			return true;
		}
		if(resource == ResourceType.ORE && ore > 0) {
			return true;
		}
		if(resource == ResourceType.SHEEP && sheep > 0) {
			return true;
		}
		if(resource == ResourceType.WHEAT && wheat > 0) {
			return true;
		}
		if(resource == ResourceType.WOOD && wood > 0) {
			return true;
		}
		return false;
	}
	
	public boolean hasResource(ResourceType type, int amount){
		switch(type){
		case BRICK: if(this.brick>=amount)
			return true;
			break;
		case ORE: if(this.ore>=amount)
			return true;
			break;
		case SHEEP: if(this.sheep>=amount)
			return true;
			break;
		case WHEAT: if(this.wheat>=amount)
			return true;
			break;
		case WOOD: if(this.wood>=amount)
			return true;
			break;
		}
		return false;
	}
	
	public boolean hasResources(ResourceList list){
		if(list.brick>0){
			if(this.brick<Math.abs(list.brick))
				return false;
		}
		if(list.ore>0){
			if(this.ore<Math.abs(list.ore))
				return false;
		}
		if(list.sheep>0){
			if(this.sheep<Math.abs(list.sheep))
				return false;
		}
		if(list.wheat>0){
			if(this.wheat<Math.abs(list.wheat))
				return false;
		}
		if(list.wood>0){
			if(this.wood<Math.abs(list.wood))
				return false;
		}
		return true;
	}
	
	public boolean haveResources(ResourceList list){
		if(this.brick<list.brick)
			return false;
		if(this.ore<list.ore)
			return false;
		if(this.sheep<list.sheep)
			return false;
		if(this.wheat<list.wheat)
			return false;
		if(this.wood<list.wood)
			return false;
		
		return true;
	}
	
	public ResourceType removeRandom(){
		Random r = new Random();
		int total = total();
		ResourceType[] resources = ResourceType.values();
		while(true){
			int index = r.nextInt(5);
			ResourceType resource = resources[index];
			if(hasResource(resource,1)){
				remove(resource);
				return resource;
			}
		}
		
	}
	
	private void remove(ResourceType type) {
		// TODO Auto-generated method stub
		switch(type){
		case BRICK: if(this.brick>=1)
			brick--;
			break;
		case ORE: if(this.ore>=1)
			ore--;
			break;
		case SHEEP: if(this.sheep>=1)
			sheep--;
			break;
		case WHEAT: if(this.wheat>=1)
			wheat--;
			break;
		case WOOD: if(this.wood>=1)
			wood--;
			break;
		}
		return;
	}

	public void discard(ResourceList toDiscard) {
		// TODO Auto-generated method stub
		brick-=toDiscard.brick;
		ore-=toDiscard.ore;
		sheep-=toDiscard.sheep;
		wheat-=toDiscard.wheat;
		wood-=toDiscard.wood;
		
	}

	public void performTrade(ResourceHand offer, int indicator) {//indicator is 1 or -1, used for accepting trades
		// TODO Auto-generated method stub
		brick+=offer.getBrick()*indicator;
		ore+=offer.getOre()*indicator;
		sheep+=offer.getSheep()*indicator;
		wheat+=offer.getWheat()*indicator;
		wood+=offer.getWood()*indicator;
	}
	
	public void gainResource(ResourceType resource, int quantity) {
		if(resource == ResourceType.BRICK) {
			brick+=quantity;
		}
		if(resource == ResourceType.ORE) {
			ore+=quantity;
		}
		if(resource == ResourceType.SHEEP) {
			sheep+=quantity;
		}
		if(resource == ResourceType.WHEAT) {
			wheat+=quantity;
		}
		if(resource == ResourceType.WOOD) {
			wood+=quantity;
		}
	}

	public void addResources(ResourceList newResources) {
		// TODO Auto-generated method stub
		brick+=newResources.brick;
		ore+=newResources.ore;
		sheep+=newResources.sheep;
		wheat+=newResources.wheat;
		wood+=newResources.wood;
		
	}
}
