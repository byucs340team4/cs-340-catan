package gamemanager.map;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.JsonObject;

import shared.locations.EdgeDirection;
import shared.locations.EdgeLocation;
import shared.locations.HexLocation;

/**
 * Road class
 * @author Michael and Bentz
 *
 */
public class Road {
	
	int owner;
	EdgeLocation location;
	List<EdgeLocation>edgeLocations;
	
	public Road(){
		//edgeLocations.add(location);
		//edgeLocations.add(new EdgeLocation(location.getHexLoc().getNeighborLoc(location.getDir()),location.getDir().getOppositeDirection()));
	}
	
	public Road(EdgeLocation edgeLoc,int owner){
		location = edgeLoc;
		this.owner = owner;
	}

	public int getOwner() {
		return owner;
	}

	public void setOwner(int owner) {
		this.owner = owner;
	}

	public EdgeLocation getLocation() {
		return location;
	}

	public void setLocation(EdgeLocation location) {
		this.location = location;
	}

	public List<EdgeLocation> getEdgeLocations() {
		edgeLocations = new ArrayList<EdgeLocation>();
		edgeLocations.add(location);
		HexLocation neighbor = location.getHexLoc().getNeighborLoc(location.getDir());
		if(neighbor!=null) edgeLocations.add(new EdgeLocation(neighbor,location.getDir().getOppositeDirection()));
		
		return edgeLocations;
	}

	public void setEdgeLocations(List<EdgeLocation> edgeLocations) {
		this.edgeLocations = edgeLocations;
	}

	public Object getEdgeDirection(HexLocation location2) {
		edgeLocations = new ArrayList<EdgeLocation>();
		edgeLocations.add(location);
		edgeLocations.add(new EdgeLocation(location.getHexLoc().getNeighborLoc(location.getDir()),location.getDir().getOppositeDirection()));
		if(location.getDir().equals(EdgeDirection.S)){
			System.out.print("");
		}
		for(EdgeLocation edge:edgeLocations){
			if(edge.getHexLoc().equals(location2)){
				return edge;
			}
		}
		return null;
	}
	
	public JsonObject toJson(){
		JsonObject result = new JsonObject();
		result.addProperty("owner", this.owner);
		result.add("location", location.toJson());
		return result;
	}
	
}
