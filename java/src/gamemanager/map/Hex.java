package gamemanager.map;

import com.google.gson.JsonObject;

import shared.locations.HexLocation;

/**
 * Class to represent the hexes in the game
 * @author Michael and Bentz
 *
 */
public class Hex {

	private HexLocation location;
	String resource;
	int number;
	
	public Hex(){
		
	}

	public HexLocation getLocation() {
		return location;
	}

	public void setLocation(HexLocation location) {
		this.location = location;
	}

	public String getResource() {
		return resource;
	}

	public void setResource(String resource) {
		this.resource = resource;
	}

	public int getNumber() {
		return number;
	}

	public void setNumber(int number) {
		this.number = number;
	}
	
	public JsonObject toJson(){
		JsonObject result = new JsonObject();
		result.add("location", location.toJson());
		if(this.resource!=null)
			result.addProperty("resource", this.resource);
		if(this.number!=0)
			result.addProperty("number", this.number);
		return result;
	}
	
	
}
