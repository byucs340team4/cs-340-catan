package gamemanager.map;


import java.util.ArrayList;

import com.google.gson.JsonObject;

import shared.locations.EdgeDirection;
import shared.locations.HexLocation;
import shared.locations.VertexDirection;
import shared.locations.VertexLocation;
/**
 * Class to represent the ports in the game
 * @author Michael and Bentz
 *
 */
public class Port {

	/**
	 * variable that holds the PortType
	 
	private PortType portType;
	
	/**
	 * list that holds to two vertex locations that the port is accesible from.
	 
	private List<VertexLocation>vertexLocations;
	
	/**
	 * varable that holds the ratio of the port, either 2 or 3
	 
	int tradeRatio;
	
	/**
	 * default constructor that takes no paramters
	 */
	String resource;
	HexLocation location;
	String direction;
	int ratio;
	
	public Port (){
		
	}

	public String getResource() {
		return resource;
	}

	public void setResource(String resource) {
		this.resource = resource;
	}

	public HexLocation getLocation() {
		return location;
	}

	public void setLocation(HexLocation location) {
		this.location = location;
	}

	public String getDirection() {
		return direction;
	}

	public void setDirection(String direction) {
		this.direction = direction;
	}

	public int getRatio() {
		return ratio;
	}

	public void setRatio(int ratio) {
		this.ratio = ratio;
	}
	
	public ArrayList<VertexLocation> getVertexLocations(){
		ArrayList<VertexLocation>result = new ArrayList<VertexLocation>();
		VertexDirection[]vDirs = VertexDirection.values();
		int edgeLoc = EdgeDirection.valueOf(direction).ordinal();
		result.add(new VertexLocation(location, vDirs[edgeLoc]));
		result.add(new VertexLocation(location,vDirs[(edgeLoc+1)%6]));
		return result;		
	}

	public JsonObject toJson(){
		JsonObject result = new JsonObject();
		result.addProperty("resource", this.resource);
		result.add("location", this.location.toJson());
		result.addProperty("direction", this.direction);
		result.addProperty("ratio", this.ratio);
		return result;
	}	
	
}
