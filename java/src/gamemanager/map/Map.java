package gamemanager.map;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import gamemanager.GameManager;
import gamemanager.game.CardDistributor;
import gamemanager.player.Player;
import gamemanager.player.ResourceList;
import shared.definitions.ResourceType;
import shared.locations.EdgeDirection;
import shared.locations.EdgeLocation;
import shared.locations.HexLocation;
import shared.locations.VertexLocation;
import shared.locations.VertexObject;
import shared.locations.VertexDirection;

public class Map {

	ArrayList<Hex>hexes;
	ArrayList<Port>ports;
	ArrayList<Road>roads;
	ArrayList<VertexObject>settlements;
	ArrayList<VertexObject>cities;
	int radius;
	HexLocation robber;
	VertexObject round2;
	
	public Map(){
		
	}

	public ResourceList addSettlement(VertexObject obj, boolean isSecond){
		settlements.add(obj);
		if(isSecond)
		{
			ResourceList init = new ResourceList();
			for(VertexLocation vLoc:obj.getLocations()){
				HexLocation hLoc = vLoc.getHexLoc();
				if(Math.abs(hLoc.getX())<=2 && Math.abs(hLoc.getY())<=2){
					Hex hex = getLocation(hLoc);
					if(hex==null)continue;
					String resource = hex.getResource();
					if(!(resource==null) && !resource.equals("DESERT")){
						ResourceType type = ResourceType.valueOf(resource.toUpperCase());
						init.gainResource(type, 1);
					}					
				}
			}
			return init;
		}
		return null;
	}
	
	public void addCity(VertexObject obj){
		settlements.remove(obj);
		cities.add(obj);
	}
	
	public void addRoad(Road road){
		roads.add(road);
	}
	
	public void removeRoad(EdgeLocation edge){
		for(Road road:roads){
			for(EdgeLocation loc:road.getEdgeLocations()){
				if(loc.equals(edge))
					roads.remove(road);
				return;
			}
		}
	}
	
	public void removeLastTwoRoads(){
		roads.remove(roads.size()-1);
		roads.remove(roads.size()-1);
	}
	
	public Hex getLocation(HexLocation loc){
		for(Hex hex:hexes){
			if( hex.getLocation().equals(loc)){
				return hex;
			}
		}
		return null;
	}
	
	/*
	 * Checks to see if player can trade 2:1 on port that has specified resource.
	 */
	public boolean canMaritimeTrade(String resource,int playerIndex){
		ArrayList<VertexObject>buildingList = new ArrayList<VertexObject>();
		buildingList.addAll(settlements);
		buildingList.addAll(cities);
		
		for(VertexObject building:buildingList){
			for(Port port:ports){

				String type = port.getResource();
				
				for(VertexLocation portLoc:port.getVertexLocations()){
					for(VertexLocation buildingLoc:building.getLocations()){
							if(buildingLoc.equals(portLoc) && type.equals(resource) && building.getOwner() == playerIndex)
								return true;
						}
				}
			}
		}
		return false;
	}
	
	/*
	 * Checks to see if the player can trade with a 3:1 ratio on a port that does not have a resource.
	 */
	public boolean canMaritimeTrade(int playerIndex){
		ArrayList<VertexObject>buildingList = new ArrayList<VertexObject>();
		buildingList.addAll(settlements);
		buildingList.addAll(cities);
		
		for(VertexObject building:buildingList){
			for(Port port:ports){				
				for(VertexLocation portLoc:port.getVertexLocations()){
					for(VertexLocation buildingLoc:building.getLocations()){
							if(buildingLoc.equals(portLoc)&& building.getOwner() == playerIndex)
								return true;
						}
				}
			}
		}
		return false;
	}
	
	public Set<Port> getMaritimePorts(int playerIndex){
		Set<Port> rPorts = new HashSet<Port>();
		ArrayList<VertexObject>buildingList = new ArrayList<VertexObject>();
		buildingList.addAll(settlements);
		buildingList.addAll(cities);
		
		for(VertexObject building:buildingList){
			for(Port port:ports){				
				for(VertexLocation portLoc:port.getVertexLocations()){
					for(VertexLocation buildingLoc:building.getLocations()){
							if(buildingLoc.equals(portLoc)&& building.getOwner() == playerIndex)
								rPorts.add(port);
						}
				}
			}
		}
		return rPorts;
	}
	
	private VertexObject findSettlement(){
		for(VertexObject building: settlements){
			if(building.getOwner() == GameManager.getInstance().getLocalPlayer().getPlayerIndex()){
				if(!isRoadAttached(building, GameManager.getInstance().getLocalPlayer().getPlayerIndex())){
					return building;	
				}
			}
		}
		return null;
	}
	
	private boolean isRoadAttached(VertexObject building,int player){
		for (VertexLocation vLoc : building.getLocations()) {
			for (Road road : roads) {
				if (road.getOwner() == player) {
					for (EdgeLocation edge : road.getEdgeLocations()) {
						try {
							if (vLoc.equals(new VertexLocation(edge.getHexLoc(),
									VertexDirection.values()[edge.getDir().ordinal()]))
									&& building.getOwner() == player) {
								return true;
							}
						} catch (Exception e) {
						}
						try {
							if (vLoc.equals(new VertexLocation(edge.getHexLoc(),VertexDirection.values()[(edge.getDir()
											.ordinal() + 1) % 6]))&& building.getOwner() == player) {
								return true;
							}
						} catch (Exception e) {
						}
					}
				}
			}
		}
		return false;
	}
	
	public boolean canBuildRoad(EdgeLocation edge, int player, boolean initialization){
		if(this.getLocation(edge.getHexLoc()) == null && this.getLocation(edge.getUnNormalizedLocation().getHexLoc()) == null){
			return false;
		}
		if (initialization && GameManager.getInstance().getGame().getTurnTracker().getStatus().equals("FirstRound")) {
			for(VertexObject building: settlements){
				for (VertexLocation vLoc : building.getLocations()) {
					try {
						if (vLoc.equals(new VertexLocation(edge.getHexLoc(),
								VertexDirection.values()[edge.getDir().ordinal()]))  && building.getOwner() == player) {
							return true;
						}
					} catch (Exception e) {
					}
					try {
						if (vLoc.equals(new VertexLocation(edge.getHexLoc(),
								VertexDirection.values()[(edge.getDir()	.ordinal() + 1) % 6]))	&& building.getOwner() == player) {
							return true;
						}
					} catch (Exception e) {
					}

				}
			}
			return false;
		}else if (initialization && GameManager.getInstance().getGame().getTurnTracker().getStatus().equals("SecondRound")) {
			if(round2 == null){
				round2 = findSettlement();
			}else{
				for (VertexLocation vLoc : round2.getLocations()) {
					try {
						if (vLoc.equals(new VertexLocation(edge.getHexLoc(),
								VertexDirection.values()[edge.getDir().ordinal()]))) {
							return true;
						}
					} catch (Exception e) {
					}
					try {
						if (vLoc.equals(new VertexLocation(edge.getHexLoc(),
								VertexDirection.values()[(edge.getDir().ordinal() + 1) % 6]))) {
							return true;
						}
					} catch (Exception e) {
					}
				}
			}
			return false;
		} else {
			for (Road road : roads) {
				List<EdgeLocation> tempLocations = road.getEdgeLocations();
				for (EdgeLocation tempEdge : tempLocations) {
					if (tempEdge.equals(edge)) {
						return false;
					}
				}
			}
			try {
				if (checkEdges(edge, player)) {
					return true;
				}
			} catch (Exception e) {}
			
			try {
				if (checkEdges(edge.getUnNormalizedLocation(), player)) {
					return true;
				}
			} catch (Exception e) {
				return false;
			}
		}
		return false;
	}
	
	public boolean canBuildRoadServer(EdgeLocation edge, int player, boolean initialization,String status){
		if(this.getLocation(edge.getHexLoc()) == null && this.getLocation(edge.getUnNormalizedLocation().getHexLoc()) == null){
			return false;
		}
		if (initialization && status.equals("FirstRound")) {
			for(VertexObject building: settlements){
				for (VertexLocation vLoc : building.getLocations()) {
					try {
						if (vLoc.equals(new VertexLocation(edge.getHexLoc(),
								VertexDirection.values()[edge.getDir().ordinal()]))  && building.getOwner() == player) {
							return true;
						}
					} catch (Exception e) {
					}
					try {
						if (vLoc.equals(new VertexLocation(edge.getHexLoc(),
								VertexDirection.values()[(edge.getDir()	.ordinal() + 1) % 6]))	&& building.getOwner() == player) {
							return true;
						}
					} catch (Exception e) {
					}

				}
			}
			return false;
		}else if (initialization && status.equals("SecondRound")) {
			
				round2 = findSettlementServer(player);
		
				for (VertexLocation vLoc : round2.getLocations()) {
					try {
						if (vLoc.equals(new VertexLocation(edge.getHexLoc(),
								VertexDirection.values()[edge.getDir().ordinal()]))) {
							return true;
						}
					} catch (Exception e) {
					}
					try {
						if (vLoc.equals(new VertexLocation(edge.getHexLoc(),
								VertexDirection.values()[(edge.getDir().ordinal() + 1) % 6]))) {
							return true;
						}
					} catch (Exception e) {
					}
				}
			
			return false;
		} else {
			for (Road road : roads) {
				List<EdgeLocation> tempLocations = road.getEdgeLocations();
				for (EdgeLocation tempEdge : tempLocations) {
					if (tempEdge.equals(edge)) {
						return false;
					}
				}
			}
			try {
				if (checkEdges(edge, player)) {
					return true;
				}
			} catch (Exception e) {}
			
			try {
				if (checkEdges(edge.getUnNormalizedLocation(), player)) {
					return true;
				}
			} catch (Exception e) {
				return false;
			}
		}
		return false;
	}
	
	
	private VertexObject findSettlementServer(int player) {
		// TODO Auto-generated method stub
		for(VertexObject building: settlements){
			if(building.getOwner() == player){
				if(!isRoadAttached(building, player)){
					return building;	
				}
			}
		}
		return null;	}

	private boolean checkEdges(EdgeLocation edge, int player) {
		boolean checkEdge1 = true;
		boolean checkEdge2 = true;

		List<VertexObject>buildingList = settlements;
		buildingList.addAll(cities);
		
		for (VertexObject building : buildingList) {// implement getVertexDireection
			for(VertexLocation vLoc:building.getLocations()){
				try{
				if (vLoc.getDir().equals( VertexDirection.values()[edge.getDir().ordinal()])
						&& building.getOwner() != (player) && edge.getHexLoc().equals(vLoc.getHexLoc())) {
					checkEdge1 = false;
					break;
				}
				}catch(Exception e){}
				try{
				if (vLoc.getDir().equals(VertexDirection.values()[(edge.getDir().ordinal() + 1) % 6])
						&& building.getOwner() != player&& edge.getHexLoc().equals(vLoc.getHexLoc())) {
					checkEdge2 = false;
					break;
				}
				}catch(Exception e){}
			}
				
			
		}
		for (Road road : roads) {
			try{
			EdgeLocation edgeD = (EdgeLocation) road.getEdgeDirection(edge.getHexLoc());
				if (checkEdge1
					&& edgeD.getDir().equals(EdgeDirection.values()[(edge.getDir().ordinal()+5) % 6])
					&& road.getOwner() == (player) && edgeD.getHexLoc().equals(edge.getHexLoc())){
				return true;
			}
			}catch(Exception e){}
			
			try{
				EdgeLocation edgeD = (EdgeLocation) road.getEdgeDirection(edge.getHexLoc());
			if (checkEdge2
					&& edgeD.getDir().equals(EdgeDirection.values()[(edge.getDir().ordinal() + 1) % 6])
					&& road.getOwner() == (player) && edgeD.getHexLoc().equals(edge.getHexLoc())){
				return true;
			}
			}catch(Exception e){}
		}
		
		return false;
	}
	

	/**
	 * method to determine whether or not a city can be built at a given vertex
	 * @param vertex - VertexLocation to check
	 * @param player - player who wants to build city
	 * @return - true if can build, else false
	 */
	public boolean canBuildCity(VertexLocation vertex, int playerIndex){
		for(VertexObject city:cities){
			for(VertexLocation loc:city.getLocations()){
				
				if(loc.equals(vertex))
					return false;
			}
		}
		
		for(VertexObject settlement:settlements){
			int owner = settlement.getOwner();
			for(VertexLocation loc:settlement.getLocations()){
				if(loc.equals(vertex)&&owner==playerIndex){
					return true;
				}
			}
		}
		return false;
	}
	
/**
 * method to determine whether or not a settlement can be built at a given vertex
 * @param vertex - VertexLocation to check
 * @param initialization - boolean if initial game setup
 * @param player - Player who wants to build the settlement
 * @return - true if can build, else false
 */
	public boolean canBuildSettlement(VertexLocation vertex, int playerIndex, boolean initialization){
		//and with player can build method
		if(this.getLocation(vertex.getHexLoc()) == null && this.getLocation(vertex.getUnNormalizedLocation().getHexLoc()) == null && this.getLocation(vertex.getUnNormalizedLocationTwo().getHexLoc()) == null){
			return false;
		}
		if(vertex==null)return false;
		
		int toCheck = vertex.getDir().ordinal();
		
		ArrayList<VertexObject> buildingList = new ArrayList<VertexObject>();
		buildingList.addAll(settlements);
		buildingList.addAll(cities);
		
		for(VertexObject building:buildingList){
			for(VertexLocation loc:building.getLocations()){
				if(loc.getHexLoc().equals(vertex.getHexLoc())){
					int vDir = loc.getDir().ordinal();
					if(toCheck==vDir || (toCheck+5)%6 == vDir || (toCheck+1)%6 == vDir){
						return false;
					}
				}
			}	
		}
		
		int edgeVal = (toCheck+5)%6;
		EdgeDirection[] edges = EdgeDirection.values();
		EdgeDirection edge = edges[edgeVal];
		Hex neighbor = new Hex();
		neighbor.setLocation(vertex.getHexLoc().getNeighborLoc(edge));
		

		for(VertexObject building:buildingList){
			for(VertexLocation loc:building.getLocations()){
				if(loc.getHexLoc().equals(neighbor.getLocation())&& (toCheck+1)%6 == loc.getDir().ordinal())
					return false;
			}
		}
			
		if(!initialization)
		{
			for(Road road:roads	){
				if(road.getOwner()!=playerIndex) //NEEDS TO BE THE INDEX, NOT THE ID
					continue;
				for(EdgeLocation loc:road.getEdgeLocations()){
					if(loc.getHexLoc().equals(vertex.getHexLoc()) && loc.getDir().ordinal()==toCheck)
						return true;
					else if(loc.getHexLoc().equals(vertex.getHexLoc()) && loc.getDir().ordinal()==(toCheck+5)%6)
						return true;
				}
			}
			
			//check the neighbor hex
			for(Road road:roads){
				if(road.getOwner()!=playerIndex) //NEEDS TO BE AN INDEX NOT PLAYER ID
					continue;
				for(EdgeLocation loc:road.getEdgeLocations()){
					if(loc.getHexLoc().equals(neighbor.getLocation())&& (toCheck+1)%6 == loc.getDir().ordinal())
						return true;
				}
			}	
			return false;
		}
		return true;

	}

	public TreeSet<Integer> getOtherHexOwners(HexLocation hexLoc,int player){
		TreeSet<Integer>result = new TreeSet<Integer>();
		
		ArrayList<VertexObject> buildingList = new ArrayList<VertexObject>();
		buildingList.addAll(settlements);
		buildingList.addAll(cities);
		boolean foundOther = false;
		for(VertexObject building:buildingList){
			for(VertexLocation vertLoc:building.getLocations()){
				if(vertLoc.getHexLoc().equals(hexLoc) && building.getOwner()!=player){
					result.add(building.getOwner());
					foundOther = true;
				}
			}
		}
		if(foundOther) return result;
		return null;
	}

	public List<Hex> getHexes() {
		return hexes;
	}
	
	public void setHexes(ArrayList<Hex> hexes) {
		this.hexes = hexes;
	}
	
	public List<Port> getPorts() {
		return ports;
	}
	
	public void setPorts(ArrayList<Port> ports) {
		this.ports = ports;
	}
	
	public ArrayList<Road> getRoads() {
		return roads;
	}
	
	public void setRoads(ArrayList<Road> roads) {
		this.roads = roads;
	}
	
	public List<VertexObject> getSettlements() {
		return settlements;
	}
	
	public void setSettlements(ArrayList<VertexObject> settlements) {
		this.settlements = settlements;
	}
	
	public List<VertexObject> getCities() {
		return cities;
	}
	
	public void setCities(ArrayList<VertexObject> cities) {
		this.cities = cities;
	}
	
	public int getRadius() {
		return radius;
	}
	
	public void setRadius(int radius) {
		this.radius = radius;
	}
	
	public HexLocation getRobber() {
		return robber;
	}
	
	public void setRobber(HexLocation robber) {
		this.robber = robber;
	}

	public EdgeLocation getRoadLoc(int playerIndex) {
		// TODO Auto-generated method stub
		for(Road road:roads){
			if(road.getOwner()==playerIndex)
				return road.getLocation();
		}
		return null;
	}

	public void rollNumber(Player[] players, int rollNumber,ResourceList bank) {
		// TODO Auto-generated method stub
		//make a resource list for each player. 
		//add to that list. then check the total of all of the lists, if greater than the amount
		//in the bank don't add any to any players
		if(rollNumber==7)return;
		ResourceList[] toDistribute = new ResourceList[4];
		toDistribute[0] = new ResourceList(); toDistribute[1] = new ResourceList(); toDistribute[2] = new ResourceList(); 
		toDistribute[3] = new ResourceList();
		ResourceList totalList = new ResourceList();
		
		for(VertexObject settlement:settlements){
			for(VertexLocation vLoc:settlement.getLocations()){
				HexLocation hLoc = vLoc.getHexLoc();
				if(Math.abs(hLoc.getX())<=2 && Math.abs(hLoc.getY())<=2){
					Hex hex = getLocation(hLoc);
					if(hex==null)continue;
					if(hex.getNumber()==rollNumber && !(hLoc.equals(robber))){
						int owner = settlement.getOwner();
						ResourceList currList = toDistribute[owner];
						String resource = hex.getResource();
						if(resource!=null && !resource.equals("DESERT")){
							currList.gainResource(ResourceType.valueOf(hex.getResource().toUpperCase()), 1);
							totalList.gainResource(ResourceType.valueOf(hex.getResource().toUpperCase()), 1);
						}
					}
				}
			}
		}
		
		for(VertexObject city:cities){
			for(VertexLocation vLoc:city.getLocations()){
				HexLocation hLoc = vLoc.getHexLoc();
				if(Math.abs(hLoc.getX())<=3 && Math.abs(hLoc.getY())<=3){
					Hex hex = getLocation(hLoc);
					if(hex==null)continue;
					if(hex.getNumber()==rollNumber && !hLoc.equals(robber)){
						int owner = city.getOwner();
						ResourceList currList = toDistribute[owner];
						String resource = hex.getResource();
						if(resource!=null && !resource.equals("DESERT")){
							currList.gainResource(ResourceType.valueOf(resource.toUpperCase()), 2);
							totalList.gainResource(ResourceType.valueOf(resource.toUpperCase()), 2);
						}
					}
				}
			}
		}		
		
		CardDistributor.distributeCards(totalList,toDistribute,bank,players);			
			
	}
	
	public JsonObject toJson(){
		JsonObject result = new JsonObject();
		JsonArray hexArray = new JsonArray();
		for(Hex hex: hexes){
			hexArray.add(hex.toJson());
		}
		result.add("hexes", hexArray);
		JsonArray portArray = new JsonArray();
		for(Port port: ports){
			portArray.add(port.toJson());
		}
		result.add("ports", portArray);
		JsonArray roadArray = new JsonArray();
		for(Road road: roads){
			hexArray.add(road.toJson());
		}
		result.add("roads", roadArray);
		JsonArray settlementsArray = new JsonArray();
		for(VertexObject settlement: settlements){
			hexArray.add(settlement.toJson());
		}
		result.add("settlements", settlementsArray);
		JsonArray citiesArray = new JsonArray();
		for(VertexObject city: cities){
			hexArray.add(city.toJson());
		}
		result.add("citites", citiesArray);
		result.addProperty("radius", this.radius);
		result.add("robber", this.robber.toJson());
		return result;
	}
	
}



