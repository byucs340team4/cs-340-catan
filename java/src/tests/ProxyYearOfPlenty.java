package tests;

import junit.framework.Assert;
import gamemanager.game.Game;

import org.junit.Before;
import org.junit.Test;

import shared.definitions.ResourceType;
import shared.proxy.MoveType;
import shared.proxy.Proxy;
import shared.proxy.request.Credentials;
import shared.proxy.request.JoinGameRequest;
import shared.proxy.request.YearOfPlentyRequest;

public class ProxyYearOfPlenty {
	
	Proxy myProxy;

	
	@Before
	public void init(){
		myProxy = new Proxy("BuildSettlementInitTest.json");
		myProxy.userLogin(new Credentials("Brooke","brooke"));
		myProxy.gamesJoin(new JoinGameRequest(2,"red"),false);
		
	}
	
//	@Test
//	public void validTest(){
//		
//		YearOfPlentyRequest request = new YearOfPlentyRequest(MoveType.YEAR_OF_PLENTY, 0, ResourceType.BRICK, ResourceType.BRICK);
//		Game game = myProxy.yearOfPlenty(request);
//		Assert.assertEquals("Game should not be null", true,game!=null);
//	}
	
	@Test
	public void invalidTest(){
		
		YearOfPlentyRequest request = new YearOfPlentyRequest(MoveType.YEAR_OF_PLENTY, 1, ResourceType.BRICK, ResourceType.BRICK);
		Game game = myProxy.yearOfPlenty(request);
		Assert.assertEquals("Game should not be null", true,game==null);
	}

}
