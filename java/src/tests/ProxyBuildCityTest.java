package tests;

import gamemanager.game.Game;
import gamemanager.map.Map;
import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;

import shared.locations.HexLocation;
import shared.locations.VertexDirection;
import shared.locations.VertexLocation;
import shared.proxy.Proxy;
import shared.proxy.request.BuildCity;
import shared.proxy.request.BuildSettlement;
import shared.proxy.request.Credentials;
import shared.proxy.request.JoinGameRequest;

public class ProxyBuildCityTest {

	Proxy myProxy;
	Map map;
	
	@Before
	public void init(){
		myProxy = new Proxy("BuildSettlementInitTest.json");
		myProxy.userLogin(new Credentials("Brooke","brooke"));
		myProxy.gamesJoin(new JoinGameRequest(2,"red"),false);
	}
	
	@Test 
	public void buildSettlementInvalid(){
		BuildSettlement request = new BuildSettlement(1,new VertexLocation(new HexLocation(-10,-10),VertexDirection.E),true);
		Game game = myProxy.buildSettlement(request);
		Assert.assertEquals("Bogus input should give a null game", null,game);
	}
	
	@Test
	public void buildSettlementValid(){
		BuildCity request = new BuildCity(0,new VertexLocation(new HexLocation(0,0),VertexDirection.E));
		Game game = myProxy.buildCity(request);
		System.out.println(game);
		Assert.assertEquals("Good input should give a game object back", true,(game!=null));
	}
	
}
