package tests;

import static org.junit.Assert.*;
import gamemanager.communication.TurnTracker;
import gamemanager.game.Game;
import gamemanager.player.Player;
import gamemanager.player.ResourceList;

import org.junit.Before;
import org.junit.Test;

import cs340.model.player.ResourceHand;
import cs340.model.player.TradeOffer;
import shared.proxy.Proxy;
import shared.proxy.request.Credentials;
import shared.proxy.request.JoinGameRequest;
//import shared.proxy.request.OfferTrade;

public class CanOfferTradeTest {

	Proxy proxy;
	Game game;
	TurnTracker turnTracker;
	TradeOffer tradeOffer;
	ResourceList offer;
	Player player;
	
	public CanOfferTradeTest(){
		proxy = new Proxy();
		game = new Game();
		turnTracker = new TurnTracker();
		offer = new ResourceList();
		player = new Player();
	}
	
	/*
	 * Test for succes on correct game status, player turn, and player available 
	 * resources.
	 */
	@Test
	public void testCanOfferTrade() {
		ResourceList resources = new ResourceList(3,1,0,2,1);
		player.setResources(resources);
		player.setOrderNumber(0);
		Player[] players = {player};
		tradeOffer = new TradeOffer();
		ResourceHand resourceHand = new ResourceHand();
		resourceHand.setBrick(2);
		resourceHand.setSheep(-2);
		tradeOffer.setSender(player.getOrderNumber());
		tradeOffer.setReciever(1);
		tradeOffer.setOffer(resourceHand);
		
		turnTracker.setCurrentTurn(0);
		turnTracker.setStatus("Playing");
		
		game.setPlayers(players);
		game.setTurnTracker(turnTracker);
		game.setTradeOffer(tradeOffer);
		
//		ResourceList list = new ResourceList(tradeOffer.getOffer().getBrick(),
//				tradeOffer.getOffer().getOre(), tradeOffer.getOffer().getSheep(), 
//				tradeOffer.getOffer().getWheat(), tradeOffer.getOffer().getWood());
		
//		OfferTrade offerTrade = new OfferTrade(player.getOrderNumber(), list, 1);
		
		if(game.canOfferTrade())
			assertTrue(true);
		else
			fail("Should have succeeded but it failed");
	}
	
	/*
	 * Test for failure due to incorrect game status.
	 */
	@Test
	public void testCantOfferTrade_Status(){
		ResourceList resources = new ResourceList(3,1,0,2,1);
		player.setResources(resources);
		player.setOrderNumber(0);
		Player[] players = {player};
		tradeOffer = new TradeOffer();
		ResourceHand resourceHand = new ResourceHand();
		resourceHand.setBrick(2);
		resourceHand.setSheep(-2);
		tradeOffer.setSender(player.getOrderNumber());
		tradeOffer.setReciever(1);
		tradeOffer.setOffer(resourceHand);
		
		turnTracker.setCurrentTurn(0);
		turnTracker.setStatus("FirstRound");
		
		game.setPlayers(players);
		game.setTurnTracker(turnTracker);
		game.setTradeOffer(tradeOffer);
		if(game.canOfferTrade())
			fail("Succeeded when it should have failed");
		else
			assertTrue(true);
	}
	
	@Test
	public void cantOfferTrade_playerTurn(){
		ResourceList resources = new ResourceList(3,1,0,2,1);
		player.setResources(resources);
		player.setOrderNumber(0);
		Player[] players = {player};
		tradeOffer = new TradeOffer();
		ResourceHand resourceHand = new ResourceHand();
		resourceHand.setBrick(2);
		resourceHand.setSheep(-2);
		tradeOffer.setSender(player.getOrderNumber()+1);
		tradeOffer.setReciever(1);
		tradeOffer.setOffer(resourceHand);
		
		turnTracker.setCurrentTurn(0);
		turnTracker.setStatus("Playing");
		
		game.setPlayers(players);
		game.setTurnTracker(turnTracker);
		game.setTradeOffer(tradeOffer);
		
		if(game.canOfferTrade()){
			fail("Should have failed due to it not being the player's turn");
		}else
			assertTrue(true);
	}
	
	@Test
	public void cantOfferTrade_improperResources(){
		ResourceList resources = new ResourceList(1,1,0,2,1);
		player.setResources(resources);
		player.setOrderNumber(0);
		Player[] players = {player};
		tradeOffer = new TradeOffer();
		ResourceHand resourceHand = new ResourceHand();
		resourceHand.setBrick(2);
		resourceHand.setSheep(-2);
		tradeOffer.setSender(player.getOrderNumber());
		tradeOffer.setReciever(1);
		tradeOffer.setOffer(resourceHand);
		
		turnTracker.setCurrentTurn(0);
		turnTracker.setStatus("Playing");
		
		game.setPlayers(players);
		game.setTurnTracker(turnTracker);
		game.setTradeOffer(tradeOffer);
		
		if(game.canOfferTrade())
			fail("Succeeded when it should have failed due to improper resources");
		else
			assertTrue(true);
	}
	
	@Before
	public void login(){
		proxy.userLogin(new Credentials("Brooke", "brooke"));
		proxy.gamesJoin(new JoinGameRequest(2,"red"),false);
	}

}
