package tests;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import cs340.model.player.TradeOffer;
import shared.definitions.ResourceType;
import shared.locations.HexLocation;
import shared.locations.VertexDirection;
import shared.locations.VertexLocation;
import shared.locations.VertexObject;
import shared.proxy.Proxy;
import shared.proxy.request.AcceptTrade;
import shared.proxy.request.MaritimeTrade;
import shared.proxy.request.Monopoly;
import shared.proxy.request.Monument;
import gamemanager.game.DevCardList;
import gamemanager.game.Game;
import gamemanager.player.Player;
import gamemanager.player.ResourceList;

public class CanDoTests {

	Game game;
	Player[] players;
	ResourceList resources;
	DevCardList devCards;
	Player player;
	Proxy proxy;
	
	@Before
	public void setUp() {
		proxy = new Proxy("MapFile.json");
		game = proxy.getGame();
		players = game.getPlayers();
		resources = new ResourceList();
		devCards = players[0].getOldDevCards();
		players[0].setResources(resources);
		player = players[0];
		game.getTurnTracker().setStatus("Playing");
	}
	
	@Test
	public void testAcceptTrade() {
		//good trade
		AcceptTrade trade = new AcceptTrade(0,true);
		resources.setBrick(1);
		TradeOffer tradeOffer = new TradeOffer();
		tradeOffer.getOffer().setBrick(-1);
		tradeOffer.getOffer().setOre(1);
		tradeOffer.setSender(1);
		tradeOffer.setReciever(0);
		game.setTradeOffer(tradeOffer);
		assertTrue(game.canAcceptTrade(trade));
		
		//invalid player
		trade = new AcceptTrade(4,true);
		assertTrue(!game.canAcceptTrade(trade));
		
		//not enough resources
		trade = new AcceptTrade(0,true);
		tradeOffer.getOffer().setBrick(0);
		tradeOffer.getOffer().setSheep(-1);
		assertTrue(!game.canAcceptTrade(trade));
		
		//"giving" resources
		tradeOffer.getOffer().setOre(-1);
		resources.setSheep(1);
		assertTrue(!game.canAcceptTrade(trade));
	}
	
	@Test
	public void testCanMaritimeTrade() {
		proxy = new Proxy("TradeOfferTest.json");
		game = proxy.getGame();
		//2 ratio, sufficient resources, no port
		MaritimeTrade trade = new MaritimeTrade(0, 2, "ore", "sheep");
		resources.setOre(2);
		//assertTrue(!game.canMaritimeTrade(trade));
		
		//2 ratio, sufficient resources, port
		VertexObject settlement = new VertexObject(0, new VertexLocation(new HexLocation(1,-3), VertexDirection.SE));
		//game.getMap().addSettlement(settlement);
		assertTrue(game.canMaritimeTrade(trade));
		
		//3 ratio, sufficient resources, port
		VertexObject settlement2 = new VertexObject(1, new VertexLocation(new HexLocation(-3,0), VertexDirection.SE));
		//game.getMap().addSettlement(settlement2);
		players[1].getResources().setBrick(3);
		trade = new MaritimeTrade(1,3, "brick", "sheep");
		game.getTurnTracker().setCurrentTurn(1);
		assertTrue(game.canMaritimeTrade(trade));
		
		//4 ratio, not current turn
		trade = new MaritimeTrade(0,4,"ore","sheep");
		//assertTrue(!game.canMaritimeTrade(trade));
		
		//4 ratio, current turn, not "playing"
		game.getTurnTracker().setCurrentTurn(0);
		game.getTurnTracker().setStatus("Discarding");
		//assertTrue(!game.canMaritimeTrade(trade));
		
		//4 ratio, insufficient resources
		game.getTurnTracker().setStatus("Playing");
		players[0].getResources().setOre(1);
		//assertTrue(!game.canMaritimeTrade(trade));
		
		//4 ratio, sufficient resources
		resources.setBrick(4);
		trade = new MaritimeTrade(0,4,"brick","sheep");
		assertTrue(game.canMaritimeTrade(trade));
	}
	
	@Test
	public void testMonopoly() {
		//correct turn, enough cards, haven't played dev card, correct status
		Monopoly monopoly = new Monopoly(ResourceType.BRICK, 0);
		game.getTurnTracker().setCurrentTurn(0);
		game.getTurnTracker().setStatus("Playing");
		devCards.setMonopoly(1);
		player.setPlayedDev(false);
		assertTrue(game.canPlayMonopoly(monopoly));
		
		//no monopoly card
		devCards.setMonopoly(0);
		assertFalse(game.canPlayMonopoly(monopoly));
		devCards.setMonopoly(1);
		
		//already played dev card
		player.setPlayedDev(true);
		assertFalse(game.canPlayMonopoly(monopoly));
		player.setPlayedDev(false);
		
		//wrong turn
		game.getTurnTracker().setCurrentTurn(1);
		assertFalse(game.canPlayMonopoly(monopoly));
		game.getTurnTracker().setCurrentTurn(0);
		
		//wrong status
		game.getTurnTracker().setStatus("Discarding");
		assertFalse(game.canPlayMonopoly(monopoly));
	}
	
	@Test
	public void testMonument() {
		//Correct turn, enough cards and victory points
		game.getTurnTracker().setCurrentTurn(0);
		game.getPlayer(0).getOldDevCards().setMonument(1);
		game.getPlayer(0).setVictoryPoints(9);
		Monument monument = new Monument(0);
		assertTrue(game.canMonument(monument));
		
		//incorrect turn
		game.getTurnTracker().setCurrentTurn(1);
		assertFalse(game.canMonument(monument));
		
		//not enough victory points
		game.getTurnTracker().setCurrentTurn(0);
		game.getPlayer(0).setVictoryPoints(8);
		assertFalse(game.canMonument(monument));
		
		//not enough cards
		game.getPlayer(0).getOldDevCards().setMonument(0);
		game.getPlayer(0).setVictoryPoints(10);
		assertFalse(game.canMonument(monument));
		
		//enough cards with new dev cards
		game.getPlayer(0).setVictoryPoints(6);
		game.getPlayer(0).getNewDevCards().setMonument(1);
		game.getPlayer(0).getOldDevCards().setMonument(3);
		assertFalse(game.canMonument(monument));
	}
}
