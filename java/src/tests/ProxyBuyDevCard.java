package tests;

import static org.junit.Assert.*;
import gamemanager.game.Game;

import org.junit.Before;
import org.junit.Test;

import shared.proxy.MoveType;
import shared.proxy.Proxy;
import shared.proxy.request.BuyDevCardRequest;
import shared.proxy.request.Credentials;
import shared.proxy.request.JoinGameRequest;

public class ProxyBuyDevCard {
	
	Proxy proxy; 
	
	@Before
	public void setUp() {
		proxy = new Proxy("DevCardTests.json");
		proxy.userLogin(new Credentials("Brooke","brooke"));
		proxy.gamesJoin(new JoinGameRequest(2,"red"),false);
	}
	
	@Test
	public void testBuyDevCard() {
		//current player
		BuyDevCardRequest request = new BuyDevCardRequest(MoveType.BUY_DEV_CARD, 0);
		Game game = proxy.buyDevCard(request);
		assertTrue(game != null);
		
		//not a valid player
//		request = new BuyDevCardRequest(MoveType.BUY_DEV_CARD, -1);
//		game = proxy.buyDevCard(request);
//		assertTrue(game == null);
		
		//not a valid player
//		request = new BuyDevCardRequest(MoveType.BUY_DEV_CARD, -1);
//		game = proxy.buyDevCard(request);
//		assertTrue(game == null);
	}
}
