package tests;

import gamemanager.map.Map;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import shared.locations.HexLocation;
import shared.locations.VertexDirection;
import shared.locations.VertexLocation;
import shared.proxy.Proxy;

public class CanBuildSettlementInit {
	
	Proxy myProxy;
	Map map;
	
	@Before 
	public void init(){
		myProxy = new Proxy("BuildSettlementInitTest.json");
		map = myProxy.getGame().getMap();
	}

	@Test
	public void testBuildSettlementInStartUp(){
		
		boolean canBuild = false;
		
		VertexLocation atLocation = new VertexLocation(new HexLocation(0,0),VertexDirection.E);
		canBuild = map.canBuildSettlement(atLocation, 1,true);
		Assert.assertEquals("Try to build a settlement where there is already a settlement",false,canBuild);
		
		atLocation = new VertexLocation(new HexLocation(0,0),VertexDirection.NE);
		canBuild = map.canBuildSettlement(atLocation, 1,true);
		Assert.assertEquals("Try to build a settlement too close to another settlement",false,canBuild);
		
		atLocation = new VertexLocation(new HexLocation(0,0),VertexDirection.SE);
		canBuild = map.canBuildSettlement(atLocation, 1,true);
		Assert.assertEquals("Try to build a settlement too close to another settlement",false,canBuild);
		
		atLocation = new VertexLocation(new HexLocation(0,0),VertexDirection.NW);
		canBuild = map.canBuildSettlement(atLocation, 1,true);
		Assert.assertEquals("Try to build a settlement two spaces away",true,canBuild);
	}
}
