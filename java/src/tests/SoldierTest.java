package tests;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Stack;

import gamemanager.game.Game;

import org.junit.Before;
import org.junit.Test;

import com.google.gson.JsonObject;

import cs340.model.devcard.DevCardType;
import shared.locations.HexLocation;
import shared.proxy.MoveType;
import shared.proxy.Proxy;
import shared.proxy.request.BuyDevCardRequest;
import shared.proxy.request.Credentials;
import shared.proxy.request.FinishTurnRequest;
import shared.proxy.request.JoinGameRequest;
import shared.proxy.request.Soldier;

public class SoldierTest {
	
	Proxy proxy = new Proxy("BuildSettlementInitTest.json");
	ArrayList<DevCardType> cardsForTest = new ArrayList<DevCardType>();
	HexLocation hexlocation = new HexLocation(4,6);
	Soldier soldier = new Soldier(0,0,hexlocation);
	@Before
	public void setUp() {
		//System.out.println("===== Test case setup =====");
		proxy.userLogin(new Credentials("Brooke","brooke"));
		proxy.gamesJoin(new JoinGameRequest(0,"red"),false);
		//System.out.println("===========================");
		//System.out.println();
	}
	@Test
	public void testLogin() {
		//System.out.println("===== Testing Login =====");
		if (proxy.gameModel() != null)
		{
			//System.out.println();
		}
		//System.out.print("gameModel: ");
		assertTrue(true);
		//System.out.println();
	}
	
	@Test
	public void testBuyDevCard() {
		//System.out.println("===== Testing Buy Developnment Card (valid player)=====");
		//current player
//		BuyDevCardRequest request = new BuyDevCardRequest(MoveType.SOLDIER, 0);
		//System.out.println(request);
//		Game game = proxy.buyDevCard(request);
		assertTrue(true);
		//System.out.println(game);
	}
	
	@Test
	public void testBuyDevCardInvalidPlayer() {
		//not a valid player
		//System.out.println("===== Testing Buy Developnment Card (invalid player) =====");
		//BuyDevCardRequest request = new BuyDevCardRequest(MoveType.SOLDIER, 0);
		//Game game = proxy.buyDevCard(request);
//		BuyDevCardRequest request = new BuyDevCardRequest(MoveType.BUY_DEV_CARD, -1);
//		Game game = proxy.buyDevCard(request);
		assertTrue(true);
		//System.out.println();
	}
	 @Test
	 public void testSoldierJSON(){
		// System.out.println("===== Testing Soldier =====");
		 BuyDevCardRequest request = new BuyDevCardRequest(MoveType.SOLDIER,0);
		 //Game game = proxy.buyDevCard(request);
		 soldier = new Soldier(0,0,hexlocation);
		 String json =  soldier.toJson().toString();
		 if(soldier.toJson() == null)
		 {
			 fail("JSON gets nothing");
		 }
		 else
		 {
			 //System.out.println("JSON form of soldier: " + json);
			 assertTrue(json != null);
		 }
		 
	 }
	 @Test
	 public void testSoldierWithDifferentLocation(){
		 //System.out.println("===== Testing Soldier (Different Location) =====");
		 
		 soldier = new Soldier(3,5,hexlocation);
		 String json =  soldier.toJson().toString();
		 if(soldier.toJson() == null)
		 {
			 fail("JSON gets nothing");
		 }
		 else
		 {
			// System.out.println("JSON form of soldier: " + json);
			 assertTrue(json != null);
		 }
	 }
	 @Test
	 public void testSoldierWithDifferentLocationMultipleTimes() {
		// System.out.println("===== Testing Soldier (Multiple Location) =====");
		 for(int x = 0;x < 12;x++)
		 {
			 for(int y = 0; y < 12; y++)
			 {
				 hexlocation = new HexLocation(x,y);
				 soldier = new Soldier(0,3,hexlocation);
				 String json = soldier.toJson().toString();
				 if (json != null)
				 {
					//System.out.println(json);
					assertTrue(true);
				 }
				 else
				 {
					 fail("JSON gets nothing");
				 }
			 }
		 }
	 }
}
