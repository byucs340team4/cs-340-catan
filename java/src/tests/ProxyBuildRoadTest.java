package tests;

import static org.junit.Assert.*;
import gamemanager.game.Game;
import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;

import shared.locations.EdgeDirection;
import shared.locations.EdgeLocation;
import shared.locations.HexLocation;
import shared.locations.VertexDirection;
import shared.locations.VertexLocation;
import shared.proxy.Proxy;
import shared.proxy.request.BuildRoad;
import shared.proxy.request.BuildSettlement;
import shared.proxy.request.Credentials;
import shared.proxy.request.JoinGameRequest;

public class ProxyBuildRoadTest {

Proxy myProxy = new Proxy("Playing.json");
	
	@Test 
	public void buildRoadInvalid(){
		//login();
		BuildRoad request = new BuildRoad(1,new EdgeLocation(new HexLocation(-10,-10),EdgeDirection.S),true);
		Game game = myProxy.buildRoad(request);
		Assert.assertEquals("Bogus input should give a null game", null,game);
	}
	
	@Test
	public void buildRoadValid(){
		login();
		BuildRoad request = new BuildRoad(1,new EdgeLocation(new HexLocation(0,0),EdgeDirection.S),true);
		Game game = myProxy.buildRoad(request);
		System.out.println(game);
		Assert.assertEquals("Good input should give a game object back", true,(game!=null));
	}
	
	public void login(){
		myProxy.userLogin(new Credentials("Brooke","brooke"));
		myProxy.gamesJoin(new JoinGameRequest(2,"red"),false);
	}

}
