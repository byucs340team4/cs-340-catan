package tests;

import static org.junit.Assert.*;
import gamemanager.game.Game;

import org.junit.Test;

import shared.proxy.Proxy;

public class DeserializerTest {

	@Test
	public void test() {
		Proxy proxy = new Proxy("MapFile.json");
		@SuppressWarnings("unused")
		Game game = proxy.getGame();
		assertTrue(game.getBank().getBrick() == 26);
		assertTrue(game.getVersion() == 3);
	}

}
