package tests.facade.user;

import static org.junit.Assert.*;

import org.junit.Test;

import server.facade.user.UserFacade;
import shared.proxy.request.Credentials;
import shared.proxy.response.LoginResponse;

public class UserFacadeTest {
	
	@Test
	public void test() {
		
		LoginResponse resp;
		String username1 = "Sam";
		String password1 = "sam";
		String username2 = "AAA";
		String password2 = "aaaaa";
		String invalidUsername = "!@#$";
		String shortpassword = "123";
		
		UserFacade userFacade = new UserFacade();
		
		Credentials cred1 = new Credentials(username1, password1);
		resp = userFacade.login(cred1);
		assertTrue(resp.getBody() == "Success");
		resp = userFacade.register(cred1);
		assertFalse(resp.getBody() ==  "Success");
		
		Credentials cred2 = new Credentials(username2, password2);
		resp = userFacade.login(cred2);
		assertFalse(resp.getBody() == "Success");
		resp = userFacade.register(cred2);
		assertTrue(resp.getBody() == "Success");
		
		Credentials cred3 = new Credentials(invalidUsername, shortpassword);
		resp = userFacade.login(cred3);
		assertFalse(resp.getBody() == "Success");
		resp = userFacade.register(cred3);
		assertFalse(resp.getBody() == "Success");
		
		// register
		
	}

}
