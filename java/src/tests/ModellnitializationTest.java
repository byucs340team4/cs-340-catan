package tests;

import junit.framework.Assert;
import gamemanager.game.Game;

import org.junit.Test;

import shared.locations.HexLocation;
import shared.proxy.Proxy;

public class ModellnitializationTest {

	@Test
	public void testInitializeModel(){
		Proxy myProxy = new Proxy("MapFile.json");
		Game game = myProxy.getGame();
		int numberOfHexes = game.getMap().getHexes().size();
		Assert.assertEquals("Check that 19 hexes have been added to the map", 19,numberOfHexes);
		int numberOfRoads = game.getMap().getRoads().size();
		Assert.assertEquals("Check that there is one road in the map", 1, numberOfRoads);
		int numberOfSettlements = game.getMap().getSettlements().size();
		Assert.assertEquals("Check that there is one settlement in the map",1,numberOfSettlements);
		int numberOfCities = game.getMap().getCities().size();
		Assert.assertEquals("Check that there is one city in the map",1,numberOfCities);
		int numberOfPorts = game.getMap().getPorts().size();
		Assert.assertEquals("Check that there are nine ports",9,numberOfPorts);
		int numberOfPlayers = game.getPlayers().length;
		Assert.assertEquals("There are four players in the game",4,numberOfPlayers);
		boolean robberIsCorrect = (new HexLocation(0,-2)).equals(game.getMap().getRobber());
		Assert.assertEquals("Robber location is equal to location in file",true,robberIsCorrect);
		
	}
}
