package tests;

import static org.junit.Assert.*;
import gamemanager.game.Game;

import org.junit.Before;
import org.junit.Test;

import shared.locations.EdgeDirection;
import shared.locations.EdgeLocation;
import shared.locations.HexLocation;
import shared.proxy.Proxy;
import shared.proxy.request.BuildRoad;

public class CanBuildRoad {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void test() {
		//get map with a road at 0,0 on the SE for player 1
		Proxy proxy = new Proxy("ForRoadTest.json");
		Game game = proxy.getGame();
		
		//check roads around the road at 0,0 for player 0, only S and NE should return true
		assertTrue(game.canBuildRoad(new BuildRoad(0,new EdgeLocation(new HexLocation(0,0), EdgeDirection.S), false)));
		assertTrue(game.canBuildRoad(new BuildRoad(0,new EdgeLocation(new HexLocation(0,0), EdgeDirection.NE), false)));
		assertFalse(game.canBuildRoad(new BuildRoad(0,new EdgeLocation(new HexLocation(0,0), EdgeDirection.SE), false)));
		assertFalse(game.canBuildRoad(new BuildRoad(0,new EdgeLocation(new HexLocation(0,0), EdgeDirection.N), false)));
		assertFalse(game.canBuildRoad(new BuildRoad(0,new EdgeLocation(new HexLocation(0,0), EdgeDirection.SW), false)));
		assertFalse(game.canBuildRoad(new BuildRoad(0,new EdgeLocation(new HexLocation(0,0), EdgeDirection.NW), false)));
		
		
		//check the same hex but with a different player, all should return false
		assertFalse(game.canBuildRoad(new BuildRoad(1,new EdgeLocation(new HexLocation(0,0), EdgeDirection.S), false)));
		assertFalse(game.canBuildRoad(new BuildRoad(1,new EdgeLocation(new HexLocation(0,0), EdgeDirection.SW), false)));
		assertFalse(game.canBuildRoad(new BuildRoad(1,new EdgeLocation(new HexLocation(0,0), EdgeDirection.SE), false)));
		assertFalse(game.canBuildRoad(new BuildRoad(1,new EdgeLocation(new HexLocation(0,0), EdgeDirection.NW), false)));
		assertFalse(game.canBuildRoad(new BuildRoad(1,new EdgeLocation(new HexLocation(0,0), EdgeDirection.N), false)));
		assertFalse(game.canBuildRoad(new BuildRoad(1,new EdgeLocation(new HexLocation(0,0), EdgeDirection.NE), false)));
		
		//check the edge of the map for edge cases to make sure they dont blow up the map
		assertFalse(game.canBuildRoad(new BuildRoad(0,new EdgeLocation(new HexLocation(2,0), EdgeDirection.S), false)));
		assertFalse(game.canBuildRoad(new BuildRoad(0,new EdgeLocation(new HexLocation(2,0), EdgeDirection.SW), false)));
		assertFalse(game.canBuildRoad(new BuildRoad(0,new EdgeLocation(new HexLocation(2,0), EdgeDirection.SE), false)));
		assertFalse(game.canBuildRoad(new BuildRoad(0,new EdgeLocation(new HexLocation(2,0), EdgeDirection.NW), false)));
		assertFalse(game.canBuildRoad(new BuildRoad(0,new EdgeLocation(new HexLocation(2,0), EdgeDirection.N), false)));
		assertFalse(game.canBuildRoad(new BuildRoad(0,new EdgeLocation(new HexLocation(2,0), EdgeDirection.NE), false)));
		
		//get map that is set during the initialization phase to check for building a road next to a settlement
		proxy = new Proxy("MapFile.json");
		game = proxy.getGame();
//		assertTrue(game.canBuildRoad(new BuildRoad(1,new EdgeLocation(new HexLocation(0,0), EdgeDirection.S), false)));
//		assertTrue(game.canBuildRoad(new BuildRoad(1,new EdgeLocation(new HexLocation(0,0), EdgeDirection.SE), false)));
//		assertFalse(game.canBuildRoad(new BuildRoad(1,new EdgeLocation(new HexLocation(0,0), EdgeDirection.NE), false)));
//		assertFalse(game.canBuildRoad(new BuildRoad(1,new EdgeLocation(new HexLocation(0,0), EdgeDirection.N), false)));
//		assertFalse(game.canBuildRoad(new BuildRoad(1,new EdgeLocation(new HexLocation(0,0), EdgeDirection.SW), false)));
//		assertFalse(game.canBuildRoad(new BuildRoad(1,new EdgeLocation(new HexLocation(0,0), EdgeDirection.NW), false)));
		
		//player with resources should be able to build road when not in setup
		assertTrue(game.getPlayer(0).canBuildRoad());
		//player without resources cannot build a road when not in setup
		assertFalse(game.getPlayer(1).canBuildRoad());
	}

}
