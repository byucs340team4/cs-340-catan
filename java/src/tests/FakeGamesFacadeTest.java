package tests;

import static org.junit.Assert.*;

import org.junit.Test;

import server.facade.games.FakeGamesFacade;
import shared.proxy.response.*;

public class FakeGamesFacadeTest {

	private FakeGamesFacade fakeFacade = new FakeGamesFacade();
	private IResponse response;

	@Test
	public void testListGame() {
		response = null;
		this.response = this.fakeFacade.listGame();
		assertTrue(response!=null);
	}

	@Test
	public void testCreateGame() {
		response = null;
		this.response = this.fakeFacade.createGame(null);
		assertTrue(this.response!=null);
	}

	@Test
	public void testJoinGame() {
		response = null;
		this.response = this.fakeFacade.joinGame(null, 0);
		assertTrue(response!=null);
	}

	@Test
	public void testLoadGame() {
		response = null;
		this.response = this.fakeFacade.loadGame(null);
		assertTrue(this.response!=null);
	}

	@Test
	public void testSaveGame() {
		response = null;
		this.response = this.fakeFacade.saveGame(null);
		assertTrue(this.response!=null);
	}

}
