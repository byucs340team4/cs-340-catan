package tests;

import gamemanager.game.Game;
import gamemanager.map.Map;
import gamemanager.player.Player;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import shared.locations.HexLocation;
import shared.locations.VertexDirection;
import shared.locations.VertexLocation;
import shared.proxy.Proxy;
import shared.proxy.request.BuildSettlement;

public class CanBuildCity {

	Proxy myProxy;
	Map map;
	Game game;
	Player p2;
	
	@Before 
	public void init(){
		myProxy = new Proxy("BuildCityTestMap.json");
		game = myProxy.getGame();
		map = game.getMap();
		p2 = game.getPlayers()[1];

	}
	
	@Test
	public void canBuildCity(){
		boolean canBuild = false;
		
		VertexLocation atLocation = new VertexLocation(new HexLocation(0,0),VertexDirection.NW);
		canBuild = map.canBuildCity(atLocation, 1);
		Assert.assertEquals("Try to build a city where there is no settlement",false,canBuild);
		
		atLocation = new VertexLocation(new HexLocation(-1,1),VertexDirection.W);
		canBuild = map.canBuildCity(atLocation, 1);
		Assert.assertEquals("Try to build a city where there is no settlement on edge",false,canBuild);

		atLocation = new VertexLocation(new HexLocation(0,0),VertexDirection.E);
		canBuild = map.canBuildCity(atLocation, 1);
		Assert.assertEquals("Try to build a city where someone else has a settlement",false,canBuild);
		
		atLocation = new VertexLocation(new HexLocation(-1,1),VertexDirection.SE);
		canBuild = map.canBuildCity(atLocation, 1);
		Assert.assertEquals("Try to build a city where someone else has a settlement on the edge",false,canBuild);
		
		atLocation = new VertexLocation(new HexLocation(0,0),VertexDirection.SW);
		canBuild = map.canBuildCity(atLocation, 1);
		Assert.assertEquals("Try to build a city where you have a settlement",true,canBuild);
		
		atLocation = new VertexLocation(new HexLocation(0,1),VertexDirection.SE);
		canBuild = map.canBuildCity(atLocation, 1);
		Assert.assertEquals("Try to build a city where you have a settlement on the edge",true,canBuild);
		
		atLocation = new VertexLocation(new HexLocation(0,0),VertexDirection.W);
		canBuild = map.canBuildCity(atLocation, 1);
		Assert.assertEquals("Try to build a city where there is already a city",false,canBuild);
		
		atLocation = new VertexLocation(new HexLocation(1,0),VertexDirection.NW);
		canBuild = map.canBuildCity(atLocation, 1);
		Assert.assertEquals("Try to build a city where there is already a city on the edge",false,canBuild);
	}
	
//	@Test
//	public void testCanBuildCityPlayer(){
//		boolean canBuild = p2.canBuildCity();
//		p2.buildCity();
//		Assert.assertEquals("Player 2 can build city on turn with the right resources", true,canBuild);
//		
//		canBuild = p2.canBuildCity();
//		Assert.assertEquals("Player 2 cannot build on turn without the resources", false,canBuild);
//		
//		game.getTurnTracker().setStatus("Playing");
//		canBuild = game.canBuildSettlement(new BuildSettlement(1,null,false));
//		Assert.assertEquals("Player cannot build when it is not their turn", false,canBuild);
//
//		
//	}
	
}
