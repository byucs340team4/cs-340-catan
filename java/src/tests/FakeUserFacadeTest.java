package tests;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import server.facade.user.FakeUserFacade;
import shared.proxy.response.IResponse;

public class FakeUserFacadeTest {

	private FakeUserFacade fakeFacade = new FakeUserFacade();
	
	private IResponse response;
	
	@Before
	public void setUp() throws Exception {
		response = null;
	}

	@Test
	public void testLogin() {
		this.response = fakeFacade.login(null);
		assertTrue(response!=null);
	}

	@Test
	public void testRegister() {
		response = fakeFacade.register(null);
		assertTrue(response!=null);
	}

}
