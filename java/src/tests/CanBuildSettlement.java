package tests;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import gamemanager.game.Game;
import gamemanager.map.Map;
import gamemanager.player.Player;
import shared.locations.HexLocation;
import shared.locations.VertexDirection;
import shared.locations.VertexLocation;
import shared.proxy.Proxy;
import shared.proxy.request.BuildSettlement;

public class CanBuildSettlement {
	
	Proxy myProxy;
	Map map;
	Player p1;
	Player p2;
	Game game;
	
	@Before
	public void init(){
		myProxy = new Proxy("BuildSettlementTest.json");
		game = myProxy.getGame();
		map = game.getMap();
		p1 = game.getPlayers()[0];
		p2 = game.getPlayers()[1];
	}
	
	@Test
	public void testCanBuildSettlementMap(){
		
		boolean canBuild = false;
		
		VertexLocation atLocation = new VertexLocation(new HexLocation(-1,0),VertexDirection.NW);
		canBuild = map.canBuildSettlement(atLocation, 1,false);
		Assert.assertEquals("Try to build a settlement where there is already a settlement",false,canBuild);
		
		atLocation = new VertexLocation(new HexLocation(0,0),VertexDirection.NW);
		canBuild = map.canBuildSettlement(atLocation, 1,false);
		Assert.assertEquals("Try to build a settlement where there is already a settlement",false,canBuild);
		
		atLocation = new VertexLocation(new HexLocation(-1,0),VertexDirection.NE);
		canBuild = map.canBuildSettlement(atLocation, 1,false);
		Assert.assertEquals("Try to build a settlement too close to another settlement",false,canBuild);
		
		atLocation = new VertexLocation(new HexLocation(-1,0),VertexDirection.W);
		canBuild = map.canBuildSettlement(atLocation, 1,false);
		Assert.assertEquals("Try to build a settlement too close to another settlement",false,canBuild);
		
		atLocation = new VertexLocation(new HexLocation(0,0),VertexDirection.SW);
		canBuild = map.canBuildSettlement(atLocation, 1,false);
		Assert.assertEquals("Try to build a settlement on another player's roads",false,canBuild);
		
		atLocation = new VertexLocation(new HexLocation(0,-1),VertexDirection.NE);
		canBuild = map.canBuildSettlement(atLocation, 1,false);
		Assert.assertEquals("Try to build a settlement on another player's roads",false,canBuild);
		
		atLocation = new VertexLocation(new HexLocation(0,-1),VertexDirection.NE);
		canBuild = map.canBuildSettlement(atLocation, 1,false);
		Assert.assertEquals("Try to build a settlement on another player's roads",false,canBuild);
		
		atLocation = new VertexLocation(new HexLocation(0,1),VertexDirection.SE);
		canBuild = map.canBuildSettlement(atLocation, 1,false);
		Assert.assertEquals("Try to build a settlement where there are no roads",false,canBuild);
		
		atLocation = new VertexLocation(new HexLocation(1,-1),VertexDirection.NE);
		canBuild = map.canBuildSettlement(atLocation, 1,false);
		Assert.assertEquals("Try to build a settlement in valid location",true,canBuild);
		
		atLocation = new VertexLocation(new HexLocation(0,0),VertexDirection.E);
		canBuild = map.canBuildSettlement(atLocation, 1,false);
		Assert.assertEquals("Try to build a settlement in valud location",true,canBuild);
	}
	
	@Test
	public void testCanBuildSettlementPlayer(){
		boolean canBuild = p2.canBuildSettlement();
		//p2.buildSettlement();
		Assert.assertEquals("Player 2 can build on turn with the right resources", true,canBuild);
		
		canBuild = p2.canBuildSettlement();
		Assert.assertEquals("Player 2 cannot build on turn without the resources", false,canBuild);
		
		game.getTurnTracker().setStatus("Playing");
		canBuild = game.canBuildSettlement(new BuildSettlement(1,null,false));
		Assert.assertEquals("Player cannot build when it is not their turn", false,canBuild);
		
		game.getTurnTracker().setStatus("FirstRound");
		canBuild = game.canBuildSettlement(new BuildSettlement(1,new VertexLocation(new HexLocation(0,0),VertexDirection.NE),false));
		Assert.assertEquals("Player cannot build when it is not their turn in the beginning phase", false,canBuild);
		
		//p2.buildSettlement();
		canBuild = p2.canBuildSettlement();
		Assert.assertEquals("Player cannot build when they are out of settlements", false,canBuild);

		
	}

}
