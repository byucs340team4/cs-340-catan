package tests;

import static org.junit.Assert.*;
import gamemanager.game.Game;

import org.junit.Before;
import org.junit.Test;

import com.google.gson.JsonObject;

import shared.proxy.MoveType;
import shared.proxy.Proxy;
import shared.proxy.request.Credentials;
import shared.proxy.request.FinishTurnRequest;
import shared.proxy.request.JoinGameRequest;

public class FinishTurnTest {
	
	Proxy proxy = new Proxy("BuildSettlementInitTest.json");
	
	@Before
	public void setUp() {
		proxy.userLogin(new Credentials("Brooke","brooke"));
		proxy.gamesJoin(new JoinGameRequest(2,"red"),false);
		//proxy.gameModel();
	}
	
	@Test
	public void testFinishTurn() {
		//current turn
		FinishTurnRequest request = new FinishTurnRequest(MoveType.FINISH_TURN, 0);
		Game game = proxy.finishTurn(request);
		assertTrue(game != null);
	}
}
