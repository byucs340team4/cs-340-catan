package tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class UnitTests {

	@Before
	public void setup() {
	}
	
	@After
	public void teardown() {
	}
	
	public static void main(String[] args) {
		
		String[] testClasses = new String[] {
				"tests.DeserializerTest",
				//"tests.ProxyLoginTest",
				//"tests.FinishTurnTest",
				//"tests.GameModelTest",
				//"tests.JoinGameTest",
				//"tests.OfferTradeTest",
				//"tests.SoldierTest",
				"tests.CanBuildRoad",
				//"tests.ProxyBuildRoadTest",
				"tests.CanBuildCity",
				"tests.CanBuildRoad",
				//"tests.CanBuildSettlement",
				//"tests.CanBuildSettlementInit",
				//"tests.CanBuyDevCard",
				"tests.CanDoTests",
				"tests.CanOfferTradeTest",
				"tests.CanYearOfPlenty",
				"tests.ModellnitializationTest",
				//"tests.PollerTest",
				//"tests.ProxyBuildCityTest",
				//"tests.ProxyBuildRoadTest",
				//"tests.ProxyBuildSettlementTest",
				//"tests.ProxyBuyDevCard",
				//"tests.ProxyLoginTest",
				//"tests.ProxyTests",
				"tests.ProxyYearOfPlenty",
				"tests.RoadCardTest",
				"tests.sendChatCommandTest",
				"tests.SoldierTest",
				"tests.FakeGameFacadeTest",
				"tests.FakeGamesFacadeTest",
				"tests.FakeMovesFacadeTest",
				"tests.FakeUserFacadeTest",
				"tests.command.games.CreateCommandTest",
				"tests.command.games.JoinCommandTest",
				"tests.command.games.ListCommandTest",
				"tests.command.games.SaveTest",
				"tests.command.games.LoadTest",
				"tests.command.moves.AcceptCommandTest",
				"tests.command.moves.BuyDevCommandTest",
				"tests.command.moves.CityCommandTest",
				"tests.command.moves.DiscardCommandTest",
				"tests.command.moves.EndTurnTest",
				"tests.command.moves.MaritimeCommandTest",
				"tests.command.moves.MonopolyCommandTest",
				"tests.command.moves.MonumentCommandTest",
				"tests.command.moves.OfferCommandTest",
				"tests.command.moves.RoadBuildingCommandTest",
				"tests.command.moves.RobCommandTest",
				"tests.command.moves.RollingCommandTest",
				"tests.command.moves.SendChatTest",
				"tests.command.moves.SettlementCommandTest",
				"tests.command.moves.SoldierCommandTest",
				"tests.command.moves.TestYopCommand",
				"tests.command.moves.YearOfPlentyCommandTest",
				"tests.command.user.LoginCommandTest",
				"tests.command.user.RegisterCommandTest",
				"tests.command.game.GameResetTest",
				"tests.command.game.ModelVersionTest",
				"tests.command.game.PostGameCommandsTest"
				
		};

		org.junit.runner.JUnitCore.main(testClasses);
	}
}
