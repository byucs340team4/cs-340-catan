package tests;

import static org.junit.Assert.*;

import org.junit.Test;

import shared.proxy.Proxy;
import shared.proxy.ProxyCommunicator;
import shared.proxy.request.Credentials;
import shared.proxy.request.JoinGameRequest;
import shared.proxy.response.JoinGameResponse;

public class JoinGameTest {

	Proxy myProxy;
	
	public JoinGameTest(){
		myProxy = new Proxy();
	}
	
	/*
	 * Test for success with a user that has already been logged in and is part of a game.
	 */
	@Test
	public void testGamesJoin() {
		myProxy.userLogin(new Credentials("Brooke", "brooke"));
		JoinGameResponse response = myProxy.gamesJoin(new JoinGameRequest(2,"red"),false);
		if(response.isSuccess()){
			assertTrue(true);
			return;
		}else
			fail("Failed when it should have succeeded");
	}
	
	/*
	 * Test for failure when someone joins a game that isn't logged in.
	 */
	@Test
	public void testUnloggedUser(){
		validLogout();
		JoinGameResponse response = myProxy.gamesJoin(new JoinGameRequest(2, "red"),false);
		if(response.isSuccess()){
			fail("Succeeded when it should have failed");
			return;
		}else
			assertTrue(true);
	}
	
	/*
	 * Test for failure with an invalid game id.
	 */
//	@Test
//	public void testInvalidGameID(){
//		validLogin();
//		JoinGameResponse response = myProxy.gamesJoin(new JoinGameRequest(-1,"red"));
//		if(response.isSuccess()){
//			fail("Succeeded when it should have failed");
//		}else
//			assertTrue(true);
//	}
	
	/*
	 * Test for failure due to invalid color choice.
	 */
	@Test
	public void testInvalidColor(){
		validLogin();
		JoinGameResponse response = myProxy.gamesJoin(new JoinGameRequest(2,"limegreen"),false);
		if(response.isSuccess()){
			fail("Succeeded when it should have failed");
		}else
			assertTrue(true);
	}
	
	
	private void validLogin(){
		myProxy.userLogin(new Credentials("Brooke", "brooke"));
	}
	
	private void validLogout(){
		ProxyCommunicator.getSingleton().setUserCookie(null);
	}
}
