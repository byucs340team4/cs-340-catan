package tests;

import static org.junit.Assert.*;

import org.junit.Test;

import server.facade.game.FakeGameFacade;
import shared.proxy.request.*;
import shared.proxy.response.IResponse;

public class FakeGameFacadeTest {

	private FakeGameFacade fakeFacade = new FakeGameFacade();
	private IRequest request;
	private IResponse response;
	
	
	@Test
	public void listAITest() {
		response = null;
		request = new ListAIRequest();
		String response = this.fakeFacade.listAI((ListAIRequest)this.request);
		assertTrue(response==null);
	}
	
	@Test
	public void modelTest(){
		response = null;
		this.response = this.fakeFacade.model(0);
		assertTrue(this.response!=null);
	}
		
	@Test
	public void resetTest(){
		response = null;
		this.response = this.fakeFacade.reset(0);
		assertTrue(this.response!=null);
	}
	
	@Test
	public void getCommandTest(){
		response = null;
		this.response = this.fakeFacade.getCommands(0);
		assertTrue(this.response!=null);
	}
	
	@Test
	public void postCommandsTest(){
		response = null;
		this.response = this.fakeFacade.postCommands(0, "test");
		assertTrue(this.response!=null);
	}
	
	@Test
	public void addAITest(){
		this.request = new AddAIRequest(null);
		String response = this.fakeFacade.addAI((AddAIRequest)this.request);
		assertTrue(response==null);
	}

}
