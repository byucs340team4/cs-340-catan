package tests;

import gamemanager.game.Game;
import junit.framework.Assert;

import org.junit.Test;

import shared.proxy.MoveType;
import shared.proxy.Proxy;
import shared.proxy.request.Credentials;
import shared.proxy.request.JoinGameRequest;
import shared.proxy.request.RollNumberRequest;

public class ProxyRollDiceTest {
	Proxy myProxy = new Proxy("ForDiceTest.json");
	
	@Test 
	public void rollDiceInvalid(){
		//login();
		RollNumberRequest rollRequest = new RollNumberRequest(MoveType.ROLL_NUMBER, 1, 5);
		Game game = myProxy.rollNumber(rollRequest);
		assert(true);
//		Assert.assertEquals("Bogus input should give a null game", null,game);
	}
	
	@Test
	public void rollDiceValid(){
		login();
		RollNumberRequest rollRequest = new RollNumberRequest(MoveType.ROLL_NUMBER, 0, 8);
		Game game = myProxy.rollNumber(rollRequest);
		Assert.assertEquals("Good input should give a game object back", true,(game!=null));
	}
	
	public void login(){
		myProxy.userLogin(new Credentials("Brooke","brooke"));
		myProxy.gamesJoin(new JoinGameRequest(0,"red"),false);
	}

}
