package tests;

import static org.junit.Assert.*;
import gamemanager.game.Game;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import shared.locations.EdgeDirection;
import shared.locations.EdgeLocation;
import shared.locations.HexLocation;
import shared.proxy.MoveType;
import shared.proxy.Proxy;
import shared.proxy.request.RoadBuildingRequest;

public class RoadCardTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void validTest() {
		Proxy proxy = new Proxy("ForRoadTest.json");
		Game game = proxy.getGame();
		
		boolean result = false;
		
		//this is a good location, it should return true
		EdgeLocation edge0 = new EdgeLocation(new HexLocation(0,0), EdgeDirection.S);
		EdgeLocation edge1 = new EdgeLocation(new HexLocation(0,0), EdgeDirection.NE);
		RoadBuildingRequest road = new RoadBuildingRequest(MoveType.ROAD_BUILDING, 0, edge0, edge1);
		result = game.canRoadBuiding(road);
		Assert.assertEquals("this is a good location, it should return true", result,true);
	}
	
	@Test
	public void invalidTest() {
		Proxy proxy = new Proxy("ForRoadTest.json");
		Game game = proxy.getGame();
		
		boolean result = false;
		
		//this is a bad location, it should return false
		EdgeLocation edge0 = new EdgeLocation(new HexLocation(0,0), EdgeDirection.S);
		EdgeLocation edge1 = new EdgeLocation(new HexLocation(0,0), EdgeDirection.N);
		RoadBuildingRequest road = new RoadBuildingRequest(MoveType.ROAD_BUILDING, 0, edge0, edge1);
		result = game.canRoadBuiding(road);
		Assert.assertEquals("this is a bad location, it should return false", result,false);
	}

}
