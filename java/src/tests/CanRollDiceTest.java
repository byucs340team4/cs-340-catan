package tests;

import static org.junit.Assert.*;
import gamemanager.game.Game;

import org.junit.Before;
import org.junit.Test;

import shared.locations.EdgeDirection;
import shared.locations.EdgeLocation;
import shared.locations.HexLocation;
import shared.proxy.MoveType;
import shared.proxy.Proxy;
import shared.proxy.request.BuildRoad;
import shared.proxy.request.RollNumberRequest;

public class CanRollDiceTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void validTest() {
		//get map in rolling phase and player 0's turn
		Proxy proxy = new Proxy("ForDiceTest.json");
		Game game = proxy.getGame();
		
		RollNumberRequest rollRequest = new RollNumberRequest(MoveType.ROLL_NUMBER, 0, 5);
		
		boolean canRoll = game.canRoll(rollRequest);
		
		//player 0 is trying to roll with a valid number, should return true
		assertTrue(canRoll);
		
		
		rollRequest = new RollNumberRequest(MoveType.ROLL_NUMBER, 1, 5);
		
		canRoll = game.canRoll(rollRequest);
		
		//player 1 is trying to roll with a valid number, should return false because its not his turn
		assertFalse(canRoll);
		
	}
	@Test
	public void invalidTest() {
		//get map in rolling phase and player 0's turn
		Proxy proxy = new Proxy("ForDiceTest.json");
		Game game = proxy.getGame();
		
		RollNumberRequest rollRequest = new RollNumberRequest(MoveType.ROLL_NUMBER, 0, 5);
		
		boolean canRoll = game.canRoll(rollRequest);
		
		//player 0 is trying to roll with a valid number, should return true
		assertTrue(canRoll);
		
		
		rollRequest = new RollNumberRequest(MoveType.ROLL_NUMBER, 1, 5);
		
		canRoll = game.canRoll(rollRequest);
		
		//player 1 is trying to roll with a valid number, should return false because its not his turn
		assertFalse(canRoll);
		
	}

}
