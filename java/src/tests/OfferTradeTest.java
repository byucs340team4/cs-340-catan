package tests;

import static org.junit.Assert.*;
import gamemanager.communication.TurnTracker;
import gamemanager.game.Game;
import gamemanager.player.Player;
import gamemanager.player.ResourceList;

import org.junit.Before;
import org.junit.Test;

import cs340.model.player.ResourceHand;
import cs340.model.player.TradeOffer;
import shared.proxy.Proxy;
import shared.proxy.request.Credentials;
import shared.proxy.request.JoinGameRequest;
import shared.proxy.request.OfferTrade;

public class OfferTradeTest {

	Proxy myProxy;
	Player player;
	TradeOffer tradeOffer;
	TurnTracker turnTracker;
	Game game;
	
	public OfferTradeTest(){
		myProxy = new Proxy();
		player = new Player();
		tradeOffer = new TradeOffer();
		turnTracker = new TurnTracker();
		game = new Game();
	}
	
	@Before
	public void initialize(){
		validLogin();
		validJoin();
		ResourceList resources = new ResourceList(3,1,0,2,1);
		player.setResources(resources);
		player.setOrderNumber(0);
		Player[] players = {player};
		tradeOffer = new TradeOffer();
		ResourceHand resourceHand = new ResourceHand();
		resourceHand.setBrick(2);
		resourceHand.setSheep(-2);
		tradeOffer.setSender(player.getOrderNumber());
		tradeOffer.setReciever(1);
		tradeOffer.setOffer(resourceHand);
		
		turnTracker.setCurrentTurn(0);
		turnTracker.setStatus("Playing");
		
		game.setPlayers(players);
		game.setTurnTracker(turnTracker);
		game.setTradeOffer(tradeOffer);
	}
	
	/*
	 * Test for success with good offer to trade with valid arguments.
	 */
	@Test
	public void testOfferTrade() {
		ResourceList list = new ResourceList(tradeOffer.getOffer().getBrick(),
				tradeOffer.getOffer().getBrick(),tradeOffer.getOffer().getBrick(),
				tradeOffer.getOffer().getBrick(),tradeOffer.getOffer().getBrick());
		OfferTrade offer = new OfferTrade(player.getOrderNumber(), list, 2);
		Game game = myProxy.offerTrade(offer);
		if(game!=null)
			assertTrue(true);
		else
			fail("Failed when it should have succeeded");
	}
	
	private void validLogin(){
		myProxy.userLogin(new Credentials("Brooke", "brooke"));
	}
	
	private void validJoin(){
		myProxy.gamesJoin(new JoinGameRequest(2, "red"),false);
	}	

}
