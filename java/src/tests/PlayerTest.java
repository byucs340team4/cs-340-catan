package tests;

import static org.junit.Assert.*;
import gamemanager.GameManager;
import gamemanager.game.Deck;
import gamemanager.game.DevCardList;
import gamemanager.game.Game;
import gamemanager.game.TradeOffer;
import gamemanager.gamestate.GamePlayState;
import gamemanager.player.Player;
import gamemanager.player.ResourceList;

import org.junit.Test;

public class PlayerTest {

	@Test
	public void testCanAcceptTrade() {
		ResourceList trade = new ResourceList();
		Player player = new Player();
		ResourceList resources = new ResourceList();
		player.setResources(resources);
		
		//valid trade
		trade.setBrick(1);
		trade.setOre(-1);
		resources.setOre(1);
		//assertTrue(player.canAcceptTrade(trade));
		
		//invalid trade
		resources.setOre(0);
	}
	
	@Test
	public void testCanBuyDevCard() {
		Player player = new Player();
		Game game = new Game();
		game.setDeck(new Deck());
		player.setGame(game);
		ResourceList resources = new ResourceList();
		player.setResources(resources);
		GameManager.getInstance().setGameState(new GamePlayState(null));
		
		//not enough resources
		assertTrue(!player.canBuyDevCard());
		resources.setOre(1);
		resources.setSheep(1);
		resources.setWheat(1);
		
		//enough resources
		assertTrue(player.canBuyDevCard());
	}
	
	@Test
	public void testBuyDevCard() {
		GameManager.getInstance().setGameState(new GamePlayState(null));
 		Player player = new Player();
		Game game = new Game();
		game.setDeck(new Deck());
		player.setGame(game);
		player.setNewDevCards(new DevCardList());
		ResourceList resources = new ResourceList();
		player.setResources(resources);
		resources.setOre(1);
		resources.setSheep(1);
		resources.setWheat(1);
		//player.buyDevCard();
		
		//hand should contain no resources
		assertTrue(resources.getSheep() == 0);
		assertTrue(resources.getOre() == 0);
		assertTrue(resources.getBrick() == 0);
		assertTrue(resources.getWheat() == 0);
		assertTrue(resources.getWood() == 0);
		
		//hand should contain only 1 dev card
		int sum = player.getNewDevCards().getMonopoly() + player.getNewDevCards().getMonument() + player.getNewDevCards().getRoadBuilding();
		sum += player.getNewDevCards().getSoldier() + player.getNewDevCards().getYearOfPlenty();
		assertTrue(sum == 1);
	}

	@Test
	public void testCanBuildRoad() {
		Player player = new Player();
		ResourceList resources = new ResourceList();
		player.setResources(resources);
		player.setRoads(14);
		
		//not enough resources
		assertTrue(!player.canBuildRoad());
		
		//enough resources
		resources.setBrick(1);
		resources.setWood(1);
		assertTrue(player.canBuildRoad());
		
		//too many roads
		player.setRoads(0);
		assertTrue(!player.canBuildRoad());
	}
	
	@Test
	public void testBuildRoad() {
		Player player = new Player();
		ResourceList resources = new ResourceList();
		player.setResources(resources);
		
		//one road built
		resources.setBrick(1);
		resources.setWood(1);
		player.setRoads(1);
		//player.buildRoad();
		assertTrue(player.getRoads() == 0);
		
		//hand should contain no resources after building road
		assertTrue(resources.getSheep() == 0);
		assertTrue(resources.getOre() == 0);
		assertTrue(resources.getBrick() == 0);
		assertTrue(resources.getWheat() == 0);
		assertTrue(resources.getWood() == 0);
	}

	@Test
	public void testCanBuildSettlement() {
		Player player = new Player();
		player.setRoads(14);
		player.setCities(3);
		player.setSettlements(4);
		ResourceList resources = new ResourceList();
		player.setResources(resources);
		
		//not enough resources
		assertTrue(!player.canBuildCity());
		
		//enough resources
		resources.setWheat(1);
		resources.setSheep(1);
		resources.setBrick(1);
		resources.setWood(1);
		assertTrue(player.canBuildSettlement());
		
		//too many settlements
		player.setSettlements(0);
		assertTrue(!player.canBuildSettlement());
	}
	
	@Test
	public void testBuildSettlement() {
		Player player = new Player();
		ResourceList resources = new ResourceList();
		player.setResources(resources);
		
		//enough resources
		resources.setBrick(1);
		resources.setWheat(1);
		resources.setSheep(1);
		resources.setBrick(1);
		resources.setWood(1);
		player.setSettlements(1);
		//player.buildSettlement();
		assertTrue(player.getSettlements() == 0);
		
		//hand should contain no resources after building settlement
		assertTrue(resources.getSheep() == 0);
		assertTrue(resources.getOre() == 0);
		assertTrue(resources.getBrick() == 0);
		assertTrue(resources.getWheat() == 0);
		assertTrue(resources.getWood() == 0);
	}

	@Test
	public void testCanBuildCity() {
		Player player = new Player();
		player.setRoads(14);
		player.setCities(3);
		player.setSettlements(1);
		ResourceList resources = new ResourceList();
		player.setResources(resources);
		
		//not enough resources
		assertTrue(!player.canBuildCity());
		
		//enough resources and settlements
		resources.setWheat(2);
		resources.setOre(3);
		player.setCities(3);
		assertTrue(player.canBuildCity());
		
		//not enough settlements
		player.setSettlements(5);
		assertTrue(player.canBuildCity());

		//too many cities
		player.setSettlements(1);
		player.setCities(0);
		assertTrue(!player.canBuildCity());
	}
	
	@Test 
	public void testBuildCity() {
		Player player = new Player();
		ResourceList resources = new ResourceList();
		player.setResources(resources);
		
		//enough resources
		resources.setWheat(2);
		resources.setOre(3);
		player.setSettlements(0);
		player.setCities(2);
		player.buildCity();
		assertTrue(player.getCities() == 1);
		
		//hand should be empty after building city
		assertTrue(resources.getSheep() == 0);
		assertTrue(resources.getOre() == 0);
		assertTrue(resources.getBrick() == 0);
		assertTrue(resources.getWheat() == 0);
		assertTrue(resources.getWood() == 0);
		
		//no settlements should remain
		assertTrue(player.getSettlements() == 1);
	}

}
