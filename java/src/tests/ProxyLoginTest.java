package tests;

import static org.junit.Assert.*;

import java.security.InvalidParameterException;

import org.junit.Test;

import shared.proxy.Proxy;
import shared.proxy.request.Credentials;
import shared.proxy.response.LoginResponse;

public class ProxyLoginTest extends Proxy {

	Proxy myProxy;
	
	public ProxyLoginTest(){
		myProxy = new Proxy();
	}
	
	/*
	 * Successful login test. Good username and good password and should successfully login.
	 */
	@Test
	public void testUserLogin() {
		LoginResponse response = myProxy.userLogin(new Credentials("Brooke","brooke"));
		if(response.isResponse()){
			assertTrue(true);
		}else{
			
			fail("Failed when it should have succeeded");
		}
	}
	
	/*
	 * Test failure due to incorrect password.
	 */
	@Test
	public void testInvalidLogin(){
		LoginResponse response = myProxy.userLogin(new Credentials("Brooke", "Brooke"));
		if(response.isResponse()){
			fail("Succeeded when it should have failed");
			return;
		}else{
			assertTrue(true);
		}
	}
	
	/*
	 * Test failure due to invalid username.
	 */
	@Test
	public void testInvalidUsernameLogin(){
		LoginResponse response = myProxy.userLogin(new Credentials("Tom", "Bombadil"));
		if(response.isResponse()){
			fail("Succeeded when it should have failed");
			return;
		}else{
			assertTrue(true);
		}
	}
	
	/*
	 * Test failure due to null values for username and password.
	 */
	@Test
	public void testNullLogin(){
		try{
			LoginResponse response = myProxy.userLogin(new Credentials(null, null));
			fail("Succeeded when it should have failed");
		}catch(InvalidParameterException e){
			assertTrue(true);
			return;
		}
			
	}

}
