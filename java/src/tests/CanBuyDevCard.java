package tests;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;

import gamemanager.GameManager;
import gamemanager.game.Game;
import gamemanager.gamestate.GamePlayState;
import gamemanager.map.Map;
import gamemanager.player.Player;
import shared.proxy.MoveType;
import shared.proxy.Proxy;
import shared.proxy.request.BuyDevCardRequest;

public class CanBuyDevCard {
	
	Proxy myProxy;
	Game game;
	Player p1;
	Map map;
	
	@Before
	public void init(){
		myProxy = new Proxy("DevCardTests.json");
		game = myProxy.getGame();
		map = game.getMap();
		p1 = game.getPlayers()[0];
	}
	
	@Test
	public void testCanBuildDevCard(){
		game.getTurnTracker().setStatus("FirstRound");
		GameManager.getInstance().setGameState(new GamePlayState(game));
		boolean canBuild = game.canBuyDevCard(new BuyDevCardRequest(MoveType.BUY_DEV_CARD, 0));
		Assert.assertEquals("Can't buy card when you aren't in playing phase",false,canBuild);
				
		game.getTurnTracker().setStatus("Playing");

		canBuild = p1.canBuyDevCard();
		Assert.assertEquals("Player with the right resources can buy dev card",true,canBuild);
		
		canBuild = game.canBuyDevCard(new BuyDevCardRequest(MoveType.BUY_DEV_CARD, 0));
		Assert.assertEquals("Can buy dev card when it's your turn", true,canBuild);
		
		canBuild = game.canBuyDevCard(new BuyDevCardRequest(MoveType.BUY_DEV_CARD, 1));
		Assert.assertEquals("Can't buy dev card when it's not your turn", false,canBuild);
		
		//p1.buyDevCard();
		canBuild = game.canBuyDevCard(new BuyDevCardRequest(MoveType.BUY_DEV_CARD, 0));
		Assert.assertEquals("Can't buy dev card when you don't have enough resources", false,canBuild);
		
		
		
	}

}
