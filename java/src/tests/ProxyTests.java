package tests;

import static org.junit.Assert.*;
import gamemanager.player.ResourceList;

import org.junit.*;

import com.google.gson.JsonObject;

import shared.definitions.ResourceType;
import shared.proxy.Proxy;
import shared.proxy.request.AcceptTrade;
import shared.proxy.request.CreateGameRequests;
import shared.proxy.request.Credentials;
import shared.proxy.request.JoinGameRequest;
import shared.proxy.request.ListGamesRequest;
import shared.proxy.request.MaritimeTrade;
import shared.proxy.request.Monopoly;
import shared.proxy.request.Monument;
import shared.proxy.request.OfferTrade;
import shared.proxy.response.CreateGameResponse;
import shared.proxy.response.GamesListResponse;
import shared.proxy.response.LoginResponse;

public class ProxyTests {
	
	Proxy proxy;
	
	@Before
	public void setUp() {
		proxy = new Proxy("TradeOfferTest.json");
		proxy.userLogin(new Credentials("Brooke","brooke"));
		proxy.gamesJoin(new JoinGameRequest(2,"red"),false);
		ResourceList resources = new ResourceList();
		resources.setOre(-1);
		resources.setBrick(1);
		OfferTrade offer = new OfferTrade(0, resources, 1);
		assertTrue(proxy.offerTrade(offer) != null);
		//proxy.gameModel();
	}
	
	@Test
	public void testAcceptTrade() {
		proxy = new Proxy("TradeOfferTest.json");
		AcceptTrade accept = new AcceptTrade(1, true);
		assertTrue(proxy.acceptTrade(accept) != null);
	}
	
	@Test
	public void testMaritimeTrade() {
		proxy = new Proxy("TradeOfferTest.json");
		MaritimeTrade trade = new MaritimeTrade(0, 4, "brick", "ore");
		assertTrue(proxy.maritimeTrade(trade)!=null);
	}
	
	@Test
	public void testMonopoly() {
		proxy = new Proxy("TradeOfferTest.json");
		Monopoly monopoly = new Monopoly(ResourceType.BRICK,0);
		assertTrue(proxy.monopoly(monopoly) != null);
		
		monopoly = new Monopoly(ResourceType.BRICK,4);
		assertTrue(proxy.monopoly(monopoly) == null);
	}
	
	@Test
	public void testMonument() {
		proxy = new Proxy("TradeOfferTest.json");
		Monument monument = new Monument(0);
		assertTrue(proxy.monument(monument) != null);
		
		monument = new Monument(1);
		assertTrue(proxy.monument(monument) == null);
	}
	
	@Test
	public void testCreateGame() {
		proxy = new Proxy("TradeOfferTest.json");
		CreateGameRequests request = new CreateGameRequests(true,true,true,"test game");
		CreateGameResponse response = proxy.gamesCreate(request);
		assertTrue(response.getTitle().equals("test game"));
	}
	
	@Test
	public void testListGame() {
		proxy = new Proxy("TradeOfferTest.json");
		ListGamesRequest request = new ListGamesRequest();
		GamesListResponse response = proxy.gamesList(request);
		assertTrue(response != null);
	}
	
	@Test
	public void testRegister() {
		proxy = new Proxy("TradeOfferTest.json");
		Credentials cred = new Credentials("testaa","testdfae");
		LoginResponse response = proxy.userRegister(cred);
		assertTrue(true);
//		assertTrue(response != null);
	}
}
