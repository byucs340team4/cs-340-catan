package tests;

import static org.junit.Assert.*;
import gamemanager.Poller;
import gamemanager.game.Game;

import org.junit.Before;
import org.junit.Test;

import shared.proxy.Proxy;
import shared.proxy.request.Credentials;
import shared.proxy.request.JoinGameRequest;

public class PollerTest {

	Poller poller;
	Proxy proxy;
	
	public PollerTest(){
		poller = new Poller();
	}
	
	@Before
	public void setUp() {
		proxy = new Proxy();
		proxy.userLogin(new Credentials("Brooke","brooke"));
		proxy.gamesJoin(new JoinGameRequest(2,"red"),false);
	}
	
	@Test
	public void testPoll() {
		poller.setProxy(proxy);
		assertTrue(poller.getGame() == null);
		poller.poll();
		
		try {
			Thread.sleep(6000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		assertTrue(poller.getGame() != null);
//		fail("Not yet implemented");
	}

}
