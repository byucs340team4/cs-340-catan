package tests;

import junit.framework.Assert;
import gamemanager.game.Game;
import gamemanager.map.Map;
import gamemanager.player.Player;

import org.junit.Before;
import org.junit.Test;

import shared.definitions.ResourceType;
import shared.proxy.MoveType;
import shared.proxy.Proxy;
import shared.proxy.request.YearOfPlentyRequest;

public class CanYearOfPlenty {
	
	Proxy myProxy;
	Game game;
	Player p1;
	Map map;
	
	@Before
	public void init(){
		myProxy = new Proxy("BuildSettlementInitTest.json");
		game = myProxy.getGame();
		map = game.getMap();
		p1 = game.getPlayers()[0];
	}
	
	@Test
	public void canPlayYearOfPlenty(){
		YearOfPlentyRequest request = new YearOfPlentyRequest(MoveType.YEAR_OF_PLENTY, 0, ResourceType.BRICK, ResourceType.BRICK);
		boolean canPlay = game.canYoP(request);
		Assert.assertEquals("Should be able to play year of plenty on turn w/ prev bought one in hand", true,canPlay);
		
		request = new YearOfPlentyRequest(MoveType.YEAR_OF_PLENTY, 0, ResourceType.ORE, ResourceType.ORE);
		canPlay = game.canYoP(request);
		Assert.assertEquals("Cannot get resources that are out", false,canPlay);			
		
		game.getTurnTracker().setCurrentTurn(1);
		request = new YearOfPlentyRequest(MoveType.YEAR_OF_PLENTY, 1, ResourceType.BRICK, ResourceType.BRICK);
		canPlay = game.canYoP(request);
		Assert.assertEquals("Cannot play if you don't have a YoP card", false,canPlay);
		
		request = new YearOfPlentyRequest(MoveType.YEAR_OF_PLENTY, 0, ResourceType.BRICK, ResourceType.BRICK);
		canPlay = game.canYoP(request);
		Assert.assertEquals("Cannot play if it isn't your turn", false,canPlay);
		
		

	}

}
