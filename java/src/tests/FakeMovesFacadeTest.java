package tests;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import server.facade.moves.FakeMovesFacade;
import shared.proxy.response.IResponse;

public class FakeMovesFacadeTest {

	private FakeMovesFacade fakeFacade = new FakeMovesFacade();
	
	private IResponse response;
	
	@Before
	public void setUp() throws Exception {
		response = null;
	}

	@Test
	public void testBuildCity() {
		response = fakeFacade.buildCity(null, 0);
		assertTrue(response!=null);
	}

	@Test
	public void testBuildRoad() {
		response = fakeFacade.buildRoad(null, 0);
		assertTrue(response!=null);
	}

	@Test
	public void testBuildSettlement() {
		response = fakeFacade.buildSettlement(null, 0);
		assertTrue(response!=null);
	}

	@Test
	public void testRobPlayer() {
		response = fakeFacade.robPlayer(null, 0);
		assertTrue(response!=null);
	}

	@Test
	public void testRollNumber() {
		response = fakeFacade.rollNumber(null, 0);
		assertTrue(response!=null);
	}

	@Test
	public void testSendChat() {
		response = fakeFacade.sendChat(null, 0);
		assertTrue(response!=null);
	}

	@Test
	public void testAcceptTrade() {
		response = fakeFacade.acceptTrade(null, 0);
		assertTrue(response!=null);
	}

	@Test
	public void testBuyDevCard() {
		response = fakeFacade.buyDevCard(null, 0);
		assertTrue(response!=null);
	}

	@Test
	public void testDiscardCards() {
		response = fakeFacade.discardCards(null, 0);
		assertTrue(response!=null);
	}

	@Test
	public void testFinishTurn() {
		response = fakeFacade.finishTurn(null, 0);
		assertTrue(response!=null);
	}

	@Test
	public void testMaritimeTrade() {
		response = fakeFacade.maritimeTrade(null, 0);
		assertTrue(response!=null);
	}

	@Test
	public void testMonopoly() {
		response = fakeFacade.monopoly(null, 0);
		assertTrue(response!=null);
	}

	@Test
	public void testMonument() {
		response = fakeFacade.monument(null, 0);
		assertTrue(response!=null);
	}

	@Test
	public void testOfferTrade() {
		response = fakeFacade.offerTrade(null, 0);
		assertTrue(response!=null);
	}

	@Test
	public void testRoadBuilding() {
		response = fakeFacade.roadBuilding(null, 0);
		assertTrue(response!=null);
	}

	@Test
	public void testSoldier() {
		response = fakeFacade.soldier(null, 0);
		assertTrue(response!=null);
	}

	@Test
	public void testYOP() {
		response = fakeFacade.YOP(null, 0);
		assertTrue(response!=null);
	}

}
