package tests.command.games;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import server.Server;
import server.commands.games.LoadCommand;
import server.commands.games.SaveCommand;
import shared.proxy.request.LoadGameRequest;
import shared.proxy.request.SaveGameRequest;
import shared.proxy.response.SaveGameResponse;

public class SaveTest {

	@Before
	public void init() {
		LoadGameRequest request = new LoadGameRequest("save");
		LoadCommand command = new LoadCommand(request);
		command.execute();
	}
	
	@Test
	public void test() {
		int index = Server.getInstance().getGameByName("save");
		SaveGameRequest request = new SaveGameRequest("test",index);
		SaveCommand command = new SaveCommand(request);
		SaveGameResponse response = (SaveGameResponse) command.execute();
		assertTrue(response.getSuccess());
		
		index = Server.getInstance().getGameList().size();
		request = new SaveGameRequest("test",index);
		command = new SaveCommand(request);
		response = (SaveGameResponse) command.execute();
		assertFalse(response.getSuccess());
	}

}
