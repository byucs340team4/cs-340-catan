package tests.command.games;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import server.Server;
import server.commands.games.LoadCommand;
import shared.proxy.request.LoadGameRequest;
import shared.proxy.response.LoadGameResponse;

public class LoadTest {

	@Before
	public void init() {
		int index = Server.getInstance().getGameByName("save");
		if(index != -1) {
			Server.getInstance().getGameList().remove(index);
		}
	}
	
	@Test
	public void test() {
		int numberOfGames = Server.getInstance().getGameList().size();
		LoadGameRequest request = new LoadGameRequest("save");
		LoadCommand command = new LoadCommand(request);
		LoadGameResponse response = (LoadGameResponse) command.execute();
		assertTrue(response.getSuccess());
		assertTrue(Server.getInstance().getGameList().size() == numberOfGames + 1);
		
		numberOfGames = Server.getInstance().getGameList().size();
		request = new LoadGameRequest("YouBetterNotMakeASaveGameFileWithThisNameOrThisTestWillFail");
		command = new LoadCommand(request);
		response = (LoadGameResponse) command.execute();
		assertFalse(response.getSuccess());
		assertTrue(Server.getInstance().getGameList().size() == numberOfGames);
	}

}
