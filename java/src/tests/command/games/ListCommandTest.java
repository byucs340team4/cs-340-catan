package tests.command.games;

import java.util.ArrayList;
import java.util.List;

import gamemanager.game.Game;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import client.data.GameInfo;
import server.commands.games.JoinCommand;
import server.commands.games.ListCommand;
import shared.proxy.Proxy;
import shared.proxy.request.JoinGameRequest;
import shared.proxy.request.ListGamesRequest;
import shared.proxy.response.GamesListResponse;
import shared.proxy.response.JoinGameResponse;

public class ListCommandTest {
	List<Game>gameList;
	@Before
	public void init(){
		gameList = new ArrayList<Game>();
		Proxy proxy = new Proxy("BuildCityTestMap.json");
		Game game = proxy.getGame(); game.setTitle("Test1");
		proxy = new Proxy("BuildCityTestMap.json");
		Game game2 = proxy.getGame(); game2.setTitle("Test2");
		gameList.add(game);
		gameList.add(game2);
	}
	
	@Test
	public void testJoinCommand(){
		ListGamesRequest request = new ListGamesRequest();
		ListCommand command = new ListCommand(request,gameList);
		GamesListResponse response = (GamesListResponse) command.execute();
		List<GameInfo> gameInfo = response.getGames();
		
		String title = gameInfo.get(0).getTitle();
		int numPlayers = gameInfo.get(0).getPlayers().size();
		Assert.assertEquals("Title should be Test1", "Test1",title);
		Assert.assertEquals("Number of players should be 4", numPlayers,4);
		
		title = gameInfo.get(1).getTitle();
		numPlayers = gameInfo.get(1).getPlayers().size();
		Assert.assertEquals("Title should be Test2", "Test2",title);
		Assert.assertEquals("Number of players should be 4", numPlayers,4);

		
	}

}
