package tests.command.games;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import server.Server;
import server.commands.games.CreateCommand;
import shared.proxy.request.CreateGameRequests;
import shared.proxy.response.CreateGameResponse;

public class CreateCommandTest {
	
	@Before
	public void init(){
		Server server = Server.getInstance();
		//server.run(8081);
	}
	
	@Test
	public void createCommandTest(){
		CreateGameRequests request = new CreateGameRequests(false, false, false, "Test");
		CreateCommand command = new CreateCommand(request);
		CreateGameResponse response = (CreateGameResponse) command.execute();
		
		Assert.assertEquals("Title should be Test","Test",response.getTitle());
		Assert.assertEquals("GameId should be 2", 2,response.getId());
		
		response = (CreateGameResponse) command.execute();
		Assert.assertEquals("Title should be Test","Test",response.getTitle());
		Assert.assertEquals("GameId should be 3", 3,response.getId());
	}

}
