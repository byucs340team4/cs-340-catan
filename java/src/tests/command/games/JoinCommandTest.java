package tests.command.games;

import gamemanager.game.Game;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import server.commands.games.JoinCommand;
import server.commands.moves.YearOfPlentyCommand;
import shared.definitions.ResourceType;
import shared.proxy.MoveType;
import shared.proxy.Proxy;
import shared.proxy.request.JoinGameRequest;
import shared.proxy.request.YearOfPlentyRequest;
import shared.proxy.response.JoinGameResponse;

public class JoinCommandTest {
	
Game game;
	
	@Before
	public void init(){
		Proxy proxy = new Proxy("BuildCityTestMap.json");
		game = proxy.getGame();
	}
	
	@Test
	public void testJoinCommand(){
		JoinGameRequest request = new JoinGameRequest(0, "yellow");
		JoinCommand command = new JoinCommand(request,0,game);
		JoinGameResponse response = (JoinGameResponse) command.execute();
		boolean successful = response.isSuccess();
		
		Assert.assertEquals("Should be successful", true,successful);
		
		request.setColor("red");
		command = new JoinCommand(request,0,game);
		response = (JoinGameResponse) command.execute();
		successful = response.isSuccess();
		
		Assert.assertEquals("Should not be successful", false,successful);
		
		command = new JoinCommand(request,5,game);
		response = (JoinGameResponse) command.execute();
		successful = response.isSuccess();
		
		Assert.assertEquals("Should not be successful", false,successful);
		
	}

}
