package tests.command.game;

import gamemanager.game.Game;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import server.commands.moves.FinishTurnCommand;
import shared.proxy.MoveType;
import shared.proxy.Proxy;
import shared.proxy.request.FinishTurnRequest;

public class ModelVersionTest {
Game game;

	
	@Before
	public void init(){
		Proxy proxy = new Proxy("BuildCityTestMap.json");
		game = proxy.getGame();
	}
	
	@Test
	public void testRollingCommand(){
		FinishTurnRequest request = new FinishTurnRequest(MoveType.FINISH_TURN, 1);
		FinishTurnCommand command = new FinishTurnCommand(request,game);
		command.execute();
		int currTurn = game.getTurnTracker().getCurrentTurn();
		String status = game.getTurnTracker().getStatus();
		
		Assert.assertEquals("Should be rolling", "Rolling",status);
		Assert.assertEquals("Should be p2's turn", 2,currTurn);
		
		Object response = command.execute();
		Assert.assertEquals("Should not be able to finish turn twice in a row", null,response);

	}
}
