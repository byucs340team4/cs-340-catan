package tests.command.game;

import gamemanager.game.Game;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Scanner;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import server.commands.game.PostCommandsCommand;
import server.commands.game.ResetCommand;
import shared.proxy.Proxy;

public class GameResetTest {
	Game game;

	@Before
	public void init() {
		Proxy proxy = new Proxy("reset.json");
		game = proxy.getGame();
	}

	@Test
	public void testPostCommand() throws FileNotFoundException {
		Scanner comm = new Scanner(new BufferedReader(new FileReader("commandsList.json")));
		StringBuilder com = new StringBuilder();

		while (comm.hasNext()) {
			com.append(comm.nextLine() + "\n");
		}
		ResetCommand command = new ResetCommand(game);

		command.testExecute(com.toString());
		
		int currTurn = game.getTurnTracker().getCurrentTurn();
		String status = game.getTurnTracker().getStatus();
		Assert.assertEquals("Runs commands till game is in a playing state, first part is Rolling, so should be Rolling now",		status, "Rolling");
		Assert.assertEquals("Should now be player 0's turn", currTurn, 0);
		
	}
}
