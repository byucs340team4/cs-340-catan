package tests.command.game;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Scanner;

import gamemanager.game.Game;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import server.commands.game.PostCommandsCommand;
import shared.proxy.Proxy;

public class PostGameCommandsTest {
	Game game;

	@Before
	public void init() {
		Proxy proxy = new Proxy("reset.json");
		game = proxy.getGame();
	}

	@Test
	public void testPostCommand() throws FileNotFoundException {
		Scanner comm = new Scanner(new BufferedReader(new FileReader(
				"commandsList.json")));
		StringBuilder com = new StringBuilder();

		while (comm.hasNext()) {
			com.append(comm.nextLine() + "\n");
		}
		PostCommandsCommand command = new PostCommandsCommand(game,
				com.toString());

		command.testExecute();
		int currTurn = game.getTurnTracker().getCurrentTurn();
		String status = game.getTurnTracker().getStatus();
		Assert.assertEquals("Runs commands till game is in a playing state",
				status, "Playing");
		Assert.assertEquals("Should now be player 3's turn", currTurn, 3);
	}
}
