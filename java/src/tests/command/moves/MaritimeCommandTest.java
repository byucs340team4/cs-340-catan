package tests.command.moves;

import gamemanager.game.Game;
import gamemanager.player.ResourceList;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import server.commands.moves.MaritimeTradeCommand;
import server.commands.moves.OfferTradeCommand;
import shared.definitions.ResourceType;
import shared.proxy.Proxy;
import shared.proxy.request.MaritimeTrade;
import shared.proxy.request.OfferTrade;

public class MaritimeCommandTest {

Game game;
	
	@Before
	public void init(){
		Proxy proxy = new Proxy("CommandSettlementTest.json");
		game = proxy.getGame();
	}
	
	@Test
	public void testOfferCommand(){
		game.getTurnTracker().setCurrentTurn(0);
		
		MaritimeTrade request = new MaritimeTrade(0, 2, "brick", "ore");
		MaritimeTradeCommand command = new MaritimeTradeCommand(game,request);
		command.execute();
		int brick = game.getPlayer(0).getResourceAmount(ResourceType.BRICK);
		int ore = game.getPlayer(0).getResourceAmount(ResourceType.ORE);
		
		Assert.assertEquals("Should have 11 ore", 11,ore);
		Assert.assertEquals("Should have 8 brick", 8,brick);
		
		request = new MaritimeTrade(0,4,"brick","ore");
		command = new MaritimeTradeCommand(game,request);
		command.execute();

		brick = game.getPlayer(0).getResourceAmount(ResourceType.BRICK);
		ore = game.getPlayer(0).getResourceAmount(ResourceType.ORE);
		
		Assert.assertEquals("Should have 12 ore", 12,ore);
		Assert.assertEquals("Should have 4 brick", 4,brick);
	}
}
