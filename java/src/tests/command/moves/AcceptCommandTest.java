package tests.command.moves;

import gamemanager.game.Game;
import gamemanager.player.ResourceList;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import server.commands.moves.AcceptTradeCommand;
import server.commands.moves.BuildSettlementCommand;
import server.commands.moves.OfferTradeCommand;
import shared.definitions.ResourceType;
import shared.locations.HexLocation;
import shared.locations.VertexDirection;
import shared.locations.VertexLocation;
import shared.proxy.Proxy;
import shared.proxy.request.AcceptTrade;
import shared.proxy.request.BuildSettlement;
import shared.proxy.request.OfferTrade;

public class AcceptCommandTest {
	
	Game game;
	
	@Before
	public void init(){
		Proxy proxy = new Proxy("CommandSettlementTest.json");
		game = proxy.getGame();
	}
	
	@Test
	public void testSettlementCommand(){
		AcceptTrade request = new AcceptTrade(1,true);
		AcceptTradeCommand command = new AcceptTradeCommand(request,game);
		Object response = command.execute();
		Assert.assertEquals("Shouldn't be able to accept trade if there isn't an offer",null,response);
		
		ResourceList trade = new ResourceList();
		trade.setBrick(-2);
		trade.setWood(1);
		OfferTrade req = new OfferTrade(1, trade, 2);
		OfferTradeCommand comm = new OfferTradeCommand(game,req);
		comm.execute();
		
		request = new AcceptTrade(2,true);
		command = new AcceptTradeCommand(request, game);
		command.execute();
		
		int wood = game.getPlayer(1).getResourceAmount(ResourceType.WOOD);
		int brick = game.getPlayer(1).getResourceAmount(ResourceType.BRICK);
		
		Assert.assertEquals("Should have 0 wood", 0,wood);
		Assert.assertEquals("Should have 3 brick",3,brick);
		
	}

}
