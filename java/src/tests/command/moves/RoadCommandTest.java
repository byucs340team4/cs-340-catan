package tests.command.moves;

import gamemanager.game.Game;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import server.commands.moves.BuildRoadCommand;
import shared.locations.EdgeDirection;
import shared.locations.EdgeLocation;
import shared.locations.HexLocation;
import shared.proxy.Proxy;
import shared.proxy.request.BuildRoad;

public class RoadCommandTest {
	
	Game game;
	
	@Before
	public void init(){
		Proxy proxy = new Proxy("ForRoadTest.json");
		game = proxy.getGame();
	}
	
	@Test
	public void testRoadCommand(){
		BuildRoad request = new BuildRoad(0,new EdgeLocation(new HexLocation(0,0), EdgeDirection.S), false);
		BuildRoadCommand command = new BuildRoadCommand(request,game);
		command.execute();
		int numRoads = game.getPlayers()[0].getRoads();
		Assert.assertEquals("One roads should be built", numRoads,14);
		
		request = new BuildRoad(0,new EdgeLocation(new HexLocation(3,0), EdgeDirection.S), false);
		command = new BuildRoadCommand(request,game);
		command.execute();
		numRoads = game.getPlayers()[0].getRoads();
		Assert.assertEquals("Should not build second road",numRoads,14);
	}
}
