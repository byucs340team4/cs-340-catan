package tests.command.moves;

import gamemanager.game.Game;
import gamemanager.player.ResourceList;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import server.commands.moves.DiscardCardsCommand;
import server.commands.moves.OfferTradeCommand;
import shared.definitions.ResourceType;
import shared.proxy.Proxy;
import shared.proxy.request.DiscardCards;
import shared.proxy.request.OfferTrade;

public class DiscardCommandTest {

Game game;
	
	@Before
	public void init(){
		Proxy proxy = new Proxy("DiscardCommandTest.json");
		game = proxy.getGame();
	}
	
	@Test
	public void testOfferCommand(){
		ResourceList trade = new ResourceList();
		trade.setBrick(4);
		DiscardCards request = new DiscardCards(2, trade);
		DiscardCardsCommand command = new DiscardCardsCommand(request,game);
		Object response = command.execute();
		
		Assert.assertEquals("Can't discard cards you don't have", null,response);
		
		trade.setBrick(0);
		trade.setWheat(5);
		request.setDiscarded(trade);
		command = new DiscardCardsCommand(request,game);
		command.execute();
		
		int wheat = game.getPlayer(2).getResourceAmount(ResourceType.WHEAT);
		String status = game.getTurnTracker().getStatus();
		
		Assert.assertEquals("Should have 5 wheat",5,wheat);
		Assert.assertEquals("Should be in discarding mode","Discarding",status);
//		game.setTradeOffer(null);
//		trade.setBrick(2);
//		trade.setWood(-1);
//		request = new OfferTrade(1,trade,2);
//		command = new OfferTradeCommand(game,request);
//		Object response = command.execute();
//		
//		Assert.assertEquals("Shouldn't be able to offer trade you don't have resources for", null,response);
		
		
		
	}
}
