package tests.command.moves;

import gamemanager.game.Game;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import server.commands.moves.BuildCityCommand;
import server.commands.moves.RollNumberCommand;
import shared.locations.HexLocation;
import shared.locations.VertexDirection;
import shared.locations.VertexLocation;
import shared.proxy.MoveType;
import shared.proxy.Proxy;
import shared.proxy.request.BuildCity;
import shared.proxy.request.RollNumberRequest;
import shared.proxy.response.IResponse;

public class RollingCommandTest {

	Game game;

	
	@Before
	public void init(){
		Proxy proxy = new Proxy("RollingMap.json");
		game = proxy.getGame();
	}
	
	@Test
	public void testRollingCommand(){
		RollNumberRequest request = new RollNumberRequest(MoveType.ROLL_NUMBER, 1, 7);
		RollNumberCommand command = new RollNumberCommand(request,game);
		command.execute();
		String status = game.getTurnTracker().getStatus();
		
		Assert.assertEquals("Should be robbing", "Robbing",status);
		
		Object response = command.execute();
		
		Assert.assertEquals("Should not be able to roll twice in a row", null,response);

	}
}
