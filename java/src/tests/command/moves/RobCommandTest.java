package tests.command.moves;

import gamemanager.game.Game;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import server.commands.moves.RobPlayerCommand;
import server.commands.moves.RollNumberCommand;
import shared.locations.HexLocation;
import shared.proxy.MoveType;
import shared.proxy.Proxy;
import shared.proxy.request.RobPlayerRequest;
import shared.proxy.request.RollNumberRequest;

public class RobCommandTest {

	Game game;

	
	@Before
	public void init(){
		Proxy proxy = new Proxy("RollingMap.json");
		game = proxy.getGame();
	}
	
	@Test
	public void testRollingCommand(){
		RollNumberRequest request = new RollNumberRequest(MoveType.ROLL_NUMBER, 1, 7);
		RollNumberCommand command = new RollNumberCommand(request,game);
		command.execute();
		
		RobPlayerRequest req = new RobPlayerRequest(MoveType.ROB_PLAYER,1,2,new HexLocation(-1,1));
		RobPlayerCommand comm = new RobPlayerCommand(req,game);
		comm.execute();
		int newCards = game.getPlayer(1).getResources().total();
		String status = game.getTurnTracker().getStatus();
		
		Assert.assertEquals("Should be robbing", "Playing",status);
		Assert.assertEquals("Should have one more card", 2,newCards);
		Object response = command.execute();
		
		Assert.assertEquals("Should not be able to rob twice in a row", null,response);

	}
}
