package tests.command.moves;

import gamemanager.game.Game;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import server.commands.moves.BuildCityCommand;
import server.commands.moves.BuildSettlementCommand;
import shared.locations.HexLocation;
import shared.locations.VertexDirection;
import shared.locations.VertexLocation;
import shared.proxy.Proxy;
import shared.proxy.request.BuildCity;
import shared.proxy.request.BuildSettlement;

public class SettlementCommandTest {

	Game game;
	
	@Before
	public void init(){
		Proxy proxy = new Proxy("CommandSettlementTest.json");
		game = proxy.getGame();
	}
	
	@Test
	public void testSettlementCommand(){
		BuildSettlement request = new BuildSettlement(1, new VertexLocation(new HexLocation(0,0),VertexDirection.E), false);
		BuildSettlementCommand command = new BuildSettlementCommand(request,game);
		command.execute();
		int numSettlement = game.getPlayers()[1].getSettlements();
		
		Assert.assertEquals("Player 0 should have 4 settlements", 4,numSettlement);
		
		Object response = command.execute();
		Assert.assertEquals("Shouldn't be able to build without resources",null,response);
		
	}
	
}
