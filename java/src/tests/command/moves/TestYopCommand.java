package tests.command.moves;

import gamemanager.game.Game;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import server.commands.moves.BuildRoadCommand;
import server.commands.moves.YearOfPlentyCommand;
import shared.definitions.ResourceType;
import shared.locations.EdgeDirection;
import shared.locations.EdgeLocation;
import shared.locations.HexLocation;
import shared.proxy.MoveType;
import shared.proxy.Proxy;
import shared.proxy.request.BuildRoad;
import shared.proxy.request.YearOfPlentyRequest;

public class TestYopCommand {

	Game game;
	
	@Before
	public void init(){
		Proxy proxy = new Proxy("BuildCityTestMap.json");
		game = proxy.getGame();
	}
	
	@Test
	public void testRoadCommand(){
		YearOfPlentyRequest request = new YearOfPlentyRequest(MoveType.YEAR_OF_PLENTY,1,ResourceType.BRICK,ResourceType.BRICK);
		YearOfPlentyCommand command = new YearOfPlentyCommand(request,game);
		command.execute();
		int numBrick = game.getPlayers()[1].getResourceAmount(ResourceType.BRICK);
		Assert.assertEquals("Should have 2 brick", numBrick,2);
		
		command.execute();
		Assert.assertEquals("Should still have 2 brick", numBrick,2);
		
	}
}
