package tests.command.moves;

import gamemanager.game.Game;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import server.commands.moves.BuyDevCardCommand;
import server.commands.moves.FinishTurnCommand;
import shared.proxy.MoveType;
import shared.proxy.Proxy;
import shared.proxy.request.BuyDevCardRequest;
import shared.proxy.request.FinishTurnRequest;

public class BuyDevCommandTest {

	Game game;

	
	@Before
	public void init(){
		Proxy proxy = new Proxy("BuildCityTestMap.json");
		game = proxy.getGame();
	}
	
	@Test
	public void testRollingCommand(){
		BuyDevCardRequest request = new BuyDevCardRequest(MoveType.BUY_DEV_CARD, 1);
		BuyDevCardCommand command = new BuyDevCardCommand(request,game);
		command.execute();
		int devCards = game.getPlayer(1).getNewDevCards().total();
		
		Assert.assertEquals("Should have 1 dev card", 1,devCards);
		
		Object response = command.execute();
		Assert.assertEquals("Should not be able to buy two dev cards without resources", null,response);

	}
}
