package tests.command.moves;

import junit.framework.Assert;
import gamemanager.game.Game;

import org.junit.Before;
import org.junit.Test;

import server.commands.moves.SendChatCommand;
import shared.proxy.Proxy;
import shared.proxy.request.SendChatRequest;

public class SendChatTest {
	
	Game game;
	
	@Before
	public void init(){
		Proxy proxy = new Proxy("MonumentTest.json");
		game = proxy.getGame();
	}
	
	@Test
	public void sendChat(){
		SendChatRequest request = new SendChatRequest(0, "This is a test");
		SendChatCommand command = new SendChatCommand(request,game);
		command.execute();
		Assert.assertEquals("Should be one message in the game", 1,game.getChat().getLog().size());
		String message = game.getChat().getLog().get(0).getMessage();
		Assert.assertEquals("Messages should be equal", message,"This is a test");
	}
}
