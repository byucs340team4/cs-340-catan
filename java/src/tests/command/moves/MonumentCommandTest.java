package tests.command.moves;

import gamemanager.game.Game;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import server.commands.moves.MonopolyCommand;
import server.commands.moves.MonumentCommand;
import shared.definitions.ResourceType;
import shared.proxy.Proxy;
import shared.proxy.request.Monopoly;
import shared.proxy.request.Monument;

public class MonumentCommandTest {
	
Game game;
	
	@Before
	public void init(){
		Proxy proxy = new Proxy("BuildCityTestMap.json");
		game = proxy.getGame();
	}
	
	@Test
	public void testMonumentCommand(){
		Monument request = new Monument(1);
		MonumentCommand command = new MonumentCommand(request,game);
		command.execute();
		int vicPoints = game.getPlayers()[1].getVictoryPoints();
		Assert.assertEquals("Should have 12 points", vicPoints,12);		
		
	}

}
