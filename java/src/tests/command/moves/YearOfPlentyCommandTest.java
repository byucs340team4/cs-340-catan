package tests.command.moves;

import gamemanager.game.Game;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import server.commands.moves.BuyDevCardCommand;
import server.commands.moves.YearOfPlentyCommand;
import shared.definitions.ResourceType;
import shared.proxy.MoveType;
import shared.proxy.Proxy;
import shared.proxy.request.BuyDevCardRequest;
import shared.proxy.request.YearOfPlentyRequest;

public class YearOfPlentyCommandTest {

	Game game;

	
	@Before
	public void init(){
		Proxy proxy = new Proxy("BuildCityTestMap.json");
		game = proxy.getGame();
	}
	
	@Test
	public void testRollingCommand(){
		YearOfPlentyRequest request = new YearOfPlentyRequest(MoveType.BUY_DEV_CARD, 1, ResourceType.BRICK, ResourceType.BRICK);
		YearOfPlentyCommand command = new YearOfPlentyCommand(request,game);
		command.execute();
		int brick = game.getPlayer(1).getResourceAmount(ResourceType.BRICK);
		
		Assert.assertEquals("Should have 2 bricks", 2,brick);
		
		Object response = command.execute();
		Assert.assertEquals("Should not be able to play two dev cards", null,response);

	}
}
