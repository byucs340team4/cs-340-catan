package tests.command.moves;

import gamemanager.game.Game;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import server.commands.moves.RoadBuildingCommand;
import server.commands.moves.YearOfPlentyCommand;
import shared.definitions.ResourceType;
import shared.locations.EdgeDirection;
import shared.locations.EdgeLocation;
import shared.locations.HexLocation;
import shared.proxy.MoveType;
import shared.proxy.Proxy;
import shared.proxy.request.RoadBuildingRequest;
import shared.proxy.request.YearOfPlentyRequest;

public class RoadBuildingCommandTest {

	Game game;

	
	@Before
	public void init(){
		Proxy proxy = new Proxy("BuildCityTestMap.json");
		game = proxy.getGame();
	}
	
	@Test
	public void testRoadBuildingCommand(){
		EdgeLocation edge0 = new EdgeLocation(new HexLocation(-1,0), EdgeDirection.NE);
		EdgeLocation edge1 = new EdgeLocation(new HexLocation(-1,0), EdgeDirection.SE);
		RoadBuildingRequest request = new RoadBuildingRequest(MoveType.ROAD_BUILDING, 1, edge0, edge1);
		RoadBuildingCommand command = new RoadBuildingCommand(request,game);
		command.execute();
		int roads = game.getPlayer(1).getRoads();
		
		Assert.assertEquals("Should have 13 roads", 13,roads);
		
		Object response = command.execute();
		Assert.assertEquals("Should not be able to play two dev cards", null,response);

	}
}
