package tests.command.moves;

import gamemanager.game.Game;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import server.commands.moves.BuildRoadCommand;
import server.commands.moves.MonopolyCommand;
import shared.definitions.ResourceType;
import shared.locations.EdgeDirection;
import shared.locations.EdgeLocation;
import shared.locations.HexLocation;
import shared.proxy.Proxy;
import shared.proxy.request.BuildRoad;
import shared.proxy.request.Monopoly;

public class MonopolyCommandTest {

Game game;
	
	@Before
	public void init(){
		Proxy proxy = new Proxy("BuildCityTestMap.json");
		game = proxy.getGame();
	}
	
	@Test
	public void testRoadCommand(){
		Monopoly request = new Monopoly(ResourceType.BRICK,1);
		MonopolyCommand command = new MonopolyCommand(request,game);
		command.execute();
		int numBrick = game.getPlayers()[1].getResourceAmount(ResourceType.BRICK);
		Assert.assertEquals("Should have 15 brick", numBrick,15);
		
		command.execute();
		numBrick = game.getPlayers()[1].getResourceAmount(ResourceType.BRICK);
		Assert.assertEquals("Should still have 15 brick", numBrick,15);
		
		
	}
}
