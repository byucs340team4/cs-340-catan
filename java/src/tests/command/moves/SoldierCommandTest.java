package tests.command.moves;

import gamemanager.game.Game;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import server.commands.moves.RoadBuildingCommand;
import server.commands.moves.RobPlayerCommand;
import server.commands.moves.RollNumberCommand;
import server.commands.moves.SoldierCommand;
import shared.locations.EdgeDirection;
import shared.locations.EdgeLocation;
import shared.locations.HexLocation;
import shared.proxy.MoveType;
import shared.proxy.Proxy;
import shared.proxy.request.RoadBuildingRequest;
import shared.proxy.request.RobPlayerRequest;
import shared.proxy.request.RollNumberRequest;
import shared.proxy.request.Soldier;

public class SoldierCommandTest {

	Game game;

	
	@Before
	public void init(){
		Proxy proxy = new Proxy("BuildCityTestMap.json");
		game = proxy.getGame();
	}
	
	@Test
	public void testSoldierCommand(){
		
		Soldier req = new Soldier(1,2,new HexLocation(-1,1));
		SoldierCommand comm = new SoldierCommand(req,game);
		comm.execute();
		int newCards = game.getPlayer(1).getResources().total();
		String status = game.getTurnTracker().getStatus();
		
		Assert.assertEquals("Should be playing", "Playing",status);
		Assert.assertEquals("Should have one more card", 5,newCards);
		Object response = comm.execute();
		
		Assert.assertEquals("Should not be able to rob twice in a row", null,response);

	}
}
