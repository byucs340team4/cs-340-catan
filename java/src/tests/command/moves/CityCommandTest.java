package tests.command.moves;

import gamemanager.game.Game;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import server.commands.moves.BuildCityCommand;
import server.commands.moves.BuildRoadCommand;
import shared.locations.EdgeDirection;
import shared.locations.EdgeLocation;
import shared.locations.HexLocation;
import shared.locations.VertexDirection;
import shared.locations.VertexLocation;
import shared.proxy.Proxy;
import shared.proxy.request.BuildCity;
import shared.proxy.request.BuildRoad;

public class CityCommandTest {

Game game;
	
	@Before
	public void init(){
		Proxy proxy = new Proxy("BuildCityTestMap.json");
		game = proxy.getGame();
	}
	
	@Test
	public void testCityCommand(){
		BuildCity request = new BuildCity(0, new VertexLocation(new HexLocation(0,0),VertexDirection.NW));
		BuildCityCommand command = new BuildCityCommand(request,game);
		command.execute();
		int numCity = game.getPlayers()[0].getCities();
		
		Assert.assertEquals("Player 0 should have all his cities", 4,numCity);
		
		request = new BuildCity(1,new VertexLocation(new HexLocation(0,0),VertexDirection.SW));
		command = new BuildCityCommand(request,game);
		command.execute();
		numCity = game.getPlayers()[1].getCities();
		
		Assert.assertEquals("Player 1 should have 1 city left", 1,numCity);
	}
	
}
