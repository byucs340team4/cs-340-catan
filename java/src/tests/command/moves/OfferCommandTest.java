package tests.command.moves;

import gamemanager.game.Game;
import gamemanager.player.ResourceList;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import server.commands.moves.BuildSettlementCommand;
import server.commands.moves.OfferTradeCommand;
import shared.locations.HexLocation;
import shared.locations.VertexDirection;
import shared.locations.VertexLocation;
import shared.proxy.Proxy;
import shared.proxy.request.BuildSettlement;
import shared.proxy.request.OfferTrade;

public class OfferCommandTest {
	
Game game;
	
	@Before
	public void init(){
		Proxy proxy = new Proxy("CommandSettlementTest.json");
		game = proxy.getGame();
	}
	
	@Test
	public void testOfferCommand(){
		ResourceList trade = new ResourceList();
		trade.setBrick(-2);
		trade.setWood(1);
		OfferTrade request = new OfferTrade(1, trade, 2);
		OfferTradeCommand command = new OfferTradeCommand(game,request);
		command.execute();
		
		boolean tradePending = (null!=game.getTradeOffer());
		Assert.assertEquals("Should be a trade offer in the game", true,tradePending);
		game.setTradeOffer(null);
		trade.setBrick(2);
		trade.setWood(-1);
		request = new OfferTrade(1,trade,2);
		command = new OfferTradeCommand(game,request);
		Object response = command.execute();
		
		Assert.assertEquals("Shouldn't be able to offer trade you don't have resources for", null,response);
		
		
		
	}
	

}
