package tests.command.user;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import gamemanager.game.Game;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import server.commands.moves.YearOfPlentyCommand;
import server.commands.user.LoginCommand;
import shared.definitions.ResourceType;
import shared.proxy.MoveType;
import shared.proxy.Proxy;
import shared.proxy.request.Credentials;
import shared.proxy.request.YearOfPlentyRequest;
import shared.proxy.response.LoginResponse;

public class LoginCommandTest {
	
	
	@Test
	public void testRoadCommand(){
		Set<String>loggedIn = new HashSet<String>();
		List<Credentials>registeredPlayers = new ArrayList<Credentials>();
		Credentials Sam = new Credentials("Sam","sam");
		Credentials Brooke = new Credentials("Brooke","brooke");
		Credentials Pete = new Credentials("Pete","pete");
		Credentials Mark = new Credentials("Mark","mark");
		registeredPlayers.add(Sam);
		registeredPlayers.add(Brooke);
		registeredPlayers.add(Pete);
		registeredPlayers.add(Mark);
		
		LoginCommand command = new LoginCommand(Sam,loggedIn,registeredPlayers);
		LoginResponse response = (LoginResponse) command.execute();
		boolean successful = response.isResponse();
	
		Assert.assertEquals("Should be successful", true,successful);
		registeredPlayers.clear();
		command = new LoginCommand(Sam,loggedIn,registeredPlayers);
		response = (LoginResponse) command.execute();
		
		successful = response.isResponse();
		
		Assert.assertEquals("Should not be successful",false,successful);
				
	}

}
