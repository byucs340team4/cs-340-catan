package tests.command.user;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Assert;
import org.junit.Test;

import server.commands.user.LoginCommand;
import server.commands.user.RegisterCommand;
import shared.proxy.request.Credentials;
import shared.proxy.response.LoginResponse;

public class RegisterCommandTest {

	@Test
	public void testRoadCommand(){
		List<Credentials>registeredPlayers = new ArrayList<Credentials>();
		Credentials Sam = new Credentials("Sam","sammy");
		
		RegisterCommand command = new RegisterCommand(Sam,registeredPlayers);
		LoginResponse response = (LoginResponse) command.execute();
		boolean successful = response.isResponse();
	
		Assert.assertEquals("Should be successful", true,successful);
		
		response = (LoginResponse) command.execute();
		successful = response.isResponse();		
		
		Assert.assertEquals("Should not be successful",false,successful);
				
	}
}
