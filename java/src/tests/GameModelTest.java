package tests;

import static org.junit.Assert.*;
import gamemanager.game.Game;

import org.junit.Test;

import shared.proxy.Proxy;
import shared.proxy.request.Credentials;
import shared.proxy.request.JoinGameRequest;

public class GameModelTest {

	Proxy proxy;
	
	public GameModelTest(){
		proxy = new Proxy();
	}
	
	/*
	 * Simple test.  If the game model is not null, it succeeds.
	 */
	@Test
	public void testGameModel() {
		validLogin();
		validJoin();
		Game game = proxy.gameModel();
		if(game==null){
			fail("Game is null");
			return;
		}
		assertTrue(true);
	}
	
	private void validLogin(){
		proxy.userLogin(new Credentials("Brooke", "brooke"));
	}
	
	private void validJoin(){
		proxy.gamesJoin(new JoinGameRequest(2, "red"),false);
	}
}
