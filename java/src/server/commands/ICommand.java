package server.commands;

/**
 * @author Bentz
 *	Inteface for the Command classes
 */
public interface ICommand {

	/**
	 * This execute method will perform the action specified by the class name on the game model
	 */
	public Object execute();
	
}
