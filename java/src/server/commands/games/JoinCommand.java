package server.commands.games;
import server.Server;
import server.commands.ICommand;
import shared.proxy.request.JoinGameRequest;
import shared.proxy.response.JoinGameResponse;
import gamemanager.game.Game;
import gamemanager.player.Player;
/**
 * 
 * @author MikeLo
 *
 */
public class JoinCommand implements ICommand{

	private JoinGameRequest request;
	private int playerId;
	private Game game;
	/**
	 * Constructor initialized variables above
	 * @param req Request generated by JSON string
	 * @param playerId 
	 */
	public JoinCommand(JoinGameRequest req, int playerId,Game game){
		request = req;
		this.playerId = playerId;
		this.game = game;
	}
	/**
	 * Executes the accept the command on the game model
	 */
	@Override
	public Object execute() {
		// TODO Auto-generated method stub
		int gameId = request.getId();
		String color = request.getColor();
				
		for(Player player:game.getPlayers()){
			if(player.getPlayerID()==playerId) //if you are rejoining nothing needs to be done to game model
			{
				if(color.equalsIgnoreCase(player.getColor().toString())){
					return new JoinGameResponse(true,gameId);
				}
				else{
					for(Player p2:game.getPlayers()){
						if(p2.getColor().toString().equalsIgnoreCase(color)){
							return new JoinGameResponse(false,gameId);
						}
					}
					player.setColor(color);
					return new JoinGameResponse(true,gameId);
				}
			}
			else if(color.equalsIgnoreCase(player.getColor().toString()))
				return new JoinGameResponse(false,gameId);
		}
		
		if(game.getPlayers().length>=4) return new JoinGameResponse(false);
		
		String name = Server.getInstance().getRegisteredPlayers().get(playerId).getUsername();
		
		game.addPlayer(new Player(playerId,name,color,game)); //if you are joining for the first time, create a new player
		return new JoinGameResponse(true,gameId);
	}
}
