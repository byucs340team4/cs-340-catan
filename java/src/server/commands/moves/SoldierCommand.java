package server.commands.moves;

import server.Server;
import server.commands.ICommand;
import shared.proxy.request.Soldier;
import shared.proxy.response.MoveResponse;
import gamemanager.game.Game;
import gamemanager.game.UpdateStatus;

public class SoldierCommand implements ICommand{
	
	/**
	 * index of the player performing the action
	 */
	private int playerIndex;
	/**
	 * index of the player being robbed, -1 if nobody
	 */
	private int victimIndex;
	/**
	 * x location of the new robber hex loc
	 */
	private int xLoc;
	/**
	 * y location of the new robber hex loc
	 */
	private int yLoc;
	/**
	 * the game model of the current game
	 */
	private Game game;
	private Soldier request;
	private int gameId;
	
	/**
	 * Constructor initializes all the variables above
	 * @param req Request generated by JSON string
	 * @param gameId Game's id of current game
	 */
	public SoldierCommand(Soldier req,Game game){
		this.request = req;
		this.game = game;
	}

	/**
	 * Executes the soldier command on the game model
	 * @return Game model after soldier card played
	 */
	@Override
	public Object execute() {
		try{
			if(!game.canPlaySoldier(request))
				return null;
			
			game.playSoldier(request);
			
			MoveResponse response = new MoveResponse(game);
			return response;
		}
		catch(Exception e){
			return null;
		}
		
	}

}
