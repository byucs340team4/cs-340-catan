package server.commands.moves;

import cs340.model.player.ResourceHand;
import server.commands.ICommand;
import shared.proxy.request.OfferTrade;
import shared.proxy.response.IResponse;
import shared.proxy.response.MoveResponse;
import gamemanager.game.Game;
import gamemanager.game.UpdateStatus;
import gamemanager.player.ResourceList;

public class OfferTradeCommand implements ICommand{
	
	/**
	 * the game model of the current game
	 */
	private Game game;
	private OfferTrade request;
	
	public OfferTradeCommand(Game game, OfferTrade trade){
		this.game = game;
		this.request = trade;
	}

	/**
	 * Executes the offer trade command on the game model
	 * @return 
	 */
	@Override
	public IResponse execute() {
		cs340.model.player.TradeOffer tradeOffer = new cs340.model.player.TradeOffer();
		tradeOffer.setSender(request.getPlayerIndex());
		tradeOffer.setReciever(request.getReceiver());
		ResourceHand offer = new ResourceHand();
		ResourceList resources = request.getOffer();
		offer.setBrick(resources.getBrick());
		offer.setOre(resources.getOre());
		offer.setSheep(resources.getSheep());
		offer.setWheat(resources.getWheat());
		offer.setWood(resources.getWood());
		tradeOffer.setOffer(offer);
		if(!game.canOfferTrade(tradeOffer))
			return null;
		game.setTradeOffer(tradeOffer);
		UpdateStatus.update(game, false);
		//Game game = Server.getInstance().getGameList().get(gameId);
		//game.offerTrade(request);
		MoveResponse response = new MoveResponse(game);
		return response;
	}

}
