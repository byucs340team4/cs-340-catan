package server.commands.moves;

import server.Server;
import server.commands.ICommand;
import shared.proxy.request.DiscardCards;
import shared.proxy.response.MoveResponse;
import gamemanager.game.Game;
import gamemanager.player.ResourceList;

public class DiscardCardsCommand implements ICommand{
	
	/**
	 * index of the player performing the action
	 */
	private int playerIndex;
	/**
	 * list of cards to be discarded
	 */
	private ResourceList discardedCards;
	/**
	 * the game model of the current game
	 */
	private Game game;
	private DiscardCards request;
	private int gameId;
	
	/**
	 * Constructor initializes all the variables above
	 * @param req The request object for this command
	 * @param gameId The id of the game this command will alter
	 */
	public DiscardCardsCommand(DiscardCards req,Game game){
		this.request = req;
		this.game = game;
	}

	/**
	 * @return 
	 * 
	 */
	@Override
	public Object execute() {
		try{
			if(!game.canDiscardCards(request))
				return null;
			
			game.discardCards(request);
			
			MoveResponse response = new MoveResponse(game);
			return response;
		}
		catch(Exception e){
			return null;
		}
		
	}

}
