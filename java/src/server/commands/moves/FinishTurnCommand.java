package server.commands.moves;

import server.Server;
import server.commands.ICommand;
import shared.proxy.request.FinishTurnRequest;
import shared.proxy.request.RobPlayerRequest;
import shared.proxy.response.MoveResponse;
import gamemanager.game.Game;

public class FinishTurnCommand implements ICommand{
	
	/**
	 * index of the player performing the action
	 */
	private int playerIndex;
	/**
	 * the game model of the current game
	 */
	private Game game;
	private FinishTurnRequest request;
	private int gameId;
	
	/**
	 * Constructor initializes all the variables above
	 * @param request The request object for this command
	 * @param gameId The id of the game this command will alter
	 */
	public FinishTurnCommand(FinishTurnRequest request,Game game){
		this.request = request;
		this.game = game;
	}

	/**
	 * Executes the finish turn card command on the game model
	 * @return 
	 */
	@Override
	public Object execute() {
		try{
			if(!game.canFinishTurn(request))
				return null;
			
			game.finishTurn(request);
			
			MoveResponse response = new MoveResponse(game);
			return response;
		}
		catch(Exception e){
			return null;
		}
		
	}

}
