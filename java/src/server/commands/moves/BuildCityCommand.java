package server.commands.moves;

import server.Server;
import server.commands.ICommand;
import shared.proxy.request.BuildCity;
import shared.proxy.request.BuildRoad;
import shared.proxy.response.MoveResponse;
import gamemanager.game.Game;

public class BuildCityCommand implements ICommand{

	/**
	 * index of the player performing the action
	 */
	private int playerIndex;
	/**
	 * x location of the city's hex loc
	 */
	private int xLoc;
	/**
	 * y location of the city's hex loc
	 */
	private int yLoc;
	/**
	 * direction of the city's vertex
	 */
	private String direction;
	/**
	 * the game model of the current game
	 */
	private Game game;
	private BuildCity request;
	private int gameId;
	
	/**
	 * Constructor initializes all the variables above
	 * @param request Request generated by JSON string
	 * @param game Game model to build city
	 */
	public BuildCityCommand(BuildCity  request, Game game){
		this.request = request;
		this.game = game;
	}

	/**
	 * Executes the build city command on the game model
	 * @return 
	 */
	@Override
	public Object execute() {
		try{
			if(!game.canBuildCity(request))
				return null;
			
			game.buildCity(request);
			
			MoveResponse response = new MoveResponse(game);
			return response;
		}
		catch(Exception e){
			return null;
		}
		// TODO Auto-generated method stub
		
	}
	
}
