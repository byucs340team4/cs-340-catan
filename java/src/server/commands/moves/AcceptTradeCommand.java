package server.commands.moves;

import server.Server;
import server.commands.ICommand;
import shared.proxy.request.AcceptTrade;
import shared.proxy.response.MoveResponse;
import gamemanager.game.Game;

/**
 * @author Bentz
 *
 */
/**
 * @author Bentz
 *
 */
/**
 * @author Bentz
 *
 */
/**
 * @author Bentz
 *
 */
/**
 * @author Bentz
 *
 */
public class AcceptTradeCommand implements ICommand{

	/**
	 * index of the player performing the action
	 */
	private int playerIndex;
	/**
	 * boolean indicating whether or not the trade will be accepted
	 */
	private boolean willAccept;
	/**
	 * the game model of the current game
	 */
	private Game game;
	private AcceptTrade request;
	private int gameId;
	/**
	 * Constructor initializes all the variables above
	 * @param req The request object for this command
	 * @param gameId The id of the game this command will alter
	 */
	public AcceptTradeCommand(AcceptTrade req,Game game){
		this.request = req;
		this.game = game;
	}

	/**
	 * Executes the accept trade command on the game model
	 */
	@Override
	public Object execute() {
		try{
						
			if(!game.canAcceptTrade(request))
				return null;
			
			game.acceptTrade(request);

			MoveResponse response = new MoveResponse(game);
			return response;
		}
		catch(Exception e){
			return null;
		}
		
		// TODO Auto-generated method stub
		
	}
	
}
