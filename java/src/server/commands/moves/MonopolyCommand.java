package server.commands.moves;

import server.Server;
import server.commands.ICommand;
import shared.proxy.request.Monopoly;
import shared.proxy.response.MoveResponse;
import gamemanager.game.Game;

public class MonopolyCommand implements ICommand{
	
	/**
	 * index of the player performing the action
	 */
	private int playerIndex;
	/**
	 * 
	 */
	private String resource;
	/**
	 * the game model of the current game
	 */
	private Game game;
	private Monopoly request;
	private int gameId;
	/**
	 * Constructor initializes all the variables above
	 * @param req The request object for this command
	 * @param gameId The id of the game this command will alter
	 */
	public MonopolyCommand(Monopoly req,Game game){
		this.request = req;
		this.game = game;
	}

	/**
	 * Executes the monopoly command on the game model
	 * @return 
	 */
	@Override
	public Object execute() {
		try{
			if(!game.canMonopoly(request))
				return null;
			
			game.playMonopoly(request);
			
			MoveResponse response = new MoveResponse(game);
			return response;
		}
		catch(Exception e){
			return null;
		}
		
	}

}
