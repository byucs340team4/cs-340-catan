package server.commands.moves;

import server.Server;
import server.commands.ICommand;
import shared.proxy.request.RobPlayerRequest;
import shared.proxy.response.MoveResponse;
import gamemanager.game.Game;

public class RobPlayerCommand implements ICommand{
	
	/**
	 * index of the player performing the action
	 */
	private int playerIndex;
	/**
	 * index of the player being robbed, -1 if nobody
	 */
	private int victimIndex;
	/**
	 * x location of the new robber hex loc
	 */
	private int xLoc;
	/**
	 * y location of the new robber hex loc
	 */
	private int yLoc;
	/**
	 * the game model of the current game
	 */
	private Game game;

	private RobPlayerRequest request;
	private int gameId;
	/**
	 * Constructor initializes all the variables above
	 * @param req The request object for this command
	 * @param gameId The id of the game this command will alter
	 */
	public RobPlayerCommand(RobPlayerRequest req,Game game){
		this.request = req;
		this.game = game;
	}

	/**
	 * Executes the rob player command on the game model
	 * @return Game model after player robbed
	 */
	@Override
	public Object execute() {
		try{
			if(!game.canRob(request))
				return null;
			
			game.robPlayer(request,false);
			
			MoveResponse response = new MoveResponse(game);
			return response;
		}
		catch(Exception e){
			return null;
		}
		
	}
}
