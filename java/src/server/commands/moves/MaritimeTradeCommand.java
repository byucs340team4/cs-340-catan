package server.commands.moves;

//import server.Server;
import server.commands.ICommand;
import shared.definitions.ResourceType;
import shared.proxy.request.MaritimeTrade;
import shared.proxy.response.IResponse;
import shared.proxy.response.MoveResponse;
import gamemanager.game.Bank;
import gamemanager.game.Game;
import gamemanager.game.UpdateStatus;
import gamemanager.player.Player;
import gamemanager.player.ResourceList;

public class MaritimeTradeCommand implements ICommand{
	
	/**
	 * index of the player performing the action
	 */
	private int playerIndex;
	/**
	 * the ratio of the port being used
	 */
	private int ratio;
	/**
	 * the resources being traded out
	 */
	private String inputResource;
	/**
	 * the resource being received
	 */
	private String outputResource;
	/**
	 * the game model of the current game
	 */
//	private Game game;
	private MaritimeTrade request;
//	private int gameId;
	private Game game;
	
	public MaritimeTradeCommand(Game game, MaritimeTrade req){
		this.game = game;
		this.request = req;
		this.playerIndex = req.getPlayerIndex();
		this.ratio = req.getRatio();
		this.inputResource = req.getInputResources();
		this.outputResource = req.getOutputResources();
	}
	/**
	 * Constructor initializes all the variables above
	 * @param req The request object for this command
	 * @param gameId The id of the game this command will alter
	 */
	public MaritimeTradeCommand(MaritimeTrade req,int gameId){
		this.request = req;
//		this.gameId = gameId;
		this.playerIndex = request.getPlayerIndex();
		this.ratio = request.getRatio();
		this.inputResource = request.getInputResources();
		this.outputResource = request.getOutputResources();
	}

	/**
	 * Executes the maritime trade command on the game model
	 * @return 
	 */
	@Override
	public IResponse execute() {
		try{
			if(!game.canMaritimeTrade(request))
				return null;
//			Game game = Server.getInstance().getGameList().get(gameId);
			//Turning the string that represents the outputResource into a ResourceType.
			ResourceType outputType = ResourceType.getType(this.outputResource);
			//List of resources that will be added to the bank.
			ResourceList outputList = new ResourceList();
			outputList.setResource(outputType, 1);
			//Same thing with inputResources as outputResources.
			ResourceType inputType = ResourceType.getType(this.inputResource);
			//List of resources that will be taken from the bank.
			ResourceList inputList = new ResourceList();
			inputList.setResource(inputType, ratio);
			Bank bank = game.getBank();
			//Adding and removing resources from the bank.
			bank.discard(inputList);
			bank.distribute(outputList);
			Player player = game.getPlayer(playerIndex);
			//Adding resources to the player's hand.
			player.addResources(outputList);
			player.gainResource(inputType, -ratio);
			UpdateStatus.update(game, false);
	//		game.maritimeTrade(request);
		
			MoveResponse response = new MoveResponse(game);
			return response;
		}catch(Exception e){
			return null;
		}
	}

}
