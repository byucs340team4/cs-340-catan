package server.commands.moves;

import server.Server;
import server.commands.ICommand;
import shared.proxy.request.BuildRoad;
import shared.proxy.response.MoveResponse;
import gamemanager.game.Game;

/**
 * @author Bentz
 *
 */
public class BuildRoadCommand implements ICommand{

	/**
	 * index of the player performing the action
	 */
	private int playerIndex;
	/**
	 * x location of the road's hex loc
	 */
	private int xLoc;
	/**
	 * y location of the road's hex loc
	 */
	private int yLoc;
	/**
	 * direction of the road's edge
	 */
	private String direction;
	/**
	 * whether or not the road costs resources
	 */
	private boolean free;
	/**
	 * the game model of the current game
	 */
	private Game game;
	private BuildRoad request;
	private int gameId;
	/**
	 * Constructor initializes all the variables above
	 * @param req Request generated by JSON string
	 * @param game2 Game model to build road
	 */
	public BuildRoadCommand(BuildRoad req,Game game){
		this.request = req;
		this.game = game;
	}
	
	/**
	 * Executes the build road command on the game model
	 * @return 
	 */
	@Override
	public Object execute() {
		try{
			if(!game.buildRoad(request))
				return null;
						
			MoveResponse response = new MoveResponse(game);
			return response;
		}
		catch(Exception e){
			return null;
		}
		// TODO Auto-generated method stub
		
	}
	
}
