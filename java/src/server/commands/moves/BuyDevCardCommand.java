package server.commands.moves;

import server.Server;
import server.commands.ICommand;
import shared.proxy.request.BuyDevCardRequest;
import shared.proxy.response.MoveResponse;
import gamemanager.game.Game;

public class BuyDevCardCommand implements ICommand{
	
	/**
	 * index of the player performing the action
	 */
	private BuyDevCardRequest req;
	/**
	 * the game model of the current game
	 */
	private Game game;
	private BuyDevCardRequest request;
	private int gameId;
	/**
	 * Constructor initializes all the variables above
	 * @param req The request object for this command
	 * @param gameId The id of the game this command will alter
	 */
	public BuyDevCardCommand(BuyDevCardRequest req,Game game){
		this.request = req;
		this.game = game;
	}

	/**
	 * Executes the buy dev card command on the game model
	 * @return Game model after move
	 */
	@Override
	public Object execute() {
		try{
			if(!game.canBuyDevCard(request))
				return null;
			
			game.buyDevCard(request);
			
			MoveResponse response = new MoveResponse(game);
			return response;
		}
		catch(Exception e){
			return null;
		}
		
	}

}
