package server.commands.moves;

import gamemanager.game.Game;
import server.Server;
import server.commands.ICommand;
import shared.proxy.request.RollNumberRequest;
import shared.proxy.response.MoveResponse;

public class RollNumberCommand implements ICommand{

	/**
	 * index of the player performing the action
	 */
	private int playerIndex;
	/**
	 * number rolled
	 */
	private int number;
	/**
	 * the game model of the current game
	 */
	private Game game;
	
	/**
	 * Constructor initializes all the variables above
	 * @param req Request generated by JSON string
	 * @param gameId Game's ID of current game
	 */
	private RollNumberRequest request;
	private int gameId;
	public RollNumberCommand(RollNumberRequest req,Game game){
		this.game = game;
		request = req;
	}
	
	/**
	 * Executes the roll number command on the game model
	 * @return Game model after roll
	 */
	@Override
	public Object execute() {
		try{
			if(!game.canRoll(request))
				return null;
			
			game.rollNumber(request);
			
			MoveResponse response = new MoveResponse(game);
			return response;
		}
		catch(Exception e){
			return null;
		}
		
	}

}
