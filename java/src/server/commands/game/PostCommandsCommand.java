package server.commands.game;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.List;

import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import server.Server;
import server.commands.ICommand;
import server.facade.moves.MovesFacade;
import shared.proxy.request.IRequest;
import shared.proxy.response.GameCommandsResponse;
import shared.proxy.response.ModelResponse;
import gamemanager.game.Game;
/**
 * 
 * @author MikeLo
 *
 */
public class PostCommandsCommand implements ICommand {
	/**
	 * The game ID
	 */
	private int gameID;
	private Game game;
	String value;
	/**
	 * Constructor initializes all the variables above
	 * @param gameID Game ID of commands to be posted
	 */
	public PostCommandsCommand(int gameID,String value){
		this.gameID = gameID;
		this.value = value;
	}
	
	public PostCommandsCommand(Game gameID,String value){
		this.game = gameID;
		this.value = value;
	}
	/**
	 * Executes the accept PostCommandsCommand on the game model
	 */
	@Override
	public Object execute() {
		MovesFacade moves = new MovesFacade();
		Gson g = new Gson();
		
		JsonParser p = new JsonParser();
		JsonArray j = (JsonArray)p.parse(value);
		

		for(int i = 0; i < j.size(); i++){
			JsonObject o = (JsonObject)j.get(i);
			
			moves.evalMethod(o.getAsJsonPrimitive("type").getAsString(), g.toJson(o).toString(), gameID);
			
		}
		
		return new ModelResponse(Server.getInstance().getGameList().get(gameID));
	}
	
	public Object testExecute() {
		MovesFacade moves = new MovesFacade();
		Gson g = new Gson();
		
		JsonParser p = new JsonParser();
		JsonArray j = (JsonArray)p.parse(value);
		

		for(int i = 0; i < j.size(); i++){
			JsonObject o = (JsonObject)j.get(i);
			
			moves.evalMethodForTesting(o.getAsJsonPrimitive("type").getAsString(), g.toJson(o).toString(), game);
			
		}
		
		return new ModelResponse(game);
	}

}
