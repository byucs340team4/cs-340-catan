package server.commands.game;
import java.util.List;

import server.Server;
import server.commands.ICommand;
import shared.proxy.response.ModelResponse;
import shared.proxy.response.ModelVersionResponse;
import gamemanager.game.Game;

/**
 * 
 * @author MikeLo
 *
 */
public class ModelVersionCommand implements ICommand{
	/**
	 * game's ID
	 */
	private int gameID;
	private int version;

	/**
	 * Constructor that gets in all of data
	 * @param gameID Game ID of model to be returned
	 */
	public ModelVersionCommand(int gameID, int version){
		this.gameID = gameID;
		this.version = version;
	}
	/**
	 * Executes the accept ModelCommand on the game model
	 */
	@Override
	public Object execute() {
		// TODO Auto-generated method stub
		Game game = Server.getInstance().getGameList().get(gameID);
		if(game.getVersion() == version) {
			return new ModelVersionResponse();
		}
		ModelVersionResponse response = new ModelVersionResponse(Server.getInstance().getGameList().get(gameID));
		return response;
	}

}
