package server.commands.game;
import java.util.List;

import server.Server;
import server.commands.ICommand;
import gamemanager.game.Game;
import shared.proxy.request.IRequest;
import shared.proxy.response.GameCommandsResponse;
import shared.proxy.response.MoveResponse;
/**
 * 
 * @author MikeLo
 *
 */
public class GetCommandsCommand implements ICommand{

	List<IRequest> commands;
	/**
	 * Game ID
	 */
	private int gameID;
	
	/**
	 * Constructor for creating this command
	 * @param gameID Game ID of commands to be returned
	 */
	public GetCommandsCommand(int gameID){
		this.gameID = gameID;
	}
	/**
	 * Executes the accept  GetCommandsCommand on the game model
	 */
	@Override
	public Object execute() {
		commands = Server.getInstance().getCommands(gameID);
		
		// TODO Auto-generated method stub
		GameCommandsResponse response = new GameCommandsResponse();
		response.setCommands(commands);
		return response;
	}

}
