package server.commands.game;
import java.util.List;

import server.Server;
import server.commands.ICommand;
import shared.proxy.response.ModelResponse;
import gamemanager.game.Game;

/**
 * 
 * @author MikeLo
 *
 */
public class ModelCommand implements ICommand{
	/**
	 * game's ID
	 */
	private int gameID;

	/**
	 * Constructor that gets in all of data
	 * @param gameID Game ID of model to be returned
	 */
	public ModelCommand(int gameID){
		this.gameID = gameID;
	}
	/**
	 * Executes the accept ModelCommand on the game model
	 */
	@Override
	public Object execute() {
		// TODO Auto-generated method stub
		ModelResponse response = new ModelResponse(Server.getInstance().getGameList().get(gameID));
		return response;
	}

}
