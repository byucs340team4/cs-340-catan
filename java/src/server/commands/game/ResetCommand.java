package server.commands.game;
import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import server.Server;
import server.commands.ICommand;
import server.facade.moves.MovesFacade;
import shared.locations.VertexObject;
import shared.proxy.request.IRequest;
import shared.proxy.response.ModelResponse;
import gamemanager.communication.ChatLog;
import gamemanager.communication.GameLog;
import gamemanager.communication.Log;
import gamemanager.communication.TurnTracker;
import gamemanager.game.Bank;
import gamemanager.game.Deck;
import gamemanager.game.Game;
import gamemanager.game.UpdateStatus;
import gamemanager.map.Hex;
import gamemanager.map.Road;
/**
 * 
 * @author MikeLo
 *
 */
public class ResetCommand implements ICommand{
	/**
	 * Game's ID
	 */
	private int gameID;
	private Game game;
/**
 * constructor initalized all variable above.
 * @param gameID ID of game to be reset
 */
	
	public ResetCommand(Game game){
		this.game = game;
	}
	
	public ResetCommand(int gameID){
		this.gameID = gameID;
	}
	/**
	 * Executes the accept ResetCommand on the game model
	 */
	@Override
	public Object execute() {
		Game g = Server.getInstance().getGameList().get(gameID);
		List<IRequest> reqs = Server.getInstance().getCommands(gameID);
		
		g.setBank(new Bank());
		g.setTurnTracker(new TurnTracker());
		g.setChat(new ChatLog());
		g.setDeck(new Deck());
		g.setLog(new GameLog());
		g.getMap().setCities(new ArrayList<VertexObject>());
		g.getMap().setSettlements(new ArrayList<VertexObject>());
		g.getMap().setRoads(new ArrayList<Road>());
		g.resetPlayers();
		g.setVersion(0);
		
		for(Hex h:g.getMap().getHexes()){
			if(h.getResource() == null || h.getResource().equals("DESERT")){
				g.getMap().setRobber(h.getLocation());
				break;
			}
			
		}
		
		MovesFacade moves = new MovesFacade();
		Gson gson = new Gson();
		
		for(int i = 0; i < reqs.size(); i++){
			if(!g.getTurnTracker().getStatus().equals("FirstRound") || !g.getTurnTracker().getStatus().equals("SecondRound")){
				reqs.remove(i);
				continue;
			}
			
			moves.evalMethod(reqs.get(i).getType(), gson.toJson(reqs.get(i)).toString(), gameID);
			if(reqs.get(i).getType().equals("finishTurn")){
				//UpdateStatus.update(g, true);
			}else{
				UpdateStatus.update(g,false);
			}
			
		}
		
		
		
		
		return new ModelResponse(Server.getInstance().getGameList().get(gameID));
	}
	
	public Object testExecute(String value) {
		Game g = this.game;
		
		g.setBank(new Bank());
		g.setTurnTracker(new TurnTracker());
		g.setChat(new ChatLog());
		g.setDeck(new Deck());
		g.setLog(new GameLog());
		g.getMap().setCities(new ArrayList<VertexObject>());
		g.getMap().setSettlements(new ArrayList<VertexObject>());
		g.getMap().setRoads(new ArrayList<Road>());
		g.resetPlayers();
		g.setVersion(0);
		
		for(Hex h:g.getMap().getHexes()){
			if(h.getResource() == null || h.getResource().equals("DESERT")){
				g.getMap().setRobber(h.getLocation());
				break;
			}
			
		}
		JsonParser p = new JsonParser();
		JsonArray reqs = (JsonArray)p.parse(value);
		
		MovesFacade moves = new MovesFacade();
		Gson gson = new Gson();
		
		for(int i = 0; i < reqs.size(); i++){
			if(!g.getTurnTracker().getStatus().equals("FirstRound") && !g.getTurnTracker().getStatus().equals("SecondRound")){
				continue;
			}
			JsonObject o = (JsonObject)reqs.get(i);
			moves.evalMethodForTesting(o.getAsJsonPrimitive("type").getAsString(), gson.toJson(o).toString(), game);
			if(o.getAsJsonPrimitive("type").getAsString().equals("finishTurn")){
				//UpdateStatus.update(g, true);
			}else{
				UpdateStatus.update(g,false);
			}
			
		}	
		return new ModelResponse(g);
	}

}
