package server.commands.util;

import java.util.logging.ConsoleHandler;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;

import server.Server;
import server.commands.ICommand;

public class ChangeLogLevelCommand implements ICommand{
	
	/**
	 * the new log level
	 */
	private String logLevel;
	
	/**
	 * Constructor initializes all the variables above
	 * @param logLevel String of log level
	 */
	public ChangeLogLevelCommand(String logLevel){
		this.logLevel = logLevel;
	}

	/**
	 * Executes the change log level command
	 * @return 
	 */
	@Override
	public Object execute() {
		try{
			
			Server.getInstance().getLogger().setLevel(Level.parse(this.logLevel));
			//Handler handler = new ConsoleHandler();
		//	handler.setLevel(Level.parse(this.logLevel));
		//	for(Handler h: Logger.getGlobal().getHandlers()){
		//		Logger.getGlobal().removeHandler(h);
		//	}
			
		//	Logger.getGlobal().addHandler(handler);
			return true;
		}catch(Exception e){
			return false;
		}
		
	}

}
