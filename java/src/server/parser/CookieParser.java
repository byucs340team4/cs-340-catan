package server.parser;

import java.net.URLDecoder;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import gamemanager.game.Game;
import gamemanager.player.Player;

public class CookieParser {
	
	/**
	 * @param cookie
	 * @return the game corresponding to the game id in the cookie
	 */
	public static int parsePlayerId(List<String> input){
		String cookie = input.get(0);
		String result = URLDecoder.decode(cookie);
		result = result.substring(result.indexOf("{"),result.indexOf("}")+1);
		Gson g = new Gson();
		JsonObject json = g.fromJson(result, JsonObject.class);
		int playerId = Integer.parseInt(json.get("playerID").toString());
		return playerId;
	}
	
	/**
	 * @param list
	 * @return the player corresponding to the player id in the cookie
	 */
	public static Player parseCookieUser(List<String> list){
		return null;
	}
	
	public static JsonObject parseCookieObject(List<String>input){
		String cookie = input.get(0);
		String result = URLDecoder.decode(cookie);
		result = result.substring(result.indexOf("{"),result.indexOf("}")+1);
		Gson g = new Gson();
		JsonObject json = g.fromJson(result, JsonObject.class);
		return json;

	}
	
	public static int parseGameId(List<String>input) throws IllegalArgumentException{
		if(input==null)
			throw new IllegalArgumentException("No game cookie");
		String cookie = input.get(0);
		cookie = cookie.substring(cookie.length()-1);
		int gameId = Integer.parseInt(cookie);
		return gameId;
	}

}
