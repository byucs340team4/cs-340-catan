package server.parser;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
//import java.io.UnsupportedEncodingException;

import com.sun.net.httpserver.HttpExchange;

//import gamemanager.game.Game;
//import gamemanager.player.Player;

public class ExchangeParser {
	
	/**
	 * Extracts the json information that is contained in the request.
	 * @param cookie
	 * @return the game corresponding to the game id in the cookie
	 * @throws IOException 
	 */
	public static String parseRequest(HttpExchange exchange) throws IOException{
		InputStreamReader isr =  new InputStreamReader(exchange.getRequestBody(),"utf-8");
		BufferedReader br = new BufferedReader(isr);
		String value = "";
		String temp = br.readLine();
		while(temp != null) {
			value += temp;
			temp = br.readLine();
		}
		
		return value;
	}
	
	/**
	 * @param cookie
	 * @return the second part of the uri that contains what operation to perform.
	 */
	public static String parseRequestType(HttpExchange exchange){
		try{
			String requestURI = exchange.getRequestURI().toString();
			requestURI = requestURI.substring(requestURI.indexOf("/", 1)+1,requestURI.length());
			return requestURI;
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}

}
