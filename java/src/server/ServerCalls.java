package server;

import server.handlers.*;

import com.sun.net.httpserver.HttpHandler;

public enum ServerCalls {

	GAME,GAMES,MOVES,USER,UTIL;
	
	private String value;
	private OurHandler handler;
	
	static{
	
		GAME.value = "/game";
		GAME.handler = new GameHandler();
		GAMES.value = "/games";
		GAMES.handler = new GamesHandler();
		MOVES.value = "/moves";
		MOVES.handler = new MovesHandler();
		USER.value = "/user";
		USER.handler = new UserHandler();
		UTIL.value = "/util";
		UTIL.handler = new UtilHandler();
		
	}
	
	public String getValue(){
		return value;
	}
	
	public OurHandler getHandler(){
		return handler;
	}
}
