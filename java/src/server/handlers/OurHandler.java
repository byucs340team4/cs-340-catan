package server.handlers;

import java.io.IOException;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

public abstract class OurHandler implements HttpHandler{

	@Override
	public abstract void handle(HttpExchange arg0) throws IOException;
	
	public abstract void setFacade(boolean isFakeFacade);

}
