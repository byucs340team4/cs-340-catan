package server.handlers;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.util.logging.Logger;

import server.facade.game.FakeGameFacade;
import server.facade.game.GameFacade;
import server.facade.game.IGameFacade;
import server.parser.CookieParser;
import server.parser.ExchangeParser;
import shared.proxy.response.IResponse;


//import com.google.gson.Gson;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

public class GameHandler extends OurHandler {

	IGameFacade gameFacade = new GameFacade();
	
	@Override
	public void handle(HttpExchange arg0) throws IOException {
		try{
			Logger.getGlobal().finer("In GameHandler"+"\n");
			String value = ExchangeParser.parseRequest(arg0);
			String requestType = ExchangeParser.parseRequestType(arg0);
			
			Logger.getGlobal().fine("Request type = "+requestType+"\n");
			
			int gameId = CookieParser.parseGameId(arg0.getRequestHeaders().get("Cookie"));
			Logger.getGlobal().finer("Game ID = "+gameId+"\n");
//			Gson g = new Gson();
			gameFacade.setExchange(arg0);
			IResponse response = gameFacade.evalMethod(requestType,value,gameId);
			
			if(response!=null){
				response.addHeaders(arg0);
				
				Logger.getGlobal().finest("Headers are "+arg0+"\n");
		
				arg0.getResponseBody().flush();
				arg0.getResponseBody().close();
			}else{
				arg0.getResponseHeaders().set("Content-Type", "text/html");
				
				arg0.sendResponseHeaders(HttpURLConnection.HTTP_BAD_REQUEST, "Error".getBytes().length);
				arg0.getResponseBody().write("Error".getBytes());
				arg0.getResponseBody().close();
				return;
			}
		}catch(Exception e){
			Logger.getGlobal().severe("Error thrown: " + e+"\n");
			arg0.getResponseHeaders().set("Content-Type", "text/html");
			
			arg0.sendResponseHeaders(HttpURLConnection.HTTP_BAD_REQUEST, e.getMessage().getBytes().length);
			arg0.getResponseBody().write(e.getMessage().getBytes());
			arg0.getResponseBody().close();
			return;
		}
	}

	@Override
	public void setFacade(boolean isFakeFacade) {
		// TODO Auto-generated method stub
		if(isFakeFacade)gameFacade = new FakeGameFacade();
	}

}
