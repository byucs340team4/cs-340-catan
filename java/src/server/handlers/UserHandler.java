package server.handlers;

import java.io.IOException;
import java.util.logging.Logger;

import server.facade.games.FakeGamesFacade;
import server.facade.user.FakeUserFacade;
import server.facade.user.IUserFacade;
import server.facade.user.UserFacade;
import server.parser.ExchangeParser;
import shared.proxy.request.Credentials;
import shared.proxy.response.LoginResponse;

import com.google.gson.Gson;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

/**
 * This class is called when the server receives a /user/ request.
 * @author Sarah
 *
 */
public class UserHandler extends OurHandler {
	IUserFacade userFacade = new UserFacade();
	@Override
	/**
	 * This class extracts the object and passes it to the appropriate facade.
	 */
	public void handle(HttpExchange arg0) throws IOException {
		//value is the json information in the request.
		Logger.getGlobal().info("In UserHandler"+"\n");
		Logger.getGlobal().warning("Can throw IOException"+"\n");
		String value = ExchangeParser.parseRequest(arg0);
		
		Gson g = new Gson();
		Credentials c =  g.fromJson(value, Credentials.class);
		Logger.getGlobal().fine("Credentials received "+ c.toJson().toString()+"\n");
		//URI of the request.
		String requestURI = ExchangeParser.parseRequestType(arg0);
		
		//returns the specific command.
		requestURI = requestURI.substring(requestURI.indexOf("/", 1)+1,requestURI.length());
		Logger.getGlobal().fine("Request type = "+requestURI+"\n");
		LoginResponse l = null;
		switch(requestURI){
			case "login":
				
				l = userFacade.login(c);
				break;
			case "register":
				l = userFacade.register(c);
				break;
			default: 
				l = new LoginResponse(false);
				break;
		}
		
		if (l.isResponse()) {
			Logger.getGlobal().finest("Good credentials and request."+"\n");
			l.addHeaders(arg0);
		}else{
			Logger.getGlobal().finest("Bad credentials or request."+"\n");
			l.setFailHeaders(arg0);
		}
	}
	
	public void setFacade(IUserFacade facade){
		this.userFacade = facade;
	}

	@Override
	public void setFacade(boolean isFakeFacade) {
		// TODO Auto-generated method stub
		if(isFakeFacade)userFacade = new FakeUserFacade();

	}

}
