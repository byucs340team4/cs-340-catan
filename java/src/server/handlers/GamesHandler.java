package server.handlers;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.util.logging.Logger;

import server.facade.game.FakeGameFacade;
import server.facade.games.FakeGamesFacade;
import server.facade.games.GamesFacade;
import server.facade.games.IGamesFacade;
import server.parser.CookieParser;
import server.parser.ExchangeParser;
import shared.proxy.response.IResponse;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

/**
 * This class is used to handle the games request that are received by the server.
 * @author Sarah
 *
 */
public class GamesHandler extends OurHandler {

	
	private IGamesFacade gamesFacade = new GamesFacade();
	
	@Override
	/**
	 * This class extracts the object and passes it to the appropriate fascade.
	 */
	public void handle(HttpExchange arg0) throws IOException {
		try{
			Logger.getGlobal().finer("In GamesHandler"+"\n");
			String value = ExchangeParser.parseRequest(arg0);
			String requestType = ExchangeParser.parseRequestType(arg0);
			int playerId = CookieParser.parsePlayerId(arg0.getRequestHeaders().get("Cookie"));
			
			Logger.getGlobal().fine("Request type = "+requestType+"\n");
			Logger.getGlobal().finer("Player ID = "+playerId+"\n");
			
			IResponse response = gamesFacade.evalMethod(requestType,value,playerId);
			//depending on the response type, if it is a join game response then you may need to
			//set the game cookie
			if(response!=null){
				response.addHeaders(arg0);
				Logger.getGlobal().finest("Headers are "+arg0+"\n");
				
				arg0.getResponseBody().flush();
				arg0.getResponseBody().close();
			}else{
				arg0.getResponseHeaders().set("Content-Type", "text/html");
				
				arg0.sendResponseHeaders(HttpURLConnection.HTTP_BAD_REQUEST, "Error".getBytes().length);
				arg0.getResponseBody().write("Error".getBytes());
				arg0.getResponseBody().close();
				return;
			}
		}catch(Exception e){
			Logger.getGlobal().severe("Error thrown: " + e+"\n");
			arg0.getResponseHeaders().set("Content-Type", "text/html");
			
			arg0.sendResponseHeaders(HttpURLConnection.HTTP_BAD_REQUEST, e.getMessage().getBytes().length);
			arg0.getResponseBody().write(e.getMessage().getBytes());
			arg0.getResponseBody().close();
			return;
		}
	}

	@Override
	public void setFacade(boolean isFakeFacade) {
		// TODO Auto-generated method stub
		if(isFakeFacade)gamesFacade = new FakeGamesFacade();
	}

}
