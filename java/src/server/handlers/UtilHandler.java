package server.handlers;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.util.logging.Level;
import java.util.logging.Logger;

import server.facade.games.FakeGamesFacade;
import server.facade.util.FakeUtilFacade;
import server.facade.util.IUtilFacade;
import server.facade.util.UtilFacade;
import server.parser.ExchangeParser;
import shared.proxy.request.ChangeLogLevelRequest;

import com.google.gson.Gson;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

/**
 * This class is called when the server receives a /util/ request.
 * @author Jaryn
 *
 */
public class UtilHandler extends OurHandler {
	private IUtilFacade utilFacade = new UtilFacade();
	@Override
	/**
	 * This class extracts the object and passes it to the appropriate facade.
	 */
	public void handle(HttpExchange arg0) throws IOException {
		Logger.getGlobal().info("In UtilHandler"+"\n");
		Logger.getGlobal().warning("Can throw IOException"+"\n");
		try{
			String value = ExchangeParser.parseRequest(arg0);
			String requestType = ExchangeParser.parseRequestType(arg0);
			Logger.getGlobal().fine("Request type = "+requestType+"\n");
			
			
			Gson g = new Gson();
			ChangeLogLevelRequest request = g.fromJson(value, ChangeLogLevelRequest.class);
			
			String response = utilFacade.changeLogLevel(Level.parse(request.getLogLevel()));
			if(response!=null){
				Logger.getGlobal().finest("Not null response"+"\n");
				arg0.getResponseHeaders().set("Content-Type", "text/html");
				arg0.sendResponseHeaders(HttpURLConnection.HTTP_OK, 7);
				arg0.getResponseBody().write("Success".getBytes());
			}else{
				Logger.getGlobal().finest("Null Reponse"+"\n");
				arg0.getResponseHeaders().set("Content-Type", "text/html");
				arg0.sendResponseHeaders(HttpURLConnection.HTTP_BAD_REQUEST, "Error".getBytes().length);
				arg0.getResponseBody().write("Error".getBytes());
			}
			arg0.getResponseBody().close();
		}catch(Exception e){
			Logger.getGlobal().severe("Error thrown: " + e+"\n");
			arg0.getResponseHeaders().set("Content-Type", "text/html");
			
			arg0.sendResponseHeaders(HttpURLConnection.HTTP_BAD_REQUEST, e.getMessage().getBytes().length);
			arg0.getResponseBody().write(e.getMessage().getBytes());
			arg0.getResponseBody().close();
			return;
		}
	}
	@Override
	public void setFacade(boolean isFakeFacade) {
		// TODO Auto-generated method stub
		if(isFakeFacade)utilFacade = new FakeUtilFacade();

	}

}
