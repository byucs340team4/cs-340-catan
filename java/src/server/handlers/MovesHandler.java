package server.handlers;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.util.logging.Logger;

import server.facade.games.FakeGamesFacade;
import server.facade.moves.FakeMovesFacade;
import server.facade.moves.IMovesFacade;
import server.facade.moves.MovesFacade;
import server.parser.CookieParser;
import server.parser.ExchangeParser;
import shared.proxy.response.IResponse;
import shared.proxy.response.MoveResponse;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

/**
 * This class is called when the server receives a /moves/ request.
 * @author Sarah
 *
 */
public class MovesHandler extends OurHandler {
	
	private IMovesFacade movesFacade = new MovesFacade();

	@Override
	/**
	 * This class extracts the object and passes it to the appropriate fascade.
	 */
	public void handle(HttpExchange arg0) throws IOException {
		// TODO Auto-generated method stub
		Logger.getGlobal().info("In MovesHandler"+"\n");
		Logger.getGlobal().warning("Can throw IOException"+"\n");
		try{
			String value = ExchangeParser.parseRequest(arg0);
			String requestType = ExchangeParser.parseRequestType(arg0);
			Logger.getGlobal().fine("Request type = "+requestType+"\n");
			int gameId = CookieParser.parseGameId(arg0.getRequestHeaders().get("Cookie"));
			Logger.getGlobal().finer("Game ID = "+gameId+"\n");
			IResponse response = null;
			
			response = movesFacade.evalMethod(requestType, value, gameId);
			Gson g = new Gson();
			if(response!=null){
				Logger.getGlobal().finest("Not null response"+"\n");
				//String jObj = ((MoveResponse)response).getGame().toJson().toString();
				String obj = g.toJson(((MoveResponse) response).getGame());
				arg0.getResponseHeaders().set("Content-Type", "application/json");
				arg0.sendResponseHeaders(HttpURLConnection.HTTP_OK, obj.length());
				arg0.getResponseBody().write(obj.getBytes());
			}else{
				Logger.getGlobal().finest("Null Reponse"+"\n");
				arg0.getResponseHeaders().set("Content-Type", "text/html");
				arg0.sendResponseHeaders(HttpURLConnection.HTTP_BAD_REQUEST, "Error".getBytes().length);
				arg0.getResponseBody().write("Error".getBytes());
			}
			arg0.getResponseBody().close();
		}catch(Exception e){
			Logger.getGlobal().severe("Error thrown: " + e+"\n");
			arg0.getResponseHeaders().set("Content-Type", "text/html");
			
			arg0.sendResponseHeaders(HttpURLConnection.HTTP_BAD_REQUEST, e.getMessage().getBytes().length);
			arg0.getResponseBody().write(e.getMessage().getBytes());
			arg0.getResponseBody().close();
			return;
		}
		
	}

	@Override
	public void setFacade(boolean isFakeFacade) {
		// TODO Auto-generated method stub
		if(isFakeFacade)movesFacade = new FakeMovesFacade();

	}

}
