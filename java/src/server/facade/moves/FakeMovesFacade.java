package server.facade.moves;

import gamemanager.game.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.logging.Logger;

import com.google.gson.Gson;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;

import shared.proxy.response.IResponse;
import shared.proxy.response.MoveResponse;

public class FakeMovesFacade implements IMovesFacade {

	private Game game;
	
	public FakeMovesFacade(){
		Gson g = new Gson();
		game = null;
		try {
			game = g.fromJson(new FileReader(new File("DefaultGame.json")), Game.class);
		} catch (JsonSyntaxException e) {
			e.printStackTrace();
		} catch (JsonIOException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public IResponse evalMethod(String requestType, String value, int gameId) {
		Logger.getGlobal().fine("In moves facade");
		return new MoveResponse(game);
	}

	@Override
	public IResponse buildCity(String req, int gameId) {
		return new MoveResponse(game);
	}

	@Override
	public IResponse buildRoad(String req, int gameId) {
		return new MoveResponse(game);
	}

	@Override
	public IResponse buildSettlement(String req, int gameId) {
		return new MoveResponse(game);
	}

	
	@Override
	public IResponse robPlayer(String req, int gameId) {
		return new MoveResponse(game);
	}

	@Override
	public IResponse rollNumber(String value, int gameId) {
		return new MoveResponse(game);
	}

	@Override
	public IResponse sendChat(String value, int gameId) {
		return new MoveResponse(game);
	}

	@Override
	public IResponse acceptTrade(String req, int gameId) {
		return new MoveResponse(game);
	}

	@Override
	public IResponse buyDevCard(String req, int gameId) {
		return new MoveResponse(game);
	}

	@Override
	public IResponse discardCards(String req, int gameId) {
		return new MoveResponse(game);
	}

	@Override
	public IResponse finishTurn(String value, int gameId) {
		return new MoveResponse(game);
	}

	@Override
	public IResponse maritimeTrade(String req, int gameId) {
		return new MoveResponse(game);
	}

	@Override
	public IResponse monopoly(String req, int gameId) {
		return new MoveResponse(game);
	}

	@Override
	public IResponse monument(String req, int gameId) {
		return new MoveResponse(game);
	}

	@Override
	public IResponse offerTrade(String req, int gameId) {
		return new MoveResponse(game);
	}

	@Override
	public IResponse roadBuilding(String req, int gameId) {
		return new MoveResponse(game);
	}

	@Override
	public IResponse soldier(String req, int gameId) {
		return new MoveResponse(game);
	}

	@Override
	public IResponse YOP(String req, int gameId) {
		return new MoveResponse(game);
	}
	
	public void setGame(Game game){
		this.game = game;
	}

}
