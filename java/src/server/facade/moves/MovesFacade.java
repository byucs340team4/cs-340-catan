package server.facade.moves;

import java.util.logging.Logger;

import gamemanager.game.Game;
import gamemanager.game.UpdateStatus;

import com.google.gson.Gson;
import com.sun.net.httpserver.HttpExchange;

import server.Server;
//import server.commands.ICommand;
import server.commands.moves.AcceptTradeCommand;
import server.commands.moves.BuildCityCommand;
import server.commands.moves.BuildRoadCommand;
import server.commands.moves.BuildSettlementCommand;
import server.commands.moves.BuyDevCardCommand;
import server.commands.moves.DiscardCardsCommand;
import server.commands.moves.FinishTurnCommand;
import server.commands.moves.MaritimeTradeCommand;
import server.commands.moves.MonopolyCommand;
import server.commands.moves.MonumentCommand;
import server.commands.moves.OfferTradeCommand;
import server.commands.moves.RoadBuildingCommand;
import server.commands.moves.RobPlayerCommand;
import server.commands.moves.RollNumberCommand;
import server.commands.moves.SendChatCommand;
import server.commands.moves.SoldierCommand;
import server.commands.moves.YearOfPlentyCommand;
import shared.proxy.request.AcceptTrade;
import shared.proxy.request.BuildCity;
import shared.proxy.request.BuildRoad;
import shared.proxy.request.BuildSettlement;
import shared.proxy.request.BuyDevCardRequest;
import shared.proxy.request.DiscardCards;
import shared.proxy.request.FinishTurnRequest;
import shared.proxy.request.IRequest;
import shared.proxy.request.MaritimeTrade;
import shared.proxy.request.Monopoly;
import shared.proxy.request.Monument;
import shared.proxy.request.OfferTrade;
import shared.proxy.request.RoadBuildingRequest;
import shared.proxy.request.RobPlayerRequest;
import shared.proxy.request.RollNumberRequest;
import shared.proxy.request.SendChatRequest;
import shared.proxy.request.Soldier;
import shared.proxy.request.YearOfPlentyRequest;
import shared.proxy.response.IResponse;

public class MovesFacade implements IMovesFacade {
	
	private Gson g = new Gson();
	private boolean testing = false;
//	private ICommand maritimeTradeCommand;
	
	private Game game;

	@Override
	public IResponse evalMethod(String requestType, String value, int gameId) {
		Logger.getGlobal().fine("In moves facade");
		synchronized(Server.getInstance()){
			game = Server.getInstance().getGameList().get(gameId);
		}
		IResponse response = null;
		switch(requestType){
		case "sendChat":
			response = sendChat(value,gameId);
			break;
		case "rollNumber":
			response = rollNumber(value,gameId);
			break;
		case "buildRoad":
			response = buildRoad(value,gameId);
			break;
		case "buildSettlement":
			response = buildSettlement(value,gameId);
			break;
		case "buildCity":
			response = buildCity(value,gameId);
			break;
		case "robPlayer":
			response = robPlayer(value,gameId);
			break;
		case "finishTurn":
			response = finishTurn(value,gameId);
			break;
		case "buyDevCard":
			response = buyDevCard(value,gameId);
			break;
		case "Road_Building":
			response = roadBuilding(value,gameId);
			break;
		case "discardCards":
			response = discardCards(value,gameId);
			break;
		case "Soldier":
			response = soldier(value,gameId);
			break;
		case "Monument":
			response = monument(value,gameId);
			break;
		case "offerTrade":
//			response = offerTrade(value,gameId);
			OfferTrade OTrequest = g.fromJson(value, OfferTrade.class);
			response = new OfferTradeCommand(game, OTrequest).execute();
			break;
		case "acceptTrade":
			response = acceptTrade(value,gameId);
			break;
		case "maritimeTrade":
			MaritimeTrade MTrequest = g.fromJson(value, MaritimeTrade.class);
			response = new MaritimeTradeCommand(game, MTrequest).execute();
//			response = maritimeTrade(value,gameId);
			break;
		case "Monopoly":
			response = monopoly(value,gameId);
			break;
		case "Year_of_Plenty":
			response = YOP(value,gameId);
			break;
		default: response = null;
		}
		
		return response;
	}

	public IResponse evalMethodForTesting(String requestType, String value, Game game) {
		testing = true;
		this.game = game;
		int gameId = 0;
		IResponse response = null;
		switch(requestType){
		case "sendChat":
			response = sendChat(value,gameId);
			break;
		case "rollNumber":
			response = rollNumber(value,gameId);
			break;
		case "buildRoad":
			response = buildRoad(value,gameId);
			break;
		case "buildSettlement":
			response = buildSettlement(value,gameId);
			break;
		case "buildCity":
			response = buildCity(value,gameId);
			break;
		case "robPlayer":
			response = robPlayer(value,gameId);
			break;
		case "finishTurn":
			response = finishTurn(value,gameId);
			break;
		case "buyDevCard":
			response = buyDevCard(value,gameId);
			break;
		case "Road_Building":
			response = roadBuilding(value,gameId);
			break;
		case "discardCards":
			response = discardCards(value,gameId);
			break;
		case "Soldier":
			response = soldier(value,gameId);
			break;
		case "Monument":
			response = monument(value,gameId);
			break;
		case "offerTrade":
//			response = offerTrade(value,gameId);
			OfferTrade OTrequest = g.fromJson(value, OfferTrade.class);
			response = new OfferTradeCommand(game, OTrequest).execute();
			break;
		case "acceptTrade":
			response = acceptTrade(value,gameId);
			break;
		case "maritimeTrade":
			MaritimeTrade MTrequest = g.fromJson(value, MaritimeTrade.class);
			response = new MaritimeTradeCommand(game, MTrequest).execute();
//			response = maritimeTrade(value,gameId);
			break;
		case "Monopoly":
			response = monopoly(value,gameId);
			break;
		case "Year_of_Plenty":
			response = YOP(value,gameId);
			break;
		default: response = null;
		}
		
		return response;
	}
	
	
	
	private void addToCommandList(IRequest request,int gameId){
		if (!testing) {
			synchronized (Server.getInstance()) {
				Server.getInstance().getCommands(gameId).add(request);
			}
		}
	}
	
	@Override
	public IResponse acceptTrade(String req,int gameId) {
		// TODO Auto-generated method stub
		AcceptTrade request = g.fromJson(req, AcceptTrade.class);
		if(!game.canAcceptTrade(request)) {
			game.setTradeOffer(null);
			UpdateStatus.update(game, false);
			return null;
		}	
		AcceptTradeCommand command= new AcceptTradeCommand(request,game);
		IResponse response = (IResponse) command.execute();
		if(response!= null){
			addToCommandList(request,gameId);
		}
		return response;
	}

	@Override
	public IResponse buildCity(String req, int gameId) {
		// TODO Auto-generated method stub
		BuildCity request = g.fromJson(req, BuildCity.class);
		BuildCityCommand command = new BuildCityCommand(request,game);
		IResponse response = (IResponse) command.execute();
		if(response!= null){
			addToCommandList(request,gameId);
		}
		
		return response;
	}

	@Override
	public IResponse buildRoad(String req, int gameId) {
		// TODO Auto-generated method stub
		BuildRoad request = g.fromJson(req, BuildRoad.class);
		BuildRoadCommand command = new BuildRoadCommand(request,game);
		IResponse response = (IResponse) command.execute();
		if(response!= null){
			addToCommandList(request,gameId);
		}
		return response;
	}

	@Override
	public IResponse buildSettlement(String req, int gameId) {
		BuildSettlement request = g.fromJson(req, BuildSettlement.class);
		BuildSettlementCommand command;
		synchronized(Server.getInstance()){
			command = new BuildSettlementCommand(request,game);
		}
		IResponse response = (IResponse) command.execute();
		if(response!= null){
			addToCommandList(request,gameId);
		}
		return response;
	}

	@Override
	public IResponse buyDevCard(String req,int gameId) {
		// TODO Auto-generated method stub
		BuyDevCardRequest request = g.fromJson(req, BuyDevCardRequest.class);
		BuyDevCardCommand command = new BuyDevCardCommand(request,game);
		IResponse response = (IResponse) command.execute();
		if(response!= null){
			addToCommandList(request,gameId);
		}
		return response;
	}

	@Override
	public IResponse discardCards(String req,int gameId) {
		// TODO Auto-generated method stub
		DiscardCards request = g.fromJson(req, DiscardCards.class);
		DiscardCardsCommand command = new DiscardCardsCommand(request,game);
		IResponse response = (IResponse) command.execute();
		if(response!= null){
			addToCommandList(request,gameId);
		}
		return response;
	}

	@Override
	public IResponse finishTurn(String value,int gameId) {
		FinishTurnRequest request = g.fromJson(value, FinishTurnRequest.class);
		FinishTurnCommand command = new FinishTurnCommand(request,game);
		IResponse response = (IResponse) command.execute();
		if(response!= null){
			addToCommandList(request,gameId);
		}
		return response;
	}

	@Override
	public IResponse maritimeTrade(String req,int gameId) {
		
		MaritimeTrade request = g.fromJson(req, MaritimeTrade.class);
		
		MaritimeTradeCommand command = new MaritimeTradeCommand(game,request);
		IResponse response = command.execute();
		if(response!= null){
			addToCommandList(request,gameId);
		}
		return response;
	}

	@Override
	public IResponse monopoly(String req,int gameId) {
		Monopoly request = g.fromJson(req, Monopoly.class);
		MonopolyCommand command = new MonopolyCommand(request,game);
		IResponse response = (IResponse) command.execute();
		if(response!= null){
			addToCommandList(request,gameId);
		}
		return response;
	}

	@Override
	public IResponse monument(String req, int gameId) {
		Monument request = g.fromJson(req, Monument.class);
		MonumentCommand command = new MonumentCommand(request,game);
		IResponse response = (IResponse) command.execute();
		if(response!= null){
			addToCommandList(request,gameId);
		}
		return response;
	}

	@Override
	public IResponse offerTrade(String req,int gameId) {
		OfferTrade request = g.fromJson(req, OfferTrade.class);
		//return new OfferTradeCommand(game, request).execute();
		OfferTradeCommand command = new OfferTradeCommand(game,request);
		IResponse response = (IResponse) command.execute();
		if(response!= null){
			addToCommandList(request,gameId);
		}
		return response;
	}

	@Override
	public IResponse roadBuilding(String req,int gameId) {
		// TODO Auto-generated method stub
		RoadBuildingRequest request = g.fromJson(req, RoadBuildingRequest.class);
		RoadBuildingCommand command = new RoadBuildingCommand(request,game);
		IResponse response = (IResponse) command.execute();
		if(response!= null){
			addToCommandList(request,gameId);
		}
		return response;
	}

	@Override
	public IResponse robPlayer(String value, int gameId) {
		// TODO Auto-generated method stub
		RobPlayerRequest request = g.fromJson(value, RobPlayerRequest.class);
		RobPlayerCommand command = new RobPlayerCommand(request,game);
		IResponse response = (IResponse) command.execute();
		if(response!= null){
			addToCommandList(request,gameId);
		}
		return response;
	}
	
	@Override 
	public IResponse rollNumber(String value,int gameId){
		RollNumberRequest request = g.fromJson(value, RollNumberRequest.class);
		RollNumberCommand command = new RollNumberCommand(request,game);
		IResponse response = (IResponse) command.execute();
		if(response!= null){
			addToCommandList(request,gameId);
		}
		return response;		
	}


	@Override
	public IResponse soldier(String req,int gameId) {
		// TODO Auto-generated method stub
		Soldier request = g.fromJson(req, Soldier.class);
		SoldierCommand command = new SoldierCommand(request,game);
		IResponse response = (IResponse) command.execute();
		if(response!= null){
			addToCommandList(request,gameId);
		}
		return response;
	}

	@Override
	public IResponse YOP(String req,int gameId) {
		// TODO Auto-generated method stub
		YearOfPlentyRequest request = g.fromJson(req, YearOfPlentyRequest.class);
		YearOfPlentyCommand command = new YearOfPlentyCommand(request,game);
		IResponse response = (IResponse) command.execute();
		if(response!= null){
			addToCommandList(request,gameId);
		}
		return response;
	}

	@Override
	public IResponse sendChat(String value, int gameId) {
		// TODO Auto-generated method stub
		SendChatRequest request = g.fromJson(value, SendChatRequest.class);
		SendChatCommand command = new SendChatCommand(request,game);
		IResponse response = (IResponse) command.execute();
		if(response!= null){
			addToCommandList(request,gameId);
		}
		return response;
	}

	
}
