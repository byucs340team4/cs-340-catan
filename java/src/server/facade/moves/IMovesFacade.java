package server.facade.moves;

import com.sun.net.httpserver.HttpExchange;

import shared.proxy.response.IResponse;

public interface IMovesFacade {

	public IResponse evalMethod(String requestType,String value, int gameId);
	/**
	 * method for the server to accept a trade
	 * @param req request generated by JSON String
	 * @return Response in JSON String form
	 */
	public IResponse acceptTrade(String req,int gameId);
	
	/**
	 * builds a city
	 * @param req request generated by JSON String
	 * @return Response in JSON String form
	 */
	public IResponse buildCity(String req, int gameId);
	
	/**
	 * builds a road
	 * @param req request generated by JSON String
	 * @return Response in JSON String form
	 */
	public IResponse buildRoad(String req, int gameId);
	
	/**
	 * builds a settlement
	 * @param req request generated by JSON String
	 * @return Response in JSON String form
	 */
	public IResponse buildSettlement(String req, int gameId);
	
	/**
	 * buys a dev card
	 * @param req request generated by JSON String
	 * @return Response in JSON String form
	 */
	public IResponse buyDevCard(String req,int gameId);
	
	/**
	 * discards cards
	 * @param req request generated by JSON String
	 * @return Response in JSON String form
	 */
	public IResponse discardCards(String req,int gameId);
	
	/**
	 * finishes a turn
	 * @param req request generated by JSON String
	 * @return Response in JSON String form
	 */
	public IResponse finishTurn(String value,int gameId);
	
	/**
	 * performs a maritime trade
	 * @param req request generated by JSON String
	 * @return Response in JSON String form
	 */
	public IResponse maritimeTrade(String req,int gameId);
	
	/**
	 * plays a monopoly card
	 * @param req request generated by JSON String
	 * @return Response in JSON String form
	 */
	public IResponse monopoly(String req,int gameId);
	
	/**
	 * plays a monument card
	 * @param req request generated by JSON String
	 * @return Response in JSON String form
	 */
	public IResponse monument(String req, int gameId);
	
	/**
	 * creates a trade offer
	 * @param req request generated by JSON String
	 * @return Response in JSON String form
	 */
	public IResponse offerTrade(String req,int gameId);
	
	/**
	 * plays a road building card
	 * @param req request generated by JSON String
	 * @return Response in JSON String form
	 */
	public IResponse roadBuilding(String req,int gameId);
	
	/**
	 * robs a player
	 * @param req request generated by JSON String
	 * @return Response in JSON String form
	 */
	public IResponse robPlayer(String req,int gameId);
	
	public IResponse rollNumber(String value,int gameId);
	
	/**
	 * sends a chat
	 * @param req request generated by JSON String
	 * @return Response in JSON String form
	 */
	public IResponse sendChat(String value,int gameId);
	
	/**
	 * plays a soldier card
	 * @param req request generated by JSON String
	 * @return Response in JSON String form
	 */
	public IResponse soldier(String req,int gameId);
	
	/**
	 * plays a year of plenty card
	 * @param req request generated by JSON String
	 * @return Response in JSON String form
	 */
	public IResponse YOP(String req,int gameId);
}
