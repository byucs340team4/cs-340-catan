package server.facade.user;

import shared.proxy.request.Credentials;
import shared.proxy.response.LoginResponse;

public interface IUserFacade {

	/**
	 * logs a user into the server
	 * @param cred
	 * @return success or failure
	 */
	public LoginResponse login(Credentials cred);
	
	/**
	 * registers a user on the server
	 * @param cred
	 * @return success or failure
	 */
	public LoginResponse register(Credentials cred);
}
