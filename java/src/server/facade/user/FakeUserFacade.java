package server.facade.user;

import shared.proxy.request.Credentials;
import shared.proxy.response.LoginResponse;

public class FakeUserFacade implements IUserFacade {

	@Override
	public LoginResponse login(Credentials cred) {
		return new LoginResponse(true, "John", "Smith", 0);
	}

	@Override
	public LoginResponse register(Credentials cred) {
		return new LoginResponse(true, "John", "Smith", 0);
	}

}
