package server.facade.user;

import shared.proxy.request.Credentials;
import shared.proxy.response.LoginResponse;
import server.Server;
import server.commands.user.LoginCommand;
import server.commands.user.RegisterCommand;

public class UserFacade implements IUserFacade {

	@Override
	public LoginResponse login(Credentials cred) {
		LoginCommand login = new LoginCommand(cred,Server.getInstance().getLoggedInPlayers(),Server.getInstance().getRegisteredPlayers());
		return (LoginResponse)login.execute();
	}

	@Override
	public LoginResponse register(Credentials cred) {
		RegisterCommand reg = new RegisterCommand(cred,Server.getInstance().getRegisteredPlayers());
		return (LoginResponse)reg.execute();
	}

}
