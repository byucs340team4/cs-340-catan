package server.facade.games;

import com.google.gson.Gson;

import server.Server;
import server.commands.games.CreateCommand;
import server.commands.games.JoinCommand;
import server.commands.games.ListCommand;
import server.commands.games.LoadCommand;
import server.commands.games.SaveCommand;
import shared.proxy.request.CreateGameRequests;
import shared.proxy.request.JoinGameRequest;
import shared.proxy.request.ListGamesRequest;
import shared.proxy.request.LoadGameRequest;
import shared.proxy.request.SaveGameRequest;
import shared.proxy.response.IResponse;

public class GamesFacade implements IGamesFacade {

	@Override
	public IResponse evalMethod(String requestType, String value,int playerId) {
		// TODO Auto-generated method stub
		IResponse response = null;
		switch(requestType){
		case "list":
			response = listGame();
			break;
		case "create":
			response = createGame(value);
			break;
		case "join":
			response = joinGame(value,playerId);
			break;
		case "save":
			response = saveGame(value);
			break;
		case "load":
			response = loadGame(value);
			break;			
		}
		
		return response;
	}

	@Override
	public IResponse listGame() {
		// TODO Auto-generated method stub
		ListCommand command = new ListCommand(new ListGamesRequest(),Server.getInstance().getGameList());
		IResponse response = (IResponse) command.execute();
		return response;
	}

	@Override
	public IResponse createGame(String value) {
		Gson g = new Gson();
		CreateGameRequests request = g.fromJson(value, CreateGameRequests.class);
		CreateCommand command = new CreateCommand(request);
		IResponse response = (IResponse) command.execute();
		return response;
	}

	@Override
	public IResponse joinGame(String req, int playerId) {
		// TODO Auto-generated method stub
		Gson g = new Gson();
		JoinGameRequest request = g.fromJson(req, JoinGameRequest.class);
		int gameId = request.getId();
		JoinCommand command = new JoinCommand(request,playerId,Server.getInstance().getGameList().get(gameId));
		IResponse response = (IResponse) command.execute();
		return response;
	}

	@Override
	public IResponse loadGame(String req) {
		// TODO Auto-generated method stub
		Gson g = new Gson();
		LoadGameRequest request = g.fromJson(req, LoadGameRequest.class);
		LoadCommand command = new LoadCommand(request);
		IResponse response = (IResponse) command.execute();
		return response;
	}

	@Override
	public IResponse saveGame(String req) {
		// TODO Auto-generated method stub
		Gson g = new Gson();
		SaveGameRequest request = g.fromJson(req, SaveGameRequest.class);
		SaveCommand command = new SaveCommand(request);
		IResponse response = (IResponse) command.execute();
		return response;
	}

}
