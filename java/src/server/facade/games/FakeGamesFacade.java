package server.facade.games;

import gamemanager.player.Player;

import java.util.ArrayList;

import client.data.GameInfo;
import shared.proxy.response.*;

public class FakeGamesFacade implements IGamesFacade {

	@Override
	public IResponse evalMethod(String requestType, String value, int playerId) {
		IResponse response = null;
		switch(requestType){
		case "list":
			response = listGame();
			break;
		case "create":
			response = createGame(value);
			break;
		case "join":
			response = joinGame(value,playerId);
			break;	
		}
		return response;
	}

	@Override
	public IResponse listGame() {
		ArrayList<GameInfo> games = new ArrayList<GameInfo>();
		for(int i=0;i<3;i++){
			GameInfo game = new GameInfo();
			game.setId(i);
			game.setTitle("Game "+ i);
			games.add(game);
		}
		IResponse response = new GamesListResponse(games);
		return response;
	}

	@Override
	public IResponse createGame(String value) {
		CreateGameResponse response = new CreateGameResponse();
		response.setId(0);
		response.setTitle("Game 1");
		response.setPlayers(new Player[0]);
		return response;
	}

	@Override
	public IResponse joinGame(String req, int playerIndex) {
		JoinGameResponse response = new JoinGameResponse();
		response.setGameId(0);
		response.setSuccess(true);
		return response;
	}

	@Override
	public IResponse loadGame(String req) {
		LoadGameResponse response = new LoadGameResponse();
		return response;
	}

	@Override
	public IResponse saveGame(String req) {
		// TODO Auto-generated method stub
		SaveGameResponse response = new SaveGameResponse();
		return response;
	}

	
}
