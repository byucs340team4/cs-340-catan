package server.facade.games;

import shared.proxy.request.CreateGameRequests;
import shared.proxy.request.JoinGameRequest;
import shared.proxy.request.LoadGameRequest;
import shared.proxy.request.SaveGameRequest;
import shared.proxy.response.IResponse;

public interface IGamesFacade {
	/**
	 * method for the server to create a game
	 * @param playerId 
	 * @return
	 */
	public IResponse evalMethod(String requestType,String value, int playerId);
	
	public IResponse listGame();
	
	
	/**
	 * method for the server to createa  game
	 * @param value request as JSON String
	 * @return response in form of JSON String
	 */
	public IResponse createGame(String value);
	
	/**
	 * method for the server to add a user to a game
	 * @param req request as JSON String
	 * @return response in form of JSON String
	 */
	public IResponse joinGame(String req,int playerIndex);
	
	/**
	 * method for the server to return a given "saved" game
	 * @param req request as JSON String
	 * @return response in form of JSON String
	 */
	public IResponse loadGame(String req);
	
	/**
	 * method for the server to save a game
	 * @param req request as JSON String
	 * @return response in form of JSON String
	 */
	public IResponse saveGame(String req);
}
