package server.facade.game;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;

import gamemanager.game.Game;

import com.google.gson.Gson;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;
import com.sun.net.httpserver.HttpExchange;

import shared.proxy.request.AddAIRequest;
import shared.proxy.request.ListAIRequest;
import shared.proxy.response.GameCommandsResponse;
import shared.proxy.response.IResponse;
import shared.proxy.response.ModelResponse;

public class FakeGameFacade implements IGameFacade {

	private Game game;
	
	private HttpExchange exchange;
	
	public FakeGameFacade(){
		Gson gson = new Gson();
		try {
			game = gson.fromJson(new FileReader(new File("DefaultGame.json")), Game.class);
			System.out.println(game.getWinner());
		} catch (JsonSyntaxException e) {
			e.printStackTrace();
		} catch (JsonIOException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
	@Override
	public String addAI(AddAIRequest req) {
		return null;
	}

	@Override
	public IResponse getCommands(int gameID) {
		GameCommandsResponse response = new GameCommandsResponse();
		response.init();
		return response;
	}

	@Override
	public String listAI(ListAIRequest req) {
		return null;
	}

	@Override
	public IResponse model(int gameID) {
		return new ModelResponse(game);
	}

	@Override
	public IResponse postCommands(int gameID,String value) {
		return new ModelResponse(game);
	}

	@Override
	public IResponse reset(int gameID) {
		return new ModelResponse(game);
	}

	@Override
	public IResponse evalMethod(String requestType, String value, int gameId) {
		IResponse response = null;
		switch(requestType){
		case "model":
			response = model(gameId);
			break;
		case "reset":
			response = this.reset(gameId);
			break;
		case "addAI":
			return null;
		case "listAI":
			return null;
		case "commands":
			if(exchange.getRequestMethod().equalsIgnoreCase("post"))
				response = this.postCommands(gameId,value);
			else if(exchange.getRequestMethod().equalsIgnoreCase("get"))
				response = getCommands(gameId);
			break;
		default: 
			return null;
		}
		return response;
	}

	@Override
	public void setExchange(HttpExchange e) {
			this.exchange = e;	
	}

}
