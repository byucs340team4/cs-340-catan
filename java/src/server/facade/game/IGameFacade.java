package server.facade.game;

import com.sun.net.httpserver.HttpExchange;

import shared.proxy.request.AddAIRequest;
import shared.proxy.request.GameCommandsRequest;
import shared.proxy.request.ListAIRequest;
import shared.proxy.response.IResponse;

public interface IGameFacade {
	
	public IResponse evalMethod(String requestType,String value, int gameId);

	/**
	 * method for the server to addAI
	 * @param req request generated from JSON string
	 * @return response in form of JSON String
	 */
	public String addAI(AddAIRequest req);
	
	/**
	 * method for the server to return list of commands
	 * @param req request generated from JSON string
	 * @return response in form of JSON String
	 */
	public IResponse getCommands(int gameID);
	
	/**
	 * method for the server to return a list of AI's
	 * @param req request generated from JSON string
	 * @return response in form of JSON String
	 */
	public String listAI(ListAIRequest req);
	
	/**
	 * method for the server to return the game model
	 * @param gameID ID of game to send model
	 * @return response in form of JSON String
	 */
	public IResponse model(int gameID);
	
	/**
	 * method for the server to run a list of commands
	 * @param gameID ID of game to post commands
	 * @return response in form of JSON String
	 */
	public IResponse postCommands(int gameID,String value);
	
	/**
	 * method for the server reset a game
	 * @param gameID ID of game to be reset
	 * @return response in form of JSON String
	 */
	public IResponse reset(int gameID);
	
	/**
	 * Method for setting the private variable exchange.  needed to determine if a commands
	 * call is a get or a post.
	 * @param e HTTPExchange that contains get and post information.
	 */
	public void setExchange(HttpExchange e);
}
