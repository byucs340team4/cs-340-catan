package server.facade.game;

import com.google.gson.Gson;
import com.sun.net.httpserver.HttpExchange;

import server.commands.game.GetCommandsCommand;
import server.commands.game.ModelCommand;
import server.commands.game.ModelVersionCommand;
import server.commands.game.PostCommandsCommand;
import server.commands.game.ResetCommand;
import server.commands.games.JoinCommand;
import shared.proxy.request.AddAIRequest;
import shared.proxy.request.GameCommandsRequest;
import shared.proxy.request.JoinGameRequest;
import shared.proxy.request.ListAIRequest;
import shared.proxy.response.IResponse;

public class GameFacade implements IGameFacade {

	private HttpExchange exchange;
	@Override
	public String addAI(AddAIRequest req) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public IResponse getCommands(int gameID) {
		GetCommandsCommand command = new GetCommandsCommand(gameID);
		IResponse response = (IResponse) command.execute();
		return response;
	}

	@Override
	public String listAI(ListAIRequest req) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public IResponse model(int gameID) {
		// TODO Auto-generated method stub
		ModelCommand command = new ModelCommand(gameID);
		IResponse response = (IResponse) command.execute();
		return response;
	}
	
	private IResponse modelVersion(int gameId, int version) {
		ModelVersionCommand command = new ModelVersionCommand(gameId, version);
		IResponse response = (IResponse) command.execute();
		return response;
	}

	@Override
	public IResponse postCommands(int gameID,String value) {
		PostCommandsCommand command = new PostCommandsCommand(gameID,value);
		IResponse response = (IResponse) command.execute();
		return response;
	}

	@Override
	public IResponse reset(int gameID) {
		ResetCommand command = new ResetCommand(gameID);
		IResponse response = (IResponse) command.execute();
		return response;
	}

	@Override
	public IResponse evalMethod(String requestType, String value, int gameId) {
		// TODO Auto-generated method stub
		IResponse response = null;
		if(requestType.contains("?version=") && requestType.contains("model")) {
			int index = requestType.indexOf("=");
			int version = Integer.valueOf(requestType.substring(index + 1));
			response = modelVersion(gameId,version);
			return response;
		}
		switch(requestType){
		case "model":
			response = model(gameId);
			break;
		case "create":
			//createGame(value);
			break;
		case "join":
			//response = joinGame(value,playerId);
			break;	
		case "commands":
			if(exchange.getRequestMethod().equalsIgnoreCase("post"))
				response = this.postCommands(gameId,value);
			else if(exchange.getRequestMethod().equalsIgnoreCase("get"))
				response = getCommands(gameId);
			break;
		case "reset":
			response = reset(gameId);
			break;
		}
		
		return response;
	}

	public void setExchange(HttpExchange e){
		this.exchange = e;
	}

}
