package server.facade.util;

import java.util.logging.Level;

public class FakeUtilFacade implements IUtilFacade {

	@Override
	public String changeLogLevel(Level level) {
		return "Success";
	}

}
