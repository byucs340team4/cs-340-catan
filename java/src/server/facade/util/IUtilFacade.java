package server.facade.util;

import java.util.logging.Level;

public interface IUtilFacade {

	/**
	 * changes the log level
	 * @return Response in JSON String form
	 */
	public String changeLogLevel(Level level);
}
