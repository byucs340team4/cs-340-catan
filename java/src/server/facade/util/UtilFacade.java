package server.facade.util;

import java.util.logging.Level;
import java.util.logging.Logger;

public class UtilFacade implements IUtilFacade {

	@Override
	public String changeLogLevel(Level level) {
		try{
			Logger.getGlobal().severe("Log level being updated from "+Logger.getGlobal().getLevel()+" to "+level+"\n");
			Logger.getGlobal().setLevel(level);
			return "Success";
		}catch(Exception e){
			return null;
		}
		
	}


}
