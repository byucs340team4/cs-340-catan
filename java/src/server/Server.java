package server;

import gamemanager.game.Game;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.ConsoleHandler;
import java.util.logging.Handler;
import java.util.logging.Logger;
import java.util.logging.Level;

import server.handlers.Handlers;
import server.handlers.OurHandler;
import shared.proxy.Deserializer;
import shared.proxy.request.Credentials;
import shared.proxy.request.IRequest;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;

public class Server {

	private HttpServer server;
	private List<Game>gameList;
	
	private List<Credentials>registeredPlayers;
	private List<List<IRequest>> commands;
	private final static Logger LOGGER = Logger.getGlobal();
	private List<OurHandler>handlers;
	private boolean useFakeFacade;
	
	public Logger getLogger(){
		return LOGGER;
	}
	
	
	/**
	 * @return the gameList
	 */
	public List<Game> getGameList() {
		return gameList;
	}



	/**
	 * @param gameList the gameList to set
	 */
	public void setGameList(List<Game> gameList) {
		this.gameList = gameList;
	}


	/**
	 * method to return the list of commands done for the current game
	 * @param gameId - int gameId of the game you want the commands for
	 * @return
	 */
	public List<IRequest> getCommands(int gameId){
		return commands.get(gameId);
	}
	
	public void addNewCommandList(){
		commands.add(new ArrayList<IRequest>());
	}

	/**
	 * @return the registeredPlayers
	 */
	public List<Credentials> getRegisteredPlayers() {
		return registeredPlayers;
	}



	/**
	 * @param registeredPlayers the registeredPlayers to set
	 */
	public void setRegisteredPlayers(List<Credentials> registeredPlayers) {
		this.registeredPlayers = registeredPlayers;
	}



	/**
	 * @return the loggedInPlayers
	 */
	public Set<String> getLoggedInPlayers() {
		return loggedInPlayers;
	}



	/**
	 * @param loggedInPlayers the loggedInPlayers to set
	 */
	public void setLoggedInPlayers(Set<String> loggedInPlayers) {
		this.loggedInPlayers = loggedInPlayers;
	}

	private Set<String>loggedInPlayers;
	private static Server instance;
	
	
	
	
	private Server() throws IOException{
		commands = new ArrayList<List<IRequest>>();
		loggedInPlayers = new HashSet<String>();
		registeredPlayers = new ArrayList<Credentials>();
		Credentials Sam = new Credentials("Sam","sam");
		Credentials Brooke = new Credentials("Brooke","brooke");
		Credentials Pete = new Credentials("Pete","pete");
		Credentials Mark = new Credentials("Mark","mark");
		registeredPlayers.add(Sam);
		registeredPlayers.add(Brooke);
		registeredPlayers.add(Pete);
		registeredPlayers.add(Mark);
		gameList = new ArrayList<Game>();
		Game defaultGame = getGame("DefaultGame.json");
		defaultGame.setTitle("default game");
		Game emptyGame = getGame("EmptyGame.json");
		emptyGame.setTitle("empty game");
		gameList.add(defaultGame);
		gameList.add(emptyGame);
		commands.add(new ArrayList<IRequest>());
		commands.add(new ArrayList<IRequest>());
		handlers = new ArrayList<OurHandler>();
		useFakeFacade = false;
		return;
	}
	
	
	private Game getGame(String string) {
		Gson gson = new Gson();
		BufferedReader reader = null;
		LOGGER.warning("Can throw FileNotFoundException"+"\n");
		try {
			reader = new BufferedReader(new FileReader(string));
		} catch (FileNotFoundException e1) {
			LOGGER.severe("FileNotFoundException thrown. "+e1+"\n");
		}
		String line = null;
		StringBuilder stringBuilder = new StringBuilder();
		String ls = System.getProperty("line.separator");

		LOGGER.warning("Can throw IOException"+"\n");
		try {
			while ((line = reader.readLine()) != null) {
				stringBuilder.append(line);
				stringBuilder.append(ls);
			}
			reader.close();
		} catch (IOException e) {
			LOGGER.severe("IOException thrown. "+e+"\n");
		}
		String temp = stringBuilder.toString();
		JsonObject json = gson.fromJson(temp, JsonObject.class);
		Game game = Deserializer.getGame(json);
		return game;
	}



	public static Server getInstance() {
		if(instance == null){
			try {
				LOGGER.setLevel(Level.ALL);
				
				//Handler handler = new ConsoleHandler();
				//handler.setLevel(Level.ALL);
				//for(Handler h: Logger.getGlobal().getHandlers()){
				//	Logger.getGlobal().removeHandler(h);
				//}
				
				//LOGGER.addHandler(handler);
				LOGGER.info("Creating new server"+"\n");
				instance = new Server();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				LOGGER.severe("IOException thrown. "+e+"\n");
			}
			return instance;
		}else{
			return instance;
		}
	}
	
	public void run(int port){
		try {
			server = HttpServer.create(new InetSocketAddress(port), 10);
			LOGGER.config("Port set to "+port+"\n");
			server.setExecutor(null);
			
			LOGGER.info("Creating server handlers");
			for(ServerCalls call: ServerCalls.values()){
				server.createContext(call.getValue(), call.getHandler());
				handlers.add(call.getHandler());
			}
			
			for(OurHandler handler:handlers){
				handler.setFacade(useFakeFacade);
			}
			
			server.createContext("/docs/api/data", new Handlers.JSONAppender(""));
			server.createContext("/docs/api/view", new Handlers.BasicFile(""));

			
			server.start();
		} catch (IOException e) {
			LOGGER.severe("IOException thrown. "+e+"\n");
		}
	}
	
	public static void main(String []args) throws IOException{
		
		Server ourServer =  Server.getInstance();
		//boolean fake = Boolean.getBoolean(args[1]);
		//ourServer.setFakeFacade(fake);
		System.out.println("Need to find some way to set the useFakeFacade boolean in the main of server");
		ourServer.run(Integer.parseInt(args[0]));
		
	}


	private void setFakeFacade(boolean fake) {
		// TODO Auto-generated method stub
		useFakeFacade = fake;
	}


	public void loadCommandList(List<IRequest> commands2) {
		commands.add(commands2);
	}


	public int getGameByName(String name) {
		for (int i = 0; i<gameList.size(); i++) {
			if (gameList.get(i).getTitle().equals(name)) {
				return i;
			}
		}
		return -1;
	}


	public List<List<IRequest>> getCommands() {
		return commands;
	}
}
