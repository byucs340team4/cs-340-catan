package shared.locations;

import com.google.gson.JsonObject;

/**
 * Represents the location of a vertex on a hex map
 */
public class VertexLocation
{
	
	private HexLocation hexLoc;
	private int x;
	private int y;
	private String direction;
	private VertexDirection dir;
	
	public VertexLocation(HexLocation hexLoc, VertexDirection dir)
	{
		x = hexLoc.getX();
		y = hexLoc.getY();
		setHexLoc(hexLoc);
		setDir(dir);
	}
	
	public HexLocation getHexLoc()
	{
		return new HexLocation(x,y);
	}
	
	private void setHexLoc(HexLocation hexLoc)
	{
		if(hexLoc == null)
		{
			throw new IllegalArgumentException("hexLoc cannot be null");
		}
		this.hexLoc = hexLoc;
	}
	
	public VertexDirection getDir()
	{
		if(direction==null){
			return dir;
		}
		return VertexDirection.valueOf(direction);
	}
	
	private void setDir(VertexDirection direction)
	{
		this.dir = direction;
	}
	
	@Override
	public String toString()
	{
		if(dir==null)dir = VertexDirection.valueOf(direction);
		if(hexLoc==null)hexLoc = new HexLocation(x,y);
		return "VertexLocation [hexLoc=" + hexLoc + ", dir=" + dir + "]";
	}
	
	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + ((dir == null) ? 0 : dir.hashCode());
		result = prime * result + ((hexLoc == null) ? 0 : hexLoc.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj)
	{
		if(dir==null)dir=VertexDirection.valueOf(direction);
		if(hexLoc==null)hexLoc = new HexLocation(x,y);

		if(this == obj)
			return true;
		if(obj == null)
			return false;
		if(getClass() != obj.getClass())
			return false;
		VertexLocation other = (VertexLocation)obj;
		if(dir != other.getDir())
			return false;
		if(hexLoc == null)
		{
			if(other.hexLoc != null)
				return false;
		}
		else if(!hexLoc.equals(other.getHexLoc()))
			return false;
		return true;
	}
	
	/**
	 * Returns a canonical (i.e., unique) value for this vertex location. Since
	 * each vertex has three different locations on a map, this method converts
	 * a vertex location to a single canonical form. This is useful for using
	 * vertex locations as map keys.
	 * 
	 * @return Normalized vertex location
	 */
	public VertexLocation getNormalizedLocation()
	{
		
		// Return location that has direction NW or NE
		if(dir==null)dir = VertexDirection.valueOf(direction);
		if(hexLoc==null)hexLoc = new HexLocation(x,y);
		switch (dir)
		{
			case NW:
			case NE:
				return this;
			case W:
				return new VertexLocation(
										  hexLoc.getNeighborLoc(EdgeDirection.SW),
										  VertexDirection.NE);
			case SW:
				return new VertexLocation(
										  hexLoc.getNeighborLoc(EdgeDirection.S),
										  VertexDirection.NW);
			case SE:
				return new VertexLocation(
										  hexLoc.getNeighborLoc(EdgeDirection.S),
										  VertexDirection.NE);
			case E:
				return new VertexLocation(
										  hexLoc.getNeighborLoc(EdgeDirection.SE),
										  VertexDirection.NW);
			default:
				assert false;
				return null;
		}
	}
	
	public VertexLocation getUnNormalizedLocation()
	{
		
		// Return location that has direction NW or NE
		if(dir==null)dir = VertexDirection.valueOf(direction);
		if(hexLoc==null)hexLoc = new HexLocation(x,y);
		switch (dir)
		{
			case NW:
				return new VertexLocation(
						  hexLoc.getNeighborLoc(EdgeDirection.NW),
						  VertexDirection.SW);
			case NE:
				return new VertexLocation(
						  hexLoc.getNeighborLoc(EdgeDirection.NE),
						  VertexDirection.W);
			case W:
			case SW:
			case SE:
			case E:
			default:
			assert false;
			return null;
		}
	}
	public VertexLocation getUnNormalizedLocationTwo()
	{
		
		// Return location that has direction NW or NE
		if(dir==null)dir = VertexDirection.valueOf(direction);
		if(hexLoc==null)hexLoc = new HexLocation(x,y);
		switch (dir)
		{
			case NW:
				return new VertexLocation(
						  hexLoc.getNeighborLoc(EdgeDirection.N),
						  VertexDirection.SW);
			case NE:
				return new VertexLocation(
						  hexLoc.getNeighborLoc(EdgeDirection.N),
						  VertexDirection.SE);
			case W:
			case SW:
			case SE:
			case E:
			default:
			assert false;
			return null;
		}
	}
	public JsonObject toJson(){
		JsonObject result = new JsonObject();
		result.addProperty("x", this.x);
		result.addProperty("y", this.y);
		result.addProperty("direction", this.direction);
		return result;
	}
}

