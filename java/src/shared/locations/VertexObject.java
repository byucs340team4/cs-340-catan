package shared.locations;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.JsonObject;

public class VertexObject {

	private int owner;
	private VertexLocation location;
	private ArrayList<VertexLocation>locations;
	
	VertexObject(){
		
	}
	
	public VertexObject(int owner,VertexLocation location) {
		this.owner = owner;
		this.location = location;
	}

	public int getOwner() {
		return this.owner;
	}

	public void setOwner(int owner) {
		this.owner = owner;
	}

	public VertexLocation getLocation() {
		return location;
	}

	public void setLocation(VertexLocation location) {
		this.location = location;
	}

	public List<VertexLocation> getLocations() {
		locations = new ArrayList<VertexLocation>();
		locations.add(location);
		VertexDirection[] vDir = VertexDirection.values();
		EdgeDirection[]eDir = EdgeDirection.values();
		int vValue = location.getDir().ordinal();
		HexLocation neighbor1 = location.getHexLoc().getNeighborLoc(eDir[vValue]);
		if(neighbor1!=null)locations.add(new VertexLocation(neighbor1,vDir[(vValue+4)%6]));
		HexLocation neighbor2 = location.getHexLoc().getNeighborLoc(eDir[(vValue + 5)%6]);
		if(neighbor2!=null)locations.add(new VertexLocation(neighbor2,vDir[(vValue+2)%6]));
		
		return locations;
	}

	public void setLocations(ArrayList<VertexLocation> locations) {
		this.locations = locations;
	}

	public VertexDirection getVertexDirection(HexLocation hexLocation){
		locations = new ArrayList<VertexLocation>();
		locations.add(location);
		VertexDirection[] vDir = VertexDirection.values();
		EdgeDirection[]eDir = EdgeDirection.values();
		int vValue = location.getDir().ordinal();
	
		locations.add(new VertexLocation(location.getHexLoc().getNeighborLoc(eDir[vValue]),vDir[(vValue+4)%6]));
		locations.add(new VertexLocation(location.getHexLoc().getNeighborLoc(eDir[(vValue + 5)%6]),vDir[(vValue+2)%6]));
		
		for(VertexLocation vert:locations){
			if(vert.getHexLoc().getX() == hexLocation.getX() && vert.getHexLoc().getY() == hexLocation.getY()){
				return vert.getDir();
			}
		}
		
		return null;
		
	}
	
	public JsonObject toJson(){
		JsonObject result = new JsonObject();
		result.addProperty("owner", this.owner);
		result.add("location", this.location.toJson());
		return result;
	}
	
}
