package shared.definitions;

import java.awt.Color;

public enum CatanColor
{
	RED, ORANGE, YELLOW, BLUE, GREEN, PURPLE, PUCE, WHITE, BROWN, PUKE;
	
	private Color color;
	private String value;
	
	static
	{
		RED.value = "red";
		RED.color = new Color(227, 66, 52);
		ORANGE.value = "orange";
		ORANGE.color = new Color(255, 165, 0);
		YELLOW.value = "yellow";
		YELLOW.color = new Color(253, 224, 105);
		BLUE.value = "blue";
		BLUE.color = new Color(111, 183, 246);
		GREEN.value = "green";
		GREEN.color = new Color(109, 192, 102);
		PURPLE.value = "purple";
		PURPLE.color = new Color(157, 140, 212);
		PUCE.value = "puce";
		PUCE.color = new Color(204, 136, 153);
		WHITE.value = "white";
		WHITE.color = new Color(223, 223, 223);
		BROWN.value = "brown";
		BROWN.color = new Color(161, 143, 112);
		PUKE.value = "puke";
		PUKE.color = new Color(179,232,100);
	}
	
	public Color getJavaColor()
	{
		return color;
	}
}

