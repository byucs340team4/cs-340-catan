package shared.definitions;

public enum DevCardType
{
	
	SOLDIER, YEAR_OF_PLENTY, MONOPOLY, ROAD_BUILD, MONUMENT;
	
	private String value;
	
	static{
		SOLDIER.value = "soldier";
		YEAR_OF_PLENTY.value = "yearOfPlenty";
		MONOPOLY.value = "monopoly";
		ROAD_BUILD.value = "roadBuilding";
		MONUMENT.value = "monument";
	}
	
	public String getValue(){
		return value;
	}
}

