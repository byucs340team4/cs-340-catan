package shared.definitions;

public enum ResourceType
{
	WOOD, BRICK, SHEEP, WHEAT, ORE;
	
	private String value;
	
	static{
		WOOD.value = "wood";
		BRICK.value = "brick";
		SHEEP.value = "sheep";
		WHEAT.value = "wheat";
		ORE.value = "ore";
	}
	
	public String getValue(){
		return value;
	}
	
	public static ResourceType getType(String type) throws IllegalArgumentException{
		switch(type){
			case "wood": return WOOD;
			case "brick": return BRICK;
			case "sheep": return SHEEP;
			case "wheat": return WHEAT;
			case "ore": return ORE;
			default: return null;
		}
	}
}

