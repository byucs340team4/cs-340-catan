package shared.proxy;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.net.URLDecoder;
import java.util.ArrayList;

import client.data.GameInfo;
import client.data.PlayerInfo;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import shared.definitions.CatanColor;
import shared.proxy.request.AcceptTrade;
import shared.proxy.request.AddAIRequest;
import shared.proxy.request.BuildCity;
import shared.proxy.request.BuildRoad;
import shared.proxy.request.BuildSettlement;
import shared.proxy.request.BuyDevCardRequest;
import shared.proxy.request.CreateGameRequests;
import shared.proxy.request.Credentials;
import shared.proxy.request.DiscardCards;
import shared.proxy.request.FinishTurnRequest;
import shared.proxy.request.GameCommandsRequest;
import shared.proxy.request.JoinGameRequest;
import shared.proxy.request.ListAIRequest;
import shared.proxy.request.ListGamesRequest;
import shared.proxy.request.LoadGameRequest;
import shared.proxy.request.MaritimeTrade;
import shared.proxy.request.Monopoly;
import shared.proxy.request.Monument;
import shared.proxy.request.OfferTrade;
import shared.proxy.request.RoadBuildingRequest;
import shared.proxy.request.RobPlayerRequest;
import shared.proxy.request.RollNumberRequest;
import shared.proxy.request.SaveGameRequest;
import shared.proxy.request.SendChatRequest;
import shared.proxy.request.Soldier;
import shared.proxy.request.YearOfPlentyRequest;
import shared.proxy.response.AddAIResponse;
import shared.proxy.response.CreateGameResponse;
import shared.proxy.response.GamesListResponse;
import shared.proxy.response.JoinGameResponse;
import shared.proxy.response.ListAIResponse;
import shared.proxy.response.LoadGameResponse;
import shared.proxy.response.LoginResponse;
import shared.proxy.response.SaveGameResponse;
import gamemanager.game.Game;

public class Proxy implements IProxy {

	private Game game;
	private Game fakeGame;
	private String host;
	private int port;
	private Gson gson;

	public Proxy(String file)  {
		gson = new Gson();
		BufferedReader reader = null;
		try {
			reader = new BufferedReader(new FileReader(file));
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		}
		String line = null;
		StringBuilder stringBuilder = new StringBuilder();
		String ls = System.getProperty("line.separator");

		try {
			while ((line = reader.readLine()) != null) {
				stringBuilder.append(line);
				stringBuilder.append(ls);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		String temp = stringBuilder.toString();
		JsonObject json = gson.fromJson(temp, JsonObject.class);
		game = Deserializer.getGame(json);
		ProxyCommunicator.setSERVER_HOST("localhost");
		ProxyCommunicator.setSERVER_PORT(8081);
		ProxyCommunicator.setURL_PREFIX();
		gson = new Gson();
	}

	public Proxy() {
		ProxyCommunicator.setSERVER_HOST("localhost");
		ProxyCommunicator.setSERVER_PORT(8081);
		ProxyCommunicator.setURL_PREFIX();
		gson = new Gson();
	}
	
	public Proxy(String host, int port) {
		ProxyCommunicator.setSERVER_HOST(host);
		ProxyCommunicator.setSERVER_PORT(port);
		ProxyCommunicator.setURL_PREFIX();
		gson = new Gson();
	}

	/**
	 * 
	 * @param cred
	 * @return
	 */
	@SuppressWarnings("deprecation")
	public LoginResponse userLogin(Credentials cred) {
		String command = "user/login";

		HttpURLResponse response = null;
		try {
			response = ProxyCommunicator.getSingleton().doPost(command, cred.toJson());
		} catch (IOException e) {
			return new LoginResponse(false,null,null,0);
		}
		//System.out.println(response.getResponseCode());
		if (response.getResponseCode() == 200) {
			String cookie = response.getHeaders().get("Set-cookie").get(0);
			cookie = cookie.substring(0, cookie.length() - 8);
			ProxyCommunicator.getSingleton().setUserCookie(cookie);
			String userCookie = ProxyCommunicator.getSingleton().getUserCookie();
			userCookie = userCookie.substring(11);
			String result = URLDecoder.decode(userCookie);
			JsonParser parser = new JsonParser();
			JsonObject json = (JsonObject)parser.parse(result);
			
			return new LoginResponse(true, json.get("name").toString(),json.get("password").toString(), Integer.parseInt(json.get("playerID").toString()));
		} else {
			return new LoginResponse(false,null,null,0);
		}
	}

	/**
	 * 
	 * @param cred
	 * @return
	 */
	public LoginResponse userRegister(Credentials cred) {
		String command = "user/register";

		HttpURLResponse response = null;
		try {
			response = ProxyCommunicator.getSingleton().doPost(command, cred.toJson());
		} catch (IOException e) {
			return new LoginResponse(false);
		}
		//System.out.println(response.getResponseCode());
		if (response.getResponseCode() == 200) {
			return new LoginResponse(true);
		} else {
			return new LoginResponse(false);
		}
	}

	/**
	 * 
	 * @param request
	 * @return
	 */

	public GamesListResponse gamesList(ListGamesRequest request) {
		if(ProxyCommunicator.getSingleton().getUserCookie()==null){
			return null;
		}
		String command = "games/list";
		InputStream response = null;
		try {
			response = (InputStream)ProxyCommunicator.getSingleton().doPost(command, new JsonObject()).getResponseBody();
		} catch (IOException e) {
			e.printStackTrace();
		}
		String result = ProxyCommunicator.getSingleton().getBody(response);
		
		JsonParser parser = new JsonParser();
		JsonArray json = (JsonArray)parser.parse(result);
		
		ArrayList<GameInfo> info = new ArrayList<GameInfo>();
		for (JsonElement elem : json) {
			GameInfo temp = gson.fromJson(elem, GameInfo.class);
			ArrayList<PlayerInfo> players = new ArrayList<>();
			players.addAll(temp.getPlayers());
			for(PlayerInfo player : temp.getPlayers()) {
				if(!player.getName().equals("")) {
					player.setColor(CatanColor.valueOf(player.getStringColor().toUpperCase()));
				}
				else {
					players.remove(player);
				}
			}
			if(temp.getPlayers().size() != players.size()) {
				temp.setPlayers(players);
			}
			info.add(temp);
		}
		
		GamesListResponse game = new GamesListResponse(info);
		
		return game;
	}

	/**
	 * 
	 * @param request
	 * @return
	 */
	public CreateGameResponse gamesCreate(CreateGameRequests request) {
		if(ProxyCommunicator.getSingleton().getUserCookie()==null){
			return null;
		}
		String command = "games/create";
		InputStream response = null;
		try {
			response = (InputStream)ProxyCommunicator.getSingleton().doPost(command, request.toJson()).getResponseBody();
		} catch (IOException e) {
			e.printStackTrace();
		}
		String result = ProxyCommunicator.getSingleton().getBody(response);
		
		JsonParser parser = new JsonParser();
		JsonObject json = (JsonObject)parser.parse(result);
		
		CreateGameResponse game = gson.fromJson(json, CreateGameResponse.class);
		
		return game;
	}

	/**
	 * 
	 * @param request
	 * @return
	 */
	public JoinGameResponse gamesJoin(JoinGameRequest request, boolean createGame) {
		String command = "games/join";
		if (ProxyCommunicator.getSingleton().getUserCookie() == null)
			return new JoinGameResponse();
		String color = request.getColor().toLowerCase();
		if (color.equals("red") || color.equals("orange") || color.equals("yellow") || color.equals("blue") || color.equals("green")
				|| color.equals("purple") || color.equals("puce") || color.equals("white")|| color.equals("brown")) {
		} else
			return new JoinGameResponse();
		HttpURLResponse response = null;
		try {
			response = ProxyCommunicator.getSingleton().doPost(command, request.toJson());
		} catch (IOException e) {
			e.printStackTrace();
		}

		if (response.getResponseCode() == 200) {
			String cookie = response.getHeaders().get("Set-cookie").get(0);
			cookie = cookie.substring(0, cookie.length() - 8);
			//System.out.println(cookie);
			if(!createGame)
				ProxyCommunicator.getSingleton().setGameCookie(cookie);
			return new JoinGameResponse(true);
		}
		return new JoinGameResponse();
	}

	/**
	 * 
	 * @param request
	 * @return
	 */
	public SaveGameResponse gamesSave(SaveGameRequest request) {
		return null;

	}

	/**
	 * 
	 * @param request
	 * @return
	 */
	public LoadGameResponse gamesLoad(LoadGameRequest request) {
		return null;

	}

	/**
	 * 
	 * @return
	 */
	public Game gameModel() {
		if(ProxyCommunicator.getSingleton().getUserCookie()==null){
			return null;
		}
		if(ProxyCommunicator.getSingleton().getGameCookie()==null)
			return null;
		String command = "game/model";
		InputStream response = null;
		try {
			response = (InputStream)ProxyCommunicator.getSingleton().doGet(command).getResponseBody();
		} catch (IOException e) {
			e.printStackTrace();
		}
		String result = ProxyCommunicator.getSingleton().getBody(response);
		
		JsonParser parser = new JsonParser();
		JsonObject json = (JsonObject)parser.parse(result);
		
		game = Deserializer.getGame(json);
		
		return game;
	}

	/**
	 * 
	 * @return
	 */
	public Game gameReset() {
		return null;

	}

	/**
	 * 
	 * @return
	 */
	public Game gameCommands(GameCommandsRequest

	request) {
		return null;

	}

	/**
	 * 
	 * @param request
	 * @return
	 */
	public AddAIResponse addAI(AddAIRequest request) {
		if(ProxyCommunicator.getSingleton().getUserCookie()==null){
			return null;
		}
		String command = "game/addAI";
		InputStream response = null;
		try {
			response = (InputStream)ProxyCommunicator.getSingleton().doPost(command,request.toJson()).getResponseBody();
		} catch (IOException e) {
			e.printStackTrace();
		}
		String result = ProxyCommunicator.getSingleton().getBody(response);
		
		return new AddAIResponse(result);

	}

	/**
	 * 
	 * @param request
	 * @return
	 */
	public ListAIResponse listAI(ListAIRequest request) {
		return null;

	}

	/**
	 * 
	 * @param request
	 * @return
	 */
	public Game sendChat(SendChatRequest request) {

		String command = "moves/sendChat";
		InputStream response = null;
		try {
			response = (InputStream)ProxyCommunicator.getSingleton().doPost(command, request.getJsonObject()).getResponseBody();
		} catch (IOException e) {
			e.printStackTrace();
		}
		//System.out.println("Proxy.java: "+response);
		if(response==null)
			return null;
		String result = ProxyCommunicator.getSingleton().getBody(response);
		JsonParser parser = new JsonParser();
		JsonObject json = (JsonObject)parser.parse(result);
		//JsonObject json = gson.fromJson(result, JsonObject.class);
		game = Deserializer.getGame(json);
		return game;
	}

	/**
	 * 
	 * @param request
	 * @return
	 */
	@SuppressWarnings("unused")
	public Game rollNumber(RollNumberRequest request) {
		//if(game.canRoll(request)) {
		if(true){
			String command = "moves/rollNumber";
			InputStream response = null;
			try {
				response = (InputStream)ProxyCommunicator.getSingleton().doPost(command, request.getJson()).getResponseBody();
			} catch (IOException e) {
				e.printStackTrace();
			}
			if(response == null)
				return null;
			String result = ProxyCommunicator.getSingleton().getBody(response);
			JsonObject json = gson.fromJson(result, JsonObject.class);
			game = Deserializer.getGame(json);
			return game;
		}
		return null;
	}

	/**
	 * 
	 * @param request
	 * @return
	 */
	@SuppressWarnings("unused")
	public Game robPlayer(RobPlayerRequest request) {
		//if(game.canRob(request)) {
		if(true){
			String command = "moves/robPlayer";
			InputStream response = null;
			try {
				response = (InputStream)

				ProxyCommunicator.getSingleton().doPost(command, request.toJson()).getResponseBody();
			} catch (IOException e) {
				e.printStackTrace();
			}
			String result = ProxyCommunicator.getSingleton().getBody(response);
			JsonObject json = gson.fromJson(result, JsonObject.class);
			game = Deserializer.getGame(json);
			return game;
		}
		return null;
	}

	/**
	 * 
	 * @param request
	 * @return
	 */
	public Game finishTurn(FinishTurnRequest request) {
		//if (game.canFinishTurn(request)) {
		if(true){
			String command = "moves/finishTurn";
			InputStream response = null;
			try {
				response = (InputStream)

				ProxyCommunicator.getSingleton().doPost(command, request.toJson()).getResponseBody();
			} catch (IOException e) {
				e.printStackTrace();
			}
			if (response == null) {
				return null;
			}
			String result = ProxyCommunicator.getSingleton().getBody(response);
			//JsonObject json = gson.fromJson(result,JsonObject.class);
			
			JsonParser parser = new JsonParser();
			JsonObject json = (JsonObject)parser.parse(result);
			
			game = Deserializer.getGame(json);
		}
		return game;
	}

	/**
	 * 
	 * @param request
	 * @return
	 */
	@SuppressWarnings("unused")
	public Game buyDevCard(BuyDevCardRequest request) {
		//if (game.canBuyDevCard(request)) {
		if(true){
			String command = "moves/buyDevCard";
			InputStream response = null;
			try {
				response = (InputStream)

				ProxyCommunicator.getSingleton().doPost(command, request.toJson()).getResponseBody();
			} catch (IOException e) {
				e.printStackTrace();
			}
			if (response == null) {
				return null;
			}
			String result = ProxyCommunicator.getSingleton().getBody(response);
			//JsonObject json = gson.fromJson(result,JsonObject.class);
			JsonParser parser = new JsonParser();
			JsonObject json = (JsonObject)parser.parse(result);
			
			game = Deserializer.getGame(json);
			return game;
		}
		return null;
	}

	/**
	 * 
	 * @param request
	 * @return
	 */
	public Game yearOfPlenty(YearOfPlentyRequest request) {
		if(game.canYoP(request)) {
			String command = "moves/Year_of_Plenty";
			InputStream response = null;
			try {
				response = (InputStream)

				ProxyCommunicator.getSingleton().doPost(command, request.toJson()).getResponseBody();
			} catch (IOException e) {
				e.printStackTrace();
			}
			if (response == null) {
				return null;
			}
			String result = ProxyCommunicator.getSingleton().getBody
	
			(response);
			//JsonObject json = gson.fromJson(result, JsonObject.class);
			JsonParser parser = new JsonParser();
			JsonObject json = (JsonObject)parser.parse(result);
			game = Deserializer.getGame(json);
			return game;
		}
		return null;
	}

	/**
	 * 
	 * @param request
	 * @return
	 */
	@SuppressWarnings("unused")
	public Game roadBuilding(RoadBuildingRequest request) {
		//if(game.canRoadBuiding(request)) {
		if(true){
			String command = "moves/Road_Building";
			InputStream response = null;
			try {
				response = (InputStream)

				ProxyCommunicator.getSingleton().doPost(command, request.toJson()).getResponseBody();
			} catch (IOException e) {
				e.printStackTrace();
			}
			if (response == null) {
				return null;
			}
			String result = ProxyCommunicator.getSingleton().getBody
	
			(response);
			//JsonObject json = gson.fromJson(result, JsonObject.class);
			JsonParser parser = new JsonParser();
			JsonObject json = (JsonObject)parser.parse(result);
			game = Deserializer.getGame(json);
			return game;
		}
		return null;
	}

	/**
	 * 
	 * @param soldier
	 * @return
	 */
	@SuppressWarnings("unused")
	public Game soldier(Soldier soldier) {
		//if (game.canPlaySoldier(soldier)) {
		if(true){
			String command = "moves/Soldier";
			InputStream response = null;
			try {
				response = (InputStream)

				ProxyCommunicator.getSingleton().doPost(command, soldier.toJson()).getResponseBody();
			} catch (IOException e) {
				e.printStackTrace();
			}
			if (response == null) {
				return null;
			}
			String result = ProxyCommunicator.getSingleton().getBody(response);
			//JsonObject json = gson.fromJson(result,JsonObject.class);
			JsonParser parser = new JsonParser();
			JsonObject json = (JsonObject)parser.parse(result);
			game = Deserializer.getGame(json);
			return game;
		}
		return null;
	}

	/**
	 * 
	 * @param monopoly
	 * @return
	 */
	public Game monopoly(Monopoly monopoly) {
		//if(game.canMonopoly(monopoly)) {
		if(true){
			String command = "moves/Monopoly";
			InputStream response = null;
			try {
				response = (InputStream) ProxyCommunicator.getSingleton().doPost(command, monopoly.toJson()).getResponseBody();
			} catch (IOException e) {
				e.printStackTrace();
			}
			if (response == null) {
				return null;
			}
			String result = ProxyCommunicator.getSingleton().getBody(response);
			//JsonObject json = gson.fromJson(result, JsonObject.class);
			JsonParser parser = new JsonParser();
			JsonObject json = (JsonObject)parser.parse(result);
			game = Deserializer.getGame(json);
			return game;
		}
		return null;
	}

	/**
	 * 
	 * @param monument
	 * @return
	 */
	public Game monument(Monument monument) {
		if(game.canMonument(monument)) {
			String command = "moves/Monument";
			InputStream response = null;
			try {
				response = (InputStream)

				ProxyCommunicator.getSingleton().doPost(command, monument.toJson()).getResponseBody();
			} catch (IOException e) {
				e.printStackTrace();
			}
			if (response == null) {
				return null;
			}
			String result = ProxyCommunicator.getSingleton().getBody
	
			(response);
			//JsonObject json = gson.fromJson(result, JsonObject.class);
			JsonParser parser = new JsonParser();
			JsonObject json = (JsonObject)parser.parse(result);
			game = Deserializer.getGame(json);
			return game;
		}
		return null;
	}

	/**
	 * 
	 * @param buildRoad
	 * @return
	 */
	@SuppressWarnings("unused")
	public Game buildRoad(BuildRoad buildRoad) {
		//if(game.canBuildRoad(buildRoad)) {
		if(true){
			String command = "moves/buildRoad";
			InputStream response = null;
			try {
				response = (InputStream)

				ProxyCommunicator.getSingleton().doPost(command, buildRoad.toJson()).getResponseBody();
			} catch (IOException e) {
				e.printStackTrace();
			}
			if (response == null) {
				return null;
			}
			String result = ProxyCommunicator.getSingleton().getBody
	
			(response);
			//JsonObject json = gson.fromJson(result, JsonObject.class);
			JsonParser parser = new JsonParser();
			JsonObject json = (JsonObject)parser.parse(result);
			game = Deserializer.getGame(json);
			return game;
		}
		return null;
	}

	/**
	 * 
	 * @param buildSettlement
	 * @return
	 */
	@SuppressWarnings("unused")
	public Game buildSettlement(BuildSettlement buildSettlement) {
		//if(game.canBuildSettlement(buildSettlement)) {
		if(true){ 
			String command = "moves/buildSettlement";
			InputStream response = null;
			try {
				response = (InputStream)

				ProxyCommunicator.getSingleton().doPost(command, buildSettlement.toJson()).getResponseBody();
			} catch (IOException e) {
				e.printStackTrace();
			}
			if (response == null) {
				return null;
			}
			String result = ProxyCommunicator.getSingleton().getBody(response);
			//JsonObject json = gson.fromJson(result, JsonObject.class);
			JsonParser parser = new JsonParser();
			JsonObject json = (JsonObject)parser.parse(result);
			game = Deserializer.getGame(json);
			return game;
		}
		return null;
	}

	/**
	 * 
	 * @param buildCity
	 * @return
	 */
	@SuppressWarnings("unused")
	public Game buildCity(BuildCity buildCity) {
		//if(game.canBuildCity(buildCity)) {
		if(true){
			String command = "moves/buildCity";
			InputStream response = null;
			try {
				response = (InputStream)

				ProxyCommunicator.getSingleton().doPost(command, buildCity.toJson()).getResponseBody();
			} catch (IOException e) {
				e.printStackTrace();
			}
			if (response == null) {
				return null;
			}
			String result = ProxyCommunicator.getSingleton().getBody
	
			(response);
			//JsonObject json = gson.fromJson(result, JsonObject.class);
			JsonParser parser = new JsonParser();
			JsonObject json = (JsonObject)parser.parse(result);
			game = Deserializer.getGame(json);
			return game;
		}
		return null;
	}

	/**
	 * 
	 * @param offer
	 * @return
	 */
	public Game offerTrade(OfferTrade offer) {
			String command = "moves/offerTrade";
			HttpURLResponse hResponse = null;
			try {
				hResponse = ProxyCommunicator.getSingleton().doPost(command, offer.toJson());
			} catch (IOException e) {
				e.printStackTrace();
			}
			if(hResponse.getResponseCode()==200){
				InputStream iResponse = (InputStream)hResponse.getResponseBody();
				String result = ProxyCommunicator.getSingleton().getBody(iResponse);
				JsonParser jParser = new JsonParser();
				JsonObject data = (JsonObject)jParser.parse(result);
				game = Deserializer.getGame(data);
				return game;
			}
			else
				return null;
		}
		
//			InputStream response = (InputStream)ProxyCommunicator.getSingleton().doPost(command, offer.toJson()).getResponseBody();
//			
//			if (response == null) {
//				return null;
//			}
//			
//			String result = ProxyCommunicator.getSingleton().getBody(response);
//			//JsonObject json = gson.fromJson(result, JsonObject.class);
//			JsonParser parser = new JsonParser();
//			JsonObject json = (JsonObject)parser.parse(result);
//			game = Deserializer.getGame(json);
//			return game;
//		}
//		return null;
//	}

	/**
	 * 
	 * @param accept
	 * @return
	 */
	public Game acceptTrade(AcceptTrade accept) {
		//if(game.canAcceptTrade(accept)) {
		String command = "moves/acceptTrade";
		InputStream response = null;
		try {
			response = (InputStream)

			ProxyCommunicator.getSingleton().doPost(command, accept.toJson()).getResponseBody();
		} catch (IOException e) {
			e.printStackTrace();
		}
		if (response == null) {
			return null;
		}
		String result = ProxyCommunicator.getSingleton().getBody

		(response);
		//JsonObject json = gson.fromJson(result, JsonObject.class);
		JsonParser parser = new JsonParser();
		JsonObject json = (JsonObject)parser.parse(result);
		if(json == null){
			return null;
		}
		game = Deserializer.getGame(json);
		return game;
		//}

	}

	/**
	 * 
	 * @param trade
	 * @return
	 */
	public Game maritimeTrade(MaritimeTrade trade) {
		if(game.canMaritimeTrade(trade)) {
			String command = "moves/maritimeTrade";
			InputStream response = null;
			try {
				response = (InputStream)ProxyCommunicator.getSingleton().doPost(command, trade.toJson()).getResponseBody();
			} catch (IOException e) {
				e.printStackTrace();
			}
			if (response == null) {
				return null;
			}
			String result = ProxyCommunicator.getSingleton().getBody(response);
			//JsonObject json = gson.fromJson(result, JsonObject.class);
			JsonParser parser = new JsonParser();
			JsonObject json = (JsonObject)parser.parse(result);
			game = Deserializer.getGame(json);
			return game;
		}
		return null;
	}

	/**
	 * 
	 * @param discard
	 * @return
	 */
	@SuppressWarnings("unused")
	public Game discardCards(DiscardCards discard) {
		if(true) {
			String command = "moves/discardCards";
			InputStream response = null;
			try {
				response = (InputStream)

				ProxyCommunicator.getSingleton().doPost(command, discard.toJson()).getResponseBody();
			} catch (IOException e) {
				e.printStackTrace();
			}
			if (response == null) {
				return null;
			}
			String result = ProxyCommunicator.getSingleton().getBody(response);
			//JsonObject json = gson.fromJson(result, JsonObject.class);
			JsonParser parser = new JsonParser();
			JsonObject json = (JsonObject)parser.parse(result);
			game = Deserializer.getGame(json);
			return game;
		}
		return null;
	}

	/**
	 * 
	 * @param request
	 * @return
	 */
	public void changeLogLevel(SetLogLevelRequest request) {

	}

	public Game getGame() {
		return game;
	}

	public void setGame(Game game) {
		this.game = game;
	}

	public Game getFakeGame() {
		return fakeGame;
	}

	public void setFakeGame(Game fakeGame) {
		this.fakeGame = fakeGame;
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}
}
