package shared.proxy.request;

import com.google.gson.JsonObject;

public class CreateGameRequests implements IRequest {
	Boolean randomTiles;
	Boolean randomNumbers;
	Boolean randomPorts;
	String name;
	
	public CreateGameRequests(Boolean randomTiles, Boolean randomNumbers,Boolean randomPorts, String name) {
		super();
		this.randomTiles = randomTiles;
		this.randomNumbers = randomNumbers;
		this.randomPorts = randomPorts;
		this.name = name;
	}
	
	public JsonObject toJson(){
		JsonObject result = new JsonObject();
		result.addProperty("randomTiles", this.randomTiles);
		result.addProperty("randomNumbers", this.randomNumbers);
		result.addProperty("randomPorts", this.randomPorts);
		result.addProperty("name", this.name);
		return result;
	}

	public Boolean getRandomTiles() {
		return randomTiles;
	}

	public void setRandomTiles(Boolean randomTiles) {
		this.randomTiles = randomTiles;
	}

	public Boolean getRandomNumbers() {
		return randomNumbers;
	}

	public void setRandomNumbers(Boolean randomNumbers) {
		this.randomNumbers = randomNumbers;
	}

	public Boolean getRandomPorts() {
		return randomPorts;
	}

	public void setRandomPorts(Boolean randomPorts) {
		this.randomPorts = randomPorts;
	}

	public String getID() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String getName(){
		return this.name;
	}
	
	@Override
	public String getType() {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public int getPlayerIndex() {
		return 0;
	}
}
