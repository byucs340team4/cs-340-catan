package shared.proxy.request;

import com.google.gson.JsonObject;

import shared.proxy.MoveType;

public class SendChatRequest implements IRequest {
	
	String type;
	int playerIndex;
	String content;
	
	public SendChatRequest(int playerIndex, String content){
		this.playerIndex = playerIndex;
		this.type = "sendChat";
		this.content = content;
	}
	
	public JsonObject getJsonObject(){
		JsonObject result = new JsonObject();
		result.addProperty("type", "sendChat");
		result.addProperty("playerIndex", playerIndex);
		result.addProperty("content", this.content);
		return result;
	}
	
	@Override
	public String getType() {
		// TODO Auto-generated method stub
		return type;
	}
	
	@Override
	public int getPlayerIndex() {
		return playerIndex;
	}
	
	public String getContent(){
		return content;
	}
}
