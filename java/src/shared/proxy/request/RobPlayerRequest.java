package shared.proxy.request;

import com.google.gson.JsonObject;

import shared.locations.HexLocation;
import shared.proxy.MoveType;

public class RobPlayerRequest implements IRequest {
	
	private String type;
	private int playerIndex;
	private int victimIndex;
	private HexLocation location;
	
	public RobPlayerRequest(MoveType type, int playerIndex, int victimIndex, HexLocation location){
		this.type = "robPlayer";
		this.playerIndex = playerIndex;
		this.victimIndex = victimIndex;
		this.location = location;
	}
	
	public JsonObject toJson(){
		JsonObject result = new JsonObject();
		result.addProperty("type", "robPlayer");
		result.addProperty("playerIndex", this.playerIndex);
		result.addProperty("victimIndex", this.victimIndex);
		JsonObject hexObject = new JsonObject();
		hexObject.addProperty("x", location.getX());
		hexObject.addProperty("y", location.getY());
		result.add("location", hexObject);
		return result;
	}

	@Override
	public String getType() {
		// TODO Auto-generated method stub
		return type;
	}
	
	@Override
	public int getPlayerIndex() {
		return playerIndex;
	}
	public void setPlayerIndex(int playerIndex) {
		this.playerIndex = playerIndex;
	}

	public int getVictimIndex() {
		return victimIndex;
	}

	public void setVictimIndex(int victimIndex) {
		this.victimIndex = victimIndex;
	}

	public HexLocation getLocation() {
		return location;
	}

	public void setLocation(HexLocation location) {
		this.location = location;
	}
	
	
}
