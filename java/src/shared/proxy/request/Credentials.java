package shared.proxy.request;

import java.security.InvalidParameterException;

import com.google.gson.JsonObject;

public class Credentials implements IRequest {
	private String username;
	private String password;
	
	public Credentials(String username, String password) throws InvalidParameterException{
		if(username==null||password==null){
			throw new InvalidParameterException("username and password cannot be null");
		}
		if(username.length() < 3 || username.length() > 7) {
			throw new InvalidParameterException("username must be between 3 and 7 characters");
		}
		if(password.length() > 16) {
			throw new InvalidParameterException("password must be between 5 and 16 characters");
		}
		this.username = username;
		this.password = password;
	}
	
	public JsonObject toJson(){
		JsonObject result = new JsonObject();
		result.addProperty("username", this.username);
		result.addProperty("password", this.password);
		return result;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	@Override
	public String getType() {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public int getPlayerIndex() {
		return 0;
	}
}
