package shared.proxy.request;

import com.google.gson.JsonObject;

import shared.definitions.ResourceType;

public class Monopoly implements IRequest {
	
	String resource;
	int playerIndex;
	String type;
	public Monopoly(ResourceType resource, int playerIndex) {
		super();
		type = "Monopoly";
		this.resource = resource.toString();
		this.playerIndex = playerIndex;
	}
	
	public JsonObject toJson(){
		JsonObject result = new JsonObject();
		result.addProperty("type", "Monopoly");
		result.addProperty("resource", resource.toString().toLowerCase());
		result.addProperty("playerIndex", this.playerIndex);
		return result;
	}

	public String getResource() {
		return resource;
	}

	public void setResource(String resource) {
		this.resource = resource;
	}

	@Override
	public String getType() {
		// TODO Auto-generated method stub
		return type;
	}
	
	@Override
	public int getPlayerIndex() {
		return playerIndex;
	}

	public void setPlayerIndex(int playerIndex) {
		this.playerIndex = playerIndex;
	}
}
