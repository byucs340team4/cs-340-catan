package shared.proxy.request;

import com.google.gson.JsonObject;

import shared.proxy.MoveType;

public class RollNumberRequest implements IRequest {
	
	private String type;
	private int playerIndex;
	private int number;
	
	public RollNumberRequest(MoveType type, int playerIndex, int number){
		this.type = "rollNumber";
		this.playerIndex = playerIndex;
		this.number = number;
	}
	
	public JsonObject getJson(){
		JsonObject result = new JsonObject();
		result.addProperty("type", "rollNumber");
		result.addProperty("playerIndex", playerIndex);
		result.addProperty("number", number);
		System.out.println(result);
		return result;
	}

	@Override
	public String getType() {
		// TODO Auto-generated method stub
		return type;
	}
	
	@Override
	public int getPlayerIndex() {
		return playerIndex;
	}

	public void setPlayerIndex(int playerIndex) {
		this.playerIndex = playerIndex;
	}

	public int getNumber() {
		return number;
	}

	public void setNumber(int number) {
		this.number = number;
	}
	
	
}
