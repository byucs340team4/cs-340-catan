package shared.proxy.request;

import com.google.gson.JsonObject;

import gamemanager.player.ResourceList;

public class DiscardCards implements IRequest {
	int playerIndex;
	ResourceList discardedCards;
	String type;
	public DiscardCards(int playerIndex, ResourceList discarded) {
		super();
		this.type = "discardCards";
		this.playerIndex = playerIndex;
		this.discardedCards = discarded;
	}
	
	public JsonObject toJson(){
		JsonObject result = new JsonObject();
		result.addProperty("type", "discardCards");
		result.addProperty("playerIndex", this.playerIndex);
		result.add("discardedCards", discardedCards.toJson());
		return result;
	}

	@Override
	public String getType() {
		// TODO Auto-generated method stub
		return type;
	}
	
	@Override
	public int getPlayerIndex() {
		return playerIndex;
	}

	public void setPlayerIndex(int playerIndex) {
		this.playerIndex = playerIndex;
	}

	public ResourceList getDiscarded() {
		return discardedCards;
	}

	public void setDiscarded(ResourceList discarded) {
		this.discardedCards = discarded;
	}
}
