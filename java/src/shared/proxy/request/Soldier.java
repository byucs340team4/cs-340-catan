package shared.proxy.request;

import com.google.gson.JsonObject;

import shared.locations.HexLocation;

public class Soldier implements IRequest{
	int playerIndex;
	int victimIndex;
	HexLocation location;
	String type;
	
	public Soldier(int playerIndex, int victimIndex, HexLocation location) {
		super();
		this.type = "Soldier";
		this.playerIndex = playerIndex;
		this.victimIndex = victimIndex;
		this.location = location;
	}
	
	public JsonObject toJson(){
		JsonObject result = new JsonObject();
		result.addProperty("type", "Soldier");
		result.addProperty("playerIndex", this.playerIndex);
		result.addProperty("victimIndex", this.victimIndex);
		JsonObject location = new JsonObject();
		location.addProperty("x", this.location.getX());
		location.addProperty("y", this.location.getY());
		result.add("location", location);
		return result;
	}

	@Override
	public String getType() {
		// TODO Auto-generated method stub
		return type;
	}
	
	@Override
	public int getPlayerIndex() {
		return playerIndex;
	}

	public void setPlayerIndex(int playerIndex) {
		this.playerIndex = playerIndex;
	}

	public int getVictimIndex() {
		return victimIndex;
	}

	public void setVictimIndex(int victimIndex) {
		this.victimIndex = victimIndex;
	}

	public HexLocation getLocation() {
		return location;
	}

	public void setLocation(HexLocation location) {
		this.location = location;
	}
	
	
}
