package shared.proxy.request;

import com.google.gson.JsonObject;

import shared.locations.EdgeLocation;

public class BuildRoad implements IRequest {
	int playerIndex;
	EdgeLocation roadLocation;
	Boolean free;
	String type;
	public BuildRoad(int playerIndex, EdgeLocation location, Boolean free) {
		super();
		this.type = "buildRoad";
		this.playerIndex = playerIndex;
		this.roadLocation = location;
		this.free = free;
	}
	
	public JsonObject toJson(){
		JsonObject result = new JsonObject();
		result.addProperty("type", "buildRoad");
		result.addProperty("playerIndex", this.playerIndex);
		JsonObject roadLoc = new JsonObject();
		roadLoc.addProperty("x", roadLocation.getHexLoc().getX());
		roadLoc.addProperty("y", roadLocation.getHexLoc().getY());
		roadLoc.addProperty("direction", roadLocation.getDir().toString());
		result.add("roadLocation", roadLoc);
		result.addProperty("free", this.free);
		return result;
	}

	public void setPlayerIndex(int playerIndex) {
		this.playerIndex = playerIndex;
	}

	public EdgeLocation getLocation() {
		return roadLocation;
	}

	public void setLocation(EdgeLocation location) {
		this.roadLocation = location;
	}

	public Boolean getFree() {
		return free;
	}

	public void setFree(Boolean free) {
		this.free = free;
	}
	@Override
	public String getType() {
		// TODO Auto-generated method stub
		return type;
	}
	
	@Override
	public int getPlayerIndex() {
		return playerIndex;
	}
	
}
