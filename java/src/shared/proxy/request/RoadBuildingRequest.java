package shared.proxy.request;

import com.google.gson.JsonObject;

import shared.locations.EdgeLocation;
import shared.proxy.MoveType;

public class RoadBuildingRequest implements IRequest {
	private String type;
	private int playerIndex;
	private EdgeLocation spot1;
	private EdgeLocation spot2;
	public RoadBuildingRequest(MoveType type, int playerIndex,
			EdgeLocation spot1, EdgeLocation spot2) {
		super();
		this.type = "Road_Building";
		this.playerIndex = playerIndex;
		this.spot1 = spot1;
		this.spot2 = spot2;
	}
	
	public JsonObject toJson(){
		JsonObject result = new JsonObject();
		result.addProperty("type", "Road_Building");
		result.addProperty("playerIndex", this.playerIndex);
		JsonObject e1 = new JsonObject();
		e1.addProperty("x", spot1.getHexLoc().getX());
		e1.addProperty("y", spot1.getHexLoc().getY());
		e1.addProperty("direction", spot1.getDir().toString());
		JsonObject e2 = new JsonObject();
		e2.addProperty("x", spot2.getHexLoc().getX());
		e2.addProperty("y", spot2.getHexLoc().getY());
		e2.addProperty("direction", spot2.getDir().toString());
		result.add("spot1", e1);
		result.add("spot2", e2);
		System.out.println(result);
		return result;
	}

	@Override
	public String getType() {
		// TODO Auto-generated method stub
		return type;
	}
	
	@Override
	public int getPlayerIndex() {
		return playerIndex;
	}

	public void setPlayerIndex(int playerIndex) {
		this.playerIndex = playerIndex;
	}

	public EdgeLocation getSpot1() {
		return spot1;
	}

	public void setSpot1(EdgeLocation spot1) {
		this.spot1 = spot1;
	}

	public EdgeLocation getSpot2() {
		return spot2;
	}

	public void setSpot2(EdgeLocation spot2) {
		this.spot2 = spot2;
	}
	
	
}
