package shared.proxy.request;

import shared.proxy.MoveType;

import com.google.gson.JsonObject;

public class BuyDevCardRequest implements IRequest {

	private String type;
	private int playerIndex;
	
	public BuyDevCardRequest(MoveType type, int playerIndex) {
		super();
		this.type = "buyDevCard";
		this.playerIndex = playerIndex;
	}
	
	public JsonObject toJson(){
		JsonObject result = new JsonObject();
		result.addProperty("type", "buyDevCard");
		result.addProperty("playerIndex", this.playerIndex);
		return result;
	}

	public void setType(MoveType type) {
		this.type = type.toString();
	}


	public void setPlayerIndex(int playerIndex) {
		this.playerIndex = playerIndex;
	}
	
	@Override
	public String getType() {
		// TODO Auto-generated method stub
		return type;
	}
	
	@Override
	public int getPlayerIndex() {
		return playerIndex;
	}
	
	
}
