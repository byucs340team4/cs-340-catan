package shared.proxy.request;

import com.google.gson.JsonObject;

public class AcceptTrade implements IRequest {
	int playerIndex;
	Boolean willAccept;
	String type;
	
	public AcceptTrade(int playerIndex, Boolean willAccept) {
		super();
		type = "acceptTrade";
		this.playerIndex = playerIndex;
		this.willAccept = willAccept;
	}
	
	public JsonObject toJson(){
		JsonObject result = new JsonObject();
		result.addProperty("type", "acceptTrade");
		result.addProperty("playerIndex", this.playerIndex);
		result.addProperty("willAccept", this.willAccept);
		return result;
	}

	public void setPlayerIndex(int playerIndex) {
		this.playerIndex = playerIndex;
	}

	public Boolean getWillAccept() {
		return willAccept;
	}

	public void setWillAccept(Boolean willAccept) {
		this.willAccept = willAccept;
	}

	@Override
	public String getType() {
		// TODO Auto-generated method stub
		return type;
	}
	
	@Override
	public int getPlayerIndex() {
		return playerIndex;
	}
	
}
