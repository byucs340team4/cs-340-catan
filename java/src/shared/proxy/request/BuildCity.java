package shared.proxy.request;

import com.google.gson.JsonObject;

import shared.locations.VertexLocation;

public class BuildCity implements IRequest {
	int playerIndex;
	VertexLocation vertexLocation;
	String type;
	public BuildCity(int playerIndex, VertexLocation location) {
		super();
		this.type = "buildCity";
		this.playerIndex = playerIndex;
		this.vertexLocation = location;
	}
	
	public JsonObject toJson(){
		JsonObject result = new JsonObject();
		result.addProperty("type", "buildCity");
		result.addProperty("playerIndex", this.playerIndex);
		JsonObject vertexLoc = new JsonObject();
		vertexLoc.addProperty("x", vertexLocation.getHexLoc().getX());
		vertexLoc.addProperty("y", vertexLocation.getHexLoc().getY());
		vertexLoc.addProperty("direction", vertexLocation.getDir().toString());
		result.add("vertexLocation", vertexLoc);
		return result;
	}

	public void setPlayerIndex(int playerIndex) {
		this.playerIndex = playerIndex;
	}

	public VertexLocation getLocation() {
		return vertexLocation;
	}

	public void setLocation(VertexLocation location) {
		this.vertexLocation = location;
	}
	
	@Override
	public String getType() {
		// TODO Auto-generated method stub
		return type;
	}
	
	@Override
	public int getPlayerIndex() {
		return playerIndex;
	}
}
