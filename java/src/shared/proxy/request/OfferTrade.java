package shared.proxy.request;

import gamemanager.player.ResourceList;

import com.google.gson.JsonObject;

public class OfferTrade implements IRequest {
	int playerIndex;
	ResourceList offer;
	int receiver;
	String type;
	
	public OfferTrade(int playerIndex, ResourceList offer, int receiver) {
		super();
		this.type = "offerTrade";
		this.playerIndex = playerIndex;
		this.offer = offer;
		this.receiver = receiver;
	}
	
	public OfferTrade(){
		offer = new ResourceList();
	}
	
	public JsonObject toJson(){
		JsonObject result = new JsonObject();
		result.addProperty("type", "offerTrade");
		result.addProperty("playerIndex", this.playerIndex);
		JsonObject offer = this.offer.toJson();
		result.add("offer", offer);
		result.addProperty("receiver", this.receiver);
		return result;
	}

	@Override
	public String getType() {
		// TODO Auto-generated method stub
		return type;
	}
	
	@Override
	public int getPlayerIndex() {
		return playerIndex;
	}

	public void setPlayerIndex(int playerIndex) {
		this.playerIndex = playerIndex;
	}

	public ResourceList getOffer() {
		return offer;
	}

	public void setOffer(ResourceList offer) {
		this.offer = offer;
	}

	public int getReceiver() {
		return receiver;
	}

	public void setReceiver(int receiver) {
		this.receiver = receiver;
	}
}
