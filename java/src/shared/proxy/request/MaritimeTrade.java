package shared.proxy.request;

import com.google.gson.JsonObject;

public class MaritimeTrade implements IRequest {
	int playerIndex;
	int ratio;
	String inputResource;
	String outputResource;
	String type;
	public MaritimeTrade(int playerIndex, int ratio, String inputResources,
			String outputResources) {
		super();
		type = "maritimeTrade";
		this.playerIndex = playerIndex;
		this.ratio = ratio;
		this.inputResource = inputResources;
		this.outputResource = outputResources;
	}
	
	public JsonObject toJson(){
		JsonObject result = new JsonObject();
		result.addProperty("type", "maritimeTrade");
		result.addProperty("playerIndex", this.playerIndex);
		if(ratio!=0)
			result.addProperty("ratio", ratio);
		if(inputResource!=null)
			result.addProperty("inputResource", inputResource.toLowerCase());
		if(outputResource!=null)
			result.addProperty("outputResource", outputResource.toLowerCase());
		return result;
	}
	@Override
	public String getType() {
		// TODO Auto-generated method stub
		return type;
	}
	
	@Override
	public int getPlayerIndex() {
		return playerIndex;
	}

	public void setPlayerIndex(int playerIndex) {
		this.playerIndex = playerIndex;
	}

	public int getRatio() {
		return ratio;
	}

	public void setRatio(int ratio) {
		this.ratio = ratio;
	}

	public String getInputResources() {
		return inputResource;
	}

	public void setInputResources(String inputResources) {
		this.inputResource = inputResources;
	}

	public String getOutputResources() {
		return outputResource;
	}

	public void setOutputResources(String outputResources) {
		this.outputResource = outputResources;
	}
	
	
}
