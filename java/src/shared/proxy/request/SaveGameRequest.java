package shared.proxy.request;

public class SaveGameRequest implements IRequest {
	int id;
	String name;
	public SaveGameRequest(String string, int index) {
		this.name = string;
		this.id = index;
	}
	@Override
	public String getType() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public int getPlayerIndex() {
		// TODO Auto-generated method stub
		return 0;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	
}
