package shared.proxy.request;

public interface IRequest {
	public String getType();
	public int getPlayerIndex();
	
}
