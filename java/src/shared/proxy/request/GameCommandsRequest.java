package shared.proxy.request;

import java.util.List;

import shared.proxy.Move;

public class GameCommandsRequest implements IRequest {
	List<Move> commands;
	@Override
	public String getType() {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public int getPlayerIndex() {
		return 0;
	}
	
	public void setCommands (List<Move> commands){
		this.commands = commands;
	}
	
	public List<Move> getCommands (){
		return this.commands;
	}
}
