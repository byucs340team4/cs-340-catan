package shared.proxy.request;

import com.google.gson.JsonObject;

import shared.definitions.CatanColor;
import shared.definitions.ResourceType;
import shared.proxy.MoveType;

public class YearOfPlentyRequest implements IRequest{
	
	private String type;
	private int playerIndex;
	private String resource1;
	private String resource2;
	CatanColor color = CatanColor.BLUE;
	
	public YearOfPlentyRequest(MoveType type, int playerIndex,
			ResourceType resource1, ResourceType resource2) {
		super();
		this.type = "Year_of_Plenty";
		this.playerIndex = playerIndex;
		this.resource1 = resource1.toString();
		this.resource2 = resource2.toString();
	}
	
	public JsonObject toJson(){
		JsonObject result = new JsonObject();
		result.addProperty("type", "Year_of_Plenty");
		result.addProperty("playerIndex", this.playerIndex);
		result.addProperty("resource1", resource1.toString().toLowerCase());
		result.addProperty("resource2", resource2.toString().toLowerCase());
		return result;
	}

	@Override
	public String getType() {
		// TODO Auto-generated method stub
		return type;
	}
	
	@Override
	public int getPlayerIndex() {
		return playerIndex;
	}

	public void setPlayerIndex(int playerIndex) {
		this.playerIndex = playerIndex;
	}

	public String getResource1() {
		return resource1;
	}

	public void setResource1(String resource1) {
		this.resource1 = resource1;
	}

	public String getResource2() {
		return resource2;
	}

	public void setResource2(String resource2) {
		this.resource2 = resource2;
	}

	public CatanColor getColor() {
		return color;
	}

	public void setColor(CatanColor color) {
		this.color = color;
	}
	
	
}	
