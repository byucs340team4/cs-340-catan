package shared.proxy.request;

import com.google.gson.JsonObject;

public class Monument implements IRequest {
	int playerIndex;
	String type;

	public Monument(int playerIndex) {
		super();
		this.type = "Monument";
		this.playerIndex = playerIndex;
	}
	
	public JsonObject toJson(){
		JsonObject result = new JsonObject();
		result.addProperty("type", "Monument");
		result.addProperty("playerIndex", this.playerIndex);
		return result;
	}

	@Override
	public String getType() {
		// TODO Auto-generated method stub
		return type;
	}
	
	@Override
	public int getPlayerIndex() {
		return playerIndex;
	}

	public void setPlayerIndex(int playerIndex) {
		this.playerIndex = playerIndex;
	}
	
	
}
