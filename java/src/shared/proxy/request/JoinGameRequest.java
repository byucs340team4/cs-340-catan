package shared.proxy.request;

import com.google.gson.JsonObject;

public class JoinGameRequest implements IRequest {
	int id;
	String color;
	public JoinGameRequest(int id, String color) {
		super();
		this.id = id;
		this.color = color;
	}
	
	public JsonObject toJson(){
		JsonObject result = new JsonObject();
		result.addProperty("id", this.id);
		result.addProperty("color", this.color);
		return result;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}
	
	@Override
	public String getType() {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public int getPlayerIndex() {
		return 0;
	}
}
