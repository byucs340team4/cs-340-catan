package shared.proxy.request;

import com.google.gson.JsonObject;

public class AddAIRequest implements IRequest {
	String AIType;
	public AddAIRequest(String AIType){
		this.AIType = AIType;
	}
	public String getAIType(){
		return this.AIType;
	}
	
	public JsonObject toJson(){
		JsonObject result = new JsonObject();
		result.addProperty("AIType", this.AIType);
		return result;
	}
	
	@Override
	public String getType() {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public int getPlayerIndex() {
		return 0;
	}
}
