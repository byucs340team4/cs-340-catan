package shared.proxy.request;

import com.google.gson.JsonObject;

import shared.locations.VertexLocation;

public class BuildSettlement implements IRequest {
	int playerIndex;
	VertexLocation vertexLocation;
	Boolean free;
	String type;
	
	public BuildSettlement(int playerIndex, VertexLocation location,
			Boolean free) {
		super();
		this.type = "buildSettlement";
		this.playerIndex = playerIndex;
		this.vertexLocation = location;
		this.free = free;
	}
	
	public JsonObject toJson(){
		JsonObject result = new JsonObject();
		result.addProperty("type", "buildSettlement");
		result.addProperty("playerIndex", this.playerIndex);
		JsonObject vertexLoc = new JsonObject();
		vertexLoc.addProperty("x", vertexLocation.getHexLoc().getX());
		vertexLoc.addProperty("y", vertexLocation.getHexLoc().getY());
		vertexLoc.addProperty("direction", vertexLocation.getDir().toString());
		result.add("vertexLocation", vertexLoc);
		result.addProperty("free", this.free);
		return result;
	}

	public void setPlayerIndex(int playerIndex) {
		this.playerIndex = playerIndex;
	}

	public VertexLocation getLocation() {
		return vertexLocation;
	}

	public void setLocation(VertexLocation location) {
		this.vertexLocation = location;
	}

	public Boolean getFree() {
		return free;
	}

	public void setFree(Boolean free) {
		this.free = free;
	}
	@Override
	public String getType() {
		// TODO Auto-generated method stub
		return type;
	}
	
	@Override
	public int getPlayerIndex() {
		return playerIndex;
	}
	
}
