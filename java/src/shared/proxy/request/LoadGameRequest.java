package shared.proxy.request;

public class LoadGameRequest implements IRequest {
	String name;
	
	public LoadGameRequest(String string) {
		name = string;
	}

	@Override
	public String getType() {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public int getPlayerIndex() {
		return -1;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	
}
