package shared.proxy.request;

import com.google.gson.JsonObject;

import shared.proxy.MoveType;

public class FinishTurnRequest implements IRequest {

	private String type;
	private int playerIndex;
	
	public FinishTurnRequest(MoveType type, int playerIndex) {
		super();
		this.type = "finishTurn";
		this.playerIndex = playerIndex;
	}
	
	public FinishTurnRequest(int i) {
		// TODO Auto-generated constructor stub
	}

	public JsonObject toJson(){
		JsonObject result = new JsonObject();
		result.addProperty("type", "finishTurn");
		result.addProperty("playerIndex", this.playerIndex);
		return result;
	}

	@Override
	public String getType() {
		// TODO Auto-generated method stub
		return type;
	}
	
	@Override
	public int getPlayerIndex() {
		return playerIndex;
	}

	public void setPlayerIndex(int playerIndex) {
		this.playerIndex = playerIndex;
	}
	
}
