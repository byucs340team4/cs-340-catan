package shared.proxy;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import gamemanager.game.Game;
import shared.proxy.request.AcceptTrade;
import shared.proxy.request.AddAIRequest;
import shared.proxy.request.BuildCity;
import shared.proxy.request.BuildRoad;
import shared.proxy.request.BuildSettlement;
import shared.proxy.request.BuyDevCardRequest;
import shared.proxy.request.CreateGameRequests;
import shared.proxy.request.Credentials;
import shared.proxy.request.DiscardCards;
import shared.proxy.request.FinishTurnRequest;
import shared.proxy.request.GameCommandsRequest;
import shared.proxy.request.JoinGameRequest;
import shared.proxy.request.ListAIRequest;
import shared.proxy.request.ListGamesRequest;
import shared.proxy.request.LoadGameRequest;
import shared.proxy.request.MaritimeTrade;
import shared.proxy.request.Monopoly;
import shared.proxy.request.Monument;
import shared.proxy.request.OfferTrade;
import shared.proxy.request.RoadBuildingRequest;
import shared.proxy.request.RobPlayerRequest;
import shared.proxy.request.RollNumberRequest;
import shared.proxy.request.SaveGameRequest;
import shared.proxy.request.SendChatRequest;
import shared.proxy.request.Soldier;
import shared.proxy.request.YearOfPlentyRequest;
import shared.proxy.response.AddAIResponse;
import shared.proxy.response.CreateGameResponse;
import shared.proxy.response.GamesListResponse;
import shared.proxy.response.JoinGameResponse;
import shared.proxy.response.ListAIResponse;
import shared.proxy.response.LoadGameResponse;
import shared.proxy.response.LoginResponse;
import shared.proxy.response.SaveGameResponse;

public class ProxyFake implements IProxy {

	Game game;
	public ProxyFake(){
		
	}
	
	public ProxyFake(String file)  {
		Gson gson = new Gson();
		BufferedReader reader = null;
		try {
			reader = new BufferedReader(new FileReader(file));
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		String line = null;
		StringBuilder stringBuilder = new StringBuilder();
		String ls = System.getProperty("line.separator");

		try {
			while ((line = reader.readLine()) != null) {
				stringBuilder.append(line);
				stringBuilder.append(ls);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		String temp = stringBuilder.toString();
		JsonObject json = gson.fromJson(temp, JsonObject.class);
		game = Deserializer.getGame(json);
	}
	
	@Override
	public LoginResponse userLogin(Credentials cred) {
		return null;
	}

	@Override
	public LoginResponse userRegister(Credentials cred) {
		return new LoginResponse(true,"Sam", "sam", 0);
	}

	@Override
	public GamesListResponse gamesList(ListGamesRequest request) {
		return null;
	}

	@Override
	public CreateGameResponse gamesCreate(CreateGameRequests request) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public JoinGameResponse gamesJoin(JoinGameRequest request, boolean createGame) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public SaveGameResponse gamesSave(SaveGameRequest request) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public LoadGameResponse gamesLoad(LoadGameRequest request) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Game gameModel() {
		return game;
	}

	@Override
	public Game gameReset() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Game gameCommands(GameCommandsRequest request) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public AddAIResponse addAI(AddAIRequest request) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ListAIResponse listAI(ListAIRequest request) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Game sendChat(SendChatRequest request) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Game rollNumber(RollNumberRequest request) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Game robPlayer(RobPlayerRequest request) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Game finishTurn(FinishTurnRequest request) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Game buyDevCard(BuyDevCardRequest request) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Game yearOfPlenty(YearOfPlentyRequest request) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Game roadBuilding(RoadBuildingRequest request) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Game soldier(Soldier soldier) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Game monopoly(Monopoly monopoly) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Game monument(Monument monument) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Game buildRoad(BuildRoad buildRoad) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Game buildSettlement(BuildSettlement buildSettlement) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Game buildCity(BuildCity buildCity) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Game offerTrade(OfferTrade offer) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Game acceptTrade(AcceptTrade accept) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Game maritimeTrade(MaritimeTrade trade) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Game discardCards(DiscardCards discard) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void changeLogLevel(SetLogLevelRequest request) {
		// TODO Auto-generated method stub

	}

}
