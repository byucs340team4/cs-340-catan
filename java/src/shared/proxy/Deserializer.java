package shared.proxy;

import java.util.ArrayList;

import shared.locations.HexLocation;
import shared.locations.VertexObject;
import gamemanager.map.Hex;
import gamemanager.map.Port;
import gamemanager.map.Road;
import gamemanager.communication.ChatLog;
import gamemanager.communication.GameLog;
import gamemanager.communication.Message;
import gamemanager.communication.TurnTracker;
import gamemanager.game.Deck;
import gamemanager.game.DevCardList;
import gamemanager.game.Game;
import gamemanager.map.Map;
import gamemanager.player.Player;
import gamemanager.player.ResourceList;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import cs340.model.player.TradeOffer;

public class Deserializer {
	
	public static Game getGame(JsonObject json) {
		Gson gson  = new Gson();
		ResourceList bank = getBank(gson.fromJson(json.get("bank"), JsonObject.class));
		GameLog log = getGameLog(gson.fromJson(json.get("log"), JsonObject.class));
		Player[] players = getPlayers(gson.fromJson(json.get("players"), JsonArray.class));
		ChatLog chat = getChatLog(gson.fromJson(json.get("chat"), JsonObject.class));
		Map map = getMap(gson.fromJson(json.get("map"), JsonObject.class));
		TradeOffer tradeOffer = gson.fromJson(json.get("tradeOffer"), TradeOffer.class);
		TurnTracker turnTracker = getTurnTracker(gson.fromJson(json.get("turnTracker"), JsonObject.class));
		Deck deck = new Deck(gson.fromJson(json.get("deck"),DevCardList.class));
		int winner = gson.fromJson(json.get("winner"), int.class);
		int version = gson.fromJson(json.get("version"), int.class);
		Game game = new Game(bank,log,players,chat,map,turnTracker,deck,version,winner,tradeOffer);
		game.setPlayersGame();
		return game;
	}
	
	
	private static TurnTracker getTurnTracker(JsonObject fromJson) {
		Gson gson = new Gson();
		TurnTracker turnTracker = new TurnTracker();
		int biggestArmy = gson.fromJson(fromJson.get("largestArmy"),int.class);
		int longestRoad = gson.fromJson(fromJson.get("longestRoad"), int.class);
		turnTracker.setLargestArmy(biggestArmy);
		turnTracker.setLongestRoad(longestRoad);
		turnTracker.setCurrentTurn(gson.fromJson(fromJson.get("currentTurn"),int.class));
		turnTracker.setStatus(gson.fromJson(fromJson.get("status"), String.class));
		return turnTracker;
	}

	private static GameLog getGameLog(JsonObject fromJson) {
		Gson gson = new Gson();
		JsonArray json = gson.fromJson(fromJson.get("lines"), JsonArray.class);
		GameLog log = new GameLog();
		for (JsonElement elem : json) {
			Message message = gson.fromJson(elem, Message.class);
			log.addMessage(message);
		}
		return log;
	}
	
	private static ChatLog getChatLog(JsonObject fromJson) {
		Gson gson = new Gson();
		JsonArray json = gson.fromJson(fromJson.get("lines"), JsonArray.class);
		ChatLog log = new ChatLog();
		for (JsonElement elem : json) {
			Message message = gson.fromJson(elem, Message.class);
			log.addMessage(message);
		}
		return log;
	}

	public static Deck getDeck(JsonObject json) {
		Deck deck = null;
		return deck;
	}
	
	public static Player[] getPlayers(JsonArray jsonArray) {
		Gson gson = new Gson();
		Player[] players = gson.fromJson(jsonArray, Player[].class);
//		Player[] players = new Player[4];
//		int i = 0;
//		for (JsonElement json : jsonArray) {
//			Player player = getPlayer(json);
//			players[i] = player;
//			i++;
//		}
		return players;
	}
	
	public static Player getPlayer(JsonElement json) {
		Gson gson = new Gson();
		JsonObject obj = json.getAsJsonObject();
		Player player = gson.fromJson(obj, Player.class);
		return player;
	}
	
	public static ResourceList getBank(JsonObject json) {
		Gson gson = new Gson();
		ResourceList bank = gson.fromJson(json, ResourceList.class);
		return bank;
	}
	
	public static Map getMap(JsonObject json){
		
		Map map = new Map();
		
		Gson gson = new Gson();
		
		ArrayList<Hex>hexes = getHexes(gson.fromJson(json.getAsJsonArray("hexes"), JsonArray.class));
		map.setHexes(hexes);
		ArrayList<Road>roads = getRoads(gson.fromJson(json.getAsJsonArray("roads"),JsonArray.class));
		map.setRoads(roads);
		ArrayList<VertexObject>cities = getVertexObjects(gson.fromJson(json.getAsJsonArray("cities"), JsonArray.class));
		map.setCities(cities);
		ArrayList<VertexObject>settlements = getVertexObjects(gson.fromJson(json.getAsJsonArray("settlements"), JsonArray.class));
		map.setSettlements(settlements);
		ArrayList<Port>ports = getPorts(gson.fromJson(json.getAsJsonArray("ports"), JsonArray.class));
		map.setPorts(ports);
		int radius = gson.fromJson(json.get("radius"),int.class);
		map.setRadius(radius);
		HexLocation robber = gson.fromJson(json.get("robber"),HexLocation.class);
		map.setRobber(robber);
		return map;

	}

	private static ArrayList<Port> getPorts(JsonArray fromJson) {
		// TODO Auto-generated method stub
		JsonArray json = fromJson;
		Gson gson = new Gson();
		ArrayList<Port> ports = new ArrayList<Port>();
		for (JsonElement elem : json) {
			Port port = gson.fromJson(elem, Port.class);
			ports.add(port);
		}
		return ports;
	}

	private static ArrayList<VertexObject> getVertexObjects(JsonArray fromJson) {
		// TODO Auto-generated method stub
		JsonArray json = fromJson;
		Gson gson = new Gson();
		ArrayList<VertexObject> buildings = new ArrayList<VertexObject>();
		for (JsonElement elem : json) {
			VertexObject building = gson.fromJson(elem, VertexObject.class);
			buildings.add(building);
		}
		return buildings;	
	}

	private static ArrayList<Road> getRoads(JsonArray fromJson) {
		// TODO Auto-generated method stub
		JsonArray json = fromJson;
		Gson gson = new Gson();
		ArrayList<Road> roads = new ArrayList<Road>();
		for (JsonElement elem : json) {
			Road road = gson.fromJson(elem, Road.class);
			roads.add(road);
		}
		
		return roads;
	}

	private static ArrayList<Hex> getHexes(JsonArray fromJson) {
		JsonArray json = fromJson;
		Gson gson = new Gson();
		ArrayList<Hex> hexes = new ArrayList<Hex>();
		for (JsonElement elem : json) {
			Hex hex = gson.fromJson(elem, Hex.class);
			hexes.add(hex);
		}

		return hexes;
	}
	
	
}
