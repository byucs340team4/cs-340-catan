package shared.proxy;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.URL;

//import com.google.gson.Gson;

/**
 * Communicates between the client and the server.
 * @author Seidl
 */
public class ProxyCommunicator {
	
	//Auxiliary Constants, Attributes, and Methods
	private static String SERVER_HOST;
	private static int SERVER_PORT = 8081;
	private static String URL_PREFIX;
	private static final String HTTP_GET = "GET";
	private static final String HTTP_POST = "POST";
	
	//Setters for the server information.
	public static void setSERVER_HOST(String ServerHostNumber){
		SERVER_HOST = ServerHostNumber;
	}
	public static void setSERVER_PORT(int ServerPortNumber){
		SERVER_PORT = ServerPortNumber;
	}
	public static void setURL_PREFIX(){
		URL_PREFIX = "http://" + SERVER_HOST + ":" + SERVER_PORT;
	}
	
	public static String getURL_PREFIX(){
		return URL_PREFIX;
	}
	
	//Singleton Instance
	private static ProxyCommunicator singleton = null; 

	public static ProxyCommunicator getSingleton() {
		if(singleton == null) {
			singleton = new ProxyCommunicator();
		}
		return singleton;
	}
//	private Gson gson;
	private String userCookie;
	private String gameCookie;
	
	private ProxyCommunicator() {
//		gson = new Gson();
	}
	
	@SuppressWarnings("finally")
	public HttpURLResponse doGet(String commandName) throws IOException{
		HttpURLResponse result = new HttpURLResponse();
		try {
			URL url = new URL(URL_PREFIX+'/'+commandName);
			HttpURLConnection connection = (HttpURLConnection)url.openConnection();
			connection.setRequestMethod(HTTP_GET);
			connection.setDoOutput(true);
			if(userCookie!=null){
				String cookie = userCookie;
				if(gameCookie!=null){
					cookie+="; "+gameCookie;
				}
				connection.setRequestProperty("Cookie", cookie);
			}
			connection.connect();
			OutputStreamWriter wr = new OutputStreamWriter(connection.getOutputStream());
			wr.write("Get request");
			wr.flush();
			connection.getOutputStream().close();
			
			result.setResponseCode(connection.getResponseCode());
			result.setResponseLength(connection.getContentLength());
			
			if(result.getResponseCode()==HttpURLConnection.HTTP_OK){
				if(connection.getContentLength()!=-1){
					result.setHeaders(connection.getHeaderFields());
					result.setResponseBody(connection.getContent());
				}
			}else{
				result.setResponseBody(connection.getContent());
			}
			return result;
		} catch (ProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
			throw e;
		}finally{
			return result;
		}
	}
	
	public HttpURLResponse doPost(String commandName, Object postData) throws IOException{
		
		HttpURLResponse result = new HttpURLResponse();
		try {
			URL url = new URL(URL_PREFIX+'/' + commandName);
			HttpURLConnection connection = (HttpURLConnection)url.openConnection();
			connection.setRequestMethod(HTTP_POST);
			connection.setDoOutput(true);
			if(userCookie!=null){
				String cookie = userCookie;
				if(gameCookie!=null){
					cookie+="; "+gameCookie;
				}
				connection.setRequestProperty("Cookie", cookie);
			}
			connection.connect();
			OutputStreamWriter wr = new OutputStreamWriter(connection.getOutputStream());
			wr.write(postData.toString());
			wr.flush();
			connection.getOutputStream().close();
			
			result.setResponseCode(connection.getResponseCode());
			result.setResponseLength(connection.getContentLength());
			
			if (connection.getResponseCode() == HttpURLConnection.HTTP_OK) {
				if(connection.getContentLength() != -1) {
					result.setHeaders(connection.getHeaderFields());
					result.setResponseBody(connection.getContent());
				}
			} else {
				result.setResponseBody(connection.getContent());
			}
		}
		catch (IOException e) {
			//e.printStackTrace();
			throw e;
		}
		return result;
	}
	
	public String getUserCookie() {
		return userCookie;
	}
	public void setUserCookie(String userCookie) {
		this.userCookie = userCookie;
	}
	public String getGameCookie() {
		return gameCookie;
	}
	public void setGameCookie(String gameCookie) {
		this.gameCookie = gameCookie;
	}
	public String getBody(InputStream response) {
		InputStreamReader isReader = new InputStreamReader(response);
		BufferedReader br = new BufferedReader(isReader);
		String result = "";
		String line;
		try {
			while ((line = br.readLine()) != null) {
				result += line;
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	}
	
	
}
