package shared.proxy;

import shared.proxy.request.AcceptTrade;
import shared.proxy.request.AddAIRequest;
import shared.proxy.request.BuildCity;
import shared.proxy.request.BuildRoad;
import shared.proxy.request.BuildSettlement;
import shared.proxy.request.BuyDevCardRequest;
import shared.proxy.request.CreateGameRequests;
import shared.proxy.request.Credentials;
import shared.proxy.request.DiscardCards;
import shared.proxy.request.FinishTurnRequest;
import shared.proxy.request.GameCommandsRequest;
import shared.proxy.request.JoinGameRequest;
import shared.proxy.request.ListAIRequest;
import shared.proxy.request.ListGamesRequest;
import shared.proxy.request.LoadGameRequest;
import shared.proxy.request.MaritimeTrade;
import shared.proxy.request.Monopoly;
import shared.proxy.request.Monument;
import shared.proxy.request.OfferTrade;
import shared.proxy.request.RoadBuildingRequest;
import shared.proxy.request.RobPlayerRequest;
import shared.proxy.request.RollNumberRequest;
import shared.proxy.request.SaveGameRequest;
import shared.proxy.request.SendChatRequest;
import shared.proxy.request.Soldier;
import shared.proxy.request.YearOfPlentyRequest;
import shared.proxy.response.AddAIResponse;
import shared.proxy.response.CreateGameResponse;
import shared.proxy.response.GamesListResponse;
import shared.proxy.response.JoinGameResponse;
import shared.proxy.response.ListAIResponse;
import shared.proxy.response.LoadGameResponse;
import shared.proxy.response.LoginResponse;
import shared.proxy.response.SaveGameResponse;
import gamemanager.game.Game;

public interface IProxy {

	
	/**
	 * 
	 * @param cred
	 * @return
	 */
	public LoginResponse userLogin(Credentials cred);
	
	/**
	 * 
	 * @param cred
	 * @return
	 */
	public LoginResponse userRegister(Credentials cred);
	
	/**
	 * 
	 * @param request
	 * @return
	 */
	
	public GamesListResponse gamesList(ListGamesRequest request);
	
	/**
	 * 
	 * @param request
	 * @return
	 */
	public CreateGameResponse gamesCreate(CreateGameRequests request);
	
	/**
	 * 
	 * @param request
	 * @return
	 */
	public JoinGameResponse gamesJoin(JoinGameRequest request, boolean createGame);
	 
	/**
	 * 
	 * @param request
	 * @return
	 */
	public SaveGameResponse gamesSave(SaveGameRequest request);
	
	/**
	 * 
	 * @param request
	 * @return
	 */
	public LoadGameResponse gamesLoad(LoadGameRequest request);
	
	/**
	 * 
	 * @return
	 */
	public Game gameModel();
	
	/**
	 * 
	 * @return
	 */
	public Game gameReset();
	
	/**
	 * 
	 * @return
	 */
	public Game gameCommands(GameCommandsRequest request);
	
	/**
	 * 
	 * @param request
	 * @return
	 */
	public AddAIResponse addAI(AddAIRequest request);
	
	/**
	 * 
	 * @param request
	 * @return
	 */
	public ListAIResponse listAI(ListAIRequest request);
	
	/**
	 * 
	 * @param request
	 * @return
	 */
	public Game sendChat(SendChatRequest request);
	
	/**
	 * 
	 * @param request
	 * @return
	 */
	public Game rollNumber(RollNumberRequest request);
	
	/**
	 * 
	 * @param request
	 * @return
	 */
	public Game robPlayer(RobPlayerRequest request);
	
	/**
	 * 
	 * @param request
	 * @return
	 */
	public Game finishTurn(FinishTurnRequest request);
	
	/**
	 * 
	 * @param request
	 * @return
	 */
	public Game buyDevCard(BuyDevCardRequest request);
	
	/**
	 * 
	 * @param request
	 * @return
	 */
	public Game yearOfPlenty(YearOfPlentyRequest request);
	
	/**
	 * 
	 * @param request
	 * @return
	 */
	public Game roadBuilding(RoadBuildingRequest request);
	
	/**
	 * 
	 * @param soldier
	 * @return
	 */
	public Game soldier(Soldier soldier);
	
	/**
	 * 
	 * @param monopoly
	 * @return
	 */
	public Game monopoly(Monopoly monopoly);
	
	/**
	 * 
	 * @param monument
	 * @return
	 */
	public Game monument(Monument monument);
	
	/**
	 * 
	 * @param buildRoad
	 * @return
	 */
	public Game buildRoad(BuildRoad buildRoad);
	
	/**
	 * 
	 * @param buildSettlement
	 * @return
	 */
	public Game buildSettlement(BuildSettlement buildSettlement);
	
	/**
	 * 
	 * @param buildCity
	 * @return
	 */
	public Game buildCity(BuildCity buildCity);
	
	/**
	 * 
	 * @param offer
	 * @return
	 */
	public Game offerTrade(OfferTrade offer);
	
	/**
	 * 
	 * @param accept
	 * @return
	 */
	public Game acceptTrade(AcceptTrade accept);
	
	/**
	 * 
	 * @param trade
	 * @return
	 */
	public Game  maritimeTrade(MaritimeTrade trade);
	
	/**
	 * 
	 * @param discard
	 * @return
	 */
	public Game discardCards(DiscardCards discard);
	
	/**
	 * 
	 * @param request
	 * @return
	 */
	public void changeLogLevel(SetLogLevelRequest request);
}
