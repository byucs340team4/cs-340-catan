package shared.proxy.response;

import java.io.IOException;
import java.net.HttpURLConnection;

import com.sun.net.httpserver.HttpExchange;

import gamemanager.game.Game;

public class ResetGameResponse implements IResponse {
	Game game;

	@Override
	public void addHeaders(HttpExchange arg0) throws IOException {
		arg0.getResponseHeaders().set("Content-Type", "application/json");
		arg0.sendResponseHeaders(HttpURLConnection.HTTP_OK, 0);
	}

	@Override
	public void setFailHeaders(HttpExchange arg0) throws IOException {
		// TODO Auto-generated method stub
		
	}
}
