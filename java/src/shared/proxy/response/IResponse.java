package shared.proxy.response;

import java.io.IOException;
import java.net.HttpURLConnection;

import com.sun.net.httpserver.HttpExchange;

public interface IResponse {

	public void addHeaders(HttpExchange arg0) throws IOException;
	
	public void setFailHeaders(HttpExchange arg0) throws IOException;
}
