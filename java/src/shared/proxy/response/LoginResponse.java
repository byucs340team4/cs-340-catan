package shared.proxy.response;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URLEncoder;

import com.google.gson.JsonObject;
import com.sun.net.httpserver.HttpExchange;

/**
 * 
 * @author Wesley
 *
 */
public class LoginResponse implements IResponse{
	boolean response;
	String body;
	String username;
	String password;
	int playerID;
	
	public LoginResponse(boolean value, String username, String password, int playerID){
		response = value;
		if(response)
			body = "Success";
		else
			body = "Unable to login";
		this.username = username;
		this.password = password;
		this.playerID = playerID;
	}

	public LoginResponse(boolean b) {
		response = b;
		if(response)
			body = "Success";
		else
			body = "Unable to login";
		body = "";
		username = "";
		password = "";
		playerID = -1;
	}
	
	public JsonObject toJson(){
		JsonObject json = new JsonObject();
		json.addProperty("name", this.getUsername());
		json.addProperty("password", this.getPassword());
		json.addProperty("playerID", this.getPlayerID());
		return json;
	}

	public boolean isResponse() {
		return response;
	}

	public void setResponse(boolean response) {
		this.response = response;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public int getPlayerID() {
		return playerID;
	}

	public void setPlayerID(int playerID) {
		this.playerID = playerID;
	}

	@Override
	public void addHeaders(HttpExchange arg0) throws IOException{
		JsonObject json = this.toJson();
		
		String response = "catan.user=";
		
		//Setting headers.
		arg0.getResponseHeaders().set("Content-Type", "text/html");
		arg0.getResponseHeaders().set("Access-Control-Allow-Origin", "*");
		//Sets the cookie header
		arg0.getResponseHeaders().set("Set-cookie", response+URLEncoder.encode(json.toString()) + ";Path=/;");
		arg0.sendResponseHeaders(HttpURLConnection.HTTP_OK, "Success".getBytes().length);
		
		arg0.getResponseBody().write("Success".getBytes());
		arg0.getResponseBody().flush();
		//xstream.toXML(t, exchange.getResponseBody());
		arg0.getResponseBody().close();
	}

	@Override
	public void setFailHeaders(HttpExchange arg0) throws IOException {
		arg0.getResponseHeaders().set("Content-Type", "text/html");
		arg0.sendResponseHeaders(HttpURLConnection.HTTP_BAD_REQUEST, "Error".getBytes().length);
		arg0.getResponseBody().write("Error".getBytes());
		arg0.getResponseBody().flush();
		arg0.getResponseBody().close();
	}
}
