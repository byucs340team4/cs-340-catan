package shared.proxy.response;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.List;

import shared.proxy.request.IRequest;
//import client.data.GameInfo;

import com.google.gson.Gson;
import com.sun.net.httpserver.HttpExchange;

//import gamemanager.game.Game;

public class GameCommandsResponse implements IResponse {
	List<IRequest> commands;
	
	
	public GameCommandsResponse(){
		
	}
	
	public void init(){
		commands = new ArrayList<IRequest>();
	}
	
	public List<IRequest> getCommands() {
		return commands;
	}

	public void setCommands(List<IRequest> commands) {
		this.commands = commands;
	}

	@Override
	public void addHeaders(HttpExchange arg0) throws IOException {
		System.out.println("addHeaders for games command executed");
		Gson g = new Gson();
		String obj = g.toJson(commands);
		arg0.getResponseHeaders().set("Content-Type", "application/json");
		arg0.sendResponseHeaders(HttpURLConnection.HTTP_OK, obj.length());
		arg0.getResponseBody().write(obj.getBytes());
	}

	@Override
	public void setFailHeaders(HttpExchange arg0) throws IOException {
		// TODO Auto-generated method stub
		
	}
}	
