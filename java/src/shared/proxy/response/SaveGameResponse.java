package shared.proxy.response;

import java.io.IOException;
import java.net.HttpURLConnection;

import com.google.gson.Gson;
import com.sun.net.httpserver.HttpExchange;

public class SaveGameResponse implements IResponse {

	private boolean successful;
	
	public SaveGameResponse(boolean successful) {
		this.successful = successful;
	}

	public SaveGameResponse() {
		successful = false;
	}
	
	@Override
	public void addHeaders(HttpExchange arg0) throws IOException {
		Gson g = new Gson();
		if(successful) {
			arg0.getResponseHeaders().set("Content-Type", "application/json");
			arg0.sendResponseHeaders(HttpURLConnection.HTTP_OK, 0);
		}
		else {
			arg0.getResponseHeaders().set("Content-Type", "application/json");
			arg0.sendResponseHeaders(HttpURLConnection.HTTP_BAD_REQUEST, 0);
		}
	}

	@Override
	public void setFailHeaders(HttpExchange arg0) throws IOException {
		// TODO Auto-generated method stub
		
	}
	
	public void setSuccessful(boolean successful){
		this.successful = successful;
	}

	public boolean getSuccess() {
		return successful;
	}

}
