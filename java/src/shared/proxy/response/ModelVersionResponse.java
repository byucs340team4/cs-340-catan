package shared.proxy.response;

import java.io.IOException;
import java.net.HttpURLConnection;

import com.google.gson.Gson;
import com.sun.net.httpserver.HttpExchange;

import gamemanager.game.Game;

public class ModelVersionResponse implements IResponse {
	Game game;
	boolean same;
	
	public ModelVersionResponse(Game game){
		this.game = game;
		same = false;
	}
	
	public ModelVersionResponse() {
		same = true;
		game = null;
	}
	
	public Game getGame(){
		return game;
	}

	@Override
	public void addHeaders(HttpExchange arg0) throws IOException {
		Gson g = new Gson();
		if(!same) {
			String obj = g.toJson(this.getGame());
			arg0.getResponseHeaders().set("Content-Type", "application/json");
			arg0.sendResponseHeaders(HttpURLConnection.HTTP_OK, obj.length());
			arg0.getResponseBody().write(obj.getBytes());
		}
		else {
			arg0.getResponseHeaders().set("Content-Type", "application/json");
			arg0.sendResponseHeaders(HttpURLConnection.HTTP_OK, 0);
		}
	}

	@Override
	public void setFailHeaders(HttpExchange arg0) throws IOException {
		// TODO Auto-generated method stub
		
	}
}