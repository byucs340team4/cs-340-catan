package shared.proxy.response;

import java.io.IOException;
import java.net.HttpURLConnection;

import com.google.gson.Gson;
import com.sun.net.httpserver.HttpExchange;

public class JoinGameResponse implements IResponse {
	
	private boolean success;
	private int gameId;
	
	public JoinGameResponse(){
		success = false;
		gameId = -1;
	}
	
	public JoinGameResponse(boolean success){
		this.success = success;
	}
	
	public JoinGameResponse(boolean success,int gameId){
		this.success = success;
		this.gameId = gameId;
	}
	
	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}
	
	public int getGameId(){
		return gameId;
	}
	
	public void setGameId(int gameId){
		this.gameId = gameId;
	}

	@Override
	public void addHeaders(HttpExchange arg0) throws IOException {
		System.out.println("JoinGameAddHeaders");
		if(this.isSuccess()){
			String cookieAppend = "catan.game=" + this.getGameId();		
			arg0.getResponseHeaders().set("Content-Type", "text/html");
			arg0.getResponseHeaders().add("Set-cookie",(cookieAppend + ";Path=/;"));
			arg0.sendResponseHeaders(HttpURLConnection.HTTP_OK, "Success".getBytes().length);
			arg0.getResponseBody().write("Success".getBytes());
		}else
			this.setFailHeaders(arg0);
	}

	@Override
	public void setFailHeaders(HttpExchange arg0) throws IOException {
		arg0.getRequestHeaders().set("Content-Type", "text/html");
		arg0.sendResponseHeaders(HttpURLConnection.HTTP_BAD_REQUEST, "Error".getBytes().length);
		arg0.getResponseBody().write("Error".getBytes());
	}
	
}
