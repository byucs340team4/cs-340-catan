package shared.proxy.response;

import java.io.IOException;

import com.sun.net.httpserver.HttpExchange;

import gamemanager.game.Game;

public class MoveResponse implements IResponse{
	Game model;
	
	public MoveResponse(Game game){
		model = game;
	}
	
	public Game getGame(){
		return model;
	}

	@Override
	public void addHeaders(HttpExchange arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setFailHeaders(HttpExchange arg0) throws IOException {
		// TODO Auto-generated method stub
		
	}
}
