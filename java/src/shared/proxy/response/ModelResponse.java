package shared.proxy.response;

import java.io.IOException;
import java.net.HttpURLConnection;

import com.google.gson.Gson;
import com.sun.net.httpserver.HttpExchange;

import gamemanager.game.Game;

public class ModelResponse implements IResponse {
	Game game;
	
	public ModelResponse(Game game){
		this.game = game;
	}
	
	public Game getGame(){
		return game;
	}

	@Override
	public void addHeaders(HttpExchange arg0) throws IOException {
		Gson g = new Gson();
		String obj = g.toJson(this.getGame());
		arg0.getResponseHeaders().set("Content-Type", "application/json");
		arg0.sendResponseHeaders(HttpURLConnection.HTTP_OK, obj.length());
		arg0.getResponseBody().write(obj.getBytes());
	}

	@Override
	public void setFailHeaders(HttpExchange arg0) throws IOException {
		// TODO Auto-generated method stub
		
	}
}
