package shared.proxy.response;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import com.google.gson.Gson;
import com.sun.net.httpserver.HttpExchange;

import client.data.GameInfo;
/**
 * 
 * @author Wesley
 *
 */
public class GamesListResponse implements IResponse {
	List<GameInfo> games;
	
	public GamesListResponse(ArrayList<GameInfo> info){
		this.games = info;
	}
	
	public void createFakeList(){
		
	}

	public List<GameInfo> getGames() {
		return games;
	}

	public void setGames(List<GameInfo> games) {
		this.games = games;
	}

	@Override
	public void addHeaders(HttpExchange arg0) throws IOException {
		Logger.getGlobal().finer("addHeaders for games list executed");
		Gson g = new Gson();
		String obj = g.toJson(this);
		obj = obj.substring(9, obj.length()-1);
		arg0.getResponseHeaders().set("Content-Type", "application/json");
		arg0.sendResponseHeaders(HttpURLConnection.HTTP_OK, obj.length());
		arg0.getResponseBody().write(obj.getBytes());
	}

	@Override
	public void setFailHeaders(HttpExchange arg0) throws IOException {
		// TODO Auto-generated method stub
		
	}
	
	
}

