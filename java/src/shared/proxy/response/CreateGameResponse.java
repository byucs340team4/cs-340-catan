package shared.proxy.response;

import java.io.IOException;
import java.net.HttpURLConnection;

import server.parser.CookieParser;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.sun.net.httpserver.HttpExchange;

import gamemanager.player.Player;

public class CreateGameResponse implements IResponse {
	String title;
	int id;
	Player[] players;
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Player[] getPlayers() {
		return players;
	}
	public void setPlayers(Player[] players) {
		this.players = players;
	}
	@Override
	public void addHeaders(HttpExchange arg0) throws IOException {
		Gson g = new Gson();
		JsonObject json = CookieParser.parseCookieObject(arg0.getRequestHeaders().get("Cookie"));
		
		String cookieAppend = "catan.game=" + json.toString();
		arg0.getResponseHeaders().set("Content-Type", "text/html");
		arg0.getResponseHeaders().add("Set-cookie",(cookieAppend + ";Path=/;"));
		arg0.sendResponseHeaders(HttpURLConnection.HTTP_OK, g.toJson(this).toString().getBytes().length);
		
		arg0.getResponseBody().write(g.toJson(this).toString().getBytes());
	}
	@Override
	public void setFailHeaders(HttpExchange arg0) throws IOException {
		arg0.getRequestHeaders().set("Content-Type", "text/html");
		arg0.sendResponseHeaders(HttpURLConnection.HTTP_BAD_REQUEST, "Error".getBytes().length);
		arg0.getResponseBody().write("Error".getBytes());
	}
	
	
}
