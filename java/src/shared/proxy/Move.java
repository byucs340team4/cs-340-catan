/**
 * 
 */
package shared.proxy;

/**
 * @author Michael
 *
 */
public abstract class Move {
	private MoveType moveType;
	private int playerIndex;
	
	public Move(){
	}

	public MoveType getMoveType() {
		return moveType;
	}

	public void setMoveType(MoveType moveType) {
		this.moveType = moveType;
	}

	public int getPlayerIndex() {
		return playerIndex;
	}

	public void setPlayerIndex(int playerIndex) {
		this.playerIndex = playerIndex;
	}
	
	
}
