package client.data;

import java.util.*;

import shared.definitions.CatanColor;

/**
 * Used to pass game information into views<br>
 * <br>
 * PROPERTIES:<br>
 * <ul>
 * <li>Id: Unique game ID</li>
 * <li>Title: Game title (non-empty string)</li>
 * <li>Players: List of players who have joined the game (can be empty)</li>
 * </ul>
 * 
 */
public class GameInfo
{
	private int id;
	private String title;
	private List<PlayerInfo> players;
	
	public GameInfo()
	{
		setId(-1);
		setTitle("");
		players = new ArrayList<PlayerInfo>();
	}
	
	public int getId()
	{
		return id;
	}
	
	public void setId(int id)
	{
		this.id = id;
	}
	
	public String getTitle()
	{
		return title;
	}
	
	public void setTitle(String title)
	{
		this.title = title;
	}
	
	public void addPlayer(PlayerInfo newPlayer)
	{
		players.add(newPlayer);
	}
	
	public List<PlayerInfo> getPlayers()
	{
		return Collections.unmodifiableList(players);
	}

	public boolean containsColor(CatanColor color) {
		boolean contains = false;
		if(players.isEmpty()) {
			return false;
		}
		for (PlayerInfo info : players) {
			if (info.getColor() == color) {
				contains = true;
			}
		}
		return contains;
	}

	public void setPlayers(List<PlayerInfo> players) {
		this.players = players;
	}

	public boolean containsPlayer(int id) {
		boolean contains = false;
		for(PlayerInfo player : players) {
			if(player.getId() == id) {
				contains = true;
			}
		}
		return contains;
	}
}

