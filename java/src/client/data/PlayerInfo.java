package client.data;

import shared.definitions.*;

/**
 * Used to pass player information into views<br>
 * <br>
 * PROPERTIES:<br>
 * <ul>
 * <li>Id: Unique player ID</li>
 * <li>PlayerIndex: Player's order in the game [0-3]</li>
 * <li>Name: Player's name (non-empty string)</li>
 * <li>Color: Player's color (cannot be null)</li>
 * </ul>
 * 
 */
public class PlayerInfo
{
	
	private int id;
	private int playerIndex;
	private String name;
	private CatanColor catanColor;
	private String color;
	private int rolledNumber = -1; //hasn't rolled
	
	public PlayerInfo()
	{
		setId(-1);
		setPlayerIndex(-1);
		setName("");
		setColor(CatanColor.RED);
	}
	
	public PlayerInfo(int id,int playerIndex,String name,CatanColor catanColor){
		this.id = id;
		this.playerIndex = playerIndex;
		this.name = name;
		this.catanColor = catanColor;
	}
	
	public int getId()
	{
		return id;
	}
	
	public void setId(int id)
	{
		this.id = id;
	}
	
	public int getPlayerIndex()
	{
		return playerIndex;
	}
	
	public void setPlayerIndex(int playerIndex)
	{
		this.playerIndex = playerIndex;
	}
	
	public String getName()
	{
		return name;
	}
	
	public void setName(String name)
	{
		this.name = name;
	}

	public int getRolledNumber() {
		return rolledNumber;
	}

	public void setRolledNumber(int rolledNumber) {
		this.rolledNumber = rolledNumber;
	}

	@Override
	public int hashCode()
	{
		return 31 * this.id;
	}

	@Override
	public boolean equals(Object obj)
	{
		if (obj == null)
		{
			return false;
		}
		if (getClass() != obj.getClass())
		{
			return false;
		}
		final PlayerInfo other = (PlayerInfo) obj;
		
		return this.id == other.id;
	}

	public CatanColor getColor() {
		return catanColor;
	}

	public void setColor(CatanColor color) {
		this.catanColor = color;
	}

	public void setStringColor(String color) {
		this.color = color;
	}

	public String getStringColor() {
		if(color==null)
			return catanColor.toString();
		return color;
	}
	
	
}

