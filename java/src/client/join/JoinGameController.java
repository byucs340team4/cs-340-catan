package client.join;

import java.security.InvalidParameterException;
import java.util.Observable;

import com.google.common.base.CharMatcher;

import gamemanager.GameManager;
import gamemanager.Poller;
import gamemanager.game.Game;
import gamemanager.gamestate.GamePlayState;
import gamemanager.gamestate.JoinState;
import gamemanager.gamestate.PlayerWaitingState;
import gamemanager.gamestate.SetupState;
import shared.definitions.CatanColor;
import shared.proxy.request.CreateGameRequests;
import shared.proxy.request.Credentials;
import shared.proxy.request.GameCommandsRequest;
import shared.proxy.request.JoinGameRequest;
import shared.proxy.request.ListGamesRequest;
import shared.proxy.response.GamesListResponse;
import shared.proxy.response.JoinGameResponse;
import client.base.*;
import client.data.*;
import client.login.InvalidInputView;
import client.misc.*;


/**
 * Implementation for the join game controller
 */
public class JoinGameController extends Controller implements IJoinGameController {

	private INewGameView newGameView;
	private ISelectColorView selectColorView;
	private IMessageView messageView;
	private IAction joinAction;
	GameInfo[] array;
	GameInfo game;
	
	/**
	 * JoinGameController constructor
	 * 
	 * @param view Join game view
	 * @param newGameView New game view
	 * @param selectColorView Select color view
	 * @param messageView Message view (used to display error messages that occur while the user is joining a game)
	 */
	public JoinGameController(IJoinGameView view, INewGameView newGameView, 
								ISelectColorView selectColorView, IMessageView messageView) {

		super(view);

		setNewGameView(newGameView);
		setSelectColorView(selectColorView);
		setMessageView(messageView);
	}
	
	public IJoinGameView getJoinGameView() {
		
		return (IJoinGameView)super.getView();
	}
	
	/**
	 * Returns the action to be executed when the user joins a game
	 * 
	 * @return The action to be executed when the user joins a game
	 */
	public IAction getJoinAction() {
		
		return joinAction;
	}

	/**
	 * Sets the action to be executed when the user joins a game
	 * 
	 * @param value The action to be executed when the user joins a game
	 */
	public void setJoinAction(IAction value) {	
		
		joinAction = value;
	}
	
	public INewGameView getNewGameView() {
		
		return newGameView;
	}

	public void setNewGameView(INewGameView newGameView) {
		
		this.newGameView = newGameView;
	}
	
	public ISelectColorView getSelectColorView() {
		
		return selectColorView;
	}
	public void setSelectColorView(ISelectColorView selectColorView) {
		
		this.selectColorView = selectColorView;
	}
	
	public IMessageView getMessageView() {
		
		return messageView;
	}
	public void setMessageView(IMessageView messageView) {
		
		this.messageView = messageView;
	}

	@Override
	public void start() {
		ListGamesRequest request = new ListGamesRequest();
		GamesListResponse response = GameManager.getInstance().getProxy().gamesList(request);
		PlayerInfo info = GameManager.getInstance().getLocalPlayer();
		array = response.getGames().toArray(new GameInfo[0]);
		if(!getJoinGameView().isModalShowing() && !getNewGameView().isModalShowing() && !getSelectColorView().isModalShowing()){
			getJoinGameView().setGames(array, info);
			getJoinGameView().showModal();
		}else{
			getJoinGameView().setGames(array, info);
			
		}
	}

	@Override
	public void startCreateNewGame() {
		this.getJoinGameView().closeModal();
		getNewGameView().showModal();
	}

	@Override
	public void cancelCreateNewGame() {
		getNewGameView().setTitle("");
		getNewGameView().closeModal();
		this.getJoinGameView().showModal();
	}

	@Override
	public void createNewGame() {
		boolean randomlyPlaceHexes = getNewGameView().getRandomlyPlaceHexes();
		boolean ramdomlyPlaceNumbers = getNewGameView().getRandomlyPlaceNumbers();
		boolean useRandomPorts = getNewGameView().getUseRandomPorts();
		String name = getNewGameView().getTitle();
		boolean isAscii = CharMatcher.ASCII.matchesAllOf(name);
		if(isAscii) {
			if(name.length() < 2 || name.length() > 16) {
				InvalidParameterException e = new InvalidParameterException("Game title must be between 2 and 16 characters");
				InvalidInputView invalid = new InvalidInputView(e);
				invalid.setVisible(true);
			}
			else {
				CreateGameRequests create = new CreateGameRequests(randomlyPlaceHexes, ramdomlyPlaceNumbers, useRandomPorts, name);
				GameManager.getInstance().getGameState().createGame(create);
				GamesListResponse response = GameManager.getInstance().getProxy().gamesList(new ListGamesRequest());
				array = response.getGames().toArray(new GameInfo[0]);
				GameInfo thisGame = array[array.length-1];
				JoinGameRequest request = new JoinGameRequest(thisGame.getId(),CatanColor.RED.toString().toLowerCase());
				GameManager.getInstance().getProxy().gamesJoin(request,true);
				getNewGameView().closeModal();
				getNewGameView().setTitle("");
				start();
			} 
		}
		else {
			InvalidInputView failed = new InvalidInputView(new InvalidParameterException("Must use ASCII characters"));
			failed.setVisible(true);
		}
	}

	@Override
	public void startJoinGame(GameInfo game) {
		if(game != null) {
			disableColors(game);
			this.game = game;
		}
		this.getJoinGameView().closeModal();//changed 
		getSelectColorView().showModal();
	}
	
	public void disableColors(GameInfo game) {
		int playerID = GameManager.getInstance().getLocalPlayer().getId();
		
		//else {
			getSelectColorView().setColorEnabled(CatanColor.BLUE, !game.containsColor(CatanColor.BLUE));
			getSelectColorView().setColorEnabled(CatanColor.BROWN, !game.containsColor(CatanColor.BROWN));
			getSelectColorView().setColorEnabled(CatanColor.GREEN, !game.containsColor(CatanColor.GREEN));
			getSelectColorView().setColorEnabled(CatanColor.ORANGE, !game.containsColor(CatanColor.ORANGE));
			getSelectColorView().setColorEnabled(CatanColor.PUCE, !game.containsColor(CatanColor.PUCE));
			getSelectColorView().setColorEnabled(CatanColor.PURPLE, !game.containsColor(CatanColor.PURPLE));
			getSelectColorView().setColorEnabled(CatanColor.RED, !game.containsColor(CatanColor.RED));
			getSelectColorView().setColorEnabled(CatanColor.WHITE, !game.containsColor(CatanColor.WHITE));
			getSelectColorView().setColorEnabled(CatanColor.YELLOW, !game.containsColor(CatanColor.YELLOW));
		//}
			
			if(game.containsPlayer(playerID)) {
				//disableAll();
				PlayerInfo player = null;
				for (PlayerInfo temp : game.getPlayers()) {
					if(temp.getId() == playerID) {
						player = temp;
					}
				}
				getSelectColorView().setColorEnabled(player.getColor(), true);
			}
	}

	private void disableAll() {
		getSelectColorView().setColorEnabled(CatanColor.BLUE, false);
		getSelectColorView().setColorEnabled(CatanColor.BROWN, false);
		getSelectColorView().setColorEnabled(CatanColor.GREEN, false);
		getSelectColorView().setColorEnabled(CatanColor.ORANGE, false);
		getSelectColorView().setColorEnabled(CatanColor.PUCE, false);
		getSelectColorView().setColorEnabled(CatanColor.PURPLE, false);
		getSelectColorView().setColorEnabled(CatanColor.RED, false);
		getSelectColorView().setColorEnabled(CatanColor.WHITE, false);
		getSelectColorView().setColorEnabled(CatanColor.YELLOW, false);
	}

	@Override
	public void cancelJoinGame() {
	
		getJoinGameView().closeModal();
	}

	@Override
	public void joinGame(CatanColor color) {
		if(game == null) {
			getSelectColorView().closeModal();
			getJoinGameView().closeModal();
			GameManager.getInstance().setGameState(new PlayerWaitingState(GameManager.getInstance().getGame()));
			joinAction.execute();
			
			
		}
		else {
			JoinGameRequest request = new JoinGameRequest(game.getId(), color.toString().toLowerCase());
			if(GameManager.getInstance().getGameState().joinGame(request)) {
				ListGamesRequest listGames = new ListGamesRequest();
				GamesListResponse response = GameManager.getInstance().getProxy().gamesList(listGames);
				array = response.getGames().toArray(new GameInfo[0]);
				int index = -1;
				for(GameInfo info : array) {
					if(info.getId() == request.getId()) {
						for(PlayerInfo player : info.getPlayers()) {
							index++;
							if(player.getId() == GameManager.getInstance().getLocalPlayer().getId()) {
								GameManager.getInstance().setLocalPlayer(player);
								GameManager.getInstance().getLocalPlayer().setPlayerIndex(index);
							}
						}
						GameManager.getInstance().setLocalGame(info);
					}
				}
				GameManager.getInstance().setGameState(new PlayerWaitingState(null));
			}
			Game game = GameManager.getInstance().getProxy().gameModel();
		//	GameManager.getInstance().notifyObservers();
			game.setVersion(-11111);
			GameManager.getInstance().setGame(game);
			getJoinGameView().closeModal();

			if(getSelectColorView().isModalShowing())getSelectColorView().closeModal(); 
		
			
			joinAction.execute();
		}	
	}

	@Override
	public void update(Observable o, Object arg) {
		if(GameManager.getInstance().getGameState() instanceof JoinState){
			start();
		}
		if(GameManager.getInstance().getGameState() instanceof GamePlayState ||
				GameManager.getInstance().getGameState() instanceof SetupState){
			if(this.getSelectColorView().isModalShowing())
				this.getSelectColorView().closeModal();
		}
	}

}

