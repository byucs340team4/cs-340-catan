package client.join;

import gamemanager.GameManager;

import java.util.Observable;

import shared.proxy.request.AddAIRequest;
import client.base.*;


/**
 * Implementation for the player waiting controller
 */
public class PlayerWaitingController extends Controller implements IPlayerWaitingController {
	int players = 0;
	
	public PlayerWaitingController(IPlayerWaitingView view) {
		
		super(view);
		players = 0;
	}

	@Override
	public IPlayerWaitingView getView() {

		return (IPlayerWaitingView)super.getView();
	}

	@Override
	public void start() {
		if(GameManager.getInstance().getGame().getNumberOfPlayers() < 4 && !getView().isModalShowing()) {
			getView().setPlayers(GameManager.getInstance().getGame().getPlayersInfo());
			players = GameManager.getInstance().getGame().getNumberOfPlayers();
			getView().showModal();
			
		}
		else if(!(GameManager.getInstance().getGame().getNumberOfPlayers() < 4)){
			if(getView().isModalShowing()){
				getView().closeModal();
			}
			try{
			//GameManager.getInstance().getGameState().nextState();
				
			}catch(Exception e){
				
			}
		}else if(getView().isModalShowing() && players != GameManager.getInstance().getGame().getNumberOfPlayers()){
			getView().closeModal();
			getView().setPlayers(GameManager.getInstance().getGame().getPlayersInfo());
			players = GameManager.getInstance().getGame().getNumberOfPlayers();
			getView().showModal();
		}
	}

	@Override
	public void addAI() {

		// TEMPORARY
		System.out.println(getView().getSelectedAI());
		System.out.println(GameManager.getInstance().getProxy().addAI(new AddAIRequest(getView().getSelectedAI())));
		
		//getView().closeModal(); 
	}

	@Override
	public void update(Observable o, Object arg) {
		if(getView().isModalShowing()){
			
			start();
			
		}
	}

}

