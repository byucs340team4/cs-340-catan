package client.resources;

import gamemanager.GameManager;
import gamemanager.gamestate.GamePlayState;
import gamemanager.gamestate.SetupState;
import gamemanager.player.Player;
import gamemanager.player.ResourceList;

import java.util.*;

//import shared.definitions.ResourceType;
import client.base.*;


/**
 * Implementation for the resource bar controller
 */
public class ResourceBarController extends Controller implements IResourceBarController {

	private Map<ResourceBarElement, IAction> elementActions;
	
	private Player player;
	
	public ResourceBarController(IResourceBarView view) {

		super(view);
		player = null;
		
		elementActions = new HashMap<ResourceBarElement, IAction>();
	}

	@Override
	public IResourceBarView getView() {
		return (IResourceBarView)super.getView();
	}

	/**
	 * Sets the action to be executed when the specified resource bar element is clicked by the user
	 * 
	 * @param element The resource bar element with which the action is associated
	 * @param action The action to be executed
	 */
	public void setElementAction(ResourceBarElement element, IAction action) {

		elementActions.put(element, action);
	}

	@Override
	public void buildRoad() {
		int index = GameManager.getInstance().getLocalPlayer().getPlayerIndex();
		player = GameManager.getInstance().getGame().getPlayers()[index];
		if(player.canBuildRoad())
			executeElementAction(ResourceBarElement.ROAD);
	}

	@Override
	public void buildSettlement() {
		int index = GameManager.getInstance().getLocalPlayer().getPlayerIndex();
		player = GameManager.getInstance().getGame().getPlayer(index);
		if(player.canBuildSettlement())
			executeElementAction(ResourceBarElement.SETTLEMENT);
	}

	@Override
	public void buildCity() {
		int index = GameManager.getInstance().getLocalPlayer().getPlayerIndex();
		player = GameManager.getInstance().getGame().getPlayers()[index];
		if(player.canBuildCity())
			executeElementAction(ResourceBarElement.CITY);
	}

	@Override
	public void buyCard() {
		int index = GameManager.getInstance().getLocalPlayer().getPlayerIndex();
		player = GameManager.getInstance().getGame().getPlayers()[index];
		if(player.canBuyDevCard())
			executeElementAction(ResourceBarElement.BUY_CARD);
	}

	@Override
	public void playCard() {
		int index = GameManager.getInstance().getLocalPlayer().getPlayerIndex();
		player = GameManager.getInstance().getGame().getPlayers()[index];
//		if(player.canPlayMonopoly()||player.getOldDevCards().getMonument()>=1||player.canPlayRoadBuilding()
//				||player.canPlaySoldier()||player.canPlayYoP())
			executeElementAction(ResourceBarElement.PLAY_CARD);
	}
	
	private void executeElementAction(ResourceBarElement element) {
		
		if (elementActions.containsKey(element)) {
			
			IAction action = elementActions.get(element);
			action.execute();
		}
	}

	private void setEnabled(){
		int index = GameManager.getInstance().getLocalPlayer().getPlayerIndex();
		this.setRoadEnabled(index);
		this.setSettlementEnabled(index);
		this.setCityEnabled(index);
		this.setBuyCardEnabled(index);
	}
	
	private void setBuyCardEnabled(int index) {
		// TODO Auto-generated method stub
		if(GameManager.getInstance().getGame().getTurnTracker().getCurrentTurn()==index && GameManager.getInstance().getGame().getPlayer(index).canBuyDevCard())
			this.getView().setElementEnabled(ResourceBarElement.BUY_CARD, true);
		else
			this.getView().setElementEnabled(ResourceBarElement.BUY_CARD, false);
	}

	private void setRoadEnabled(int index){
		if(GameManager.getInstance().getGame().getTurnTracker().getCurrentTurn()==index && GameManager.getInstance().getGame().getPlayer(index).canBuildRoad())
			this.getView().setElementEnabled(ResourceBarElement.ROAD, true);
		else
			this.getView().setElementEnabled(ResourceBarElement.ROAD, false);
	}
	
	private void setSettlementEnabled(int index){
		if(GameManager.getInstance().getGame().getTurnTracker().getCurrentTurn()==index && GameManager.getInstance().getGame().getPlayer(index).canBuildSettlement())
			this.getView().setElementEnabled(ResourceBarElement.SETTLEMENT, true);
		else
			this.getView().setElementEnabled(ResourceBarElement.SETTLEMENT, false);
	}
	
	private void setCityEnabled(int index){
		if(GameManager.getInstance().getGame().getTurnTracker().getCurrentTurn()==index && GameManager.getInstance().getGame().getPlayer(index).canBuildCity())
			this.getView().setElementEnabled(ResourceBarElement.CITY, true);
		else
			this.getView().setElementEnabled(ResourceBarElement.CITY, false);
	}
	
	@Override
	public void update(Observable o, Object arg) {
		if (GameManager.getInstance().getGameState() instanceof SetupState || GameManager.getInstance().getGameState() instanceof GamePlayState) {
			int playerIndex = GameManager.getInstance().getLocalPlayer()
					.getPlayerIndex();
			// if(this.player==null)
			Player player = GameManager.getInstance().getGame()
					.getPlayer(playerIndex);
			this.updateResources(player);
			if(GameManager.getInstance().getGame().getTurnTracker().getCurrentTurn()==playerIndex){
				this.getView().setElementEnabled(ResourceBarElement.PLAY_CARD, true);
				this.getView().setElementEnabled(ResourceBarElement.BUY_CARD, true);
			}else{
				this.getView().setElementEnabled(ResourceBarElement.PLAY_CARD, false);
				this.getView().setElementEnabled(ResourceBarElement.BUY_CARD, false);
			}
			this.setEnabled();
		}
	}

	private void updateResources(Player player){
		ResourceList resources = player.getResources();
		this.updateBrick(resources);
		this.updateOre(resources);
		this.updateSheep(resources);
		this.updateWheat(resources);
		this.updateWood(resources);
		this.updateBuildObjects(player);
		this.getView().setElementAmount(ResourceBarElement.SOLDIERS, player.getSoldiers());
	}
	
//	private void updateResource(ResourceBarElement element){
//		ResourceType type = element.getResourceType();
//		if(type!=null){
//			
//		}
//	}
	
	private void updateBuildObjects(Player player){
		this.getView().setElementAmount(ResourceBarElement.CITY, player.getCities());
		this.getView().setElementAmount(ResourceBarElement.ROAD, player.getRoads());
		this.getView().setElementAmount(ResourceBarElement.SETTLEMENT, player.getSettlements());
	}
	
	
	private void updateBrick(ResourceList resources){
		this.getView().setElementAmount(ResourceBarElement.BRICK, resources.getBrick());
	}
	
	private void updateOre(ResourceList resources){
		this.getView().setElementAmount(ResourceBarElement.ORE, resources.getOre());
	}
	
	private void updateSheep(ResourceList resources){
		this.getView().setElementAmount(ResourceBarElement.SHEEP, resources.getSheep());
	}
	
	private void updateWheat(ResourceList resources){
		this.getView().setElementAmount(ResourceBarElement.WHEAT, resources.getWheat());
	}
	
	private void updateWood(ResourceList resources){
		this.getView().setElementAmount(ResourceBarElement.WOOD, resources.getWood());
	}
}

