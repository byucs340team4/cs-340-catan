package client.resources;

import shared.definitions.ResourceType;

public enum ResourceBarElement
{
	WOOD, BRICK, SHEEP, WHEAT, ORE, ROAD, SETTLEMENT, CITY, BUY_CARD, PLAY_CARD, SOLDIERS;
	
	private ResourceType type;
	
	static{
		WOOD.type = ResourceType.WOOD;
		BRICK.type = ResourceType.BRICK;
		SHEEP.type = ResourceType.SHEEP;
		WHEAT.type = ResourceType.WHEAT;
		ORE.type = ResourceType.ORE;
		ROAD.type = null;
		SETTLEMENT.type = null;
		CITY.type = null;
		BUY_CARD.type = null;
		PLAY_CARD.type = null;
		SOLDIERS.type = null;
	}
	
	public ResourceType getResourceType(){
		return type;
	}
}

