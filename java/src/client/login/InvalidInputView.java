package client.login;

import java.awt.*;
import java.awt.event.*;
import java.security.InvalidParameterException;

import javax.swing.*;

@SuppressWarnings("serial")
public class InvalidInputView extends JDialog implements ActionListener{

	public InvalidInputView(InvalidParameterException e) {
	this.setTitle("Login Failed");
	this.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
	this.setLocation(100,100);
	
	JPanel panel = new JPanel(new GridLayout(2,1));
	
	JLabel label = new JLabel(e.getMessage());
	this.setResizable(false);
	this.setModal(true);
	JButton button = new JButton("OK");
	button.setVerticalTextPosition(AbstractButton.CENTER);
	button.setHorizontalTextPosition(AbstractButton.LEADING);
	button.setActionCommand("exit");
	button.addActionListener(this);
	
	
	panel.add(label);
	panel.add(button);
	
	this.add(panel);
	this.pack();
	}
	
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if("exit".equals(e.getActionCommand())) {
			this.dispose();
		}
		
	}

}
