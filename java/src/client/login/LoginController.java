package client.login;

import client.base.*;
import client.misc.*;
import gamemanager.GameManager;
import gamemanager.gamestate.JoinState;
import gamemanager.gamestate.LoginState;

import java.net.*;
import java.io.*;
import java.security.InvalidParameterException;
import java.util.*;
import java.lang.reflect.*;

import shared.proxy.request.Credentials;
import shared.proxy.response.LoginResponse;

import com.google.common.base.CharMatcher;
import com.google.gson.*;
import com.google.gson.reflect.TypeToken;


/**
 * Implementation for the login controller
 */
public class LoginController extends Controller implements ILoginController {

	private IMessageView messageView;
	private IAction loginAction;
	
	/**
	 * LoginController constructor
	 * 
	 * @param view Login view
	 * @param messageView Message view (used to display error messages that occur during the login process)
	 */
	public LoginController(ILoginView view, IMessageView messageView) {

		super(view);
		this.messageView = messageView;
		GameManager.getInstance().setGameState(new LoginState(GameManager.getInstance().getGame()));
	}
	
	public ILoginView getLoginView() {
		
		return (ILoginView)super.getView();
	}
	
	public IMessageView getMessageView() {
		
		return messageView;
	}
	
	/**
	 * Sets the action to be executed when the user logs in
	 * 
	 * @param value The action to be executed when the user logs in
	 */
	public void setLoginAction(IAction value) {
		
		loginAction = value;
	}
	
	/**
	 * Returns the action to be executed when the user logs in
	 * 
	 * @return The action to be executed when the user logs in
	 */
	public IAction getLoginAction() {
		
		return loginAction;
	}

	@Override
	public void start() {
		
		getLoginView().showModal();
	}

	@Override
	public void signIn() {
		
		// TODO: log in user
		String user = getLoginView().getLoginUsername();
		String pass = getLoginView().getLoginPassword();
		boolean isAsciiUser = CharMatcher.ASCII.matchesAllOf(user);
		boolean isAsciiPass = CharMatcher.ASCII.matchesAllOf(pass);
		if(isAsciiUser&&isAsciiPass) {
			Credentials cred = null;
			try {
				cred = new Credentials(user, pass);
	    			boolean response = GameManager.getInstance().getGameState().login(cred);
				if(response) {
				// If log in succeeded
					GameManager.getInstance().setGameState(new JoinState(null));
					getLoginView().closeModal();
					loginAction.execute();
				}
				else {
					LoginFailedView failed = new LoginFailedView();
					failed.setVisible(true);
				}
			} catch (InvalidParameterException e) {
				InvalidInputView failed = new InvalidInputView(e);
				failed.setVisible(true);
			}
		}
		else {
			InvalidInputView failed = new InvalidInputView(new InvalidParameterException("Must use ASCII characters"));
			failed.setVisible(true);
		}
	}

	@Override
	public void register() {
		String user = getLoginView().getRegisterUsername();
		String pass = getLoginView().getRegisterPassword();
		String repeat = getLoginView().getRegisterPasswordRepeat();
		boolean isAsciiUser = CharMatcher.ASCII.matchesAllOf(user);
		if(!CharMatcher.JAVA_LETTER_OR_DIGIT.matchesAllOf(user)) {
			for(int i=0;i<user.length();i++) {
				char temp = user.charAt(i);
				if(temp != '_' && temp != '-' && !CharMatcher.JAVA_LETTER_OR_DIGIT.matches(temp)) {
					isAsciiUser = false;
				}
			}
		}
		boolean isAsciiPass = CharMatcher.ASCII.matchesAllOf(pass);
		if(!CharMatcher.JAVA_LETTER_OR_DIGIT.matchesAllOf(pass)) {
			for(int i=0;i<pass.length();i++) {
				char temp = pass.charAt(i);
				if(temp != '_' && temp != '-' && !CharMatcher.JAVA_LETTER_OR_DIGIT.matches(temp)) {
					isAsciiPass = false;
				}
			}
		}
		boolean isAsciiRepeat = CharMatcher.ASCII.matchesAllOf(repeat);
		Credentials cred = null;
		if(isAsciiUser&&isAsciiPass&&isAsciiRepeat) {
			if(pass.equals(repeat)) {
				try {
					cred = new Credentials(getLoginView().getRegisterUsername(), pass);
					if(GameManager.getInstance().getGameState().register(cred)) {
						if(GameManager.getInstance().getGameState().login(cred))
						{
							GameManager.getInstance().setGameState(new JoinState(null));
							getLoginView().closeModal();
							loginAction.execute();
						}
					}
					else {
						InvalidInputView failed = new InvalidInputView(new InvalidParameterException("User already registered"));
						failed.setVisible(true);
					}
				} catch (InvalidParameterException e) {
					InvalidInputView failed = new InvalidInputView(e);
					failed.setVisible(true);
				}
			}
			else {
				PasswordMismatch fail = new PasswordMismatch();
				fail.setVisible(true);
			}
		}
		else {
			InvalidInputView failed = new InvalidInputView(new InvalidParameterException("Must use letters, numbers, - or _"));
			failed.setVisible(true);
		}
	}

	@Override
	public void update(Observable o, Object arg) {
		// TODO Auto-generated method stub
		
	}

}

