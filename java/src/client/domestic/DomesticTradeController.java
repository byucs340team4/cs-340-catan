package client.domestic;

import gamemanager.GameManager;
import gamemanager.gamestate.GamePlayState;
import gamemanager.player.Player;
import gamemanager.player.ResourceList;

import java.util.HashSet;
import java.util.Observable;
import java.util.Set;

import cs340.model.player.ResourceHand;
import cs340.model.player.TradeOffer;
import shared.definitions.*;
import shared.proxy.request.AcceptTrade;
import shared.proxy.request.OfferTrade;
import client.base.*;
import client.data.PlayerInfo;
import client.misc.*;


/**
 * Domestic trade controller implementation
 */
public class DomesticTradeController extends Controller implements IDomesticTradeController {

	private IDomesticTradeOverlay tradeOverlay;
	private IWaitView waitOverlay;
	private IAcceptTradeOverlay acceptOverlay;
	
	ResourceList resources;
	
	Set<ResourceType> resourceToSend;
	
	Set<ResourceType> resourceToReceive;
	
	OfferTrade offer;
	
	Player player;
	
	ResourceList playerResources;
	
	private int playerIndex = -1;

	/**
	 * DomesticTradeController constructor
	 * 
	 * @param tradeView Domestic trade view (i.e., view that contains the "Domestic Trade" button)
	 * @param tradeOverlay Domestic trade overlay (i.e., view that lets the user propose a domestic trade)
	 * @param waitOverlay Wait overlay used to notify the user they are waiting for another player to accept a trade
	 * @param acceptOverlay Accept trade overlay which lets the user accept or reject a proposed trade
	 */
	public DomesticTradeController(IDomesticTradeView tradeView, IDomesticTradeOverlay tradeOverlay,
									IWaitView waitOverlay, IAcceptTradeOverlay acceptOverlay) {

		super(tradeView);
		
		setTradeOverlay(tradeOverlay);
		setWaitOverlay(waitOverlay);
		setAcceptOverlay(acceptOverlay);
		resources = new ResourceList();
		offer = new OfferTrade();
		this.resourceToReceive = new HashSet<ResourceType>();
		this.resourceToSend = new HashSet<ResourceType>();
		this.player = null;
		this.playerResources = null;
	}
	
	public IDomesticTradeView getTradeView() {
		
		return (IDomesticTradeView)super.getView();
	}

	public IDomesticTradeOverlay getTradeOverlay() {
		return tradeOverlay;
	}

	public void setTradeOverlay(IDomesticTradeOverlay tradeOverlay) {
		this.tradeOverlay = tradeOverlay;
	}

	public IWaitView getWaitOverlay() {
		return waitOverlay;
	}

	public void setWaitOverlay(IWaitView waitView) {
		this.waitOverlay = waitView;
	}

	public IAcceptTradeOverlay getAcceptOverlay() {
		return acceptOverlay;
	}

	public void setAcceptOverlay(IAcceptTradeOverlay acceptOverlay) {
		this.acceptOverlay = acceptOverlay;
	}

	@Override
	public void startTrade() {
		if(GameManager.getInstance().getGame().getTurnTracker().getCurrentTurn()==this.playerIndex){
			this.getTradeOverlay().reset();
			this.getTradeOverlay().setPlayerSelectionEnabled(true);
			this.getTradeOverlay().setStateMessage("Starting Trade");
			
			for(ResourceType type: ResourceType.values()){
				this.unsetResource(type);
			}
			this.offer.setPlayerIndex(this.playerIndex);
			this.offer.setReceiver(-1);
			getTradeOverlay().showModal();
		}
	}

	@Override
	public void decreaseResourceAmount(ResourceType resource) {
		resources.setResource(resource, resources.getResource(resource)-1);
		int amount = resources.getResource(resource);
		if(amount==0){
			this.getTradeOverlay().setResourceAmountChangeEnabled(resource, true, false);
		}else{
			this.getTradeOverlay().setResourceAmountChangeEnabled(resource, true, true);
		}
		
		checkTrade();
	}

	@Override
	public void increaseResourceAmount(ResourceType resource) {
		resources.setResource(resource, resources.getResource(resource)+1);
		int amount = resources.getResource(resource);
		if(amount>ResourceList.MaxResource){
			this.getTradeOverlay().setResourceAmountChangeEnabled(resource, false, true);
			return;
		}if(this.resourceToSend.contains(resource)){
			if(amount>=this.playerResources.getResource(resource))
				this.getTradeOverlay().setResourceAmountChangeEnabled(resource, false, true);
			else
				this.getTradeOverlay().setResourceAmountChangeEnabled(resource, true, true);
		}else
			this.getTradeOverlay().setResourceAmountChangeEnabled(resource, true, true);
		
		checkTrade();
	}

	@Override
	public void sendTradeOffer() {
		for(ResourceType type:ResourceType.values()){
			if(this.resourceToReceive.contains(type)&&resources.getResource(type)>0){
				int newAmount = resources.getResource(type);
				newAmount = newAmount - (2*newAmount);
				this.resources.setResource(type, newAmount);
			}
		}
		offer.setOffer(resources);
		getTradeOverlay().closeModal();
		GameManager.getInstance().getGameState().domesticTrade(offer);
		this.getWaitOverlay().showModal();
//		this.getAcceptOverlay().showModal();
//		getWaitOverlay().showModal();
	}

	@Override
	public void setPlayerToTradeWith(int playerIndex) {
		offer.setReceiver(playerIndex);
		System.out.println("Trading with: "+playerIndex);
		this.checkTrade();
	}

	@Override
	public void setResourceToReceive(ResourceType resource) {
		this.resourceToReceive.add(resource);
		this.resources.setResource(resource, 0);
		this.getTradeOverlay().setResourceAmount(resource, "0");
		if(this.resourceToSend.contains(resource))
			this.resourceToSend.remove(resource);
		this.getTradeOverlay().setResourceAmountChangeEnabled(resource, true, false);
		this.checkTrade();
	}

	@Override
	public void setResourceToSend(ResourceType resource) {
		this.resourceToSend.add(resource);
		this.resources.setResource(resource, 0);
		this.getTradeOverlay().setResourceAmount(resource, "0");
		if(this.resourceToReceive.contains(resource))
			this.resourceToReceive.remove(resource);
		if(!this.playerResources.has(resource)){
			this.getTradeOverlay().setResourceAmountChangeEnabled(resource, false, false);
			return;
		}
		this.getTradeOverlay().setResourceAmountChangeEnabled(resource, true, false);
		this.checkTrade();
		
	}

	@Override
	public void unsetResource(ResourceType resource) {
		this.resourceToReceive.remove(resource);
		this.resourceToSend.remove(resource);
		this.resources.setResource(resource, 0);
		this.getTradeOverlay().setResourceAmount(resource, "0");
		checkTrade();
	}

	@Override
	public void cancelTrade() {
		resources = new ResourceList();
		offer = new OfferTrade();
		offer.setReceiver(-1);
		this.resourceToReceive.clear();
		this.resourceToSend.clear();
		this.getTradeOverlay().reset();
		getTradeOverlay().closeModal();
	}

	@Override
	public void acceptTrade(boolean willAccept) {
		AcceptTrade acceptor = new AcceptTrade(this.playerIndex,willAccept);
		GameManager.getInstance().getGameState().acceptTrade(acceptor);
		getAcceptOverlay().closeModal();
	}

	@Override
	public void update(Observable o, Object arg) {
		if(GameManager.getInstance().getGameState() instanceof GamePlayState){
			if(playerIndex==-1){
				try{
					PlayerInfo[] info = GameManager.getInstance().getGame().getPlayersInfo();
					playerIndex = GameManager.getInstance().getLocalPlayer().getPlayerIndex();
					this.getTradeOverlay().setPlayers(info);
				}catch(NullPointerException e){}
			}
			if(GameManager.getInstance().getGame().getTurnTracker().getCurrentTurn()!=this.playerIndex)
				this.getTradeView().enableDomesticTrade(false);
			else
				this.getTradeView().enableDomesticTrade(true);
		
			player = GameManager.getInstance().getGame().getPlayer(playerIndex);
			this.playerResources = player.getResources();
			TradeOffer tempOffer = GameManager.getInstance().getGame().getTradeOffer();
			if(tempOffer!=null){
				if(tempOffer.getReceiver()==this.playerIndex){
					if(!this.acceptOverlay.isModalShowing()){
						this.addTradeInfo(tempOffer.getOffer());
						this.acceptOverlay.showModal();
					}
				}
			}if (tempOffer==null && this.getWaitOverlay().isModalShowing()){
				this.getWaitOverlay().closeModal();
				
			}
		}
		else
			this.getTradeView().enableDomesticTrade(false);
	}
	
	public void addTradeInfo(ResourceHand offerResources){
		this.acceptOverlay.reset();
		int offerPlayerIndex = GameManager.getInstance().getGame().getTradeOffer().getSender();
		String offeringPlayer = GameManager.getInstance().getGame().getPlayer(offerPlayerIndex).getName();
		this.acceptOverlay.setPlayerName(offeringPlayer);
		this.acceptOverlay.setAcceptEnabled(true);
		if(offerResources.getBrick()<0){
			this.acceptOverlay.addGiveResource(ResourceType.BRICK, Math.abs(offerResources.getBrick()));
			if(Math.abs(offerResources.getBrick())>playerResources.getBrick())
				this.acceptOverlay.setAcceptEnabled(false);
		}else if(offerResources.getBrick()>0){
			this.acceptOverlay.addGetResource(ResourceType.BRICK, offerResources.getBrick());
		}
		if(offerResources.getOre()<0){
			this.acceptOverlay.addGiveResource(ResourceType.ORE, Math.abs(offerResources.getOre()));
			if(Math.abs(offerResources.getOre())>playerResources.getOre())
				this.acceptOverlay.setAcceptEnabled(false);
		}else if(offerResources.getOre()>0){
			this.acceptOverlay.addGetResource(ResourceType.ORE, offerResources.getOre());
		}
		if(offerResources.getSheep()<0){
			this.acceptOverlay.addGiveResource(ResourceType.SHEEP, Math.abs(offerResources.getSheep()));
			if(Math.abs(offerResources.getSheep())>playerResources.getSheep())
				this.acceptOverlay.setAcceptEnabled(false);
		}else if(offerResources.getSheep()>0){
			this.acceptOverlay.addGetResource(ResourceType.SHEEP, offerResources.getSheep());
		}
		if(offerResources.getWheat()<0){
			this.acceptOverlay.addGiveResource(ResourceType.WHEAT, Math.abs(offerResources.getWheat()));
			if(Math.abs(offerResources.getWheat())>playerResources.getWheat())
				this.acceptOverlay.setAcceptEnabled(false);
		}else if(offerResources.getWheat()>0){
			this.acceptOverlay.addGetResource(ResourceType.WHEAT, offerResources.getWheat());
		}
		if(offerResources.getWood()<0){
			this.acceptOverlay.addGiveResource(ResourceType.WOOD, Math.abs(offerResources.getWood()));
			if(Math.abs(offerResources.getWood())>playerResources.getWood())
				this.acceptOverlay.setAcceptEnabled(false);
		}else if(offerResources.getWood()>0){
			this.acceptOverlay.addGetResource(ResourceType.WOOD, offerResources.getWood());
		}
//		if(requestedResources>0&&offeredResources>0){
//			this.acceptOverlay.setAcceptEnabled(true);
//		}else{
//			this.acceptOverlay.setAcceptEnabled(false);
//		}
	}
	
	public void checkTrade(){
		for(ResourceType r: resourceToReceive){
			if(resources.getResource(r) < 1)
			{
				this.getTradeOverlay().setTradeEnabled(false);
				this.getTradeOverlay().setStateMessage("Starting trade");
				return;
			}
		}

		for(ResourceType r: resourceToSend){
			if(resources.getResource(r) < 1)
			{
				this.getTradeOverlay().setTradeEnabled(false);
				this.getTradeOverlay().setStateMessage("Starting trade");
				return;
			}
		}
		
		System.out.println(resources.total());
		if(this.offer.getReceiver()!=-1&&this.resourceToReceive.size()>0&&this.resourceToSend.size()>0){
			this.getTradeOverlay().setTradeEnabled(true);
			this.getTradeOverlay().setStateMessage("Trade");
		}
		else{
			this.getTradeOverlay().setTradeEnabled(false);
			this.getTradeOverlay().setStateMessage("Starting trade");
		}
	}

}

