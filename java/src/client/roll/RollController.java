package client.roll;

import gamemanager.GameManager;
import gamemanager.game.Game;
import gamemanager.gamestate.GamePlayState;

import java.util.Observable;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import shared.proxy.MoveType;
import shared.proxy.request.RollNumberRequest;
import client.base.*;


/**
 * Implementation for the roll controller
 */
public class RollController extends Controller implements IRollController {
	
	Timer timer;
    int countDown = 5;

	private IRollResultView resultView;

	/**
	 * RollController constructor
	 * 
	 * @param view Roll view
	 * @param resultView Roll result view
	 */
	public RollController(IRollView view, IRollResultView resultView) {

		super(view);
		
		setResultView(resultView);
	}
	
	public IRollResultView getResultView() {
		return resultView;
	}
	public void setResultView(IRollResultView resultView) {
		this.resultView = resultView;
	}

	public IRollView getRollView() {
		return (IRollView)getView();
	}
	
	@Override
	public void rollDice() {
		System.out.println("calls roll dice");
		timer.cancel();
		timer = null;
		countDown = 5;
		Random rand = new Random();
		int result = rand.nextInt(36) + 1;
		
		result = getRoll(result);
		
		RollNumberRequest request = new RollNumberRequest(MoveType.ROLL_NUMBER, GameManager.getInstance().getLocalPlayer().getPlayerIndex(), result);
		Game newModel = GameManager.getInstance().getProxy().rollNumber(request);
		if(newModel == null)
		{
			rollDice();
			return;
		}
		GameManager.getInstance().getLocalPlayer().setRolledNumber(result);
		GameManager.getInstance().setGame(newModel);
		
		getResultView().setRollValue(result);
		getResultView().showModal();
		
	}
	
	/**
	 * method to return the roll result with the correct distribution
	 * @param roll, int from 1 to 36
	 * @return int from 2 to 12
	 */
	private int getRoll(int roll){
		if(roll == 1){
			return 2;
		}else if(roll >= 2 && roll <=3){
			return 3;
		}else if(roll >= 4 && roll <=6){
			return 4;
		}else if(roll >= 7 && roll <=10){
			return 5;
		}else if(roll >= 11 && roll <=15){
			return 6;
		}else if(roll >= 16 && roll <=21){
			return 7;
		}else if(roll >= 21 && roll <=26){
			return 8;
		}else if(roll >= 27 && roll <=30){
			return 9;
		}else if(roll >= 31 && roll <=33){
			return 10;
		}else if(roll >= 34 && roll <=35){
			return 11;
		}else{
			return 12;
		}
		
		
	}

	@Override
	public void update(Observable o, Object arg) {
		// TODO Auto-generated method stub
		//getResultView().showModal();
		//this.getRollView().showModal();
		//System.out.println(this.getRollView().isModalShowing());
		if(!this.getResultView().isModalShowing() && !this.getRollView().isModalShowing() && GameManager.getInstance().getGameState() instanceof GamePlayState && (GameManager.getInstance().getGame().getTurnTracker().getCurrentTurn()== GameManager.getInstance().getLocalPlayer().getPlayerIndex() && GameManager.getInstance().getGame().getTurnTracker().getStatus().equals("Rolling")))
		{
			this.getRollView().showModal();
			if(timer == null){
				timer = new Timer();
				timer.scheduleAtFixedRate(new RollTask(this), 0, 1000);
			}
		}
		
		
//		this.getRollView().closeModal();
//		rollDice();

	}

}

	class RollTask extends TimerTask{
		RollController controller;
		public RollTask(RollController r){
			controller = r;
			//run();
		}
		
		public void run(){
			controller.countDown--;
			controller.getRollView().setMessage("Rolling in " + controller.countDown + " seconds.");
			if(controller.countDown==0){
				controller.countDown=5;
				controller.getRollView().setMessage("Rolling in " + controller.countDown + " seconds.");
				controller.getRollView().closeModal();
				controller.rollDice();
			}
			
		}
		
	}

