package client.points;

import gamemanager.GameManager;
import gamemanager.gamestate.GamePlayState;
import gamemanager.gamestate.SetupState;

import java.util.Observable;

import client.base.*;


/**
 * Implementation for the points controller
 */
public class PointsController extends Controller implements IPointsController {

	private IGameFinishedView finishedView;
	
	/**
	 * PointsController constructor
	 * 
	 * @param view Points view
	 * @param finishedView Game finished view, which is displayed when the game is over
	 */
	public PointsController(IPointsView view, IGameFinishedView finishedView) {
		
		super(view);
		
		setFinishedView(finishedView);
		try{
			initFromModel();
		}catch(NullPointerException e){
			
		}
	}
	
	public IPointsView getPointsView() {
		
		return (IPointsView)super.getView();
	}
	
	public IGameFinishedView getFinishedView() {
		return finishedView;
	}
	public void setFinishedView(IGameFinishedView finishedView) {
		this.finishedView = finishedView;
	}

	private void initFromModel() {
		// <temp>
		System.out.println("Points set: "
				+ GameManager
						.getInstance()
						.getGame()
						.getPlayer(
								GameManager.getInstance().getLocalPlayer()
										.getPlayerIndex()).getVictoryPoints());
		getPointsView().setPoints(
				GameManager
						.getInstance()
						.getGame()
						.getPlayer(
								GameManager.getInstance().getLocalPlayer()
										.getPlayerIndex()).getVictoryPoints());
		// </temp>
	}

	@Override
	public void update(Observable o, Object arg) {
		// TODO Auto-generated method stub
		if (GameManager.getInstance().getGameState() instanceof SetupState
				|| GameManager.getInstance().getGameState() instanceof GamePlayState) {
			int totalPoints = GameManager.getInstance().getGame().getPlayer(GameManager.getInstance().getLocalPlayer().getPlayerIndex()).getVictoryPoints();
			if(totalPoints>10)totalPoints=10;
			getPointsView().setPoints(totalPoints);
			int winner = GameManager.getInstance().getGame().getWinner();
			int localIndex = GameManager.getInstance().getLocalPlayer().getPlayerIndex();
			if (winner!=-1) {
				boolean localPlayer = (localIndex==winner);
				this.getFinishedView().setWinner(
						GameManager.getInstance().getGame().getPlayers()[winner].getName(),
						localPlayer);
				this.getFinishedView().showModal();
			}
		}

	}
	
}

