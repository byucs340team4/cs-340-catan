package client.maritime;

import gamemanager.GameManager;
import gamemanager.communication.TurnTracker;
import gamemanager.game.Bank;
import gamemanager.game.Game;
import gamemanager.gamestate.GamePlayState;
import gamemanager.map.Port;
import gamemanager.player.Player;
import gamemanager.player.ResourceList;

import java.util.HashSet;
import java.util.Observable;
import java.util.Set;

import shared.definitions.*;
import shared.proxy.request.MaritimeTrade;
import client.base.*;


/**
 * Implementation for the maritime trade controller
 */
public class MaritimeTradeController extends Controller implements IMaritimeTradeController {

	private IMaritimeTradeOverlay tradeOverlay;
	
	private Set<Port> ports;
	
	private MaritimeRatios giveTypes;
	
	private Set<ResourceType> getTypes;
	
	private ResourceType[] giveResources;
	
	private ResourceType[] getResources;
	
	private ResourceType offerResource;
	
	private ResourceType receiveResource;
	
	private Player player;
	
	public MaritimeTradeController(IMaritimeTradeView tradeView, IMaritimeTradeOverlay tradeOverlay) {
		
		super(tradeView);
		ports = new HashSet<Port>();
		giveTypes = new MaritimeRatios();
		getTypes = new HashSet<ResourceType>();
		setTradeOverlay(tradeOverlay);
		
	}
	
	public IMaritimeTradeView getTradeView() {
		
		return (IMaritimeTradeView)super.getView();
	}
	
	public IMaritimeTradeOverlay getTradeOverlay() {
		return tradeOverlay;
	}

	public void setTradeOverlay(IMaritimeTradeOverlay tradeOverlay) {
		this.tradeOverlay = tradeOverlay;
	}
	
	private void setGiveTypes(){
		GameManager manager = GameManager.getInstance();
		player = manager.getGame().getPlayer(manager.getLocalPlayer().getPlayerIndex());
		ports = manager.getGame().getMap().getMaritimePorts(manager.getLocalPlayer().getPlayerIndex());
		MaritimeRatios availableRatios = new MaritimeRatios(ports);
		ResourceList resources = player.getResources();
		ResourceType[] types = availableRatios.getResources();
		giveTypes = new MaritimeRatios();
		
		
		for(ResourceType type: ResourceType.values()){
			if(resources.hasResource(type, 4))
				giveTypes.addRatio(type, 4);
		}
		if(availableRatios.canTradeThree()){
			for(ResourceType type: ResourceType.values()){
				if(resources.hasResource(type, 3))
					giveTypes.addRatio(type, 3);
			}
		}
		for(ResourceType type: types){
			int ratio = availableRatios.getRatio(type);
			if(resources.hasResource(type, ratio))
				giveTypes.addRatio(type, ratio);
		}
	}
	private void setGetTypes(){
		GameManager manager = GameManager.getInstance();
		Bank bank = manager.getGame().getBank();
		ResourceType[] resources = ResourceType.values();
		for(ResourceType resource: resources){
			if(bank.hasResource(resource, 1))
				getTypes.add(resource);
		}
	}

	@Override
	public void startTrade() {
		
		this.setGiveTypes();
		this.setGetTypes();
		giveResources = giveTypes.getResources();
		if(giveResources.length>0){
			getResources = new ResourceType[getTypes.size()];
			int i = 0;
			for(ResourceType resource: getTypes){
				getResources[i] = resource;
				i++;
			}
		}
		else
			getResources = new ResourceType[]{};
		this.getTradeOverlay().showGetOptions(getResources);
		this.getTradeOverlay().showGiveOptions(giveResources);
		this.getTradeOverlay().setTradeEnabled(false);
		if(!getTradeOverlay().isModalShowing())
			getTradeOverlay().showModal();
		
//		GameManager.getInstance().getGame().setTradeOffer(null);
	}

	@Override
	public void makeTrade() {
		GameManager manager = GameManager.getInstance();
		if(this.offerResource!=null&&this.receiveResource!=null){
			MaritimeTrade trade = new MaritimeTrade(manager.getLocalPlayer().getPlayerIndex(),
					giveTypes.getRatio(offerResource), offerResource.toString().toLowerCase(),
					receiveResource.toString().toLowerCase());
			manager.getGameState().maritimeTrade(trade);
		}
		getTradeOverlay().closeModal();
	}

	@Override
	public void cancelTrade() {
		this.receiveResource = null;
		this.offerResource = null;
		getTradeOverlay().closeModal();
	}

	@Override
	public void setGetResource(ResourceType resource) {
		this.getTradeOverlay().selectGetOption(resource, 1);
		receiveResource = resource;
		this.checkConditions();
		if(this.offerResource!=null)
			this.getTradeOverlay().setTradeEnabled(true);
	}

	@Override
	public void setGiveResource(ResourceType resource) {
		this.getTradeOverlay().selectGiveOption(resource, giveTypes.getRatio(resource));
		offerResource = resource;
		
		this.checkConditions();
		
		if(this.receiveResource!=null)
			this.getTradeOverlay().setTradeEnabled(true);
	}

	@Override
	public void unsetGetValue() {
		this.receiveResource=null;
		this.getTradeOverlay().hideGetOptions();
		this.getTradeOverlay().showGetOptions(this.getResources);
		this.checkConditions();
	}

	@Override
	public void unsetGiveValue() {
		this.offerResource = null;
		this.getTradeOverlay().hideGiveOptions();
		this.getTradeOverlay().showGiveOptions(this.giveResources);
		this.checkConditions();
	}

	@Override
	public void update(Observable o, Object arg) {
		GameManager manager = GameManager.getInstance();
		Game game = manager.getGame();
		if(manager.getGameState() instanceof GamePlayState){
			TurnTracker tracker = game.getTurnTracker();
			if(tracker.getCurrentTurn()==manager.getLocalPlayer().getPlayerIndex())
				this.getTradeView().enableMaritimeTrade(true);
			else
				this.getTradeView().enableMaritimeTrade(false);
		}
		else
			this.getTradeView().enableMaritimeTrade(false);
	}
	
	public void checkConditions(){
		if(this.receiveResource!=null&&this.offerResource!=null)
			this.getTradeOverlay().setTradeEnabled(true);
		else
			this.getTradeOverlay().setTradeEnabled(false);
	}

}

