package client.maritime;

import gamemanager.map.Port;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import shared.definitions.ResourceType;

public class MaritimeRatios {

	/*
	 * List of all types of trades that player can make with maritime trade.
	 * ResourceType - The type of resource.
	 * Integer - Integer:1 ratio for trading specific resource.
	 */
	private Map<ResourceType, Integer> ratios;
	
	/*
	 * Boolean for checking whether the player can trade on a 3:1 ratio with any resource.
	 */
	private boolean canTradeThree;
	
	
	public MaritimeRatios(){
		ratios = new HashMap<ResourceType, Integer>();
		this.canTradeThree = false;
	}
	
	/**
	 * Takes a set of ports and translates them into ratios for specific resources.
	 * @param ports Set of ports that the player has access to.
	 */
	public MaritimeRatios(Set<Port> ports){
		this.canTradeThree = false;
		ratios = new HashMap<ResourceType, Integer>();
		for(Port port: ports){
			if(port.getResource()!=null){
				int ratio = port.getRatio();
				ResourceType resource = ResourceType.valueOf(port.getResource().toUpperCase());
				this.addRatio(resource, ratio);
			}else{
				this.canTradeThree = true;
			}
		}
	}
	
	/**
	 * If ratios contains a ratio for this type already, it keeps the smallest ratio available.
	 * If ratios does not contain this type, then it adds the ratio.
	 * @param type
	 * @param ratio
	 */
	public void addRatio(ResourceType type, int ratio){
		if(ratios.containsKey(type)){
			if(ratios.get(type)>ratio){
				ratios.put(type, ratio);
			}
		}else
			ratios.put(type, ratio);
	}
	
	/**
	 * Returns a ResourceType array of ratios that the player has access to.
	 * @return
	 */
	public ResourceType[] getResources(){
		ResourceType[] resources = new ResourceType[ratios.size()];
		Object[] oResources = ratios.keySet().toArray();
		for(int i=0;i<resources.length;i++){
			resources[i] = (ResourceType)oResources[i];
		}
		return resources;
	}
	
	/**
	 * Returns the ratio of the given type.
	 * @param type The resource that you want to know the ratio about.
	 * @return the ratio for this resource.
	 */
	public int getRatio(ResourceType type){
		if(ratios.containsKey(type))
			return ratios.get(type);
		else
			return 0;
	}
	
	public boolean canTradeThree(){
		return this.canTradeThree;
	}
	
	public int size(){
		return this.ratios.size();
	}
}
