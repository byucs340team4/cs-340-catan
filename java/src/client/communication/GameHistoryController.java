package client.communication;

import gamemanager.GameManager;
import gamemanager.communication.Message;
import gamemanager.gamestate.GamePlayState;
import gamemanager.gamestate.SetupState;

import java.util.*;

import client.base.*;
import shared.definitions.*;


/**
 * Game history controller implementation
 */
public class GameHistoryController extends Controller implements IGameHistoryController {
	
	public GameHistoryController(IGameHistoryView view) {
		
		super(view);
		try{
			initFromModel();
		}catch(NullPointerException e){
			
		}
	}
	
	@Override
	public IGameHistoryView getView() {
		
		return (IGameHistoryView)super.getView();
	}
	
	private void initFromModel() {
		
		//<temp>
		//Should this be the GameLog log object in our Game model?
		List<LogEntry> entries = new ArrayList<LogEntry>();
		for(Message message:GameManager.getInstance().getGame().getLog().getLog()){
			entries.add(new LogEntry(GameManager.getInstance().getGame().getPlayer(message.getSource()).getColor(),message.getMessage()));
		}
		
		
		getView().setEntries(entries);
		//</temp>
	}

	@Override
	public void update(Observable o, Object arg) {
		// TODO Auto-generated method stub
		if (GameManager.getInstance().getGameState() instanceof SetupState || GameManager.getInstance().getGameState() instanceof GamePlayState) {

		List<LogEntry> entries = new ArrayList<LogEntry>();
		for(Message message:GameManager.getInstance().getGame().getLog().getLog()){
			entries.add(new LogEntry(GameManager.getInstance().getGame().getPlayer(message.getSource()).getColor(),message.getMessage()));
		}
		getView().setEntries(entries);
		}	
	}
	
}

