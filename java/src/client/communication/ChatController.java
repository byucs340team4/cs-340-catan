package client.communication;

import gamemanager.GameManager;
import gamemanager.communication.Message;
import gamemanager.game.Game;
import gamemanager.gamestate.GamePlayState;
import gamemanager.gamestate.SetupState;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;

import shared.proxy.request.SendChatRequest;
import client.base.*;


/**
 * Chat controller implementation
 */
public class ChatController extends Controller implements IChatController {

	public ChatController(IChatView view) {
		
		super(view);
	}

	@Override
	public IChatView getView() {
		return (IChatView)super.getView();
	}

	@Override
	public void sendMessage(String message) {
		if(!message.matches("\\p{ASCII}*")){
			GameManager.getInstance().getGameState().sendChat("Invalid character sent in chat.");
			return;
		}
		GameManager.getInstance().getGameState().sendChat(message);
	}

	@Override
	public void update(Observable arg0, Object arg1) {
		// TODO Auto-generated method stub
		if (GameManager.getInstance().getGameState() instanceof SetupState || GameManager.getInstance().getGameState() instanceof GamePlayState) {

		List<LogEntry> entries = new ArrayList<LogEntry>();
		Game test = GameManager.getInstance().getGame();
		for(Message message:GameManager.getInstance().getGame().getChat().getLog()){
			entries.add(new LogEntry(GameManager.getInstance().getGame().getPlayer(message.getSource()).getColor(),message.getMessage()));
		}
		
		getView().setEntries(entries);
		}
	}

}

