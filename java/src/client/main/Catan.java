package client.main;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import gamemanager.GameManager;
import gamemanager.Poller;
import gamemanager.gamestate.GamePlayState;
import gamemanager.gamestate.SetupState;

import javax.swing.*;

import shared.definitions.CatanColor;
import shared.proxy.Proxy;
import shared.proxy.request.Credentials;
import shared.proxy.request.JoinGameRequest;
import client.catan.*;
import client.data.PlayerInfo;
import client.login.*;
import client.join.*;
import client.misc.*;
import client.base.*;

/**
 * Main entry point for the Catan program
 */
@SuppressWarnings("serial")
public class Catan extends JFrame
{
	
	private CatanPanel catanPanel;
	
	public Catan()
	{
		GameManager.getInstance().setProxy(new Proxy());
		
		Poller poll = new Poller();
		poll.setProxy(GameManager.getInstance().getProxy());
		GameManager.getInstance().setPoller(poll);
		poll.poll();
		
		//just for testing below
		//Proxy test = new Proxy("MapControllerTest.json");
		//test.userLogin(new Credentials("Sam","sam"));
		//test.gamesJoin(new JoinGameRequest(2,"red"));
		//GameManager.getInstance().setProxy(test);
		//GameManager.getInstance().setGame(test.getGame());
		//Poller poll = new Poller();
		//poll.setProxy(test);
		//GameManager.getInstance().setPoller(poll);
		//poll.poll();
		//PlayerInfo pTest = new PlayerInfo();
		//pTest.setColor(CatanColor.RED);
		//pTest.setPlayerIndex(0); 
		//GameManager.getInstance().setLocalPlayer(pTest);
		//GameManager.getInstance().setGameState(new GamePlayState(null));
		//just for testing above
		
		client.base.OverlayView.setWindow(this);
		
		this.setTitle("Settlers of Catan");
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		
		catanPanel = new CatanPanel();
		this.setContentPane(catanPanel);
		
		display();
	}
	
	public Catan(String host, int port)
 {
		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {

				dispose();
				System.exit(0); // calling the method is a must
			}
		});
		GameManager.getInstance().setProxy(new Proxy(host,port));
		
		Poller poll = new Poller();
		poll.setProxy(GameManager.getInstance().getProxy());
		GameManager.getInstance().setPoller(poll);
		poll.poll();
		
		client.base.OverlayView.setWindow(this);
		
		this.setTitle("Settlers of Catan");
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		
		catanPanel = new CatanPanel();
		this.setContentPane(catanPanel);
		
		display();
	}
	
	private void display()
	{
		pack();
		setVisible(true);
	}
	
	//
	// Main
	//
	
	public static void main(final String[] args)
	{
		try
		{
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		SwingUtilities.invokeLater(new Runnable() {
			public void run()
			{
				
					try{
						new Catan(args[0], Integer.parseInt(args[1]));
					}catch(NumberFormatException e){
						System.out.println("Invalid command line parameters. Host then port");
					}
				
				
				PlayerWaitingView playerWaitingView = new PlayerWaitingView();
				final PlayerWaitingController playerWaitingController = new PlayerWaitingController(
																									playerWaitingView);
				playerWaitingView.setController(playerWaitingController);
				
				JoinGameView joinView = new JoinGameView();
				NewGameView newGameView = new NewGameView();
				SelectColorView selectColorView = new SelectColorView();
				MessageView joinMessageView = new MessageView();
				final JoinGameController joinController = new JoinGameController(
																				 joinView,
																				 newGameView,
																				 selectColorView,
																				 joinMessageView);
				joinController.setJoinAction(new IAction() {
					@Override
					public void execute()
					{
						playerWaitingController.start();
					}
				});
				joinView.setController(joinController);
				newGameView.setController(joinController);
				selectColorView.setController(joinController);
				joinMessageView.setController(joinController);
				
				LoginView loginView = new LoginView();
				MessageView loginMessageView = new MessageView();
				LoginController loginController = new LoginController(
																	  loginView,
																	  loginMessageView);
				loginController.setLoginAction(new IAction() {
					@Override
					public void execute()
					{
						joinController.start();
					}
				});
				loginView.setController(loginController);
				loginView.setController(loginController);
				
				loginController.start();
			}
		});
	}
	
}

