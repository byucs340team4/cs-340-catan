package client.turntracker;

import gamemanager.GameManager;
import gamemanager.communication.TurnTracker;
import gamemanager.gamestate.JoinState;
import gamemanager.gamestate.PlayerWaitingState;
import gamemanager.player.Player;

import java.util.Observable;

import shared.definitions.CatanColor;
import shared.proxy.request.SendChatRequest;
import client.base.*;


/**
 * Implementation for the turn tracker controller
 */
public class TurnTrackerController extends Controller implements ITurnTrackerController {
	int lastInitialized = -1;
	boolean initFromModel = false;
	
	public TurnTrackerController(ITurnTrackerView view) {
		
		super(view);
		try{
			initFromModel();
		}catch(NullPointerException e){
			
		}
		
	}
	
	@Override
	public ITurnTrackerView getView() {
		
		return (ITurnTrackerView)super.getView();
	}

	@Override
	public void endTurn() {
		
		if(GameManager.getInstance().getGame().getTurnTracker().getCurrentTurn() == GameManager.getInstance().getLocalPlayer().getPlayerIndex() && !GameManager.getInstance().getGame().getTurnTracker().getStatus().equals("FirstRound") && !GameManager.getInstance().getGame().getTurnTracker().getStatus().equals("SecondRound"))
			GameManager.getInstance().getGameState().endTurn();	}
	
	private void initFromModel() {
		//<temp>
		client.data.PlayerInfo playerInfo = GameManager.getInstance().getLocalPlayer();

		if(playerInfo==null)System.out.println("BLah");

		getView().setLocalPlayerColor(playerInfo.getColor());
		getView().initializePlayer(playerInfo.getPlayerIndex(), playerInfo.getName(), playerInfo.getColor());
		//</temp>
	}

	@Override
	public void update(Observable o, Object arg) {
		// TODO Auto-generated method stub
		 
		if(!initFromModel && !(GameManager.getInstance().getGameState() instanceof JoinState)){
			getView().setLocalPlayerColor(GameManager.getInstance().getLocalPlayer().getColor());
			initFromModel=true;
		}
		
		try{
		for(Player player:GameManager.	getInstance().getGame().getPlayers()){
			if(player==null)break;
			if(player.getOrderNumber() > lastInitialized){
				getView().initializePlayer(player.getOrderNumber(), player.getName(), player.getColor());
				lastInitialized++;
			}
			if(GameManager.getInstance().getGame().getTurnTracker().getCurrentTurn() == GameManager.getInstance().getLocalPlayer().getPlayerIndex()){
				if(GameManager.getInstance().getGame().getTurnTracker().getStatus().equals("Playing")){
					getView().updateGameState("End Turn", true);
				}else{
					getView().updateGameState(GameManager.getInstance().getGame().getTurnTracker().getStatus(), true);
				}
			}else{
				getView().updateGameState("Waiting for other players", false);
			}
			boolean highlight = GameManager.getInstance().getGame().getTurnTracker().getCurrentTurn() == player.getOrderNumber()? true:false;
			boolean largestArmy = GameManager.getInstance().getGame().getTurnTracker().getLargestArmy() == player.getOrderNumber()? true:false;
			boolean longestRoad = GameManager.getInstance().getGame().getTurnTracker().getLongestRoad() == player.getOrderNumber()? true:false;
			getView().updatePlayer(player.getOrderNumber(), player.getVictoryPoints(), highlight, largestArmy, longestRoad);
		}		
		 
		}catch(Exception e){}
	}

}

