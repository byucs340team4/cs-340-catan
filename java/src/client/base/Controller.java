package client.base;

import java.util.Observer;

import gamemanager.GameManager;

/**
 * Base class for controllers
 */
public abstract class Controller implements IController, Observer
{
	
	private IView view;
	
	protected Controller(IView view)
	{
		GameManager.getInstance().addObserver(this);
		setView(view);
	}
	
	private void setView(IView view)
	{
		this.view = view;
	}
	
	@Override
	public IView getView()
	{
		return this.view;
	}
	
}

