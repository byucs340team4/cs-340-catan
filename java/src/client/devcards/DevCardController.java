package client.devcards;

import gamemanager.GameManager;
import gamemanager.game.DevCardList;
import gamemanager.gamestate.GamePlayState;
import gamemanager.gamestate.SetupState;
import gamemanager.player.Player;

import java.util.Observable;

import shared.definitions.DevCardType;
import shared.definitions.ResourceType;
import shared.proxy.MoveType;
import shared.proxy.request.BuyDevCardRequest;
import shared.proxy.request.Monopoly;
import shared.proxy.request.Monument;
import shared.proxy.request.YearOfPlentyRequest;
import client.base.*;


/**
 * "Dev card" controller implementation
 */
public class DevCardController extends Controller implements IDevCardController {

	private IBuyDevCardView buyCardView;
	private IAction soldierAction;
	private IAction roadAction;
	private DevCardType newDevCard;
	private DevCardList oldDevCards;
	private DevCardList newDevCards;
	private Player player;
	
	/**
	 * DevCardController constructor
	 * 
	 * @param view "Play dev card" view
	 * @param buyCardView "Buy dev card" view
	 * @param soldierAction Action to be executed when the user plays a soldier card.  It calls "mapController.playSoldierCard()".
	 * @param roadAction Action to be executed when the user plays a road building card.  It calls "mapController.playRoadBuildingCard()".
	 */
	public DevCardController(IPlayDevCardView view, IBuyDevCardView buyCardView, 
								IAction soldierAction, IAction roadAction) {

		super(view);
		
		this.buyCardView = buyCardView;
		this.soldierAction = soldierAction;
		this.roadAction = roadAction;
		this.player = null;
		this.newDevCard = null;
		this.oldDevCards = null;
		this.newDevCards = null;
	}

	public IPlayDevCardView getPlayCardView() {
		return (IPlayDevCardView)super.getView();
	}

	public IBuyDevCardView getBuyCardView() {
		return buyCardView;
	}

	@Override
	public void startBuyCard() {
		if(GameManager.getInstance().getLocalPlayer().getPlayerIndex() == GameManager.getInstance().getGame().getTurnTracker().getCurrentTurn())
			getBuyCardView().showModal();
	}

	@Override
	public void cancelBuyCard() {
		
		getBuyCardView().closeModal();
	}

	@Override
	public void buyCard() {
		System.out.println("BUYING CARD");
		GameManager.getInstance().getGameState().buyDevCard(new BuyDevCardRequest(MoveType.BUY_DEV_CARD, GameManager.getInstance().getLocalPlayer().getPlayerIndex()));
		
		getBuyCardView().closeModal();
	}

	@Override
	public void startPlayCard() {
		System.out.println("In startPlay card");
		getPlayCardView().showModal();
	}

	@Override
	public void cancelPlayCard() {

		getPlayCardView().closeModal();
	}

	@Override
	public void playMonopolyCard(ResourceType resource) {
		if(GameManager.getInstance().getGame().getPlayer(GameManager.getInstance().getLocalPlayer().getPlayerIndex()).canPlayMonopoly()){
			
			Monopoly request = new Monopoly(resource, GameManager.getInstance().getLocalPlayer().getPlayerIndex());
			GameManager.getInstance().getProxy().monopoly(request);
			//might need to reset gameModel after monopoly and monument cards
		}
	
	}

	@Override
	public void playMonumentCard() {
		if(GameManager.getInstance().getGame().getPlayer(GameManager.getInstance().getLocalPlayer().getPlayerIndex()).canPlayMonument())
		{
				Monument request = new Monument(GameManager.getInstance().getLocalPlayer().getPlayerIndex());
				GameManager.getInstance().setGame(GameManager.getInstance().getProxy().monument(request));
			
		}
	}

	@Override
	public void playRoadBuildCard() {
		if(GameManager.getInstance().getGame().getPlayer(GameManager.getInstance().getLocalPlayer().getPlayerIndex()).canPlayRoadBuilding())
			roadAction.execute();

			//System.out.println("Uncomment check for if they can play road building in devcardcontroll");	
	}

	@Override
	public void playSoldierCard() {
		if(GameManager.getInstance().getGame().getPlayer(GameManager.getInstance().getLocalPlayer().getPlayerIndex()).canPlaySoldier())
			soldierAction.execute();
	}

	@Override
	public void playYearOfPlentyCard(ResourceType resource1, ResourceType resource2) {
		int playerIndex = GameManager.getInstance().getLocalPlayer().getPlayerIndex();
		YearOfPlentyRequest request = new YearOfPlentyRequest(MoveType.YEAR_OF_PLENTY, playerIndex, resource1, resource2);
		GameManager.getInstance().getProxy().yearOfPlenty(request);
	}

	@Override
	public void update(Observable o, Object arg) {
		// TODO Auto-generated method stub
		if (GameManager.getInstance().getGameState() instanceof SetupState || GameManager.getInstance().getGameState() instanceof GamePlayState) {

		Player local = GameManager.getInstance().getGame().getPlayer(GameManager.getInstance().getLocalPlayer().getPlayerIndex());
		this.oldDevCards = local.getOldDevCards();
		this.newDevCards = local.getNewDevCards();
//		DevCardType type = DevCardType.MONOPOLY;
		for(DevCardType type: DevCardType.values()){
			this.getPlayCardView().setCardAmount(type, this.oldDevCards.getDevCard(type)+this.newDevCards.getDevCard(type));
			this.checkToEnable(type);
		}
//		int monopoly = local.getOldDevCards().getMonopoly();
//		this.getPlayCardView().setCardAmount(DevCardType.MONOPOLY, this.oldDevCards.getDevCard(DevCardType.MONOPOLY));
//		int roadBuilder = local.getOldDevCards().getRoadBuilding();
//		this.getPlayCardView().setCardAmount(DevCardType.ROAD_BUILD, ));
//		int YoP = local.getOldDevCards().getYearOfPlenty();
//		this.getPlayCardView().setCardAmount(DevCardType.YEAR_OF_PLENTY, YoP);
//		int monument = local.getOldDevCards().getMonument();
//		this.getPlayCardView().setCardAmount(DevCardType.MONUMENT, monument);
//		int soldier = local.getOldDevCards().getSoldier();
//		this.getPlayCardView().setCardAmount(DevCardType.SOLDIER, soldier);
//		this.oldDevCards = local.getOldDevCards();
		this.newDevCards = local.getNewDevCards();
		}
		
	}
	
	private void checkToEnable(DevCardType type){
		Player local = GameManager.getInstance().getGame().getPlayer(GameManager.getInstance().getLocalPlayer().getPlayerIndex());
		if(type != DevCardType.MONUMENT) {
			if(local.getPlayedDev())
				this.getPlayCardView().setCardEnabled(type, false);
			else if(this.newDevCards.getDevCard(type)>0&&this.oldDevCards.getDevCard(type)==0)
				this.getPlayCardView().setCardEnabled(type, false);
			else if(this.oldDevCards.getDevCard(type)==0)
				this.getPlayCardView().setCardEnabled(type, false);
			else
				this.getPlayCardView().setCardEnabled(type, true);
		}
		else {
			if(local.canPlayMonument()) {
				this.getPlayCardView().setCardEnabled(type, true);
			}
			else {
				this.getPlayCardView().setCardEnabled(type, false);
			}
		}
	}
		
}

