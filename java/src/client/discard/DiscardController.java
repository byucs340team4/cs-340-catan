package client.discard;

import gamemanager.GameManager;
import gamemanager.player.ResourceList;

import java.util.HashMap;
import java.util.Map;
import java.util.Observable;

import shared.definitions.*;
import shared.proxy.request.DiscardCards;
import client.base.*;
import client.misc.*;


/**
 * Discard controller implementation
 */
public class DiscardController extends Controller implements IDiscardController {

	private IWaitView waitView;
	private Map<ResourceType,Integer> discards;
	private Map<ResourceType,Integer> maxAmounts;
	int totalDiscarded;
	int amountToDiscard;
	
	/**
	 * DiscardController constructor
	 * 
	 * @param view View displayed to let the user select cards to discard
	 * @param waitView View displayed to notify the user that they are waiting for other players to discard
	 */
	public DiscardController(IDiscardView view, IWaitView waitView) {
		
		super(view);
		
		this.waitView = waitView;
		totalDiscarded = 0;
		amountToDiscard = 0;
		discards = new HashMap<ResourceType, Integer>();
		maxAmounts = new HashMap<ResourceType,Integer>();
		
	}

	public IDiscardView getDiscardView() {
		return (IDiscardView)super.getView();
	}
	
	public IWaitView getWaitView() {
		return waitView;
	}

	@Override
	public void increaseAmount(ResourceType resource) {
		if (totalDiscarded < amountToDiscard && discards.get(resource)< maxAmounts.get(resource)) {
			discards.put(resource, discards.get(resource) + 1);
			getDiscardView().setResourceDiscardAmount(resource,	discards.get(resource));
			getDiscardView().setResourceAmountChangeEnabled(resource, true,	true);
			totalDiscarded++;
			this.getDiscardView().setStateMessage(totalDiscarded+"/"+amountToDiscard);
		}if(discards.get(resource)==maxAmounts.get(resource)){
			getDiscardView().setResourceAmountChangeEnabled(resource, false, true);
		}
		
		if (amountToDiscard == totalDiscarded) {
			getDiscardView().setDiscardButtonEnabled(true);
		}
	}

	@Override
	public void decreaseAmount(ResourceType resource) {
		if (discards.get(resource) > 0) {
			discards.put(resource, discards.get(resource) - 1);
			getDiscardView().setResourceDiscardAmount(resource,	discards.get(resource));
			getDiscardView().setResourceAmountChangeEnabled(resource, true,	true);
			totalDiscarded--;
			this.getDiscardView().setStateMessage(totalDiscarded+"/"+amountToDiscard);
		}if(discards.get(resource)==0){
			getDiscardView().setResourceAmountChangeEnabled(resource, true, false);
		}
		if (amountToDiscard != totalDiscarded) {
			getDiscardView().setDiscardButtonEnabled(false);
		}
		
	}

	@Override
	public void discard() {
		if (amountToDiscard == totalDiscarded) {
			ResourceList rl = new ResourceList();
			for(int i = 0; i < 5; i++){
				rl.setResource(ResourceType.values()[i], discards.get(ResourceType.values()[i]));
				getDiscardView().setResourceDiscardAmount(ResourceType.values()[i],0);
			}
			GameManager.getInstance().getProxy()
					.discardCards(new DiscardCards(GameManager.getInstance().getLocalPlayer().getPlayerIndex(), rl));
			getDiscardView().closeModal();
		}
	}

	@Override
	public void update(Observable o, Object arg) {
		ResourceList resources;
		
		
		try{
			resources = GameManager.getInstance().getGame().getPlayer(GameManager.getInstance().getLocalPlayer().getPlayerIndex()).getResources();

			if(!GameManager.getInstance().getGame().getTurnTracker().getStatus().equals("Discarding")&&this.getWaitView().isModalShowing())
				this.getWaitView().closeModal();
			
		}catch(NullPointerException e){
			resources = new ResourceList();
		}catch(ArrayIndexOutOfBoundsException e){
			return;
		}
		int aTD = 0;
		for(int i = 0; i < 5; i++){
			aTD+=resources.getResource(ResourceType.values()[i]);
		}
		try{ 
			if(aTD> 7 && GameManager.getInstance().getGame().getTurnTracker().getStatus().equals("Discarding") && !GameManager.getInstance().getGame().getPlayer(GameManager.getInstance().getLocalPlayer().getPlayerIndex()).getDiscarded() && !getDiscardView().isModalShowing()){
				totalDiscarded =  0;
				amountToDiscard = 0;
				getDiscardView().setDiscardButtonEnabled(false);
				//loop to initialize cards selected to be discarded
				for(int i = 0; i < 5; i++){
					amountToDiscard+=resources.getResource(ResourceType.values()[i]);
					discards.put(ResourceType.values()[i], 0);
					maxAmounts.put(ResourceType.values()[i], resources.getResource(ResourceType.values()[i]));
					getDiscardView().setResourceMaxAmount(ResourceType.values()[i], resources.getResource(ResourceType.values()[i]));
					if(resources.getResource(ResourceType.values()[i]) > 0){
						getDiscardView().setResourceAmountChangeEnabled(ResourceType.values()[i], true, false);
					}
				}
				amountToDiscard/=2;
				this.getDiscardView().setStateMessage(totalDiscarded+"/"+amountToDiscard);
				this.getDiscardView().showModal();
			}
			
			else if(GameManager.getInstance().getGame().getTurnTracker().getStatus().equals("Discarding") && !this.getWaitView().isModalShowing() && !this.getDiscardView().isModalShowing()){
				this.getWaitView().showModal();
			}
		}catch(Exception e){}
		
		
	}

}

