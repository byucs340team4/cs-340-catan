package client.map;

import gamemanager.GameManager;
import gamemanager.gamestate.GamePlayState;
import gamemanager.gamestate.SetupState;
import gamemanager.map.Hex;
import gamemanager.map.Port;
import gamemanager.map.Road;

import java.util.*;

import shared.definitions.*;
import shared.locations.*;
import client.base.*;
import client.data.*;
/*
 * To Do:
 * Fix canPlaceRoad
 * Make sure placeRoad works
 * 
 * PlayRoadBuilding card - make sure canBuildRoad in map works right then this should be good
 * Finish soldier. Mostly done. Figure out how to get it to work after calling robPlayer. Right not it doesn't
 */

/**
 * Implementation for the map controller
 */
public class MapController extends Controller implements IMapController, Observer{
	
	private IRobView robView;
	private boolean mapDrawn = false;
	private HexLocation hexLoc;
	private boolean roadBuilding = false;
	
	public MapController(IMapView view, IRobView robView) {
		
		super(view);
		
		setRobView(robView);
		try{
			initFromModel();
		}catch(Exception e){
			
		}
	}
	
	public IMapView getView() {
		
		return (IMapView)super.getView();
	}
	
	private IRobView getRobView() {
		return robView;
	}
	private void setRobView(IRobView robView) {
		this.robView = robView;
	}
	
	protected void initFromModel() {
		
		//<temp>
		//this is how the board gets drawn originally. need to make this so it matches the original json		
		for(Hex hex:GameManager.getInstance().getGame().getMap().getHexes()){
			String resource = hex.getResource();
			if(resource == null)
			{
				HexType hexType = HexType.DESERT;
				HexLocation hexLoc = hex.getLocation();
				getView().addHex(hexLoc, hexType);
				continue;
			}
			resource = resource.toUpperCase();
			HexType hexType =  HexType.valueOf(resource);
			HexLocation hexLoc = hex.getLocation();
			getView().addHex(hexLoc, hexType);
			getView().addNumber(hexLoc, hex.getNumber());
			
		}
		
		for(Port port:GameManager.getInstance().getGame().getMap().getPorts()){
			EdgeLocation edgeLoc = new EdgeLocation(port.getLocation(),EdgeDirection.valueOf(port.getDirection()));
			String portType = port.getResource();
			if(portType == null){
				getView().addPort(edgeLoc, PortType.THREE);
			}
			else{
				getView().addPort(edgeLoc, PortType.valueOf(portType.toUpperCase()));
			}
		}
		
		getView().placeRobber(GameManager.getInstance().getGame().getMap().getRobber());
		
		//Draw buildings. You can delete this once create game works
		//draw roads
				for(Road road:GameManager.getInstance().getGame().getMap().getRoads()){
					getView().placeRoad(road.getLocation(), GameManager.getInstance().getGame().getPlayer(road.getOwner()).getColor());
				}
				//draw settlements
				for(VertexObject settlement:GameManager.getInstance().getGame().getMap().getSettlements()){
					getView().placeSettlement(settlement.getLocation(), GameManager.getInstance().getGame().getPlayer(settlement.getOwner()).getColor());
				}
				//draw cities
				for(VertexObject city:GameManager.getInstance().getGame().getMap().getCities()){
					getView().placeCity(city.getLocation(), GameManager.getInstance().getGame().getPlayer(city.getOwner()).getColor());
				}
				
		drawWater();

	}

	private void drawWater(){
		this.getView().addHex(new HexLocation(-3,0), HexType.WATER);
		this.getView().addHex(new HexLocation(-3,1), HexType.WATER);
		this.getView().addHex(new HexLocation(-3,2), HexType.WATER);
		this.getView().addHex(new HexLocation(-3,3), HexType.WATER);
		this.getView().addHex(new HexLocation(3,0), HexType.WATER);
		this.getView().addHex(new HexLocation(3,-1), HexType.WATER);
		this.getView().addHex(new HexLocation(3,-2), HexType.WATER);
		this.getView().addHex(new HexLocation(3,-3), HexType.WATER);
		
		this.getView().addHex(new HexLocation(2,1), HexType.WATER);
		this.getView().addHex(new HexLocation(1,2), HexType.WATER);
		
		this.getView().addHex(new HexLocation(0,3), HexType.WATER);
		this.getView().addHex(new HexLocation(-1,3), HexType.WATER);
		this.getView().addHex(new HexLocation(-2,3), HexType.WATER);
		this.getView().addHex(new HexLocation(-3,3), HexType.WATER);
		this.getView().addHex(new HexLocation(0,-3), HexType.WATER);
		this.getView().addHex(new HexLocation(1,-3), HexType.WATER);
		this.getView().addHex(new HexLocation(2,-3), HexType.WATER);
		this.getView().addHex(new HexLocation(3,-3), HexType.WATER);
		
		this.getView().addHex(new HexLocation(-1,-2), HexType.WATER);
		this.getView().addHex(new HexLocation(-2,-1), HexType.WATER);
	}
	
	
	public boolean canPlaceRoad(EdgeLocation edgeLoc) {
		
		return GameManager.getInstance().canPlaceRoad(edgeLoc);
	}

	public boolean canPlaceSettlement(VertexLocation vertLoc) {
		System.out.println(vertLoc);
		System.out.println(GameManager.getInstance().canPlaceSettlement(vertLoc));
		return GameManager.getInstance().canPlaceSettlement(vertLoc);
	}

	public boolean canPlaceCity(VertexLocation vertLoc) {
		
		return GameManager.getInstance().canPlaceCity(vertLoc);
	}

	public boolean canPlaceRobber(HexLocation hexLoc) {
		
		return GameManager.getInstance().canPlaceRobber(hexLoc);
	}

	//this should call the canPlaceRoad method first, if true it should build that road and call
	//GameManager.placeRoad()
	public void placeRoad(EdgeLocation edgeLoc) {
		
		//this if need to be here for if the proxy returns a null game we don't want to draw the road
		if(GameManager.getInstance().placeRoad(edgeLoc)){
			getView().placeRoad(edgeLoc, GameManager.getInstance().getLocalPlayer().getColor());
			if(roadBuilding){
				startMove(PieceType.ROAD,true,false);
				roadBuilding = false;
			}
		}
	}

	public void placeSettlement(VertexLocation vertLoc) {
		//call GameManager.placeSettlement() if true then draw road in that spot
		if(GameManager.getInstance().placeSettlement(vertLoc))
			getView().placeSettlement(vertLoc, GameManager.getInstance().getLocalPlayer().getColor());
	}

	public void placeCity(VertexLocation vertLoc) {
		
		if(GameManager.getInstance().placeCity(vertLoc))
			getView().placeCity(vertLoc, GameManager.getInstance().getLocalPlayer().getColor());
	}

	public void placeRobber(HexLocation hexLoc) {
		//if place robber returns true, then place robber and show modal
		//if(GameManager.getInstance().placeSoldier(hexLoc)){
			this.hexLoc = hexLoc;
			getView().placeRobber(hexLoc);
			//set the players to rob in 
			Set<Integer> playerIndexes = GameManager.getInstance().getGame().getMap().getOtherHexOwners(hexLoc, GameManager.getInstance().getLocalPlayer().getPlayerIndex());
			ArrayList<RobPlayerInfo>toRob = new ArrayList<RobPlayerInfo>();
			if(playerIndexes!=null){
				for(int i:playerIndexes){
					PlayerInfo pInfo = GameManager.getInstance().getGame().getPlayersInfo()[i];
					int numCards = GameManager.getInstance().getGame().getPlayer(i).getResources().total();
					RobPlayerInfo rInfo = new RobPlayerInfo(pInfo.getId(),pInfo.getPlayerIndex(),pInfo.getName(),pInfo.getColor());
					rInfo.setNumCards(numCards);;
					if(numCards!=0) toRob.add(rInfo);
				}
			}
			RobPlayerInfo[] robArray = new RobPlayerInfo[toRob.size()];
			toRob.toArray(robArray);
			if(robArray.length==0 || playerIndexes == null){
				//getRobView().setPlayers(robArray);
				RobPlayerInfo victim = new RobPlayerInfo(); victim.setPlayerIndex(-1);
				while(!GameManager.getInstance().getGameState().robPlayer(victim,hexLoc)){};
			}
			else{
				getRobView().setPlayers(robArray);
				getRobView().showModal();
			}

		//}
	
	}
	
	public void startMove(PieceType pieceType, boolean isFree, boolean allowDisconnected) {	
		//call gamemanager startMove
		//GameManager.getInstance().setGame(GameManager.getInstance().getGame());
		//check to see if the player can build that thing at that spot, if not, don't call 
		//getView.startDrop(), if so, call getView.startDrop() to pull up the modal
		System.out.println("In start move");
		if(GameManager.getInstance().getGameState() instanceof SetupState)
		{
			if(pieceType == PieceType.CITY || pieceType == PieceType.ROBBER)
				return;
		}
		System.out.println("Fix start move for cirt. currently acting as end turn button");
		//if(pieceType == PieceType.CITY){
			//GameManager.getInstance().getGameState().endTurn();
			//return;
		//}
		
		if(GameManager.getInstance().startMove(pieceType, isFree)){
			if(GameManager.getInstance().getGame().getTurnTracker().getStatus().equals("Robbing"))
				getView().startDrop(pieceType, GameManager.getInstance().getLocalPlayer().getColor(), false);
			else if(GameManager.getInstance().getGameState() instanceof SetupState)
				getView().startDrop(pieceType, GameManager.getInstance().getLocalPlayer().getColor(), false);
			else
				getView().startDrop(pieceType, GameManager.getInstance().getLocalPlayer().getColor(), true);
		}
	}
	
	public void cancelMove() {
		//should exit from the modal overlay. I think this works as is??
		
	}
	
	//should call startMove with SOLDIER as PieceType and isFree = FALSE
	public void playSoldierCard() {	
		System.out.println("Calls playSoldierCard");
			//in the devcard thing to pull up modal it should check if it can play dev card in that state
			//or if the player has dev cards
			startMove(PieceType.ROBBER,false,false);
	}
	
	public void playRoadBuildingCard() {	
		if(GameManager.getInstance().getGame().getPlayer(GameManager.getInstance().getLocalPlayer().getPlayerIndex()).getRoads()==1)
			startMove(PieceType.ROAD,true,false);
		
		else{
			startMove(PieceType.ROAD,true,false);
			roadBuilding = true;
			//startMove(PieceType.ROAD,true,false);
		}
	}
	
	public void robPlayer(RobPlayerInfo victim) {
		System.out.println("Victim info: " + victim);
		while(!GameManager.getInstance().getGameState().robPlayer(victim,hexLoc)){};
	}

	@Override
	public void update(Observable arg0, Object arg1) {
		//draw roads
		
		if (GameManager.getInstance().getGameState() instanceof SetupState || GameManager.getInstance().getGameState() instanceof GamePlayState) {

		if(mapDrawn==false && GameManager.getInstance().getGame()!=null)
			drawMap();
		
		for(Road road:GameManager.getInstance().getGame().getMap().getRoads()){
			getView().placeRoad(road.getLocation(), GameManager.getInstance().getGame().getPlayer(road.getOwner()).getColor());
		}
		//draw settlements
		for(VertexObject settlement:GameManager.getInstance().getGame().getMap().getSettlements()){
			getView().placeSettlement(settlement.getLocation(), GameManager.getInstance().getGame().getPlayer(settlement.getOwner()).getColor());
		}
		//draw cities
		for(VertexObject city:GameManager.getInstance().getGame().getMap().getCities()){
			getView().placeCity(city.getLocation(), GameManager.getInstance().getGame().getPlayer(city.getOwner()).getColor());
		}
		//draw robber
		getView().placeRobber(GameManager.getInstance().getGame().getMap().getRobber());
		
		if(GameManager.getInstance().getGame().getTurnTracker().getCurrentTurn()==GameManager.getInstance().getLocalPlayer().getPlayerIndex() && GameManager.getInstance().getGame().getTurnTracker().getStatus().equals("Robbing")){
			startMove(PieceType.ROBBER,true,true);
			GameManager.getInstance().getLocalPlayer().setRolledNumber(-2);
		}
		
		if(GameManager.getInstance().getGameState() instanceof SetupState && GameManager.getInstance().getGame().getTurnTracker().getCurrentTurn()==GameManager.getInstance().getLocalPlayer().getPlayerIndex() && GameManager.getInstance().getGame().getPlayer(GameManager.getInstance().getLocalPlayer().getPlayerIndex()).getRoads()>13)
		{
			SetupState state= (SetupState) GameManager.getInstance().getGameState();
			if(state.getNumRoads()==state.getNumSettlements()){
				startMove(PieceType.SETTLEMENT,true,true);
			}
			else{
				startMove(PieceType.ROAD,true,true);
			}
		}
		
		int currTurn = GameManager.getInstance().getGame().getTurnTracker().getCurrentTurn();
		int localPlayer = GameManager.getInstance().getLocalPlayer().getPlayerIndex();
		if(currTurn!=localPlayer)
		{
			getView().closeModal();
		}
		//if it's not your turn and a window is showing, close the window
		
		//if(GameManager.getInstance().getLocalPlayer().getRolledNumber()==7){
			//startMove(PieceType.ROBBER,true,true);
			//GameManager.getInstance().getLocalPlayer().setRolledNumber(-2);
		//}
		}
		
		
		//with this here, you may not need to call draw methods in the place methods above
		// TODO Auto-generated method stub
		//just a big for loop where you call getView.place road/city/settlement
		//at the appropriate place with the appropriate color
		//if robPlayer returns a null game from the server, you'll need to update the drawn spot of the robber in here
	}
	
	public void drawMap(){
		for(Hex hex:GameManager.getInstance().getGame().getMap().getHexes()){
			String resource = hex.getResource();
			if(resource == null)
			{
				HexType hexType = HexType.DESERT;
				HexLocation hexLoc = hex.getLocation();
				getView().addHex(hexLoc, hexType);
				continue;
			}
			resource = resource.toUpperCase();
			HexType hexType =  HexType.valueOf(resource);
			HexLocation hexLoc = hex.getLocation();
			getView().addHex(hexLoc, hexType);
			getView().addNumber(hexLoc, hex.getNumber());
		}
		
		for(Port port:GameManager.getInstance().getGame().getMap().getPorts()){
			EdgeLocation edgeLoc = new EdgeLocation(port.getLocation(),EdgeDirection.valueOf(port.getDirection()));
			String portType = port.getResource();
			if(portType == null){
				getView().addPort(edgeLoc, PortType.THREE);
			}
			else{
				getView().addPort(edgeLoc, PortType.valueOf(portType.toUpperCase()));
			}
		}
		
		getView().placeRobber(GameManager.getInstance().getGame().getMap().getRobber());
		drawWater();
		mapDrawn = true;
	}
	
}

